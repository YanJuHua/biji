TTS学习平台
www.tmooc.cn

192.168.1.1 255.255.255.0 = 192.168.1.1/24（具有24个网络位置，因为IPV4一共有32个二进制网络位，每个位置8个网络位，/24代表一共占据了前三个位置）

9.29
——————————————————————————————————————————————————————————————————————————————————————
ADMIN（云计算系统管理）

一云计算的介绍
什么是服务器
•能够为其他计算机提供服务的更高级的电脑
–机架式
–塔式
–机柜式
–刀片式
典型服务模式
•C/S，Client/Server架构
–由服务器提供资源或某种功能
–客户机使用资源或功能


二、TCP/IP协议及配置
•TCP/IP是最广泛支持的通信协议集合
–包括大量Internet应用中的标准协议
–支持跨网络架构、跨操作系统平台的通信

•主机与主机之间通信的三个要素
–IP地址（IP address）
–子网掩码（subnet mask）
–IP路由（IP router）网络设备 路由器

三、IPv4地址（IP address） 
•作用：用来标识一个节点（连网设备）的网络地址 
•地址组成（点分十进制）：     
–一共32个二进制位
–表示为4个十进制数，以 . 隔开
32个二进制数：11001100.01010101.11110000.10101010
4个十进制数: 192.168.1.1    1.2.3.4    18.17.16.15
  1.1.1.1   2.2.2.2
二进制的 11111111 = 十进制的 255

四、网络位与主机位
网络名称规则：网络位不变，主机位全为0
相同区域(网络)通信：
192.168.1.1=来自192.168.1.0区域(网络)，编号为1的主机
192.168.1.2=来自192.168.1.0区域(网络)，编号为2的主机

不相同区域通信：
192.168.1.1=来自192.168.1区域，编号为1的主机
192.168.2.1=来自192.168.2区域，编号为1的主机

•IP地址的分类：判断的依据只需要查看IP地址的第一个十进制数 192.168.1.1   18.19.20.21    
•用于一般计算机网络
–A类：1 ~ 127         网+主+主+主
–B类：128 ~ 191      网+网+主+主
–C类：192 ~ 223      网+网+网+主
•组播及科研专用（了解内容）
–D类：224 ~ 239 组播
–E类：240 ~ 254 科研

私网有ip地址
A 10.0.0.0~10.255.255.255
B 172.16.0.0~172.31.255.255
C 192.168.0.0~192.168.255.255

9.30
————————————————————————————————————————————————
重点需要记下：1970.1.1纪念Linux的诞生

linux目录下只有第一个"/"表示为"根目录"，后面的"/"为分割号

linux终端里输入命令时，如果结尾是路径例如 ls /root时，结尾的"/"可大可不打，即（ls /root/），如果结尾路径是文件，例如cat /etc/wd时，结尾的"/"不可以打

命令行提示符
[当前登录的用户@主机名 当前所在的目录]
以#结尾表示当前登录的身份为root
以$结尾表示当前登录的身份为普通用户
[root@local ~]#

•pwd — Print Working Directory
–用途：查看当前所在位置
•cd — Change Directory
–用途：切换工作目录
–格式：cd  [目标文件夹位置]
•ls — List
–格式：ls   [目录或文件名]…

[root@localhost ~]# pwd    #显示当前所在的位置
[root@localhost ~]# cd   /   #切换到根目录下
[root@localhost /]# pwd 
[root@localhost /]# ls       #显示当前目录下内容


绝对路径：以根开始的路径
相对路径：以当前为参照的路径 
]# cd  /opt   #绝对路径,与当前所在位置无关
]# pwd
]# ls 
]# cd   rh     #相对路径,与当前所在位置有关
]# pwd

]# cd  /etc/pki  #绝对路径,与当前所在位置无关
]# pwd
]# ls 
]# cd  CA       #相对路径,与当前所在位置有关
]# pwd

..：表示上一层目录
[root@localhost CA]# cd    ..   #后退

目录：蓝色
文本文件：黑色

cat查看文本文件内容,适合查看内容较少文件
less查看文本文件内容,适合查看内容较多文件
[root@localhost /]# less  /etc/passwd

hostname查看主机名       临时设置
[root@localhost /]# hostname  abc.haha.xixi
永久设置主机名  
]# hostnamectl   set-hostname   nb.qq.com
]# hostname       #查看主机名

列出CPU处理器信息
[root@A ~]# lscpu
…….
CPU(s):            1     #核心数
…….
型号名称：Intel(R) Core(TM) i5-4430 CPU @ 3.00GHz
…….

列出内存信息
[root@A ~]# cat   /proc/meminfo
MemTotal:         997956 kB

查看网卡的IP地址  
]# ifconfig
lo: 本机回环接口   IP永远为127.0.0.1
   127.0.0.1：永远代表本机
]# ifconfig  eth0  192.168.1.1   #临时设置IP
]# ifconfig
]# ping   192.168.1.1
Ctrl+c：结束正在运行命令

mkdir创建目录        
[root@A ~]# mkdir   /opt/test
touch创建文本文件
[root@A ~]# touch   /opt/1.txt

head、tail 命令
–格式：head  -n 数字 文件名
      tail  -n  数字 文件名

]# head  -1  /etc/passwd    #显示文件头部信息
]# head  -2  /etc/passwd
]# head  -3  /etc/passwd

]# tail  -1  /etc/passwd  #显示文件的尾部信息
]# tail  -2  /etc/passwd
]# tail  -3  /etc/passwd


grep命令过滤文本文件内容 
作用：输出包含指定字符串的行
[root@A /]# grep  root  /etc/passwd

vim修改文本文件内容(文本编辑器)
三个模式：命令模式、插入模式、末行模式

vim当文件不存在时，会自动创建此文件
vim不能创建目录
[root@A /]# vim  /opt/haxi.txt
命--- i键 或者 o键---》插入模式(Esc回到命令模式)
令
模
式--- 英文的冒号：---》末行模式(Esc回到命令模式)
末行模式 :wq   #保存并退出
末行模式 :q!    #强制不保存并退出


关机poweroff与重启reboot
[root@A /]# poweroff




————————————————————————————————————————————————
10.8
命令的执行默认解释器/bin/bash

文件颜色，浅蓝是快捷方式。黑色背景黄颜色字文件，代表是一个设备

mount 让目录成为设备的访问点

linux的设备类似于windows上的U盘设备显示，是一个进入口的媒介。
linux允许一个设备挂载多个目录，不允许多个设备在同一个目录下
[root@localhost ~] 其中“~”代表当前所在位置，为当前登录用户的家

mv移动后，在移动路径结尾添加不存在名字时，则为移动+改名。
待定待认证mv移动后遇到同名文件夹时，则会移动到该同名文件夹的里面；
mv移动后遇到同名文件或文件夹时，则会提示是否覆盖。
cp拷贝后，在移动路径结尾添加不存在名字时，则为拷贝+改名。
cp拷贝后遇到同名文件非文件夹时，或文件夹里有同名文件时，则会提示是否覆盖。
cp拷贝后遇到同名文件夹非文件时，则会直接覆盖不提示。

一、Linux命令行基础
•Linux命令
–用来实现某一类功能的指令或程序 
  在Linux中执行大多数命令时，都要找到命令所对应的程序
]# hostname
]# which  hostname     #查询命令对应的程序
]# ls  /usr/bin/hostname
]# which firefox
/usr/bin/firefox



–命令的执行依赖于解释器（默认解释器/bin/bash）
   用户--->解释器--->内核--->硬件
绿色：可以执行的程序


•命令行完整格式：
–命令字   [选项]…    [参数1]  [参数2]…

]# cat  --help           #查看命令帮助信息
]# cat  -n  /etc/shells   #显示行号
]# cat  -n  /etc/passwd
]# cat  -n  /etc/redhat-release
]# cat  -n  /etc/fstab     


]# ls  -l    /etc/passwd    #长格式显示
]# ls  -l    /etc/fstab      #显示详细属性
]# ls  -l    /etc/shells
]# ls  -l    /root/   #默认显示目录内容的详细属性




•Tab键自动补全
–可补齐命令字、选项、参数、文件路径、软件名、服务名
]# if(tab) (tab)        #列出以if开头的命令  
]# ifco(tab)

]# cat   /etc/re(tab) (tab)
]# cat   /etc/red(tab)

]# ls  /etc/sysconfig/network-scripts/
]# ls  /et(tab)/sysco(tab)/netw(tab)- (tab)


•快捷键               
–Ctrl + c：结束正在运行的命令  
–Esc+.或Alt+.：粘贴上一个命令的参数
[root@localhost ~]# ls  /etc/hosts
[root@localhost ~]# ls  -l  Alt + . 
[root@localhost ~]# cat  Alt + . 
[root@localhost ~]# cat  -n  Alt + .
–Ctrl + l：清空整个屏幕
–Ctrl + u：从光标处清空至行首
–Ctrl + w：往回删除一个单词（以空格界定）

软件包安装：
光盘内容有众多的软件包
将光盘镜像文件放入光驱设备

访问点


Windows显示光盘内容：
光盘---->光驱设备---->DVD驱动器（图标）

Linux显示光盘内容：
光盘---->光驱设备---->访问点（目录）
           /dev/cdrom




二、mount挂载操作：让目录成为设备的访问点
命令行显示光盘内容
Windows：
 光盘--->光驱设备--->CD驱动器(图标)
Linux：
 光盘--->光驱设备--->访问点(目录)

Linux：将光盘镜像文件放入虚拟光驱设备中



Windows：将光盘镜像文件放入虚拟光驱设备中


2.查看Linux光驱设备
[root@localhost ~]# ls  -l  /dev/cdrom
[root@localhost ~]# ls  /dev/sr0
3.挂载操作
•使用mount命令
–格式：mount   设备路径   挂载点目录
]# mkdir  /dvd       #创建目录/dvd
]# ls   /dvd
]# mount  /dev/cdrom  /dvd
mount: /dev/sr0 写保护，将以只读方式挂载
]# ls  /dvd/
]# ls  /dvd/P(tab）    #大写的P




4.卸载操作       
]# umount   /dvd
]# ls  /dvd/

]# mkdir   /mydvd
]# mount   /dev/cdrom  /mydvd
mount: /dev/sr0 写保护，将以只读方式挂载
]# ls   /mydvd



注意事项：
1.卸载：当前所在的路径是挂载点目录
[root@localhost mydvd]# umount  /mydvd
umount: /mydvd：目标忙。
        (有些情况下通过 lsof(8) 或 fuser(1) 可以
         找到有关使用该设备的进程的有用信息)
2.挂载允许一个设备，挂载到不同的挂载点目录
3.挂载不允许一个挂载点目录，挂载多个设备
4.建议挂载点目录，最好自行创建



三、目录与文件管理
cd的命令使用
.	当前目录
..	父目录（上一层）
~：表示家目录
家目录：专门存放用户个性化信息的目录
~user：用户user的家目录
/root:是Linux的家目录
/home:存放所有普通用户的家目录
]# cd  ~root     #去往root用户的家目录
]# pwd

]# cd  ~lisi      #去往lisi用户的家目录
]# pwd

]# useradd  dc    #创建用户dc
]# cd  ~dc        #去往dc用户的家目录
]# pwd

]# useradd  tom   #创建用户tom
]# cd  ~tom       #去往tom用户的家目录
]# pwd


•ls — List
–格式：ls  [选项]...  [目录或文件名]
•常用命令选项
–-l：以长格式显示，显示详细信息
–-h：提供易读的容量单位（K、M等）
–-d：显示目录本身（而不是内容）的属性
[root@localhost /]# ls  -ld  /home
[root@localhost /]# ls  -ld  /root
[root@localhost /]# ls  -ld  /
[root@localhost /]# ls  -l   /etc/passwd
[root@localhost /]# ls  -lh  /etc/passwd

–-A：显示所有内容，包含隐藏数据
–-R：递归显示内容  
[root@localhost /]# ls  -A    /root
[root@localhost /]# touch   /opt/.nsd.txt
[root@localhost /]# ls   /opt/
[root@localhost /]# ls  -A   /opt/
[root@localhost /]# ls  -R   /opt/


使用通配符
•针对不确定的文档名称，以特殊字符表示
–*：任意多个任意字符
[root@localhost /]# ls  /root/a*
[root@localhost /]# ls  /boot/vm*
[root@localhost /]# ls  /etc/*tab
[root@localhost /]# ls  /etc/*.conf
[root@localhost /]# ls  /dev/tty*




–?：单个字符
[root@localhost /]# ls  /etc/??tab
[root@localhost /]# ls  /dev/tty?
[root@localhost /]# ls  /dev/tty??




–[a-z]：多个字符或连续范围中的一个，若无则忽略
–{a,min,xy}：多组不同的字符串，全匹配
[root@localhost /]# ls  /dev/tty[3-9]
[root@localhost /]# ls  /dev/tty[1-7]

[root@localhost /]# ls  /dev/tty{1,17,20}
[root@localhost /]# ls  /dev/tty{10,18,22,33}
[root@localhost /]# ls  /dev/tty{26,19,21,30,40}


别名的定义:简化复杂的命令
•查看已设置的别名
–alias  [别名名称]
•定义新的别名
–alias  别名名称= '实际执行的命令行'
•取消已设置的别名
–unalias  [别名名称]  

]# hostname          
]# alias  hn='hostname'   #定义别名
]# hn

]# alias           #查看系统中有哪些别名
]# unalias  hn    #删除hn别名
]# hn
bash: hn: 未找到命令...   

]# alias  myls='ls  -l' 
]# myls  /etc/passwd



•mkdir — Make Directory
–格式：mkdir    [/路径/]目录名…
      [-p]：连同父目录一并创建
]# mkdir  -p  /opt/aa/bb/cc/dd
]# ls  -R  /opt/aa

]# mkdir  -p  /nsd04/test04
]# ls  -R   /nsd04

rm 删除 
•rm — Remove    
–格式：rm  [选项]...  文件或目录…
•常用命令选项
–-r、-f：递归删除（含目录）、强制删除
[root@localhost /]# ls  /opt
[root@localhost /]# touch  /opt/1.txt
[root@localhost /]# ls  /opt
[root@localhost /]# rm  -rf   /opt/1.txt
[root@localhost /]# rm  -rf   /opt/aa




mv — Move移动：源数据会消失
–格式：mv   原文件…   目标路径
[root@localhost /]# mkdir  /opt/nsd01
[root@localhost /]# touch  /opt/1.txt
[root@localhost /]# ls  /opt/

[root@localhost /]# mv  /opt/1.txt  /opt/nsd01
[root@localhost /]# ls  /opt/
[root@localhost /]# ls  /opt/nsd01/




重命名：路径不变的移动
]# ls   /opt/
]# mv   /opt/nsd01   /opt/abc
]# ls   /opt/

]# mv   /opt/abc/   /opt/student
]# ls   /opt/

]# mv  /opt/student    /mnt/stu01
]# ls  /mnt/


cp — Copy：源数据不会消失
–格式：cp  [选项]...  原文件…  目标路径
•常用命令选项
–-r：递归，复制目录时必须有此选项
[root@localhost /]# cp  /etc/passwd   /opt/
[root@localhost /]# ls  /opt/

[root@localhost /]# cp  -r  /boot/   /opt/
[root@localhost /]# ls  /opt/

[root@localhost /]# cp  -r  /home/   /opt/
[root@localhost /]# ls  /opt/




重名进行强制覆盖

]# \cp -r  /boot/   /opt/ #本次操作临时取消别名
]# \cp -r  /boot/   /opt/ #本次操作临时取消别名









复制可以支持重新命名，目标路径下数据的名称
]# cp  -r  /home/   /opt/myhome
]# ls  /opt/

]# cp  /etc/redhat-release      /opt/r.txt
]# ls  /opt/
]# cp  -r  /root/  /opt/myroot
]# ls  /opt/

]# cp  -r  /root/  /opt/myroot  
]# ls  /opt/myroot/



复制可以支持两个以上的参数，永远把最后一个参数作为目标，其他的所有的参数都作为源数据
]# mkdir  /nsd05
]# cp -r /home/  /etc/passwd   /boot/  /etc/shells   /nsd05

]# ls   /nsd05

复制与一个点进行连用，将数据复制到当前路径下
]# cd    /etc/sysconfig/network-scripts/
]# pwd
]# cp   /etc/passwd     .  
]# ls


在文本文件内容中过滤，包含指定字符串的行
–grep  [选项]  '字符串'   文本文件...
•常用命令选项
–-v，取反匹配
–-i，忽略大小写
]# grep  root   /etc/passwd    #包含root的行
]# grep  -v   root  /etc/passwd #不包含root的行
]# grep  ROOT  /etc/passwd
]# grep  -i  ROOT  /etc/passwd  #忽略大小写



–^word 			以字符串word开头
–word$ 			以字符串word结尾
]# grep   ^root    /etc/passwd
]# grep   ^bin     /etc/passwd
]# grep   root$     /etc/passwd
]# grep   bash$    /etc/passwd





------------------------------------------------------------------------------------------------------
10.9

tar命令没有新建目录的功能，释放必须写路径
tar的-f命令必须在所有选项的最后
压缩包的结尾，一般命名规则为 .tar+压缩格式为结尾，即为xxxx.tar.gz或(.bz.xz)
偷懒的命名则是把tar缩写为t，即xxxx.tgz
tar命令释放看到同名的即自动覆盖
重定向有新建文件的功能，但是不能新建目录
前面命令没有输出，则不能使用管道





ls /etc/*.conf不会搜索目录下的字目录，则find会全部的搜索
find不会查找文本文件内容，主要查找数据位置。
find 只能查找硬盘的数据，不能查询内存的数据，/proc:内存的数据，不占用硬盘空间
-find [目录][条件]
-type 类型(f文件、d目录、l快捷方式)
-name '文档名称' (名称用''或"")
-size +|-文件大小(k小写、M大写、G大写) 
	##"-"不能写1k、1M、1G,需要改成1024k、1024M、1024G
-user 用户名
-mtime 修改时间（所有时间都是过去时间）

find高级命令
-exec cp -r {} \;(“{}”大括号代表里面是的源，就是装的-exec之前，find所查询到的结果内容) 



一、归档及压缩
作用：1.减小占用空间大小  2.整合分散的数据
•归档的含义
将许多零散的文件整理为一个文件，文件总的大小基本不变
•压缩的含义
按某种算法减小文件所占用空间的大小
恢复时按对应的逆向算法解压


常见的压缩格式及命令工具
.gz ---gzip 速度快压缩比例不高
.bz2 ---bzip2速度一般慢，压缩比例较好
.xz ---xz   速度很慢，压缩比例更好

-c 创建归档
-x 释放归档
-f指定归档文件名称,必须在所有选项的最后
-z、-j、-J调用.gz .bz2 .xz格式工具进行处理
-t显示归档中的文件清单
-C指定释放路径
-p 打包时候连同文件权限一同打包
tar -zcf /路径/归档名称 /归档目标
tar -zcf /root/tzy.tar.gz /etc/passwd/      #创建
tar -tf /root/tzy.tar.gz     			#查看
tar -xf /root/tzy.tar.gz  -C /tzy   		#释放

tar 制作压缩包（tar打包）
格式：tar  选项   /路径/压缩包名字   /源数据……. 


]# tar -zcf /root/xixi.tar.gz /etc/passwd  /home
]# ls  /root/

]# tar -jcf  /root/haha.tar.bz2  /opt  /home
]# ls  /root/

]# tar -Jcf  /root/hehe.tar.xz   /opt  /home
]# ls  /root/

tar 释放压缩包（tar解包）
tar 选项  /路径/压缩包名字  选项  /释放的位置 
–-x：释放归档
–-f：指定归档文件名称,必须在所有选项的最后
–-C：指定释放路径    
]# mkdir /nsd01     
]# tar -xf  /root/haha.tar.bz2  -C  /nsd01
]# ls  /nsd01
]# ls  /nsd01/etc
]# ls  /nsd01/home


二、重定向（重新定向命令的输出）
将前面命令的输出，作为内容，写入到后面的文件
重定向：重新定向命令的输出；命令和文件之间的连接
将前面命令的输出，放到后面文本文件上，重定向会
重定向有新建文件的功能，但是不能新建目录
>:覆盖重定向
>>:追加重定向
[root@A /]# head -5  /etc/passwd > /opt/p.txt
[root@A /]# cat  /opt/p.txt

[root@A /]# hostname 
[root@A /]# hostname  >>  /opt/p.txt 
[root@A /]# cat  /opt/p.txt

]# echo 123456	
]# echo 123456  >  /opt/p.txt
]# cat  /opt/p.txt
]# cat  /etc/hostname 
]# echo  nb.tedu.cn  > /etc/hostname
]# cat  /etc/hostname


三、管道（操作符号 |  ）
作用：将前面命令的输出，传递给后面命令，作为后面命令的参数
前面命令没有输出，则不能使用管道
前面命令的输出 给后面命令当参数
有两个参数的命令都不支持管道
]# head  -4  /etc/passwd   |      tail  -1 
]# head  -8  /etc/passwd  |   tail  -1
]# cat -n  /etc/passwd  |  head -8  |   tail  -1
]# ifconfig  |  head  -2


]# head  -12  /etc/passwd   |    tail  -5
]# head -12 /etc/passwd  |   tail -5  |  cat  -n

]# cat -n  /etc/passwd   |  head  -12
]# cat -n  /etc/passwd  |  head -12  |   tail -5
]# cat -n  /etc/passwd  |  head -12  |   tail -5  > /opt/pa.txt
]# cat  /opt/pa.txt

四、grep高级使用
 作用：从文本文件内容中，过滤关键字符串
]# grep  root  /etc/passwd
]# grep -v  root  /etc/passwd   #取反匹配
]# grep  ^root  /etc/passwd  #以root开头
]# grep  bash$  /etc/passwd  #以bash结尾

^$：表示空行
grep ^$ 代表空行
]# cat  /etc/default/useradd
]# grep  -v  ^$  /etc/default/useradd


Linux中大多数配置文件内容，以#开头的行为注释行
显示配置文件有效信息（去除以#开头的注释行和去除空行）
有效配置（去除以#开头的注释行，去掉空行）
]# grep  -v  ^#  /etc/login.defs
]#grep -v  ^#  /etc/login.defs  |  grep -v  ^$

]# grep -v  ^#  /etc/login.defs  |  grep -v ^$  >  /opt/log.txt

]# cat  /opt/log.txt


五、find精确查找
格式：find    [目录]   [条件1] 
–常用条件表示：
-type  类型（f、d、l）
-name  "文档名称"
-size  +|-文件大小（k、M、G）
-user  用户名
-mtime  修改时间


-type  类型（f文本文件、d目录、l快捷方式）
[root@A /]# find  /boot  -type  d
[root@A /]# find  /opt  -type  d
[root@A /]# find  /etc  -type  l   
[root@A /]# find  /boot  -type  f


-name  "文档名称"
]# find  /etc/  -name  "passwd"
]# find  /etc/  -name  "*tab"
]# find  /etc/  -name  "*.conf"

]# find  /etc/  -name  "*tab"   |  wc  -l
]# find  /etc/  -name  "*.conf"  |  wc  -l
]# find  /etc/  -name  "*.conf"  |  cat  -n






]# mkdir   /mnt/cbd01
]# mkdir   /mnt/cbd02
]# touch   /mnt/cbd03.txt
]# find   /mnt/  -name   "cbd*"
]# find   /mnt/  -name   "cbd*"  -type  d
]# find   /mnt/  -name   " cbd*"  -type  f


-size  +或- 文件大小（k、M、G）
[root@A /]# find  /boot/  -size  +300k
[root@A /]# find  /boot/  -size  +10M
[root@A /]# find  /boot/  -size  +1M



-user  用户名 （按照数据的所有者）
[root@A /]# useradd  natasha  #创建用户
[root@A /]# find  /home/  -user  natasha
[root@A /]# find  /   -user  natasha

/proc：内存的数据，不占用硬盘空间

-mtime  修改时间 （所有的时间都是过去时间）
    -mtime  +90  #90天之前的数据
    -mtime  -90   #最近90天之内的数据
[root@A /]# find  /root  -mtime  +90
[root@A /]# find  /root  -mtime  -10


六、find高级使用
处理find找到的数据，每查找的一个就传递一次
–find  [范围]  [条件]  -exec  处理命令  {}   \;
-exec额外操作的开始
{} 永远表示前面find查找的结果
\;  额外操作的结束
]# find  /boot/  -size  +10M
]# find  /boot/  -size  +10M  -exec   cp  {}  /mnt  \;
]# ls /mnt/

]#  mkdir  /nsd06
]# find /etc/ -name "*tab" -exec  cp  -r  {}  /nsd06  \;
]# ls /nsd06

]# touch  /mnt/abc0{1,2,3,4,5}.txt
]# ls  /mnt/
]# find  /mnt/  -name  "abc*" 
]# find /mnt/ -name "abc*" -exec   rm -rf {}  \;
]# ls  /mnt






案例2：查找并处理文件
1.利用find查找数据，所有者为 student 并且必须是文件,把它们拷贝到 /root/findfiles/ 文件夹中
]# useradd  student      
]# mkdir  /root/findfiles

]# find  /  -user  student   -type  f
]# find  /  -user  student  -type  f  -exec cp  {}  /root/findfiles  \;

]# ls  -A  /root/findfiles/


七、vim编辑技巧
当文件不存在，则自动新建，vim不能新建目录
三个模式：命令模式   插入模式（输入模式）    末行模式
[root@localhost ~]# vim    /opt/nsd.txt      
   命-------i键   或  o键---------》插入模式（按Esc键回到命令模式）
   令
   模
   式------输入英文的冒号 ":"-----》末行模式（按Esc键回到命令模式）

  末行模式 输入  :wq      #保存并退出
  末行模式 输入  :q!       #强制不保存退出






---------------------------------------------------------------------------------------
10.11

RPM
软件名是指软件的名字，软件包名是指完整的名字
rpm不能解决依赖关系
yum能自动解决依赖关系
本地yum仓库 客户端和服务端是一台机器
网络yum仓库 客户端和服务端是两台机器
仓库配置：/etc/yum.repos.d/*.repo
配置文件路径必须是/etc/yum.repos.d/，结尾名称必须是.repo结尾

yum   配置文件  /mnt
[nsd20]         		#仓库名称
name=centos07 		#仓库描述信息（一般为光盘版本信息）
baseurl=file:///mnt	#指定服务端位置file://表示本地为服务端
enabled=1			#本文件启用
gpgcheck=0			#不检测红帽签名信息


rpm -qf 
只能查询已经安装的

yum providea
查询整个仓库的所有包

yum list


命令补充
获取命令帮助
cat --help

man cat

man 5 passwd (5代表是查看配置文件的帮助信息，不加5默认查看的是passwd命令的帮助信息)


制作链接文件(制作快捷方式)
软链接
格式:ln -s /路径/源数据     /路径/快捷方式的名称        #软链接
]# ln -s /etc/sysconfig/network-scripts/ifcfg-lo  /mylo
软链接
软链接优势：可以针对目录与文件制作快捷方式，支持跨分区
软链接缺点：源数据消失，快捷方式失效
硬连接
优势源数据消失，快捷方式仍然有效
缺点只能针对文件制作快捷方式，不支持跨分区



一、环境准备
1.光盘文件放入光驱设备
2.挂载光驱设备
[root@localhost ~]# mount   /dev/cdrom     /mnt
mount: /dev/sr0 写保护，将以只读方式挂载
[root@localhost ~]# ls   /mnt
[root@localhost ~]# ls   /mnt/Packages


二、RPM软件包简介
•RPM Package Manager
由红帽公司提出，RedHat、SUSE等系列采用
建立集中数据库，记录软件包安装/卸载等变化信息，分析软件包依赖关系 
•RPM包文件名特征
–软件名-版本信息.操作系统.硬件架构.rpm
 firefox-52.7.0-1.el7.centos.x86_64.rpm
软件名：firefox
软件包名：firefox-52.7.0-1.el7.centos.x86_64.rpm
软件名是指软件的名字，软件包名是指完整的名字

•RPM包的一般安装位置（分散） 
文件类别	默认安装位置
普通执行程序 	/usr/bin/ 、/bin/
服务器程序、管理工具	/usr/sbin/ 、/sbin/
配置文件 	/etc/ 、/etc/软件名/
日志文件 	/var/log/、/var/log/软件名/
程序文档、man帮助手册页 	/usr/share/doc/ 、/usr/share/man/



三、查询软件信息
查询软件是否安装      
]# rpm  -qa                 #当前系统中所有已安装的软件
]# rpm  -q   firefox     #查看firefox是否安装
firefox-52.7.0-1.el7.centos.x86_64
]# rpm  -q   httpd       #查看httpd是否安装
未安装软件包 httpd
]# rpm  -q   bash         #查看bash是否安装
bash-4.2.46-30.el7.x86_64
]# rpm  -qa |  grep  firefox  #在已经安装的软件中过滤firefox

]# rpm  -qi   firefox       #查询软件信息

]# rpm  -ql   firefox         #查询软件安装了哪些内容(安装清单)
]# rpm  -ql    firefox   |   less



•查询某个目录/文件是哪个RPM包带来的
–格式：rpm  -qf  [文件路径]…
–即使目标文件被删除，也可以查询
[root@localhost ~]# which  vim   #查询命令对应的程序文件
/usr/bin/vim
[root@localhost ~]# rpm -qf  /usr/bin/vim
vim-enhanced-7.4.160-4.el7.x86_64
[root@localhost ~]# rpm -q vim-enhanced
vim-enhanced-7.4.160-4.el7.x86_64


查询未安装软件包
]# rpm  -q   vsftpd     #查询vsftpd软件是否安装
未安装软件包 vsftpd 
]# ls /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm

查询软件包的安装清单：
]# rpm -qpl  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm
查询软件包信息
]# rpm -qpi /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm


导入红帽签名信息(了解)   
]# rpm  --import    /mnt/RPM-GPG-KEY-CentOS-7
查询软件包信息
]# rpm -qpi  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm

安装RPM软件包
1.通过rpm命令:不能解决依赖关系
2.通过yum命令: 能自动解决依赖关系

四、安装RPM软件(了解)
–格式：rpm   -i   RPM包文件...
•辅助选项
– -v：显示细节信息
– -h：以#号显示安装进度
– --force：强制安装、覆盖安装
– --test：测试安装，不做真实安装动作

]# rpm  -q   vsftpd        #查询当前的系统是否安装了该软件
未安装软件包 vsftpd 

]# rpm  -ivh  /mnt/Packages/vsftpd-3.0.2-22.el7.x86_64.rpm 
]# rpm  -q  vsftpd    #查询当前的系统是否安装了该软件
vsftpd-3.0.2-22.el7.x86_64

]# rpm -e vsftpd            #卸载软件
]# rpm -q vsftpd            #查询当前的系统是否安装了该软件
未安装软件包 vsftpd

--force：强制安装、覆盖安装
]# which   hostname
/usr/bin/hostname
]# rm   -rf   /usr/bin/hostname
]# hostname
bash: hostname: 未找到命令...

]# rpm -qf   /usr/bin/hostname     #查看由哪个软件包产生
hostname-3.13-3.el7.x86_64

]# rpm  -ivh   --force   /mnt/Packages/hostname-3.13-3.el7.x86_64.rpm


解决依赖关系
–先安装/卸载要求的包
–如果RPM包齐全但比较多，可以用通配符 *
•忽略依赖关系（不推荐）
–可能会导致软件运行异常
–辅助选项 --nodeps
常见依赖关系的报错：
[root@localhost ~]# rpm -ivh /mnt/Packages/bind-chroot-9.9.4-61.el7.x86_64.rpm 
错误：依赖检测失败：
	bind = 32:9.9.4-61.el7 被 bind-chroot-32:9.9.4-61.el7.x86_64 需要


五、构建Yum软件包仓库
作用：自动解决依赖关系安装软件
服务：自动解决依赖关系安装软件
rpm不能解决依赖关系
yum能自动解决依赖关系

服务端(本机):    1.众多的软件   
2.仓库数据文件（仓库清单repodata）
3.FTP协议  或   http  协议 (网络Yum仓库）     
本地Yum仓库：服务端需要有光盘内容即可

Yum---->/etc/yum.repos.d/*.repo---->仓库位置
客户端(本机)：指定服务端位置
–仓库配置：/etc/yum.repos.d/*.repo
–错误的文件会影响正确的文件
仓库配置：/etc/yum.repos.d/*.repo
配置文件路径必须是/etc/yum.repos.d/，结尾名称必须是.repo结尾
本地yum仓库 客户端和服务端是一台机器
网络yum仓库 客户端和服务端是两台机器




客户端文件配置内容：
–[源名称] ：自定义名称，具有唯一性
–name：本软件源的描述字串
–baseurl：指定YUM服务端的URL地址 
–enabled：是否启用此频道
–gpgcheck：是否验证待安装的RPM包
–gpgkey：用于RPM软件包验证的密钥文件


yum   配置文件  /mnt
]# vim /etc/yum.repos.d/*.repo
[nsd20]         		#仓库名称
name=centos07 		#仓库描述信息（一般为光盘版本信息）
baseurl=file:///mnt	#指定服务端位置file://表示本地为服务端
enabled=1			#本文件启用
gpgcheck=0			#不检测红帽签名信息
gpgcheck写1则需要写下面
gpgkey=file:///mnt/RPM-GPG-KEY-CentOS-7


]# yum   repolist      #列出仓库信息


总结：本地Yum仓库构建方法
1.服务端：显示光盘的内容，挂载光驱设备
2.客户端：书写客户端配置文件，指定服务端位置
3.执行流程:  yum命令--->/etc/yum.repos.d/*.repo--->baseurl=file:///mnt


总结：排错思路
1.是否挂载光驱设备(/dev/cdrom)
2.错误的文件会影响正确的文件
3.配置文件内容是否书写正确

六、Yum的使用
安装软件
[root@localhost ~]# yum -y install httpd
[root@localhost ~]# rpm -q httpd

[root@localhost ~]# yum -y install bind-chroot
[root@localhost ~]# rpm -q  bind-chroot

[root@localhost ~]# yum -y install sssd
[root@localhost ~]# rpm -q  sssd

[root@localhost ~]# yum -y install gcc
[root@localhost ~]# rpm -q  gcc

[root@localhost ~]# yum -y install  xorg-x11-apps
[root@localhost ~]# rpm -q  xorg-x11-apps

[root@localhost ~]# rpm  -ql   xorg-x11-apps   |   grep bin
[root@localhost ~]# xeyes


卸载软件
[root@localhost ~]# yum   remove   gcc
[root@localhost ~]# rpm  -q   gcc

[root@localhost ~]# yum   remove   httpd
[root@localhost ~]# rpm  -q   httpd

[root@localhost ~]# yum   remove   sssd
[root@localhost ~]# rpm   -q  sssd



查询
[root@localhost ~]# yum list  ftp    #查询仓库是否有ftp软件
可安装的软件包      #表示当前系统没有安装该软件
ftp.x86_64        0.17-67.el7         nsd20

[root@localhost ~]# yum  list  httpd
[root@localhost ~]# yum  search  ftp  #包含ftp就匹配

]# yum   provides   /usr/bin/hostname 
]# yum  provides  /etc/passwd#仓库中那个软件包产生该文件


重新安装软件，进行简单的修复         
[root@nb ~]# ls  /usr/bin/hostname
/usr/bin/hostname
[root@nb ~]# rm -rf   /usr/bin/hostname
[root@nb ~]# ls   /usr/bin/hostname
ls: 无法访问/usr/bin/hostname: 没有那个文件或目录
[root@nb ~]# hostname
bash: hostname: 未找到命令...
[root@nb ~]# yum   provides   /usr/bin/hostname
[root@nb ~]# yum -y  reinstall  hostname    #重新安装
[root@nb ~]# ls   /usr/bin/hostname
[root@nb ~]# hostname


清空yum缓存
[root@localhost ~]# yum  clean   all    #清空yum缓存
[root@localhost ~]# yum   repolist
[root@localhost ~]# yum   repolist



七、命令补充
获取命令帮助
方式一：命令  --help
[root@localhost ~]# cat  --help
方式二：man   命令  
[root@localhost ~]# man   cat        #按q退出
[root@localhost ~]# man   passwd    #显示passwd命令帮助
[root@localhost ~]# man  5  passwd
数字5表示帮助的类型，表示配置文件类型

历史命令
管理/调用曾经执行过的命令
–history：查看历史命令列表
–history  -c：清空历史命令
–!n：执行命令历史中的第n条命令
–!str：执行最近一次以str开头的历史命令
[root@svr7 ~]# vim  /etc/profile
HISTSIZE=1000      #默认记录1000条


[root@localhost ~]# history          #显示历史命令列表
[root@localhost ~]# history   -c    #清空历史命令
[root@localhost ~]# history                 
[root@localhost ~]# cat   /etc/redhat-release 
[root@localhost ~]# ls   /root
[root@localhost ~]# history

[root@localhost ~]# !cat  #执行最近一条以cat开头的历史命令
[root@localhost ~]# !ls    #执行最近一条以ls开头的历史命令


du，统计文件的占用空间
–du  [选项]...  [目录或文件]...
–-s：只统计每个参数所占用的总空间大小
–-h：提供易读容量单位（K、M等） 
[root@localhost ~]# du  -sh   /root

date，查看/调整系统日期时间
–date  +%F、date +%R
–date  +"%Y-%m-%d %H:%M:%S"
–date  -s  "yyyy-mm-dd  HH:MM:SS" 

]# date
]# date  -s    "2008-9-6   11:11:11"     #修改系统时间
]# date

]# date   -s    "2020-9-5   15:37:11"   
]# date

[root@localhost ~]# date   +%Y     #显示年
[root@localhost ~]# date   +%m    #显示月
[root@localhost ~]# date   +%d    #显示日期

[root@localhost ~]# date   +%H   #显示时
[root@localhost ~]# date   +%M   #显示分
[root@localhost ~]# date   +%S    #显示秒

[root@localhost ~]# date   +%F   #显示年-月-日
[root@localhost ~]# date   +%R   #显示时:分
 

制作链接文件（制作快捷方式）
格式：ln  -s   /路径/源数据     /路径/快捷方式的名称    #软链接

]# ls   /etc/sysconfig/network-scripts/ifcfg-lo 
]# ln -s  /etc/sysconfig/network-scripts/ifcfg-lo   /mylo
]# ls   /
]# ls   -l   /mylo      #查看详细信息





]# ln  -s    /etc/sysconfig/network-scripts/   /ns
]# ls   /
]# ls   -l   /ns    #查看快捷方式的信息

]# touch   /ns/haha.txt
]# touch   /ns/maohehaozi.txt
]# touch   /ns/shukehebeita.txt
]# ls   /etc/sysconfig/network-scripts/

格式：ln -s /路径/源数据     /路径/快捷方式的名称    #硬链接
软链接优势：可以针对目录与文件制作快捷方式，支持跨分区
软链接缺点：源数据消失，快捷方式失效




格式：ln     /路径/源数据     /路径/快捷方式的名称    #硬链接
硬链接优势：源数据消失，快捷方式仍然有效
硬链接缺点：只能针对文件制作快捷方式，不支持支持跨分区



]# echo  123   >   /opt/A.txt
]# ln  -s   /opt/A.txt    /opt/B.txt   #软链接
]# ls /opt/

]# ln    /opt/A.txt    /opt/C.txt   #硬链接
]# ls    /opt/
]# cat    /opt/B.txt 
]# cat    /opt/C.txt 
]# rm  -rf   /opt/A.txt 
]# ls   /opt/
]# cat  /opt/B.txt      #软链接失效
cat: /opt/B.txt: 没有那个文件或目录
]# cat   /opt/C.txt     #硬链接仍然有效

zip归档工具，跨平台（没有明显缺点）
•归档+压缩操作: zip  [-r]   备份文件.zip     被归档的文档... 
[-r]:被归档的数据有目录，必须加上此选项
]# zip   -r     /opt/abc.zip        /etc/passwd     /home
]# ls   /opt/
•释放归档+解压操作:  unzip   备份文件.zip    [-d  目标文件夹]  
]# mkdir   /nsd20
]# unzip       /opt/abc.zip       -d    /nsd20
]# ls   /nsd20
]# ls   /nsd20/etc/
]# ls   /nsd20/home/
]# unzip -l #显示包里面内容





-----------------------------------------------------------------------------
10.12
唯一标识UID(编号从0开始，默认最大60000)
UID是用户 GID是组
一个用户必须至少属于一个组
基本组 系统创建，与用户同名
附加组（从属组）由管理员创建，由管理员进行加入
/etc/passwd ：国家户口系统
用户家目录 ： 房子
root:  x:        0:   0:       root:       /root:/bin/bash
用户名：密码占位符：UID：基本组GID：用户描述信息：家目录：解释器

/etc/shadow，保存密码字串/有效期等信息
–每个用户记录一行，以：分割为9个字段
[root@localhost ~]# grep nsd01  /etc/shadow
nsd01:$6$NVe937Nd$B0n94XrpQ.LipQHTpYh0iV/M4jCLdccfHxzRLprdxDzwk8WDDh/TzdTfh8lA9y9WKJ.8Ls/l5.w/1W.nV6CFX/:18481:0:99999:7:::
上一次修改密码的时间：自1970-1-1到达上一次修改密码的时间，所经历的天数
字段1：用户帐号的名称
字段2：加密后的密码字符串
字段3：上次修改密码的时间
字段4：密码的最短有效天数，默认0
字段5：密码的最长有效天数，默认99999
字段6：密码过期前的警告天数，默认7
字段7：密码过期后多少天禁用此用户账号
字段8：帐号失效时间，默认值为空
字段9：保留字段（未使用）
root用户"su"不进去nologin的用户
只有root用户才能passwd 用户 的密码


组账户管理
/etc/group，保存组帐号的基本信息
每个组记录一行，以：分割为4个字段
[root@localhost ~]# grep stugrp /etc/group
stugrp:x:1504:nsd06
组名:组密码占位符:组的GID:组成员列表

/etc/gshadow：组的管理信息配置文件
[root@localhost ~]# grep tarena  /etc/gshadow
tarena:!:nb:
组名:密码加密字符串:组的管理员列表:组成员列表

总结：
/etc/passwd：用户基本信息配置文件
/etc/shadow：用户密码信息配置文件
/etc/group：组的基本信息配置文件
/etc/gshadow：组的管理信息配置文件







三、用户管理简介
用户账户
作用:  1.可以登陆操作系统   2.不同的用户具备不同的权限
唯一标识：UID（编号从0开始的编号，默认最大60000
管理员root的UID：永远为0
普通用户的UID：默认从1000开始


组账户                                  
作用: 方便管理用户
唯一标识：GID（编号从0开始的编号，默认最大60000）
sg（1500）
原则：Linux一个用户必须至少属于一个组
组账户的分类：
基本组：系统创建，与用户同名
附加组（从属组）：由管理员创建，由管理员进行加入


本地账户的数据文件
–/etc/passwd、/etc/shadow
–/etc/group、/etc/gshadow

/etc/passwd：存放用户基本信息配置文件
[root@localhost ~]# head -1  /etc/passwd
root:x:0:0:root:/root:/bin/bash
用户名:密码占位符:UID:基本组GID:用户描述信息:家目录:解释器

/etc/passwd:国家户口系统
用户家目录:房子




用户账号创建
命令useradd
–格式：useradd  [选项]...  用户名
•常用命令选项
:-u 指定UID标记号
:-d 指定宿主目录(家目录)，默认为/home/用户名（只能为新建用户指定家目录）
:-G 指定所属附加组 
:-s 指定用户的解释器(默认是/bin/bash)

备注:-d不能指定多层且不存在的目录；不能指定已经存在的文件夹作为家目录；多个用户不能共用一个家目录；

-u：指定 UID 编号
]# useradd  -u  1500    nsd02   #指定UID创建用户
]# id   nsd02                   #查看nsd02用户基本信息
]# grep   nsd   /etc/passwd     #查看用户信息

-d：指定宿主目录（家目录），缺省为 /home/用户名
[root@localhost ~]# useradd -d   /opt/nsd04      nsd04
[root@localhost ~]# grep  nsd04  /etc/passwd
[root@localhost ~]# ls  /opt/


-G：指定所属的附加组
]# groupadd   stugrp    #单独创建stugrp组
]# useradd  -G  stugrp   nsd06
]# id nsd06
uid=1504(nsd06) gid=1505(nsd06) 组=1505(nsd06),1504(stugrp)

-s：指定用户的登录解释器
shell：壳，解释器
用户---->解释器---->内核---->硬件
/sbin/nologin:禁止用户登录操作系统

[root@localhost ~]# useradd -s /sbin/nologin   nsd10
[root@localhost ~]# grep nsd10 /etc/passwd


usermod命令
–格式：usermod  [选项]...  用户名
•常用命令选项 
:-l 修改用户名字 
:-u 修改用户UID	
:-s 修改用户解释器
:-d 修改用户家目录(不会新建家目录，只会更改/etc/passwd)
:-G 重置用户附加组


-l：更改用户帐号的登录名称
-u：用户id
-s：登录解释器

]# usermod   -l  stu13   nsd13       #修改用户名字
]# usermod  -u 1600   stu13           #修改用户的UID
]# usermod -s /sbin/nologin   stu13 #修改用户的解释器程序



-d：家目录路径（不会自动创建家目录）
[root@localhost ~]# usermod  -d   /etc/abc   nsd15
[root@localhost ~]# grep  nsd15   /etc/passwd
nsd15:x:1601:1601::/etc/abc:/bin/bash

-G：修改用户的附加组        #重置附加组
[root@localhost ~]# groupadd  tmooc
[root@localhost ~]# usermod  -G  tmooc  nsd16


设置密码
passwd命令
–格式：passwd  [选项]...  用户名
[root@localhost ~]# passwd  nsd01       #交互式设置
[root@localhost ~]# su  -  nsd01       #临时切换用户身份
[nsd01@localhost ~]$ passwd 
[nsd01@localhost ~]$  exit    #退出。回到root用户身份

passwd命令，支持非交互式设置密码
--stdin：从标准输入（比如管道）取密码
]# echo 123   |   passwd  --stdin   nsd01
]# echo 123456   |   passwd  --stdin  nsd01
]# echo redhat   |   passwd  --stdin  nsd01



/etc/shadow，保存密码字串/有效期等信息 
–每个用户记录一行，以：分割为9个字段
[root@localhost ~]# grep nsd01  /etc/shadow
nsd01:$6$NVe937Nd$B0n94XrpQ.LipQHTpYh0iV/M4jCLdccfHxzRLprdxDzwk8WDDh/TzdTfh8lA9y9WKJ.8Ls/l5.w/1W.nV6CFX/:18481:0:99999:7:::
上一次修改密码的时间：自1970-1-1到达上一次修改密码的时间，所经历的天数

字段1：用户帐号的名称
字段2：加密后的密码字符串
字段3：上次修改密码的时间
字段4：密码的最短有效天数，默认0
字段5：密码的最长有效天数，默认99999
字段6：密码过期前的警告天数，默认7
字段7：密码过期后多少天禁用此用户账号
字段8：帐号失效时间，默认值为空
字段9：保留字段（未使用）

用户初始配置文件
配置文件来源
新建用户时，新建用户家目录，根据 /etc/skel 模板目录复制
[root@localhost ~]# ls -A /etc/skel/

[root@localhost ~]# touch  /etc/skel/haxi.txt
[root@localhost ~]# mkdir  /etc/skel/test
[root@localhost ~]# ls -A  /etc/skel/


主要的初始配置文件
–~/.bash_profile：每次登录系统时执行，定义初始变量值
–~/.bashrc：每次进入新的Bash环境时执行(开启新的终端)



为root用户定义永久的别名
[root@localhost ~]# vim   /root/.bashrc     #定义永久别名
alias   haha='echo   I  Love  Dcc'

开启新的终端进行测试：
[root@localhost ~]# haha


–/etc/bashrc：全局配置文件，影响全体用户 (开启新的终端)
[root@localhost ~]# vim   /etc/bashrc 
alias   xixi='echo xixi'


七、删除用户
userdel命令
格式：userdel  [-r]  用户名
添加 -r 选项，宿主目录/用户邮件也一并删除
]# userdel  -r  nsd01  #连同家目录一并删除
]# userdel    nsd02   #不删除家目录，只删除用户信息



组账户管理
/etc/group，保存组帐号的基本信息
每个组记录一行，以：分割为4个字段
[root@localhost ~]# grep stugrp /etc/group
stugrp:x:1504:nsd06
组名:组密码占位符:组的GID:组成员列表

[root@localhost ~]# groupadd  tarena     #新建一个组
[root@localhost ~]# grep tarena  /etc/group  #查看组信息
tarena:x:1607:

gpasswd命令
–格式：gpasswd  [选项]...  组名
•常用命令选项
–-a：添加组成员，每次只能加一个
–-d：删除组成员，每次只能删一个
–-M：定义组成员用户列表，可设置多个（重新定义组即把原有的清空，重新覆盖放入新的内容）
–-A：定义组管理员列表（同时也具备重新定义的效果；下放权限只有-a,-d，管理员可以不属于本组，可以是另外的组）



]# gpasswd -a  kaka  tarena     #添加用户kaka到tarena组
]# grep  tarena  /etc/group     #查看组信息
]# gpasswd  -M ‘jack,kenji’  tarena   #定义组成员列表
]# gpasswd -d nb  tarena      #将nb用户从tarena组中删除
]# gpasswd  -M ‘’  tarena   #删除组中所有成员


-A：定义组管理员列表                
[root@localhost ~]# gpasswd -A  nb  tarena  #设置组管理员


/etc/gshadow：组的管理信息配置文件
[root@localhost ~]# grep tarena  /etc/gshadow
tarena:!:nb:
组名:密码加密字符串:组的管理员列表:组成员列表
                                



删除组账号：删除组的时候，不可以删除基本组
[root@localhost ~]# grep  tmooc  /etc/group
[root@localhost ~]# grep  tarena  /etc/group

[root@localhost ~]# groupdel  tarena
[root@localhost ~]# groupdel  tmooc
[root@localhost ~]# grep  tmooc  /etc/group
[root@localhost ~]# grep  tarena  /etc/group





九、计划任务
用途：按照设置的时间间隔为用户反复执行某一项固定的系统任务
软件包：cronie、crontabs
系统服务：crond
日志文件：/var/log/cron

使用 crontab 命令
–编辑：crontab  -e  [-u  用户名]
–查看：crontab  -l  [-u  用户名]
–清除：crontab  -r  [-u  用户名]

计划任务书写格式
分  时  日  月  周      任务命令行（绝对路径）   
*     *    *    *   *            #第一个*号前不能有空格  
30   8   *    *   *        #每天早上8:30 执行一次
30   23   *    *   *       #每天晚上23:30 执行一次
30   23   *    *   5       #每周的周五23:30 执行一次
30   23   *    *   1-5     #每周的周一至周五23:30 执行一次
30   23   *    *   1,3,6   #每周的周一周三周六23:30 执行一次
30   23   1    *   1       #每月的1号或每周一晚上23:30 执行一次
*     *    *    *   *            #每分钟运行一次
*/5     *    *    *   *          #每隔5分钟运行一次
1     */2    *    *   *          #每隔2小时运行一次 如果不写数字则代表每2小时运行60次，写数字代表每2小时的第x分钟运行一次


*：匹配范围内任意时间
,：分隔多个不连续的时间点
-：指定连续时间范围
/n：指定时间频率，每n ...


案例：
   每分钟记录当前的系统时间，写入/opt/time.txt
[root@localhost ~]# date
[root@localhost ~]# date  >>  /opt/time.txt
[root@localhost ~]# cat /opt/time.txt
[root@localhost ~]# crontab  -e        #编写计划任务
*   *   *   *   *    date  >>  /opt/time.txt

[root@localhost ~]# crontab  -l           #查看计划任务内容
*  *  *   *   *    date  >>  /opt/time.txt

[root@localhost ~]# cat  /opt/time.txt


-------------------------------------------------------------------------------
10.13


-文本文件 .后数字代表硬连接的数
d目录     .后数字代表目录有多少个子目录
l快捷方式  .

r 读
w 写 
x 执行
读r和执行x一般都是一起给的
如果只有写w权限，没有执行x权限，Linux判定这个w权限无效
在父目录没有任何权限，子目录有所有权限的情况下，依旧无法进入操作
u 所有者 
g 所属组
o 其他人

rwx 421

gpasswd 用户加入组会继承该组的权限，需要重新登录su-才可以生效

setfacl [选项] u:用户名:权限：[路径文件]
-m 修改ACL策略
-x 清除指定的ACL策略
-b 清除所有已设置的ACL策略
-R 递归设置ACL策略

ACL判定是最高的，ACL策略--->属主权限--->属组权限--->其他人权限
重点！！！！！“能够为个别用户、个别组，设置独立的权限”
权限后看到有+号显示，代表有ACL策略
权限后看到有+号显示，就要到getfacl /文件/ 查看权限，这样更加准确。



粘滞位
t权限 防止公共目录下不同用户相互删除


setGID
与chown -R 不同的是，他只影响未来，即在设置SetGID权限之后的文件内新建文件后，都会继承原有的所属组








一、环境的准备
还原快照，开启CentOS7虚拟机，利用root进行登录
构建本地Yum仓库
[root@localhost ~]# mkdir    /nsd30
[root@localhost ~]# mount   /dev/cdrom   /nsd30
[root@localhost ~]# ls   /nsd30


[root@localhost ~]# rm -rf  /etc/yum.repos.d/*
[root@localhost ~]# vim  /etc/yum.repos.d/dvd.repo
[haha]
name=xixi
baseurl=file:///nsd30
enabled=1
gpgcheck=0
[root@localhost ~]# yum -y install xorg-x11-apps




二、基本权限与归属
•访问权限
–读取：允许查看内容-read
–写入：允许修改内容-write
–可执行：允许运行和切换-excute
  对于文本文件：
       r读取权限：cat、less、grep、head、tail
       w写入权限：vim、> 、 >>
       x可执行权限：Shell与Python



•归属关系
–所有者(属主)：拥有此文件/目录的用户-user
–所属组(属组)：拥有此文件/目录的组-group
–其他用户：除所有者、所属组以外的用户-other
   zhangsan（所有者）    zhangsan(所属组)      1.txt




•执行 ls  -l或者ls  -ld 命令查看
      以-开头：文本文件
      以d开头：目录
      以l开头：快捷方式

[root@localhost ~]# ls -ld   /etc/
[root@localhost ~]# ls -l     /etc/passwd
[root@localhost ~]# ls -ld   /root
[root@localhost ~]# ls -ld   /tmp         #有特殊权限
[root@localhost ~]# ls -l    /etc/shadow

[root@localhost ~]# useradd  zhangsan
[root@localhost ~]# ls -ld   /home/zhangsan



三、修改权限
•chmod命令
–格式：chmod   [ugoa] [+-=][rwx]    文件...
•常用命令选项
–-R：递归修改权限（了解）



]# mkdir   /nsd10	
]# ls   -ld   /nsd10
]# chmod   u-w    /nsd10      #所有者去掉w权限
]# ls -ld    /nsd10
]# chmod   u+w  /nsd10       #所有者加上w权限
]# ls  -ld   /nsd10
]# chmod   g+w   /nsd10      #所属组加上w权限
]# ls   -ld    /nsd10
]# chmod    g=r    /nsd10      #所属组重新定义权限
]# ls    -ld    /nsd10
]# chmod    a=rwx    /nsd10    #a表示所有人
]# ls   -ld     /nsd10
]# chmod  u=---,g=rx,o=rwx     /nsd10
]# ls   -ld   /nsd10


-R：递归修改权限
[root@localhost ~]# mkdir -p /opt/aa/bb/cc
[root@localhost ~]# chmod -R o=--- /opt/aa
[root@localhost ~]# ls -ld /opt/aa
[root@localhost ~]# ls -ld /opt/aa/bb
[root@localhost ~]# ls -ld /opt/aa/bb/cc



Linux中判断用户具备的权限：      
1.首先查看用户对于此数据是否有ACL策略
2.查看用户，对于该数据所处的身份，顺序所有者>所属组>其他人，原则是匹配及停止
3.查看相应身份的权限位     



对于目录：
   读取权限：查看目录内容
   写入权限：能够创建、删除、修改等目录的内容
   执行权限：能够cd切换到此目录下




案例1：设置基本权限
1）以root身份新建/nsddir1/目录，在此目录下新建readme.txt文件
[root@localhost ~]# mkdir  /nsddir1
[root@localhost ~]# echo  123456   >  /nsddir1/readme.txt
[root@localhost ~]# cat   /nsddir1/readme.txt
2）使用户zhangsan能够修改readme.txt文件内容
[root@localhost ~]# chmod  o+w  /nsddir1/readme.txt
3）使用户zhangsan不可以修改readme.txt文件内容
[root@localhost ~]# chmod  o-w  /nsddir1/readme.txt
4）使用户zhangsan能够在此目录下创建/删除子目录
[root@localhost ~]# chmod  o+w   /nsddir1/
5）调整此目录的权限，使任何用户都不能进入，然后测试用户zhangsan是否还能修改readme.txt（测试结果为不能，因为对父目录没有权限）
[root@localhost ~]# chmod  a-x  /nsddir1/
6）为此目录及其下所有文档设置权限 rwxr-x---
[root@localhost ~]# chmod -R u=rwx,g=rx,o=--- /nsddir1/


四、修改归属关系
•chown命令  
–chown  属主  文件...
–chown  属主:属组  文件...
–chown  :属组  文件...
•常用命令选项
–-R：递归修改归属关系

]# mkdir  /nsd15
]# ls -ld   /nsd15
]# groupadd   tmooc        #创建组tmooc

]# chown   lisi:tmooc   /nsd15    #修改所有者与所属组
]# ls -ld /nsd15

]# chown   zhangsan   /nsd15    #仅修改所有者
]# ls -ld /nsd15

]# chown   :root  /nsd15     #仅修改所属组
]# ls  -ld   /nsd15


]# mkdir  -p  /opt/test/haha/xixi
]# chown -R  zhangsan  /opt/test  #递归修改归属关系
]# ls   -ld   /opt/test
]# ls   -ld   /opt/test/haha/
]# ls   -ld   /opt/test/haha/xixi/









案例2：归属关系练习        
1）利用root的身份新建/tarena目录，并进一步完成下列操作 
[root@localhost ~]# mkdir  /tarena       
2）将/tarena属主设为gelin01，属组设为tmooc组  
[root@localhost ~]# useradd    gelin01 
[root@localhost ~]# groupadd   tmooc
[root@localhost ~]# chown   gelin01:tmooc    /tarena
3）使用户gelin01对此目录具有rwx权限，除去所有者与所属组之外的用户对此目录无任何权限
[root@localhost ~]# chmod  o=---  /tarena
4）使用户gelin02能进入、查看此目录
[root@localhost ~]# useradd  gelin02
[root@localhost ~]# gpasswd  -a  gelin02  tmooc 
5）将gelin01加入tmooc组，将tarena目录的权限设为450，测试gelin01用户能否进入此目录（测试结果为不能）
[root@localhost ~]# gpasswd  -a  gelin01  tmooc 
[root@localhost ~]# chmod  450  /tarena

权限利用数字方式表示
•权限位的8进制数表示
–r、w、x分别对应4、2、1，后3组分别求和
分组	User权限	Group权限	Other权限
字符	r	w	x	r	-	x	r	-	x
数字	4	2	1	4	0	1	4	0	1
求和	7	5	5

[root@localhost ~]# mkdir   /nsd14
[root@localhost ~]# ls  -ld   /nsd14

[root@localhost ~]# chmod   700    /nsd14
[root@localhost ~]# ls   -ld   /nsd14

[root@localhost ~]# chmod   007   /nsd14
[root@localhost ~]# ls   -ld   /nsd14

[root@localhost ~]# chmod    750    /nsd14
[root@localhost ~]# ls   -ld   /nsd14
[root@localhost ~]# chmod    755    /nsd14
[root@localhost ~]# ls   -ld   /nsd14





五、附加权限(特殊权限)
•粘滞位，Sticky Bit 权限（t权限）
–占用其他人（Other）的 x 位
–显示为 t 或 T，取决于其他人是否有 x 权限
–适用于目录，用来限制用户滥用写入权
–在设置了t权限的文件夹下，即使用户有写入权限，也不能删除或改名其他用户文档 


]# mkdir   /home/public
]# chmod  777   /home/public
]# ls   -ld   /home/public
]# chmod  o+t  /home/public
]# ls  -ld  /home/public






•Set GID权限
–占用属组（Group）的 x 位
–显示为 s 或 S，取决于属组是否有 x 权限
–对目录有效
–在一个具有SGID权限的目录下，新建的文档会自动继承此目录的属组身份   


]# mkdir   /nsd18
]# chown   :tmooc   /nsd18
]# ls  -ld   /nsd18

]# mkdir  /nsd18/abc01
]# ls -ld   /nsd18/abc01

]# chmod  g+s  /nsd18     #赋予SGID特殊权限
]# ls -ld  /nsd18

]# mkdir   /nsd18/abc02
]# ls -ld  /nsd18/abc02


六、ACL策略管理
•文档归属的局限性：
–任何人只属于三种角色：属主、属组、其他人
–针对特殊的人实现更精细的控制
•acl访问策略作用：
–能够对个别用户、个别组设置独立的权限
–大多数挂载的EXT3/4、XFS文件系统默认已支持

•setfacl命令
–格式：setfacl [选项] u:用户名:权限  文件...
        setfacl [选项] g:组名:权限  文件...
•常用命令选项
–-m：修改ACL策略
–-x：清除指定的ACL策略
–-b：清除所有已设置的ACL策略
–-R：递归设置ACL策略

[root@nb ~]# setfacl -Rm u:lisi:rx  /opt/test/
[root@nb ~]# getfacl /opt/test/


]# mkdir    /nsd19
]# chmod    770   /nsd19       
]# ls   -ld   /nsd19
]# su  -   dc
]$ cd     /nsd19
-bash: cd: /nsd19: 权限不够
]$ exit

]# setfacl   -m   u:dc:rx    /nsd19    #单独赋予dc权限
]# getfacl   /nsd19                         #查看ACL策略
]# su  -   dc 
]$ cd    /nsd19
]$ pwd
]$ exit

ACL命令的练习：    
]# mkdir   /nsd22
]# setfacl  -m  u:dc:rx    /nsd22
]# setfacl  -m  u:zhangsan:rwx    /nsd22
]# setfacl  -m  u:lisi:rx     /nsd22
]# setfacl  -m  u:gelin01:rwx    /nsd22
]# getfacl     /nsd22

]# setfacl  -x  u:zhangsan   /nsd22 #删除指定用户ACL
]# getfacl   /nsd22

]# setfacl   -x   u:dc    /nsd22    #删除指定用户ACL
]# getfacl   /nsd22
]# setfacl   -b   /nsd22    #清除目录所有ACL策略
]# getfacl   /nsd22


七、补充内容
ACL策略-黑名单的使用（单独拒绝某些用户）
]# setfacl  -m   u:lisi:---    /home/public/
]# getfacl    /home/public/




附加权限SUID权限
占用属主（User）的 x 位
显示为 s 或 S，取决于属主是否有 x 权限
仅对可执行的程序有意义
当其他用户执行带SUID标记的程序时，具有此程序属主的身份和相应权限


[root@localhost ~]# which mkdir
/usr/bin/mkdir
[root@localhost ~]# /usr/bin/mkdir   /opt/abc01
[root@localhost ~]# ls /opt/
[root@localhost ~]# cp  /usr/bin/mkdir   /usr/bin/hahadir
[root@localhost ~]# ls -l /usr/bin/hahadir
[root@localhost ~]# /usr/bin/hahadir   /opt/abc02
[root@localhost ~]# ls /opt/

[root@localhost ~]# chmod  u+s  /usr/bin/hahadir 
[root@localhost ~]# ls -l  /usr/bin/hahadir
[root@localhost ~]# su - zhangsan
[zhangsan@localhost ~]$ /usr/bin/mkdir   zs01
[zhangsan@localhost ~]$ ls  -l
[zhangsan@localhost ~]$ /usr/bin/hahadir zs02
[zhangsan@localhost ~]$ ls -l

文件/目录的默认权限
•新建文件/目录的默认权限
–一般文件默认均不给 x 执行权限
–其他取决于 umask(权限掩码) 设置
–新建目录默认权限为755
–新建文件默认权限为644
[root@localhost ~]# umask 
0022
[root@A ~]# umask -S
u=rwx,g=rx,o=rx


---------------------------------------------------------------------------------------
10.14

KVM虚拟机(Linux)
dev/vda 虚拟接口第一块
dev/vdb 虚拟接口第二块

VMware虚拟机(win)
dev/sda 虚拟接口第一块
dev/sdb 虚拟接口第二块

磁道
扇区 硬盘里最小的区域 默认512字节
 一个磁道可以有多个扇区

识别硬盘--->分区规划--->--->挂载使用

识别硬盘 
]#lsblk #列出当前系统识别的硬盘
分区规划
分区方案(分区模式):MBR与GPT(两种模式不能混搭)
MBP分区模式 最大支持2.2TB硬盘
主分区：最多只能有4个，如果已经分完4个，就算硬盘有剩余空间也不可使用。
扩展分区：最多只能有一个，需要占用剩下的所有空间；自身不能储存数据
逻辑分区：最多无上限，需要在扩展分区内
扩展分区不能格式化
主分区和逻辑分区用来储存数据

命令fdisk
m列出指令帮助
p查看现有分区表
n新建分区
d删除分区
q放弃更改退出
w保存更改退出

格式化:赋予空间文件系统的的过程
文件系统:数据在空间中存放的规则
命令mkfs
windows常见文件系统:NTFS FAT
Linux常见文件系统:ext4(RHEL6) xfs(RHEL7) FAT
xfs(RHEL7)适合文件数量较少，单个文件较大
ext4适合文件数量较多，单个文件较小
vfat是是高级版的FAT

格式化后UUID是分区后的唯一标识

总结
识别硬盘 lsblk
分区规划 fdisk
刷新分区表
格式化文件系统
挂载 mount /etc/

设备路径       挂载点       文件系统类型 参数    备份标记 检测顺序
/dev/vdb1   /mypart1   ext4    defaults    0      0

备注 挂载点为根的 建议检测

GPT
最多支持128个主分区，最大支持18EB磁盘

命令parted 回车即是保存
	mktable gpt  分区类型	
	mkpart 	创建
	print  	查询
	起始点减去结束点 等于设置的分区大小
	unit GB



交换空间(虚拟内存)

swapon查看交换空间
mkswap格式化交换空间系统
swapon /dev/vdc2启用交换空间
swapoff /dev/vdc2停用交换空间
free -m查看交换空间
只有扩展空间不能作为交换空间



KVM虚拟化 
添加虚拟及硬盘，真机会创建一个大文件，充当虚拟机的硬盘，当删除虚拟机添加的硬盘，但是真机不会删除对应的大文件。






二、磁盘空间的管理
真机是Linux：KVM虚拟机

真机是windows：添加一块新的硬盘（磁盘）
关闭虚拟机CentOS7
[root@localhost ~]# poweroff 


2.查看本机识别的新的硬盘
KVM虚拟机（真机为Linux同学）
[root@localhost ~]# ls   /dev/vda  #虚拟接口第一块
[root@localhost ~]# ls   /dev/vdb  #虚拟接口第二块
[root@localhost ~]# lsblk  #列出当前系统识别的硬盘
VMware虚拟机（真机为windows同学）
[root@localhost ~]# ls   /dev/sda  #SCSI接口第一块
[root@localhost ~]# ls   /dev/sdb  #SCSI接口第二块
[root@localhost ~]# lsblk  #列出当前系统识别的硬盘


扇区的大小：512字节
计算机容量单位：
一般用B，KB，MB，GB，TB，PB，EB，ZB，YB，BB来表示，它们之间的关系是：
1KB (Kilobyte 千字节)=1024B,
1MB (Megabyte 兆字节 简称“兆”)=1024KB，
1GB (Gigabyte 吉字节 又称“千兆”)=1024MB,
1TB (Terabyte 万亿字节 太字节)=1024GB,
1PB (Petabyte 千万亿字节 拍字节)=1024TB,
1EB (Exabyte 百亿亿字节 艾字节)=1024PB,
1ZB (Zettabyte 十万亿亿字节 泽字节)= 1024 EB,
1YB (Yottabyte 一亿亿亿字节 尧字节)= 1024 ZB,
1BB (Brontobyte 一千亿亿亿字节)= 1024 YB.


一块硬盘的“艺术”之旅（硬盘空间使用，经历的步骤）
•识别硬盘 => 分区规划 => 格式化 => 挂载使用

  
三、识别硬盘
[root@localhost ~]# lsblk #列出当前系统识别的硬盘
四、分区规划 
•分区方案（分区模式）： MBR与GPT
•MBR/msdos分区模式
–分区类型：主分区、扩展分区(占用所有剩余空间)、逻辑分区
–最多只能有4个主分区
–扩展分区可以没有，至多有一个
–1~4个主分区，或者 3个主分区+1个扩展分区（n个逻辑分区）
–最大支持容量为 2.2TB 的磁盘
–扩展分区不能格式化，空间不能直接存储数据
–可以用于存储数据的分区：主分区与逻辑分区


fdisk常用交互指令：
m 列出指令帮助
p 查看现有的分区表（存放分区信息的表格）
n 新建分区
d 删除分区
q 放弃更改并退出
w 保存更改并退出（写入进行改变硬盘结构）


[root@localhost ~]# ls  /dev/sdb
[root@localhost ~]# fdisk    /dev/sdb  
n 创建新的分区----->分区类型 回车----->分区编号 回车---->起始扇区 回车----->在last结束时 +2G
p 查看分区表
n 创建新的分区----->分区类型 回车----->分区编号 回车---->起始扇区 回车----->在last结束时 +1G
w 保存并退出                            
[root@localhost ~]# lsblk
[root@localhost ~]# ls  /dev/sdb[1-2]



五、格式化：赋予空间文件系统的过程
文件系统：数据在空间中存放的规则

Windows常见的文件系统：NTFS    FAT(兼容性强)
Linux常见的文件系统：ext4(RHEL6)   xfs(RHEL7)   FAT
ext4:适合文件数量较多，单个文件较小
xfs:适合文件数量较少，单个文件较大

]# ls  /dev/sdb[1-2]
/dev/sdb1 /dev/sdb2
]# mkfs.(tab)(tab)                 #连续按两次tab键
]# mkfs.ext4   /dev/sdb1     #格式化文件系统ext4
]# mkfs.xfs     /dev/sdb2     #格式化文件系统xfs
]# blkid    /dev/sdb1     #查看文件系统类型
]# blkid    /dev/sdb2      #查看文件系统类型
 
重复格式化
[root@nb ~]# mkfs.xfs  -f   /dev/sdb2    #强制格式化
[root@nb ~]# mkfs.ext4    /dev/sdb2     #无限重复格式化


六、挂载使用                          
]# mkdir   /mypart1
]# blkid   /dev/sdb1                   #查看文件系统类型
]# mount   /dev/sdb1    /mypart1
]# df   -h     |   grep   mypart      #显示正在挂载的设备信息

]# mkdir   /mypart2
]# mount   /dev/sdb2   /mypart2
]# df    -h     |   grep   mypart     #显示正在挂载的设备信息


总结：
1.识别硬盘    lsblk
2.划分分区   MBR分区模式    fdisk 
3.格式化文件系统   mkfs.xfs   mkfs.ext4   blkid  
4.挂载使用   mount    df  -h



七、开机自动挂载/etc/fstab 
–设备路径    挂载点    文件系统类型   参数    备份标记   检测顺序
[root@localhost ~]# blkid   /dev/sdb1
[root@localhost ~]# blkid   /dev/sdb2
[root@localhost ~]# vim    /etc/fstab    #VMware虚拟机
/dev/sdb1   /mypart1   ext4    defaults   0   0
/dev/sdb2   /mypart2    xfs    defaults   0   0
[root@localhost ~]# vim    /etc/fstab    #KVM虚拟机
/dev/vdb1   /mypart1   ext4    defaults   0   0
/dev/vdb2   /mypart2    xfs    defaults   0   0
[root@localhost ~]# umount   /mypart1       #卸载
[root@localhost ~]# umount   /mypart2       #卸载
[root@localhost ~]# df   -h    |   grep  mypart
[root@localhost ~]# mount   -a 
检测/etc/fstab开机自动挂载配置文件,格式是否正确
检测/etc/fstab中,书写完成,但当前没有挂载的设备,进行挂载 
[root@localhost ~]# df  -h     |    grep  mypart


如果/etc/fstab文件有误：修复办法 
1.输入root的密码

2.修改/etc/fstab文件内容


八、综合分区
[root@localhost ~]# fdisk   /dev/sdb
p 查看分区表
n 创建主分区--->回车--->回车--->回车--->在last结束时 +2G
p 查看分区表
n 创建扩展分区 --->回车--->起始回车--->结束回车   将所有剩余空间给扩展分区
p 查看分区表
n 创建逻辑分区----->起始回车------>结束+2G
n 创建逻辑分区----->起始回车------>结束+2G
p 查看分区表                                        
w 保存并退出
[root@localhost ~]# lsblk
[root@localhost ~]# partprobe        #刷新分区表
Warning: 无法以读写方式打开 /dev/sr0 (只读文件系统)。/dev/sr0 已按照只读方式打开。
[root@localhost ~]# lsblk


九、总结
1.识别硬盘 lsblk
2.分区规划 fdisk       MBR分区模式    
3.刷新分区表 partprobe
4.格式化文件系统 mkfs.ext4     mkfs.xfs     blkid
5.挂载   mount     /etc/fstab        mount -a     df -h

  /dev/sda5表示含义：SCSI接口设备第一块的第一个逻辑分区
  /dev/sda6表示含义：SCSI接口设备第一块的第二个逻辑分区




十、分区模式GPT
1.关闭计算机添加新的硬盘
[root@localhost ~]# poweroff 
[root@localhost ~]# lsblk       #查看识别的硬盘

2.分区模式GPT 
•GPT，GUID Partition Table
–全局唯一标识分区表
–突破固定大小64字节的分区表限制
–最多可支持128个主分区，最大支持18EB容量 
** 1 EB = 1024 PB = 1024 x 1024 TB 



•parted常用分区指令
–help    //查看指令帮助
–mktable  gpt    //建立指定模式分区表
–mkpart  分区的名称  文件系统类型  start  end
    //指定大小或百分比%作为起始、结束位置
–print   //查看分区表
–rm  序号    //删除指定的分区
–quit   //退出交互环境


•parted进行分区
[root@localhost ~]# parted     /dev/sdc 
(parted) mktable   gpt           #指定为gpt的分区模式
(parted) mkpart                     #划分新的分区 
分区名称？ []? haha                 #随意写
文件系统类型？ [ext2]? ext4    #随意写，不会进行格式化
起始点？ 0 
结束点？ 5G 
忽略/Ignore/放弃/Cancel? Ignore  #选择忽略，输入i(tab)补全
(parted) print                                #查看分区表信息
(parted) unit   GB                          #使用GB作为单位
(parted) print 
(parted) mkpart                            #划分新的分区
分区名称？ []? haha 
文件系统类型？ [ext2]? ext4 
起始点？ 5G                                  #为上一个分区的结束
结束点？ 10G  
(parted) print                      
(parted) quit 
[root@localhost ~]# lsblk 


十一、交换空间（虚拟内存）          
利用硬盘的空间，充当内存的空间
CPU----->内存----->硬盘
当物理内存占满了，CPU可以将内存的中数据，暂时放入交换空间中，缓解真实物理内存的压力
交换空间最好为内存的2倍，交换空间最大不会超过16G



方式一：利用硬盘分区制作交换空间
]# ls  /dev/sdc2
]# mkswap   /dev/sdc2   #格式化交换文件系统
]# blkid   /dev/sdc2        #查看文件系统类型
]# swapon                        #查看交换空间组成的成员信息
]# swapon   /dev/sdc2    #启用交换分区
]# swapon                         #查看交换空间组成的成员信息
]# free -m                          #查看交换空间的大小

]# swapoff  /dev/sdc2        #停用交换分区
]# swapon                #查看交换空间组成的成员信息
]# free   -m               #查看交换空间的大小 

开机自动启用交换分区
[root@localhost ~]# vim    /etc/fstab 
/dev/sdc2   swap   swap    defaults  0   0
[root@localhost ~]# swapoff     /dev/sdc2     #停用
[root@localhost ~]# swapon    #查看交换空间组成的成员信息
[root@localhost ~]# swapon -a     #专门检测交换分区的书写
[root@localhost ~]# swapon    #查看交换空间组成的成员信息



方式二：利用文件方式
1.生成较大的文件
dd  if=数据的源头  of=生成的文件  bs=每次读写数据的大小  count=次数

/dev/zero:拥有无限的数据
]# dd  if=/dev/zero of=/opt/sw.txt  bs=1M   count=2048
]# ls -lh /opt/sw.txt
]# mkswap /opt/sw.txt
]# swapon /opt/sw.txt
swapon: /opt/sw.txt：不安全的权限 0644，建议使用 0600。
]# swapon

-----------------------------------------------------------------------------------
问题1 两种什么区别
重复格式化
[root@nb ~]# mkfs.xfs  -f   /dev/sdb2    #强制格式化
[root@nb ~]# mkfs.ext4    /dev/sdb2     #无限重复格式化


组建逻辑卷
对象可以是分区也可以是硬盘，不同硬盘的分区也可以
必须时没有使用过的空间
扩展分区不可以
将众多的物理卷整合成卷组，再从卷组中划分出辑卷
逻辑卷在扩展空间时，只能找一个卷组去要，而且必须是之前给空间的卷组
逻辑卷扩展需要空间的扩展，文件系统的扩展
逻辑卷支持缩减
xfs文件系统不支持缩减
ext4文件系统支持缩减

卷组划分空间的单位 PE 
默认一个PE的大小为4M

修改PE个数（PE必须是2的倍数；必须可以整除的数）
vgchange -s PE大小 卷组

创建逻辑卷时候指定PE个数
lvcreate -l
  PE 个数 -n 逻辑卷名 卷组名
物理卷PV     卷组VG      逻辑卷LV     

vgcreate 卷组名称 物理卷位置
lvcreate -L 大小G -n 逻辑卷名字 卷组名


xfs_growfs 刷新xfs文件系统
resize2fs  刷新ext文件系统
lvs 显示空间的大小


删除卷组前提 基于此卷组创建的所有逻辑卷，要全部删除
删除逻辑卷前提 不能删除正在挂载使用
umonut 逻辑卷的挂载
vim/fstab 删除该逻辑卷的启动自动挂载
lvremove 路径 删除逻辑卷

进程管理
程序 静态没有执行的代码 硬盘空间
进程 动态执行的代码cpu与内存资源

进程编号
进程编号PID
systemd（PID永远为1）是所有进程的父进程 

pstree -a,-p,-ap 用户名

ps  aux 列出正在运行的所有进程，显示进程信息非常详细
ps -elf 列出正在运行的所有进程，显示进程父进程信息；PPID为父进程的PID


top -d [刷新秒数] -U [用户名]（不写则查询默认用户）
P按CPU排序 M按内存排序

pgrep [选项][参数]

-l输出进程名
-u检索指定用户的进程
-x精确匹配完整的进程名

[-9]代表强杀
-kill [-9] PID , kill [-9] %
-killall [-9]进程名
-pkill [-9]查找条件（包含就算）


问题
一个物理卷是否可以加入多个卷组



四、逻辑卷
作用：1.整合分散的空间   2.空间支持扩大  

逻辑卷制作过程：将众多的物理卷（PV）组建成卷组（VG），再从卷组中划分出逻辑卷（LV）

LVM管理工具集
功能	物理卷管理	卷组管理	逻辑卷管理
Scan 扫描	pvscan	vgscan	lvscan
Create 创建	pvcreate	vgcreate	lvcreate
Display 显示	pvdisplay	vgdisplay	lvdisplay
Remove 删除	pvremove	vgremove	lvremove
Extend 扩展	/	vgextend	lvextend


五、制作逻辑卷
建立卷组（VG）                     
 格式：vgcreate   卷组名    设备路径…….
Successfully:成功
[root@localhost ~]# vgcreate   systemvg   /dev/sdb[1-2]
[root@localhost ~]# pvs    #查看系统所有物理卷信息
[root@localhost ~]# vgs    #查看系统卷组信息

建立逻辑卷（LV）   
 格式: lvcreate  -L   大小G    -n  逻辑卷名字     卷组名
[root@localhost ~]# lvcreate   -L  16G   -n    vo    systemvg
[root@localhost ~]# vgs    #查看卷组信息
[root@localhost ~]# lvs     #查看逻辑卷信息

使用逻辑卷（LV）
]# ls   /dev/systemvg/vo 
]# ls -l    /dev/systemvg/vo

]# mkfs.xfs    /dev/systemvg/vo    #格式化xfs文件系统
]# blkid   /dev/systemvg/vo   #查看文件系统类型

]# vim  /etc/fstab
/dev/systemvg/vo   /mylv    xfs   defaults  0   0
]# mkdir    /mylv
]# mount   -a       #检测fstab文件内容书写是否正确
]# df    -h             #查看查看正在挂载使用的设备

六、逻辑卷的扩展
卷组有足够的剩余空间
1.扩展逻辑卷的空间
]# df   -h   |   grep   vo
]# vgs
]# lvextend    -L   18G    /dev/systemvg/vo
]# vgs
]# lvs

2.扩展逻辑卷的文件系统（刷新文件系统）
xfs_growfs：刷新xfs文件系统
resize2fs：刷新ext4文件系统
]# xfs_growfs  /dev/systemvg/vo
]# df   -h   |   grep   vo
]# lvs

卷组没有足够的剩余空间
1.扩展卷组的空间
]# vgextend   systemvg    /dev/sdb3   /dev/sdb5
]# vgs
2.扩展逻辑卷的空间
]# vgs
]# lvextend    -L   25G    /dev/systemvg/vo
]# vgs
]# df   -h   |   grep   vo
3.扩展逻辑卷的文件系统（刷新文件系统）
]# xfs_growfs  /dev/systemvg/vo
]# df   -h   |   grep   vo

七、逻辑卷的补充
逻辑卷支持缩减
xfs文件系统：不支持缩减
ext4文件系统：支持缩减

卷组划分空间的单位 PE             
默认1个PE的大小为4M
]# vgdisplay  systemvg
PE Size               4.00 MiB


请创建一个大小为250M的逻辑卷名字为lvredhat
]# vgchange  -s  1M  systemvg    #修改PE大小
]# vgdisplay  systemvg                 #查看卷组详细信息

]# lvcreate  -L  250M  -n   lvredhat   systemvg
]# lvs


•创建逻辑卷的时候指定PE个数
–lvcreate  -l  PE个数  -n  逻辑卷名  卷组名  

[root@localhost ~]# lvcreate -l 108 -n lvhaha  systemvg
 
[root@localhost ~]# lvs


逻辑卷的删除
删除卷组的前提：基于此卷组创建的所有逻辑卷，要全部删除
删除逻辑卷的前提：不能删除正在挂载使用的逻辑卷
[root@localhost ~]# lvremove  /dev/systemvg/vo 
  Logical volume systemvg/vo contains a filesystem in use.
[root@localhost ~]# umount   /mylv/
[root@localhost ~]# lvremove  /dev/systemvg/vo
Do you really want to remove active logical volume systemvg/vo? [y/n]: y
  Logical volume "vo" successfully removed
[root@localhost ~]# vim /etc/fstab #仅删除vo开机自动挂载
[root@localhost ~]# lvremove  /dev/systemvg/lvredhat 
Do you really want to remove active logical volume systemvg/lvredhat? [y/n]: y
  Logical volume "vo" successfully removed


八、RAID磁盘阵列
需要服务器硬件RAID卡

•廉价冗余磁盘阵列
–Redundant Arrays of Inexpensive Disks 
–通过硬件/软件技术，将多个较小/低速的磁盘整合成一个大磁盘
–阵列的价值：提升I/O效率、硬件级别的数据冗余
–不同RAID级别的功能、特性各不相同





•RAID 0，条带模式
–同一个文档分散存放在不同磁盘
–并行写入以提高效率
–至少需要两块磁盘组成，磁盘利用率100%

•RAID 1，镜像模式
–一个文档复制成多份，分别写入不同磁盘
–多份拷贝提高可靠性，效率无提升
–至少需要两块磁盘组成，磁盘利用率50%

•RAID5，高性价比模式
–相当于RAID0和RAID1的折中方案
–需要至少一块磁盘的容量来存放校验数据
–至少需要三块磁盘组成，磁盘利用率n-1/n


•RAID6，高性价比/可靠模式
–相当于扩展的RAID5阵列，提供2份独立校验方案
–需要至少两块磁盘的容量来存放校验数据
–至少需要四块磁盘组成，磁盘利用率n-2/n


•RAID 0+1/RAID 1+0
–整合RAID 0、RAID 1的优势
–并行存取提高效率、镜像写入提高可靠性
–至少需要四块磁盘组成，磁盘利用率50%




九、进程管理

 程序：静态没有执行的代码      硬盘空间

 进程：动态执行的代码   CPU与内存资源

 父进程与子进程   树型结构
 进程编号：PID


pstree查看进程    
•常用命令选项
–-a：显示完整的命令行
–-p：列出对应进程的PID编号 

systemd(PID永远为1)：所有进程的父进程（上帝进程）
[root@localhost ~]#pstree  -p  lisi
bash(9609)───vim(9656)
[root@localhost ~]# pstree  -a  lisi
bash
  └─vim haha.txt
[root@localhost ~]# pstree  -ap  lisi


ps — Processes Snapshot
–格式：ps  [选项]...
•常用命令选项
–aux：显示当前终端所有进程（a）、当前用户在所有终端下的进程（x）、以用户格式输出（u）
–-elf：显示系统内所有进程（-e）、以长格式输出（-l）信息、包括最完整的进程信息（-f）

•ps  aux 操作
–列出正在运行的所有进程，显示进程信息非常详细
用户  进程ID  %CPU  %内存  虚拟内存  固定内存  终端  状态  起始时间  CPU时间  程序指令  

•ps  -elf 操作
–列出正在运行的所有进程，显示进程父进程信息
–PPID为父进程的PID

请计算正在运行的进程有多少个？
[root@localhost ~]# wc  -l    /etc/passwd
[root@localhost ~]# ps  aux   |   wc   -l
[root@localhost ~]# ps   -elf   |   wc   -l



top 交互式工具
–格式：top  [-d  刷新秒数]  [-U  用户名]

[root@localhost ~]# top   -d   1
按大写P进行CPU排序
按大写M进行内存排序


pgrep — Process Grep
–用途：pgrep  [选项]...  查询条件
•常用命令选项
–-l：输出进程名，而不仅仅是 PID
–-U：检索指定用户的进程
–-x：精确匹配完整的进程名

]# pgrep    -l    a
]# pgrep   -U   lisi

]# pstree   -p   lisi

]# pgrep -x crond
]# pgrep -lx crond


十、控制进程（进程前后台的调度）


进程的前后台调度
•&符号：正在运行的状态放入后台
•Ctrl + z 组合键
–挂起当前进程（暂停并转入后台）
•jobs 命令
–查看后台任务列表
•fg 命令
–将后台任务恢复到前台运行
•bg 命令
–激活后台被挂起的任务

[root@localhost ~]# yum -y  install  xorg-x11-apps
[root@localhost ~]# xeyes
^Z             #按Ctrl+z  暂停放入后台
[1]+  已停止               xeyes
[root@localhost ~]# jobs     #查看后台进程信息
[root@localhost ~]# bg 1     #让后台编号为1 的进程继续运行

[root@localhost ~]# jobs     #查看后台进程信息
[root@localhost ~]# fg  1   #让后台编号为1 的进程恢复到前台
xeyes
^C           #按Ctrl+c   结束
[root@localhost ~]#


十一、VDO 了解内容
•Virtual Data Optimizer（虚拟数据优化器）
–一个内核模块，目的是通过重删减少磁盘的空间占用，以及减少复制带宽
–VDO是基于块设备层之上的，也就是在原设备基础上映射出mapper虚拟设备，然后直接使用即可

•重复数据删除
–输入的数据会判断是不是冗余数据
–判断为重复数据的部分不会被写入，然后对源数据进行更新，直接指向原始已经存储的数据块即可

•压缩
–对每个单独的数据块进行处理


[root@svr7 ~]# yum  -y  install  vdo  kmod-kvdo    #所需软件包

•制作VDO卷
•vdo基本操作：参考man vdo 全文查找/example
–vdo  create  --name=VDO卷名称  --device=设备路径 --vdoLogicalSize=逻辑大小
–vdo  list
–vdo  status  -n  VDO卷名称
–vdo  remove  -n  VDO卷名称
–vdostatus  [--human-readable] [/dev/mapper/VDO卷名称]

•VDO卷的格式化加速（跳过去重分析）：
–mkfs.xfs  –K   /dev/mapper/VDO卷名称
–mkfs.ext4  -E  nodiscard  /dev/mapper/VDO卷名称

前提制作VDO需要2G以上的内存
[root@nb ~]# vdo create --name=vdo0 --device=/dev/sdc --vdoLogicalSize=200G
[root@nb ~]# mkfs.xfs -K /dev/mapper/vdo0 
[root@nb ~]# mkdir /nsd01
[root@nb ~]# mount /dev/mapper/vdo0 /nsd01
[root@nb ~]# df -h
[root@nb ~]# vdostats --hum /dev/mapper/vdo0 #查看vdo设备详细信息

[root@svr7 ~]# vim /etc/fstab 
/dev/mapper/vdo0  /nsd01  xfs  defaults,_netdev 0 0 


---------------------------------------------------------------------------------------
10.18
问题 大小L的区别？？？
请创建一个大小为250M的逻辑卷名字为lvredhat
]# vgchange  -s  1M  systemvg    #修改PE大小
]# vgdisplay  systemvg                 #查看卷组详细信息
]# lvcreate  -L  250M  -n   lvredhat   systemvg
]# lvs
•创建逻辑卷的时候指定PE个数
–lvcreate  -l  PE个数  -n  逻辑卷名  卷组名  



]# lvcreate -l 108 -n lvhaha  systemvg
]# lvcreate  -L  250M  -n   lvredhat   systemvg




三种方法1nmcli 2nmtui 3修改配置文件
nmcli特点 非交互 代码长 
命令必须要再起一个网卡名，此命令必须使用自己起的名字进行配置
解析
nmcli connection 添加 类型以太网设备 网卡设备名eth0 nmcil命令的命名为eth0
]#nmcli connection add type ethernet ifname eth0 con-name eht0


修改IP地址、子网掩码、网关默认
]#nmcli connection modify eth0
ipv4.method manual
ipv4.addresses 192.168.4.100/24
ipv4.gateway 192.168.4.200
connection.autoconnect yes

nmcli connection 修改 网卡名
ipv4方法 手工配置
ipv4地址 192.168.4.100/24 
ipv4网关 192.168.4.200
每次开机自动启动以上所有参数
connection.autoconnect yes

]#nmcli connection up eth0 激活

DNS服务器:负责域名解析的机器，将域名解析为IP地址

远程登录工具ssh
ssh [选项] 服务器
ssh [选项] 用户名@服务器
]# ssh root@192.168.4.207
ssh [选项] 


安全复制工具scp=ssh+cp
scp [-r] 用户名@服务器:路径  本地路径
scp [-r] 本地路径  用户名@服务器:路径

生成公钥、私钥
ssh-keygen
发送公钥
ssh-copy-id root@192.168.4.207 

日志的功能
日志文件	主要用途
/var/log/messages	记录内核消息、各种服务的公共消息
/var/log/dmesg	记录系统启动过程的各种消息
/var/log/cron	记录与cron计划任务相关的消息
/var/log/maillog	记录邮件收发相关的消息
/var/log/secure	记录与访问限制相关的安全消息

•users、who、w 命令
–查看已登录的用户信息，详细度不同

•last、lastb 命令
–查看最近登录成功/失败的用户信息

•Linux内核定义的事件紧急程度
–分为 0~7 共8种优先级别
–其数值越小，表示对应事件越紧急/重要


二、配置网络参数之主机名   
配置永久的主机名
]# hostnamectl  set-hostname  svr7.tedu.cn
]# hostname
svr7.tedu.cn

新开终端进行验证
或者：
]# hostname  svr7.tedu.cn   #修改当前
]# echo svr7.tedu.cn  >  /etc/hostname
]# cat  /etc/hostname
svr7.tedu.cn

]# hostname
svr7.tedu.cn

开启一个新的终端查看提示符的变化




三、配置网络参数之IP地址与子网掩码、网关地址

修改网卡命令规则(eth0、eth1、eth2……)
]# ifconfig   |    head    -2
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 00:0c:29:8a:72:4f  txqueuelen 1000  (Ethernet)

]# vim   /etc/default/grub      #grub内核引导程序
……..此处省略一万字
GRUB_CMDLINE_LINUX="…….. quiet   net.ifnames=0  biosdevname=0"
……..此处省略一万字            
]# grub2-mkconfig  -o  /boot/grub2/grub.cfg  #让网卡命名规则生效

]# reboot                               
]# ifconfig   |   head   -2
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.81.132  netmask 255.255.255.0  broadcast 192.168.81.255



三种方法：1.nmcli   2.nmtui   3.修改配置文件

 网卡名eth0  
 nmcli命令必须要在起一个网卡名，此命令必须使用自己起的名字进行配置


nmcli命令的网卡命名，删除错误网卡命名
KVM虚拟机：
[root@svr7 ~]# nmcli   connection   show             #查看
[root@svr7 ~]# nmcli   connection   delete   eth0
[root@svr7 ~]# nmcli   connection   show             #查看
VMware虚拟机：
[root@svr7 ~]# nmcli   connection   show             #查看
[root@svr7 ~]# nmcli   connection   delete   ens33
[root@svr7 ~]# nmcli   connection   show

[root@svr7 ~]# nmcli   connection   show
[root@svr7 ~]# nmcli   connection   delete   有线连接\ 1 
[root@svr7 ~]# nmcli   connection   show


nmcli命令的网卡命名，添加网卡命名     
[root@svr7 ~]# nmcli connection  add  type  ethernet      ifname eth0   con-name   eth0
解析： nmcli connection 添加   类型   以太网设备
网卡设备名为eth0    nmcli命令的命名为eth0

[root@svr7 ~]# nmcli  connection   show

修改IP地址、子网掩码、网关地址       
[root@svr7 ~]# nmcli connection modify  eth0    
ipv4.method    manual                          
ipv4.addresses   192.168.4.100/24                
ipv4.gateway   192.168.4.200              
connection.autoconnect    yes

[root@svr7 ~]# nmcli connection   修改  网卡名    
ipv4.方法    手工配置                          
ipv4.地址   192.168.4.100/24                
ipv4.网关   192.168.4.200                
每次开机自动启用以上所有参数
[root@svr7 ~]# nmcli connection up eth0     #激活
[root@svr7 ~]# ifconfig   |   head   -2

网卡配置文件：/etc/sysconfig/network-scripts/ifcfg-eth0

]# cat   /etc/sysconfig/network-scripts/ifcfg-eth0

]# route   -n        #查看网关地址信息

    Gateway     
192.168.4.254   




nmtui命令行图形交互式 （字体要比较小）

四、配置网络参数之DNS服务器地址

DNS服务器：负责域名解析的机器，将域名解析为IP地址
/etc/resolv.conf:最终有效配置文件

[root@svr7 ~]# echo nameserver   8.8.8.8  > /etc/resolv.conf
[root@svr7 ~]# cat   /etc/resolv.conf
nameserver   8.8.8.8


五、模板机器的修改   

KVM虚拟机:将UUID进行修改，修改为/dev/vda1
[root@svr7 ~]# vim   /etc/fstab
…….此处省略一万字
/dev/vda1      /boot                   xfs     defaults        0 0
……. 此处省略一万字


VMware虚拟机:将UUID进行修改，修改为/dev/sda1
[root@svr7 ~]# vim   /etc/fstab
…….此处省略一万字
/dev/sda1      /boot                   xfs     defaults        0 0
……. 此处省略一万字


六、真机与虚拟机的通信
真机为Linux查看虚拟网卡private1的IP地址：
[root@localhost ~]# ifconfig  private1
private1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.4.254  netmask 255.255.255.0  broadcast 192.168.4.255

虚拟机配置网卡，连接private1网络

十二、远程管理(Linux与Linux)
软件包的安装
[root@svr7 /]# rpm  -qa    |   grep   openssh
openssh-7.4p1-16.el7.x86_64
openssh-server-7.4p1-16.el7.x86_64
openssh-clients-7.4p1-16.el7.x86_64

远程登录工具 ssh
虚拟机A：
[root@svr7 /]#  ssh    root@192.168.4.207
………necting (yes/no)? yes
root@192.168.4.207's password:      #输入密码
[root@pc207 ~]# touch   /root/hahaxixi.txt
[root@pc207 ~]# exit
登出
Connection to 192.168.4.207 closed.
[root@svr7 /]# cat /root/.ssh/known_hosts  #记录曾经远程管理的机器


实现ssh远程管理无密码验证
虚拟机A：
1.生成公钥(锁)与私钥(钥匙)进行验证    
[root@svr7 ~]# ssh-keygen      #一路回车
…….save the key (/root/.ssh/id_rsa):    #回车 设置默认保存位置
……..assphrase):     #回车 设置密码为空
…….. again:     #回车 设置密码为空
[root@svr7 ~]# ls   /root/.ssh/
id_rsa(私钥)    id_rsa.pub(公钥)     known_hosts
2.将公钥(锁)传递给虚拟机B 
[root@svr7 ~]# ssh-copy-id   root@192.168.4.207  
[root@svr7 ~]# ssh   root@192.168.4.207      #测试无密码
[root@pc207 ~]# exit
登出
Connection to 192.168.4.207 closed.
[root@svr7 ~]#
虚拟机B          
[root@pc207 ~]# ls    /root/.ssh/
authorized_keys(别的机器传递过来的公钥)    known_hosts
[root@pc207 ~]#


安全复制工具 scp=ssh+cp
–scp  [-r]  用户名@服务器:路径      本地路径
–scp  [-r]  本地路径    用户名@服务器:路径
虚拟机A：
]# scp    /etc/passwd      root@192.168.4.207:/root
]# scp  -r  /home      root@192.168.4.207:/root/
]# scp    root@192.168.4.207:/etc/shadow       /mnt/

虚拟机B：
]# ls   /root


十三、日志管理
•系统和程序的“日记本”
–记录系统、程序运行中发生的各种事件
–通过查看日志，了解及排除故障
–信息安全控制的“依据”

•由系统服务rsyslog统一记录/管理
–日志消息采用文本格式
–主要记录事件发生的时间、主机、进程、内容

•常见的日志文件       
日志文件	主要用途
/var/log/messages	记录内核消息、各种服务的公共消息
/var/log/dmesg	记录系统启动过程的各种消息
/var/log/cron	记录与cron计划任务相关的消息
/var/log/maillog	记录邮件收发相关的消息
/var/log/secure	记录与访问限制相关的安全消息


•通用分析工具
–tail、tailf、less、grep等文本浏览/检索命令
–awk、sed等格式化过滤工具
tailf：实时跟踪
[root@svr7 /]# echo 123456  >  /opt/1.txt
[root@svr7 /]# tailf  /opt/1.txt


•users、who、w 命令
–查看已登录的用户信息，详细度不同

•last、lastb 命令
–查看最近登录成功/失败的用户信息
  
[root@svr7 /]# users
[root@svr7 /]# who
 pts：图形命令行终端

[root@svr7 /]# last          #登录成功的用户
[root@svr7 /]# lastb        #登录失败的用户



•Linux内核定义的事件紧急程度
–分为 0~7 共8种优先级别
–其数值越小，表示对应事件越紧急/重要


------------------------------------------------------------------------------------
10.19

•SELinux的运行模式
getenforce 
–enforcing（强制）、permissive（宽松）
–disabled（彻底禁用）
  任何模式变成disabled模式，都要经历重启系统

•切换运行模式
–临时切换：setenforce  1或0
–固定配置：/etc/selinux/config 文件

ftp连接测试链接失败
ftp拒绝链接 没有启动软件
没有到主机的路由 防火墙给拦截


防火墙的策略管理
作用：隔离，严格过滤入站，放行出站
硬件防火墙
软件防火墙
•系统服务：firewalld
•管理工具：firewall-cmd、firewall-config

•根据所在的网络场所区分，预设保护规则集
–public：仅允许访问本机的ssh、dhcp、ping服务
–trusted：允许任何访问
–block：拒绝任何来访请求，明确拒绝客户端
–drop：丢弃任何来访的数据包，不给任何回应

•防火墙判定原则：
1.查看客户端请求中来源IP地址，查看自己所有区域中规则，那个区域中有该源IP地址规则，则进入该区域
2.进入默认区域（默认情况下为public）

手动方式
/usr/sbin/httpd 
killall httpd

systemd方式:上帝进程
用户-----》systemd----》配置文件----》运行httpd


二、常用的网络工具
ip命令
1.查看IP地址
]# ip  address  show
]# ip  a  s
2.临时添加IP地址
]# ip address add 192.168.10.1/24  dev  eth0
3.临时删除IP地址
]# ip address del 192.168.10.1/24  dev eth0

ping 命令，测网络连接
–选项 -c 包个数
[root@svr7 ~]# ping -c  2  192.168.4.207

十三、日志管理
•系统和程序的“日记本”
–记录系统、程序运行中发生的各种事件
–通过查看日志，了解及排除故障
–信息安全控制的“依据”

•由系统服务rsyslog统一记录/管理
–日志消息采用文本格式
–主要记录事件发生的时间、主机、进程、内容

•常见的日志文件       
日志文件	主要用途
/var/log/messages	记录内核消息、各种服务的公共消息
/var/log/dmesg	记录系统启动过程的各种消息
/var/log/cron	记录与cron计划任务相关的消息
/var/log/maillog	记录邮件收发相关的消息
/var/log/secure	记录与访问限制相关的安全消息


•通用分析工具
–tail、tailf、less、grep等文本浏览/检索命令
–awk、sed等格式化过滤工具
tailf：实时跟踪
[root@svr7 /]# echo 123456  >  /opt/1.txt
[root@svr7 /]# tailf  /opt/1.txt

•users、who、w 命令
–查看已登录的用户信息，详细度不同

•last、lastb 命令
–查看最近登录成功/失败的用户信息
  
[root@svr7 /]# users     
[root@svr7 /]# who
 pts：图形命令行终端

[root@svr7 /]# last  -2        #登录成功的用户（2条）
[root@svr7 /]# lastb   -2     #登录失败的用户（2条）


•Linux内核定义的事件紧急程度
–分为 0~7 共8种优先级别
–其数值越小，表示对应事件越紧急/重要

SELinux概述
•Security-Enhanced Linux
–美国NSA国家安全局主导开发，一套增强Linux系统安全的强制访问控制体系
–集成到Linux内核（2.6及以上）中运行
–RHEL7基于SELinux体系针对用户、进程、目录和文件提供了预设的保护策略，以及管理工具

•SELinux的运行模式
–enforcing（强制）、permissive（宽松）
–disabled（彻底禁用）
  任何模式变成disabled模式，都要经历重启系统

•切换运行模式
–临时切换：setenforce  1或0
–固定配置：/etc/selinux/config 文件

[root@svr7 /]# getenforce     #查看当前运行模式
[root@svr7 /]# setenforce  0  #修改当前运行模式
[root@svr7 /]# vim  /etc/selinux/config  
SELINUX=permissive

六、系统故障修复
如果/etc/fstab文件有误：修复办法 
1.输入root的密码
2.修改/etc/fstab文件内容
破解root密码思路
前提：必须是服务器的管理者，涉及重启服务器
1)重启系统,进入救援模式
[root@A ~]# reboot 
开启虚拟机A，在此界面按e键
找到linux16该行
在linux16该行的最后，空格输入 rd.break 
按 ctrl + x 启动，会看到switch_root:/#
2)以可写方式重新挂载 /sysroot,并切换到此环境
# mount   -o   remount,rw   /sysroot    #可以读也可以写入
# chroot    /sysroot      #切换环境，切换到硬盘操作系统的环境
3)重新设置root的密码 
sh-4.2# echo   1    |    passwd   --stdin    root
4)如果SELinux是强制模式，才需要重设SELinux策略（其他模式不需要做此操作）
sh-4.2# vim /etc/selinux/config #查看SELinux开机的运行模式
sh-4.2# touch   /.autorelabel    #让SELinux失忆
5)强制重启系统完成修复        
sh-4.2# reboot   -f 



七、构建基本服务
构建Web服务
Web服务：提供一个页面内容的服务
提供Web服务的软件：httpd、Nginx、tomcat
http协议：超文本传输协议

daemon
英 [ˈdiːmən]   美 [ˈdiːmən]  
n.(古希腊神话中的)半神半人精灵（守护神）

1.安装软件包
[root@svr7 ~]# yum  -y  install  httpd
2.运行提供Web服务程序     
]#  > /etc/resolv.conf     #清空此文件内容
]# /usr/sbin/httpd          #绝对路径运行程序
]# pgrep  -l  httpd           #查看进程信息
4.书写一个页面文件         
[root@svr7 ~]# vim /var/www/html/index.html
hahaxixi NSD2109 Web
5.浏览器访问测试
 [root@svr7 ~]# curl   http://192.168.4.7
hahaxixi NSD2109 Web

构建FTP服务                 
FTP：文本传输协议
实现FTP服务功能的软件：vsftpd
   默认共享数据的主目录：/var/ftp/
虚拟机A
1.安装软件包
[root@svr7 ~]# yum  -y  install  vsftpd
[root@svr7 ~]# rpm  -q  vsftpd
2.运行程序
[root@svr7 ~]# /usr/sbin/vsftpd
3.访问测试
[root@svr7 ~]# curl  ftp://192.168.4.7


八、防火墙的策略管理
作用：隔离，严格过滤入站，放行出站
硬件防火墙
软件防火墙
•系统服务：firewalld
•管理工具：firewall-cmd、firewall-config

•根据所在的网络场所区分，预设保护规则集
–public：仅允许访问本机的ssh、dhcp、ping服务
–trusted：允许任何访问
–block：拒绝任何来访请求，明确拒绝客户端
–drop：丢弃任何来访的数据包，不给任何回应

•防火墙判定原则：
1.查看客户端请求中来源IP地址，查看自己所有区域中规则，那个区域中有该源IP地址规则，则进入该区域

2.进入默认区域（默认情况下为public）

防火墙默认区域的修改

]# firewall-cmd  --get-default-zone    #查看默认区域

虚拟机A：修改默认区域
]# firewall-cmd  --set-default-zone=trusted

防火墙public区域添加规则
虚拟机A：添加允许的协议
]# firewall-cmd  --set-default-zone=public
]# firewall-cmd  --zone=public    --add-service=http   
]# firewall-cmd  --zone=public   --list-all

虚拟机A：添加允许的协议
]# firewall-cmd  --zone=public    --add-service=ftp
]# firewall-cmd  --zone=public  --list-all

防火墙public区域添加规则（永久）
-永久（--permanent）
]# firewall-cmd  --reload  #加载防火墙永久策略
]# firewall-cmd  --permanent    --zone=public --add-service=http
]# firewall-cmd  --permanent    --zone=public --add-service=ftp
]# firewall-cmd  --reload  #加载防火墙永久策略
]# firewall-cmd  --zone=public   --list-all

防火墙public区域删除规则（永久）
-永久（--permanent）   
]# firewall-cmd  --permanent    --zone=public  --remove-service=http
]# firewall-cmd  --reload  #加载防火墙永久策略


防火墙在区域中添加IP地址策略（了解）
单独拒绝192.168.4.207所有访问
[root@svr7 ~]# firewall-cmd --zone=block          --add-source=192.168.4.207
[root@svr7 ~]# firewall-cmd --zone=block --remove-source=192.168.4.207

手动方式：
/usr/sbin/httpd       killall  httpd

systemd方式：上帝进程
用户---》systemd--》配置文件--》运行httpd
用户---》systemd--》配置文件--》killall  http

九、服务的管理
用户---》systemd---》服务

•上帝进程：systemd
•Linux系统和服务管理器
–是内核引导之后加载的第一个初始化进程（PID=1）
–负责掌控整个Linux的运行/服务资源组合

•一个更高效的系统&服务管理器
–开机服务并行启动，各系统服务间的精确依赖
–配置目录：/etc/systemd/system/
–服务目录：/lib/systemd/system/
–主要管理工具：systemctl

[root@svr7 ~]# systemctl -t service --all #列出所有的服务

•对于服务的管理（与手动启动有冲突）
systemctl restart    服务名    #重起服务
systemctl start     服务名    #开启服务 
systemctl stop    服务名      #停止服务
systemctl status   服务名      #查看服务当前的状态

systemctl enable   服务名    #设置服务开机自启动
systemctl  disable   服务名   #设置服务禁止开机自启动
systemctl  is-enabled 服务名   #查看服务是否开机自启



]# > /etc/resolv.conf    #清除文件内容
]# killall httpd       #杀死手动启动的httpd
]# systemctl  restart  httpd   #重启httpd服务
]# systemctl  status   httpd    #查看服务httpd状态
]# systemctl  enable  httpd   #设置httpd开机自启动
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
]# systemctl  is-enabled  httpd #查看httpd是否是开机自启动
]# systemctl disable  httpd   #关闭httpd开机自启动
Removed symlink /etc/systemd/system/multi-user.target.wants/httpd.service.
]# systemctl  is-enabled  httpd  #查看httpd是否是开机自启动

管理运行级别
RHEL6:运行级别    300
0：关机   0个服务
1：单用户模式（基本功能的实现，破解Linux密码） 50个服务
2：多用户字符界面（不支持网络） 80个服务
3：多用户字符界面（支持网络）服务器默认运行级别 100个服务
4：未定义 0个服务
5：图形界面     300个服务
6：重起 0个服务
切换运行级别：init   数字 

RHEL7：运行模式（运行级别） 
字符模式：multi-user.target
图形模式：graphical.target

当前直接切换到字符模式 
]# systemctl isolate multi-user.target     #相当于原来的init 3
当前直接切换到图形模式
]# systemctl isolate graphical.target      #相当于原来的init 5

查看每次开机默认进入模式
[root@svr7 /]# systemctl get-default
设置永久策略，每次开机自动进入multi-user.target 
[root@svr7 /]# systemctl  set-default   multi-user.target 
[root@svr7 /]# reboot 


------------------------------------------------------------------------------------
10.20



Listen：监听IP地址: 监听端口（80）
端口:数字编号起到标识作用，标识协议
  http协议默认端口：80
建议自定义端口时大于1024，端口的极限65535
[root@svr7 ~]# vim  /etc/httpd/conf/httpd.conf
…….此处省略一万字
Listen  80
Listen  8000

•虚拟Web主机
–由同一台服务器提供多个不同的Web站点
•区分方式
–基于域名的虚拟主机
–基于端口的虚拟主机
–基于IP地址的虚拟主机

使用虚拟Web主机后，在–/etc/httpd/conf/httpd.conf主配置文件内的所有DocumenRoot都失效，默认只显示虚拟站点配置文件内的第一个
•配置文件路径
–/etc/httpd/conf/httpd.conf  #主配置文件
–/etc/httpd/conf.d/*.conf   #调用配置文件

•为每个虚拟站点添加配置
<VirtualHost   IP地址:端口>
       ServerName  此站点的DNS名称
       DocumentRoot  此站点的网页根目录
</VirtualHost>


/etc/hosts可以充当域名解析
必须要手写；只为本机提供域名解析
错误解答
构建Web服务器显示测试页面的原因：
1.自己没有书写任何的网页文件
2.网页文件名称有误
3.Web服务独有的访问控制，拒绝此次访问

访问Web服务常见失败情况：
1.出现“拒绝连接”  原因:httpd服务没有启动成功
2.出现“没有到主机的路由” 原因:防火墙开启
3.出现“未知的错误”   原因:没有域名解析(利用/etc/hosts文件进行域名解析)



一、环境准备
开启虚拟机A与虚拟机B

两台机器设置SELinux运行模式
[root@svr7 ~]# getenforce
Enforcing
[root@svr7 ~]# setenforce 0
[root@svr7 ~]# getenforce
Permissive
[root@svr7 ~]# vim   /etc/selinux/config
SELINUX=permissive
两台机器设置防火墙（停止防火墙服务）
[root@svr7 ~]# systemctl  stop  firewalld
[root@svr7 ~]# systemctl  disable  firewalld


一、服务的管理
用户---》systemd---》服务

•上帝进程：systemd
•Linux系统和服务管理器
–是内核引导之后加载的第一个初始化进程（PID=1）
–负责掌控整个Linux的运行/服务资源组合

•一个更高效的系统&服务管理器
–开机服务并行启动，各系统服务间的精确依赖
–配置目录：/etc/systemd/system/
–服务目录：/lib/systemd/system/
–主要管理工具：systemctl


[root@svr7 ~]# systemctl -t service --all #列出所有的服务

•对于服务的管理（与手动启动有冲突）
systemctl restart    服务名    #重起服务
systemctl start     服务名    #开启服务 
systemctl stop    服务名      #停止服务
systemctl status   服务名      #查看服务当前的状态

systemctl enable   服务名    #设置服务开机自启动
systemctl  disable   服务名   #设置服务禁止开机自启动
systemctl  is-enabled 服务名   #查看服务是否开机自启





]# yum -y install httpd
]# > /etc/resolv.conf    #清除文件内容
]# killall httpd       #杀死手动启动的httpd
]# systemctl  restart  httpd    #重启httpd服务
]# systemctl  status  httpd    #查看服务httpd状态
]# systemctl  enable  httpd   #设置httpd开机自启动
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
]# systemctl  is-enabled  httpd  #查看httpd是否是开机自启动
]# systemctl disable  httpd   #关闭httpd开机自启动
Removed symlink /etc/systemd/system/multi-user.target.wants/httpd.service.
]# systemctl  is-enabled  httpd  #查看httpd是否是开机自启动


管理运行级别
RHEL6:运行级别 300
0：关机 0个服务
1：单用户模式（基本功能的实现，破解Linux密码） 50个服务
2：多用户字符界面（不支持网络） 80个服务
3：多用户字符界面（支持网络）服务器默认运行级别 100个服务
4：未定义 0个服务
5：图形界面     300个服务
6：重起 0个服务
切换运行级别：init   数字 


RHEL7：运行模式（运行级别） 
字符模式：multi-user.target
图形模式：graphical.target

当前直接切换到字符模式 
]# systemctl isolate multi-user.target    #相当于原来的init 3
当前直接切换到图形模式
]# systemctl isolate graphical.target    #相当于原来的init 5

查看每次开机默认进入模式
[root@svr7 /]# systemctl get-default
设置永久策略，每次开机自动进入multi-user.target 
[root@svr7 /]# systemctl  set-default   multi-user.target 
[root@svr7 /]# reboot 


构建Web服务器显示测试页面的原因
1自己没有书写任何的网页文件
2网页文件名名称有误
3Web服务独有的访问控制，拒绝此次访问
Web服务独有的访问控制:针对存放网页文件目录
子目录自动继承，父目录访问控制规则
除非对子目录有单独的配置


/               #拒绝所有人
/var/www/   #允许所有人
总结：只有网页文件放在/var/www/才能访问
<Directory    />
    Require all denied     #拒绝所有人访问
</Directory>
<Directory    "/var/www">
    Require all granted      #允许所有人访问
</Directory>





二、Web服务器简介
•基于 B/S （Browser/Server）架构的网页服务
–服务端提供网页
–浏览器下载并显示网页
•Hyper Text Markup Language，超文本标记语言
•Hyper Text Transfer  Protocol，超文本传输协议
三步骤策略：装包、配置、启服务
实现Web功能软件：httpd、Nginx、Tomcat
httpd由软件基金会Apache
•提供的默认配置
主配置文件：/etc/httpd/conf/httpd.conf
–Listen：监听地址:端口（80）
–ServerName：本站点注册的DNS名称（空缺）
–DocumentRoot：网页根目录（/var/www/html）
–DirectoryIndex：起始页/首页文件名（index.html）

]# > /etc/resolv.conf          #防止寻找DNS服务器

主配置文件：/etc/httpd/conf/httpd.conf
DocumentRoot：网页文件根目录（/var/www/html）
]# vim    /etc/httpd/conf/httpd.conf
…….此处省略一万字
DocumentRoot    "/var/www/myweb"
…….此处省略一万字

基于网页文件目录，进行访问控制
当子目录没有规则，默认继承上一级目录规则
针对此目录有单独配置，则不继承上一级目录规则

<Directory    />
    Require all denied     #拒绝所有人访问
</Directory>

<Directory    "/var/www">
    Require all granted      #允许所有人访问
</Directory>

/webroot/abc/test    #拒绝所有人访问

/var/www/myweb    #允许所有人访问


虚拟机A：                      
]# mkdir   /webroot
]# echo wo shi webroot  >  /webroot/index.html
]# vim   /etc/httpd/conf/httpd.conf
…….此处省略一万字
DocumentRoot    "/webroot"
<Directory   "/webroot"> #针对于/webroot路径
    Require all granted       #允许所有人访问
</Directory>
…
….此处省略一万字
]# systemctl restart httpd    #重启服务
]# curl http://192.168.4.7       
 wo shi webroot


网络路径与实际路径

DocumentRoot ---》/webroot

访问过程：客户端curl   http://192.168.4.7--->服务端192.168.4.7---》http协议-》httpd---》/etc/httpd/conf/httpd.conf---》DocumentRoot ---》/webroot---》index.html

网络路径：curl   http://192.168.4.7
实际服务：/webroot

网络路径：curl   http://192.168.4.7/abc
实际服务： /webroot/abc/index.html

DocumentRoot ---》/webroot
网络路径：curl   http://192.168.4.7/webroot/abc
实际服务：/webroot/webroot/abc/index.html

Listen：监听IP地址: 监听端口（80）
端口:数字编号起到标识作用，标识协议
http协议默认端口：80

建议自定义端口时大于1024，端口的极限65535

[root@svr7 ~]# vim  /etc/httpd/conf/httpd.conf
…….此处省略一万字
Listen  80
Listen  8000
…….此处省略一万字
[root@svr7 ~]# systemctl   restart httpd


三、虚拟Web主机

•虚拟Web主机
–由同一台服务器提供多个不同的Web站点
•区分方式
–基于域名的虚拟主机
–基于端口的虚拟主机
–基于IP地址的虚拟主机

•配置文件路径
–/etc/httpd/conf/httpd.conf  #主配置文件
–/etc/httpd/conf.d/*.conf   #调用配置文件

•为每个虚拟站点添加配置
<VirtualHost   IP地址:端口>
       ServerName  此站点的DNS名称
       DocumentRoot  此站点的网页根目录
</VirtualHost>

虚拟机A：
[root@svr7 ~]# vim  /etc/httpd/conf.d/haha.conf
<VirtualHost    *:80>          #在所有IP地址监听80
   ServerName   www.qq.com        #网站的域名
   DocumentRoot   /var/www/qq   #网页文件路径
</VirtualHost>

采用/etc/hosts文件直接解析域名，只为本机解析
]# vim   /etc/hosts
…….此处省略一万字
192.168.4.7   www.qq.com    www.lol.com
]# curl  http://www.qq.com
一旦使用虚拟Web主机功能，所有的网站都必须使用虚拟Web进行呈现


四、NFS服务基础
•Network File System，网络文件系统
–用途：为客户机提供共享使用的文件夹
–协议：NFS（ 2049）、RPC（ 111）

•所需软件包：nfs-utils
•系统服务：nfs-server

虚拟机A：服务端
[root@svr7 ~]# rpm  -q  nfs-utils
nfs-utils-1.3.0-0.54.el7.x86_64

[root@svr7 ~]# mkdir   /public     #创建共享目录
[root@svr7 ~]# echo  haha  >  /public/h.txt
[root@svr7 ~]# echo  xixi  >  /public/x.txt
[root@svr7 ~]# ls  /public/

[root@svr7 ~]# vim   /etc/exports
文件夹路径      客户机地址(权限)  
/public     *(ro)        #允许所有客户端进行只读访问
[root@svr7 ~]# systemctl  restart  nfs-server

虚拟机B：客户端
[root@pc207 ~]# rpm  -q  nfs-utils
nfs-utils-1.3.0-0.54.el7.x86_64
[root@pc207 ~]# mkdir /mnt/mynfs

]# mount    192.168.4.7:/public     /mnt/mynfs
]# ls    /mnt/mynfs
]# df   -h    #查看正在挂载的设备信息

实现开机自动挂载
_netdev：声明网络设备，系统在具备网络参数后，再进行挂载本设备
[root@pc207 ~]# vim    /etc/fstab
……此处省略一万字
192.168.4.7:/public   /mnt/mynfs    nfs defaults,_netdev   0   0

[root@pc207 ~]# umount   /mnt/mynfs/
[root@pc207 ~]# ls   /mnt/mynfs/
[root@pc207 ~]# mount   -a
[root@pc207 ~]# ls   /mnt/mynfs/



五、触发挂载（了解内容）

•由 autofs 服务提供的“按需访问”机制
–只要访问挂载点就会触发响应，自动挂载指定设备
–闲置超过时限（默认5分钟）后，会自动卸载

[root@pc207 ~]# yum  -y  install  autofs
[root@pc207 ~]# systemctl start autofs
[root@pc207 ~]# ls  /misc
[root@pc207 ~]# ls  /misc/cd   


触发挂载实现，必须多级的目录结构： /监控目录/挂载点目录

•主配置文件 /etc/auto.master
–监控点目录  	挂载配置文件的路径

•挂载配置文件，比如 /etc/auto.misc
–触发点子目录  	  -挂载参数    		:设备名

案例：虚拟机B访问/myauto/nsd,光驱设备挂载/ myauto/nsd
[root@pc207 ~]# yum -y install autofs
[root@pc207 ~]# mkdir /myauto   #创建监控目录
[root@pc207 ~]# ls /myauto
[root@pc207 ~]# vim   /etc/auto.master
……此处省略一万字 
/myauto      /opt/xixi.txt
……此处省略一万字
[root@pc207 ~]# cp  /etc/auto.misc   /opt/xixi.txt
[root@pc207 ~]# vim  /opt/xixi.txt
nsd    -fstype=iso9660     :/dev/cdrom

[root@pc207 ~]# systemctl  restart   autofs
[root@pc207 ~]# ls   /myauto/
[root@pc207 ~]# ls   /myauto/nsd


触发挂载进阶autofs与NFS
虚拟机B访问/myauto/nfs,虚拟机A 的nfs共享/public挂载到/myauto/nfs

[root@pc207 ~]# yum -y install autofs
[root@pc207 ~]# mkdir /myauto   #创建监控目录
[root@pc207 ~]# ls /myauto
[root@pc207 ~]# vim   /etc/auto.master
……此处省略一万字 
/myauto      /opt/xixi.txt
……此处省略一万字 
[root@pc207 ~]# vim   /opt/xixi.txt
nsd    -fstype=iso9660     :/dev/cdrom
nfs    -fstype=nfs         192.168.4.7:/public
[root@pc207 ~]# systemctl  restart  autofs
[root@pc207 ~]# ls  /myauto/nfs



----------------------------------------------------------------------------------
10.21

二、DNS服务
为什么需要DNS系统
www.baidu.com 与 119.75.217.56，哪个更好记？
互联网中的114查号台/导航员
DNS服务器的功能
正向解析：根据注册的域名查找其对应的IP地址
反向解析：根据IP地址查找对应的注册域名，不常用
   DNS服务器分类：
      根域名服务器、一级DNS服务器、二级DNS服务器、三级DNS服务器

   域名系统: 所有的域名都必须要以点作为结尾，树型结构
         www.qq.com      www.qq.com.
根域名 :                          .

 一级域名：   .cn        .us     .tw     .hk     .jp      .kr    ……….

二级域名:     .com.cn    .org.cn   .net.cn ………

三级域名：   haha.com.cn    xixi.com.cn    .nb.com.cn  …..

FQDN（完全合格的域名）：站点名+注册的域名
     vip.nb.com


BIND（Berkeley Internet Name Daemon）
–伯克利 Internet 域名服务 
–官方站点：https://www.isc.org/  

•BIND服务器端程序
–主要执行程序：/usr/sbin/named
–系统服务：named
–DNS协议默认端口：TCP/UDP 53
–运行时的虚拟根环境：/var/named/chroot/

•主配置文件：/etc/named.conf    #设置负责解析的域名 
•地址库文件：/var/named/     #完全合格的域名与IP地址对应关系




虚拟机A：构建DNS服务器
1.安装软件包
[root@svr7 ~]# yum  -y  install   bind    bind-chroot
bind（主程序）
bind-chroot（提供牢笼政策）



2.修改主配置文件
[root@svr7 ~]# cp  /etc/named.conf    /root    #备份数据
[root@svr7 ~]# vim   /etc/named.conf
options {
        directory     "/var/named";     #定义地址库文件存放路径
};
zone "tedu.cn"  IN {           #定义负责的解析tedu.cn域名
        type  master;               #权威主DNS服务器
        file   "tedu.cn.zone";     #地址库文件名称
};
 

3.建立地址库文件
  保证named用户对地址库文件有读取权限
 所有的域名都要以点作为结尾
  如果没有以点作为结尾，那么默认补全本地库文件负责的域名
]# cd     /var/named/
]# cp  -p   named.localhost    tedu.cn.zone    #保持权限不变
]# ls   -l   tedu.cn.zone

]# vim     tedu.cn.zone
……此处省略一万字
tedu.cn.      NS      svr7              #声明DNS服务器为svr7
svr7              A       192.168.4.7#svr7解析结果为192.168.4.7
www             A        1.1.1.1
ftp                 A        2.2.2.2

[root@svr7 named]# systemctl   restart    named

 


虚拟机B：测试DNS服务器
]# echo  nameserver   192.168.4.7  >  /etc/resolv.conf
]# cat     /etc/resolv.conf
]# nslookup    www.tedu.cn  
]# nslookup    ftp.tedu.cn  


三、多区域的DNS服务器
虚拟机A：
[root@svr7 /]# vim   /etc/named.conf
options {
        directory       "/var/named";
};
zone "tedu.cn"  IN  {
        type   master;
        file  "tedu.cn.zone";
};
zone  "lol.com"  IN  {
         type  master;
         file  "lol.com.zone";
};

[root@svr7 /]# cd   /var/named/     
[root@svr7 named]# cp  -p   tedu.cn.zone    lol.com.zone
[root@svr7 named]# vim     lol.com.zone
……此处省略一万字
lol.com.    NS    svr7
svr7          A      192.168.4.7
www         A       4.4.4.4
vip            A       5.5.5.5

[root@svr7 named]# systemctl  restart named

虚拟机B
[root@pc207 ~]# nslookup   www.lol.com
 


六、特殊解析
DNS的轮询(负载均衡)
[root@svr7 /]# vim  /var/named/tedu.cn.zone
……此处省略一万字
tedu.cn.   NS   svr7
svr7          A    192.168.4.7
www         A    192.168.4.20
www        A     192.168.4.21
www        A     192.168.4.22
ftp           A     2.2.2.2
[root@svr7 /]# systemctl  restart named

DNS的泛域名解析
[root@svr7 /]# vim    /var/named/tedu.cn.zone
……此处省略一万字
tedu.cn.    NS   svr7
svr7           A     192.168.4.7
www          A     192.168.4.20
ftp             A      2.2.2.2
*                A      6.6.6.6
tedu.cn.    A      7.7.7.7
[root@svr7 /]# systemctl   restart  named
虚拟机B测试：
[root@pc207 /]#  nslookup    wwwwww.tedu.cn
[root@pc207 /]#  nslookup    tedu.cn

有规律的DNS解析记录
pc1.tedu.cn ---->192.168.20.1
pc2.tedu.cn ---->192.168.20.2
pc3.tedu.cn ---->192.168.20.3
pc4.tedu.cn ---->192.168.20.4
    ………
pc50.tedu.cn ---->192.168.20.50

内置函数：$GENERATE   造数工具，制造连续的数字

[root@svr7 /]# vim /var/named/tedu.cn.zone
……此处省略一万字
$GENERATE  1-50       pc$          A     192.168.20.$
[root@svr7 /]# systemctl  restart named

[root@svr7 /]# nslookup  pc33.tedu.cn


DNS的解析记录的别名
[root@svr7 /]# vim   /var/named/tedu.cn.zone
……此处省略一万字
tedu.cn.   NS   svr7
svr7          A     192.168.4.7
www         A     192.168.4.20
ftp           A      2.2.2.2
*               A      6.6.6.6
tedu.cn.   A      7.7.7.7
nsd       CNAME    ftp         #nsd解析结果与ftp解析结果一致
[root@svr7 /]# systemctl   restart    named
虚拟机B：测试
[root@pc207 ~]# nslookup    nsd.tedu.cn



DNS服务器资源解析记录的类型：    
      NS:DNS服务器声明记录
      A：正向解析记录
      CNAME：解析记录的别名 



虚拟机B练习搭建DNS服务器
要求：www.sina.com解析结果为192.168.4.100
虚拟机B：构建负责sina.com的DNS服务器
[root@pc207 ~]# yum -y install  bind  bind-chroot
[root@pc207 ~]# vim   /etc/named.conf
options {
        directory       "/var/named";
};
zone "sina.com" IN {
        type master;
        file "sina.com.zone";
};
]# cd   /var/named
]# cp -p   named.localhost    sina.com.zone
]# vim sina.com.zone
sina.com.       NS       pc207
pc207             A         192.168.4.207
www               A         192.168.4.100
]# systemctl   restart   named
]# echo nameserver 192.168.4.207 > /etc/resolv.conf
]# nslookup www.sina.com  














##########################################

七、递归查询（递归解析）与迭代查询  （迭代解析）   

递归查询：客户端发送请求给首选DNS服务器，首选DNS服务器与其他的DNS服务器交互，最终将解析结果带回来过程

迭代查询: 客户端发送请求给首选DNS服务器，首选DNS服务器告知下一个DNS服务器地址


虚拟机B：构建DNS服务器负责bj.tedu.cn
[root@pc207 ~]# yum -y install  bind  bind-chroot
options {
        directory       "/var/named";
};
zone "bj.tedu.cn" IN {
        type master;
        file "bj.tedu.cn.zone";
};
]# cd /var/named/
]# cp -p named.localhost     bj.tedu.cn.zone
]# vim bj.tedu.cn.zone
……此处省略一万字
bj.tedu.cn.     NS   pc207
pc207           A    192.168.4.207
www             A     9.9.9.9
]# systemctl restart named

虚拟机A:子域授权
[root@svr7 /]# vim /var/named/tedu.cn.zone
tedu.cn.      NS    svr7
bj.tedu.cn.  NS    pc207
svr7            A      192.168.4.7
pc207         A      192.168.4.207
www           A      192.168.4.20
ftp               A      2.2.2.2
*                  A       4.4.4.4
tedu.cn.       A       5.5.5.5
vip              CNAME  ftp
[root@svr7 /]# systemctl restart named
虚拟机B：测试
[root@pc207 /]# nslookup   www.bj.tedu.cn      192.168.4.7
Server:         192.168.4.7
Address:        192.168.4.7#53

Non-authoritative answer:    #非权威解答
Name:   www.bj.tedu.cn
Address: 9.9.9.9




虚拟机A:禁止递归查询
[root@svr7 /]# vim /etc/named.conf
options {
        directory       "/var/named";
        recursion no;    #禁止递归查询
};
zone "tedu.cn" IN {
        type master;
        file "tedu.cn.zone";
};
zone "lol.com" IN {
        type master;
        file "lol.com.zone";
};
[root@svr7 /]# systemctl restart named

[root@pc207 /]# dig  @192.168.4.7   www.bj.tedu.cn   #专业域名解析的工具
bj.tedu.cn.             86400   IN      NS      pc207.tedu.cn.

;; ADDITIONAL SECTION:
pc207.tedu.cn.          86400   IN      A       192.168.4.207




八、DNS主从架构
 作用：提高可靠性，从DNS服务器备份主DNS服务器的数据

虚拟机A：主DNS服务器，以lol.com域名
虚拟机B：从DNS服务器，以lol.com域名

虚拟机A：主DNS服务器     
1.授权从DNS服务器
[root@svr7 /]# man  named.conf         #参考man帮助
[root@svr7 /]# vim   /etc/named.conf
options {
     directory       "/var/named";
     allow-transfer  {  192.168.4.207;   };    #允许谁进行传输数据
};
zone "tedu.cn" IN {
        type master;
        file "tedu.cn.zone";
};
zone "lol.com" IN {
        type master;
        file "lol.com.zone";
};
2.声明从DNS服务器
[root@svr7 /]# vim     /var/named/lol.com.zone
……此处省略一万字
lol.com.   NS   svr7
lol.com.   NS   pc207            #声明从DNS服务器
svr7         A     192.168.4.7
pc207      A     192.168.4.207
www        A     4.4.4.4
[root@svr7 /]# systemctl    restart    named


虚拟机B：从DNS服务器
1.安装软件包
[root@pc207 /]# yum  -y   install    bind    bind-chroot
2.修改主配置文件
[root@pc207 /]# vim   /etc/named.conf
options  {
        directory       "/var/named";
};
zone  "lol.com"  IN  {
  type   slave;               #类型为从服务器
  file "/var/named/slaves/lol.com.slave";  #确保named用户有读写执行权限
#可以放到任意位置，但是需要改rwx权限；地址库文件也可以写相对路径，例如file "/slaves/lol.com.slave";，因为options已经定义默认的地址 directory"/var/named";
  masters     {   192.168.4.7;    };   #指定主DNS服务器
  masterfile-format    text;        #地址库文件明文存储
};
[root@pc207 /]# ls   /var/named/slaves/                    [root@pc207 /]# systemctl    restart   named
[root@pc207 /]# ls    /var/named/slaves/
lol.com.slave

[root@pc207 /]# vim  /etc/resolv.conf
nameserver 192.168.4.7
nameserver 192.168.4.207
[root@pc207 /]# nslookup   www.lol.com


九、DNS主从数据同步
虚拟机A：
[root@svr7 /]# vim    /var/named/lol.com.zone
…….此处省略一万字
     2020111801    ; serial  #数据的版本号，由10个数字组成
           1D      ; refresh    #每隔1天主从进行数据交互
           1H      ; retry        #失效之后的时间间隔每一个1小时
           1W      ; expire     #真正的失效时间，1周
           3H )    ; minimum  #失效记录的记忆时间3小时
lol.com.    NS   svr7
lol.com.    NS   pc207
svr7          A     192.168.4.7
pc207       A     192.168.4.207         
www         A     11.12.13.14
[root@svr7 /]# systemctl  restart  named
虚拟机B：
[root@pc207 /]# nslookup  www.lol.com  192.168.4.207



---------------------------------------------------------------------------------
10.22


缓存DNS服务器
缓存解析结果，加快解析
利用内存缓存优点是读取速度快，缺点重启清空缓存
options  {
       directory      "/var/named";
       forwarders   {   192.168.4.7;   };      #转发给192.168.4.7
};

四、分离解析:为客户端提供最近的服务器
•当收到客户机的DNS查询请求的时候
能够区分客户机的来源地址
为不同类别的客户机提供不同的解析结果（IP地址）

自定义Yum仓库
1众多的软件包 
2仓库的数据文件（仓库清单）


更新Yum软件包仓库信息
1删除仓库数据文件，重新生成仓库数据文件
2清空Yum缓存（yum clean all）

四、自定义Yum仓库:将自己下载的RPM包构建为仓库

• Yum仓库：软件包仓库
软件包仓库：1.众多的软件包   2.仓库数据文件（仓库清单）

•第一阶段 tools.tar.gz 
•由真机将tools.tar.gz传递到虚拟机A的/root目录下 

真机为Linux系统同学             
[root@localhost /]# ls /linux-soft/
[root@localhost /]# ls /linux-soft/1
[root@localhost /]# scp  /linux-soft/1/tools.tar.gz root@192.168.4.7:/root

真机为windows系统同学












[root@svr7 ~]# ls  /root
[root@svr7 ~]# tar -tf   /root/tools.tar.gz   #查看tar包内容
[root@svr7 ~]# tar -xf  /root/tools.tar.gz  -C   /
[root@svr7 ~]# ls   /
[root@svr7 ~]# ls   /tools/
[root@svr7 ~]# ls   /tools/other/

[root@svr7 ~]# createrepo /tools/other/ #生成仓库数据文件
[root@svr7 ~]# ls /tools/other/ 


[root@svr7 ~]# vim    /etc/yum.repos.d/mydvd.repo 
[mydvd]
name=centos7
baseurl=file:///mydvd
enabled=1
gpgcheck=0
[rpm]           #唯一标识 ,多个仓库仓库表示要唯一
name=myrpm
baseurl=file:///tools/other   #指定Yum仓库的路径
enabled=1
gpgcheck=0
[root@svr7 ~]# yum  repolist

[root@svr7 ~]# yum   -y   install    sl
[root@svr7 ~]# yum   -y   install    cmatrix
[root@svr7 ~]# yum   -y   install    oneko

• 更新Yum软件包仓库信息
1.删除仓库数据文件，重新生成仓库数据文件
2.清空Yum缓存（yum  clean  all）


---------------------------------------------------------------------------------
10.25

二、构建DHCP服务器                 
•Dynamic Host Configuration Protocol
动态主机配置协议，由 IETF（Internet 网络工程师任务小组）组织制定，用来简化主机地址分配管理

•主要分配以下入网参数
IP地址/子网掩码/广播地址
默认网关地址、DNS服务器地址

•DHCP地址分配的四次会话(以广播形式进行，先到先得)
DISCOVERY --> OFFER --> REQUEST -->ACK
•一个网络中只能有一台DHCP服务器

服务端基本概念
租期：允许客户机租用IP地址的时间期限，单位为秒。
作用域：分配给客户机的IP地址所在的网段
地址池：用来动态分配的IP地址的范围

三、网络装机服务器简介
•规模化：同时装配多台主机
•自动化：装系统、配置各种服务
•远程实现：不需要光盘、U盘等物理安装介质
•PXE，Pre-boot eXecution Environment
•预启动执行环境，在操作系统之前运行
•可用于远程安装
•工作模式
•PXE client 集成在网卡的启动芯片中
•当计算机引导时，从网卡芯片中把PXE client调入内存执行，获取PXE server配置、显示菜单，根据用户选择将远程引导程序下载到本机运行

开机启动项
1本机硬盘
2光驱设备
3U盘
4网络引导安装

•网络装机服务器:
DHCP服务，分配IP地址、定位引导程序
TFTP服务，提供引导程序下载
HTTP服务（或FTP/NFS），提供yum安装源

五、配置tftp服务，传输众多的引导文件
tftp:简单的文件传输协议   默认端口：69
##无论身份，只要能ping通，就可以获得全部
tftp默认共享的主目录:/var/lib/tftpboot




/var/lib/tftpboot/
##pxelinux.cfg文件夹是自建目录，里面存放/mydvd/isolinux/目录下的isolinux.cfg文件，拷贝并改名为default
initrd.img（驱动）
pxelinux.cfg（菜单文件存放目录）
vesamenu.c32（图形模块）
pxelinux.0（网卡引导文件）
splash.png（背景图片）
vmlinuz（内核）

初步检查：
1.dhcp的主配置文件
next-server 192.168.4.7；
filename “pxelinux.0”；
2.菜单文件路径与名字
/var/lib/tftpboot/pxelinux.cfg/default
3.集齐的6样内容
initrd.img  pxelinux.0  pxelinux.cfg  
splash.png  vesamenu.c32  vmlinuz
4.关闭防火墙与SElinux

总结思路:
1.dhcp服务---》IP地址、next-server、filename   "pxelinux.0"
2.tftp服务---》 "pxelinux.0"
3.pxelinux.0---》读取菜单文件/var/lib/tftpboot/pxelinux.cfg/default
4.default---》vesamenu.c32、读秒时间、vmlinuz、initrd.img、ftp://192.168.4.7/ks.cfg
5.ks.cfg应答文件---》语言、键盘类型、分区、安装方式url --url="ftp://192.168.4.7/centos"

在虚拟机B构建网络装机时，关闭虚拟机A的DHCP服务，避免冲突

二、构建DHCP服务器                 
•Dynamic Host Configuration Protocol
动态主机配置协议，由 IETF（Internet 网络工程师任务小组）组织制定，用来简化主机地址分配管理

•主要分配以下入网参数
IP地址/子网掩码/广播地址
默认网关地址、DNS服务器地址

•DHCP地址分配的四次会话(以广播形式进行，先到先得)
DISCOVERY --> OFFER --> REQUEST -->ACK
•一个网络中只能有一台DHCP服务器

1.安装软件包
[root@svr7 /]# yum  -y   install   dhcp
[root@svr7 /]# rpm  -q  dhcp
2.修改配置文件    
[root@svr7 /]# vim   /etc/dhcp/dhcpd.conf
末行模式下  :r   /usr/share/doc/dhcp*/dhcpd.conf.example
subnet  192.168.4.0   netmask   255.255.255.0  {  #分配网段
  range  192.168.4.100    192.168.4.200;     #分配IP地址范围
  option  domain-name-servers   8.8.8.8;  #分配DNS
  option  routers   192.168.4.254;    #分配的网关地址
  default-lease-time   600;
  max-lease-time  7200;       
}
[root@svr7 /]# systemctl   restart   dhcpd


开启计算机：开机启动项（开机时从什么地方寻找操作系统）
1.本机硬盘
2.光驱设备
3.U盘
4.网络引导安装







三、网络装机服务器简介

•规模化：同时装配多台主机
•自动化：装系统、配置各种服务
•远程实现：不需要光盘、U盘等物理安装介质
•PXE，Pre-boot eXecution Environment
•预启动执行环境，在操作系统之前运行
•可用于远程安装
•工作模式
•PXE client 集成在网卡的启动芯片中
•当计算机引导时，从网卡芯片中把PXE client调入内存执行，获取PXE server配置、显示菜单，根据用户选择将远程引导程序下载到本机运行

•网络装机服务器:
DHCP服务，分配IP地址、定位引导程序
TFTP服务，提供引导程序下载
HTTP服务（或FTP/NFS），提供yum安装源




四、配置DHCP服务      
[root@svr7 /]# vim   /etc/dhcp/dhcpd.conf
此处省略一万字……
  next-server    192.168.4.7;    #下一个服务器的IP地址
  filename    "pxelinux.0";     #指明网卡引导文件名称
}
[root@svr7 /]# systemctl   restart   dhcpd
pxelinux.0：网卡引导文件（网络装机说明书）
                  二进制文件，安装一个软件可以获得该文件



五、配置tftp服务，传输众多的引导文件
tftp:简单的文件传输协议   默认端口：69
       tftp默认共享的主目录:/var/lib/tftpboot
安装软件
[root@svr7 /]# yum   -y   install   tftp-server
[root@svr7 /]# systemctl  restart  tftp
部署pxelinux.0文件
]# yum   provides   */pxelinux.0   #查询哪个包产生该文件
]# yum -y install syslinux    #安装syslinux软件包
]# rpm -ql syslinux  |  grep  pxelinux.0 #查询软件包安装清单
]# cp  /usr/share/syslinux/pxelinux.0    /var/lib/tftpboot/
]# ls   /var/lib/tftpboot/
pxelinux.0  




总结思路:
1.DHCP服务: IP地址、next-server、filename  pxelinux.0
2.tftp服务: pxelinux.0
3. pxelinux.0： /var/lib/tftpboot/pxelinux.cfg/default(默认菜单文件)








部署菜单文件(将光盘中的菜单文件进行复制)
[root@svr7 /]# ls   /mydvd/isolinux/        #查看光盘内容
[root@svr7 /]# mkdir  /var/lib/tftpboot/pxelinux.cfg
[root@svr7 /]# ls  /var/lib/tftpboot/

[root@svr7 /]# cp  /mydvd/isolinux/isolinux.cfg     /var/lib/tftpboot/pxelinux.cfg/default

[root@svr7 /]# ls  /var/lib/tftpboot/pxelinux.cfg/



部署图形模块(vesamenu.c32)与背景图片（splash.png）
[root@svr7 /]# cp  /mydvd/isolinux/vesamenu.c32      /mydvd/isolinux/splash.png     /var/lib/tftpboot/

[root@svr7 /]# ls   /var/lib/tftpboot/
pxelinux.0    splash.png
pxelinux.cfg  vesamenu.c32




部署启动内核(vmlinuz)与驱动程序（initrd.img）
[root@svr7 /]# cp   /mydvd/isolinux/vmlinuz   /mydvd/isolinux/initrd.img   /var/lib/tftpboot/

[root@svr7 /]# ls    /var/lib/tftpboot/
initrd.img(驱动)  
pxelinux.cfg(菜单文件存放目录)  
vesamenu.c32(图形模块)
pxelinux.0(网卡引导文件)  
splash.png(背景图片)    
vmlinuz(内核)




修改菜单文件内容
]# vim      /var/lib/tftpboot/pxelinux.cfg/default
末行模式:set   nu开启行号功能
1 default  vesamenu.c32       #默认加载运行图形模块
2 timeout 600                       #读秒时间60秒，1/10秒
此处省略一万字……..
10 menu background  splash.png            #背景图片
11 menu title  NSD   PXE    Server       #菜单界面的标题
此处省略一万字……..
61 label  linux
 62   menu label  ^Install  CentOS 7    #界面显示内容
 63   menu  default           #读秒结束后默认的选项
 64   kernel  vmlinuz          #加载内核
 65   append  initrd=initrd.img    #加载驱动程序
以下全部删除                      


初步检查：
1.dhcp的主配置文件         
[root@svr7 /]# vim   /etc/dhcp/dhcpd.conf
  next-server   192.168.4.7;
  filename   "pxelinux.0";
}
2.菜单文件路径与名字
[root@svr7 /]# ls  /var/lib/tftpboot/pxelinux.cfg/
default
3.集齐6样内容
[root@svr7 /]# ls   /var/lib/tftpboot/
initrd.img  pxelinux.cfg  vesamenu.c32
pxelinux.0  splash.png    vmlinuz
[root@svr7 /]#
4.关闭防火墙与SElinux
[root@svr7 /]# setenforce 0
[root@svr7 /]# getenforce
[root@svr7 /]# systemctl  stop   firewalld
5.重启相关服务
[root@svr7 /]# systemctl restart dhcpd
[root@svr7 /]# systemctl restart tftp


















总结思路:
1.DHCP服务: IP地址、next-server、filename  pxelinux.0
2. tftp服务: pxelinux.0
3. pxelinux.0： /var/lib/tftpboot/pxelinux.cfg/default(默认菜单文件)
4. default: 图形模块、背景图片、内核、驱动程序…..








初步测试：KVM虚拟机
1.新建虚拟机







修改启动项





六、初步测试:VMware虚拟机
关闭VMware软件的DHCP服务




新建虚拟机，内存2G，网络类型选项vmnet1



菜单界面的显示










排错思路：                              
1.查看DHCP服务配置文件
      filename  "pxelinux.0";

2.查看/var/lib/tftpboot目录内容
[root@svr7 /]# ls  /var/lib/tftpboot/
initrd.img  pxelinux.cfg  vesamenu.c32
pxelinux.0  splash.png    vmlinuz

3.菜单文件的名称
[root@svr7 /]# ls  /var/lib/tftpboot/pxelinux.cfg
default


总结思路:
1.DHCP服务: IP地址、next-server、filename  pxelinux.0
2. tftp服务: pxelinux.0
3. pxelinux.0： /var/lib/tftpboot/pxelinux.cfg/default(默认菜单文件)
4. default: 图形模块、背景图片、内核、驱动程序…..



七、构建FTP服务，提供光盘内容
  FTP:文件传输协议    默认端口:21
  默认共享数据的主目录:/var/ftp
1.安装软件包
[root@svr7 /]# yum -y  install  vsftpd
[root@svr7 /]# systemctl  restart   vsftpd
2.建立挂载点
[root@svr7 /]# mkdir   /var/ftp/centos
[root@svr7 /]# mount   /dev/cdrom     /var/ftp/centos
mount: /dev/sr0 写保护，将以只读方式挂载
[root@svr7 /]# ls    /var/ftp/centos
3.测试
[root@svr7 /]# curl    ftp://192.168.4.7/centos/




八、实现无人值守安装，生成应答文件

安装system-config-kickstart图形的工具  
[root@svr7 /]# yum -y install  system-config-kickstart
[root@svr7 /]# system-config-kickstart    #运行
system-config-kickstart程序需要Yum仓库的支持才能显示软件包的选择，必须要求Yum仓库的标识为[development]
[root@svr7 /]# vim   /etc/yum.repos.d/mydvd.repo 
[development]
name=centos7
baseurl=file:///mydvd          
enabled=1
gpgcheck=0
[root@svr7 /]# system-config-kickstart 
首先查看“软件包选择”是否可用



—运行图形的工具system-config-kickstart 进行选择
[root@svr7 ~]#  system-config-kickstart


ftp://192.168.4.7/centos




重新划分新的分区


























[root@svr7 /]# ls   /root/ks.cfg    
/root/ks.cfg
[root@svr7 /]# vim   /root/ks.cfg
2.利用FTP服务共享应答文件
[root@svr7 /]# cp   /root/ks.cfg    /var/ftp/
[root@svr7 /]# ls   /var/ftp/
centos  ks.cfg  pub
[root@svr7 /]#
[root@svr7 ~]# curl  ftp://192.168.4.7/ks.cfg

3.修改菜单文件，指定应答文件获取方式
[root@svr7 /]# vim   /var/lib/tftpboot/pxelinux.cfg/default
……..此处省略一万字
label linux
menu label ^Install  CentOS  7
menu  default
kernel vmlinuz
append initrd=initrd.img   ks=ftp://192.168.4.7/ks.cfg


总结思路：   
1.dhcp服务---》IP地址、next-server、filename   "pxelinux.0"
2.tftp服务---》 "pxelinux.0"
3.pxelinux.0---》读取菜单文件/var/lib/tftpboot/pxelinux.cfg/default
4.default---》vesamenu.c32、读秒时间、vmlinuz、initrd.img、ftp://192.168.4.7/ks.cfg
5.ks.cfg应答文件---》语言、键盘类型、分区、安装方式url --url="ftp://192.168.4.7/centos"



在虚拟机B构建网络装机时，关闭虚拟机A的DHCP服务，避免冲突



------------------------------------------------------------------------------------
10.26
##源码包不指定默认安装在/usr/local下
源码包-----开发工具
先有的源码包，后有的rpm软件包
rpm缺点是不能自定安装内容

RPM软件包：rpm -ivh 或者 yum -y install 
源码包----开发工具gcc与make----》可以执行的程序-----》运行安装
•主要优点
–获得软件的最新版，及时修复bug
–软件功能可按需选择/定制，有更多软件可供选择
–源码包适用各种平台

步骤1：安装开发工具gcc与make，释放源代码至指定目录
步骤2：tar解包，释放源代码至指定目录
步骤3：./configure 配置，指定安装目录/功能模块等选项
步骤4：make 编译，生成可执行的二进制程序文件
步骤5：make install 安装，将编译好的文件复制到安装目录

一、数据同步

•命令用法
rsync  [选项...]  源目录     目标目录
•同步与复制的差异
复制：完全拷贝源到目标
同步：增量拷贝，只传输变化过的数据

•rsync操作选项
-n：测试同步过程，不做实际修改
--delete：删除目标文件夹内多余的文档
-a：归档模式，相当于-rlptgoD
-v：显示详细操作信息
-z：传输过程中启用压缩/解压

远程同步
•与远程的 SSH目录保持同步               
下行：rsync  [...]   user@host:远程目录    本地目录
上行：rsync  [...]   本地目录    user@host:远程目录

•基本用法
inotifywait  [选项]  目标文件夹
•常用命令选项
-m，持续监控（捕获一个事件后不退出）
-r，递归监控、包括子目录及文件
-q，减少屏幕输出信息
-e，指定监视的 modify、move、create、delete、attrib 等事件类别     



由真机将tools.tar.gz传递到虚拟机A的/root目录下 

真机为Linux系统同学             
[root@localhost /]# ls /linux-soft/
[root@localhost /]# ls /linux-soft/1
[root@localhost /]# scp  /linux-soft/1/tools.tar.gz root@192.168.4.7:/root

真机为windows系统同学












一、源码编译安装

RPM软件包：rpm -ivh 或者 yum -y install 
源码包----开发工具gcc与make----》可以执行的程序-----》运行安装








•主要优点
–获得软件的最新版，及时修复bug
–软件功能可按需选择/定制，有更多软件可供选择
–源码包适用各种平台
–……

步骤1：安装开发工具gcc与make，释放源代码至指定目录
步骤2：tar解包，释放源代码至指定目录
步骤3：./configure 配置，指定安装目录/功能模块等选项
步骤4：make 编译，生成可执行的二进制程序文件
步骤5：make install 安装，将编译好的文件复制到安装目录






1.安装开发工具
[root@svr7 ~]# yum -y install gcc make
[root@svr7 ~]# rpm -q gcc
gcc-4.8.5-28.el7.x86_64
[root@svr7 ~]# rpm -q make
make-3.82-23.el7.x86_64
[root@svr7 ~]# 




2.进行解压缩    
[root@svr7 ~]# tar  -xf  /root/tools.tar.gz  -C  /usr/local/
[root@svr7 ~]# ls  /usr/local/tools
3.进行tar解包 
[root@svr7 ~]# tar -xf /usr/local/tools/inotify-tools-3.13.tar.gz -C   /usr/local/
[root@svr7 ~]# ls /usr/local/
[root@svr7 ~]# cd /usr/local/inotify-tools-3.13/
[root@svr7 inotify-tools-3.13]# ls



4.运行configure脚本
作用1：检测当前系统是否安装gcc
   作用2：指定安装位置与功能
]# cd  /usr/local/inotify-tools-3.13/
]# ./configure --help
]# ./configure --prefix=/opt/myrpm #指定安装位置，此步骤不产生相应的目录

]# ls       Makefile     #源码编译安装步骤文件



常见的报错信息：gcc开发工具没有安装
checking for gcc... no
checking for cc... no
checking for cl.exe... no
configure: error: no acceptable C compiler found in $PATH
See `config.log' for more details.



5.进行make编译，变成可以执行的程序（放在内存中）
[root@svr7 ~]# cd   /usr/local/inotify-tools-3.13/
[root@svr7 inotify-tools-3.13]# make
6.进行make install安装
[root@svr7 ~]# cd   /usr/local/inotify-tools-3.13/
[root@svr7 inotify-tools-3.13]# make install 
[root@svr7 inotify-tools-3.13]# ls /opt/
[root@svr7 inotify-tools-3.13]# ls /opt/myrpm/
[root@svr7 inotify-tools-3.13]# ls /opt/myrpm/bin/





一、数据同步

•命令用法
rsync  [选项...]  源目录     目标目录
•同步与复制的差异
复制：完全拷贝源到目标
同步：增量拷贝，只传输变化过的数据

•rsync操作选项
-n：测试同步过程，不做实际修改
--delete：删除目标文件夹内多余的文档
-a：归档模式，相当于-rlptgoD
-v：显示详细操作信息
-z：传输过程中启用压缩/解压

本地同步   
[root@svr7 ~]# mkdir     /mydir   /todir    
[root@svr7 ~]# cp /etc/passwd    /mydir
[root@svr7 ~]# touch   /mydir/1.txt
[root@svr7 ~]# ls /mydir

[root@svr7 ~]# rsync -av   /mydir    /todir     #同步目录本身
[root@svr7 ~]# ls   /todir

[root@svr7 ~]# rsync -av   /mydir/    /todir    #同步目录内容
[root@svr7 ~]# ls   /todir

[root@svr7 ~]# touch   /mydir/2.txt
[root@svr7 ~]# rsync -av   /mydir/    /todir    #同步目录内容
[root@svr7 ~]# ls   /todir
[root@svr7 ~]# echo  123 >  /mydir/1.txt
[root@svr7  ~]# rsync -av   /mydir/    /todir   #同步目录内容
[root@svr7 ~]# ls   /todir



[root@svr7 ~]# rsync  -av  --delete   /mydir/   /todir/
[root@svr7 ~]# ls   /mydir/
[root@svr7 ~]# ls   /todir/

[root@svr7 ~]# touch   /todir/a.txt
[root@svr7 ~]# ls  /todir/
[root@svr7 ~]# rsync  -av  --delete  /mydir/   /todir/
[root@svr7 ~]# ls   /todir/
[root@svr7 ~]# ls   /mydir/





远程同步
•与远程的 SSH目录保持同步               
下行：rsync  [...]   user@host:远程目录    本地目录
上行：rsync  [...]   本地目录    user@host:远程目录







虚拟机A的/mydir目录的内容与虚拟机B的/opt进行同步
虚拟机A：
]# rsync  -av  --delete    /mydir/    root@192.168.4.207:/opt
……..connecting (yes/no)? yes
root@192.168.4.207's password:         #输入密码
虚拟机B：
]# ls /opt/



二、实时数据同步
虚拟机A的/mydir/目录的内容与虚拟机B的/opt进行同步

实现ssh无密码验证(公钥与私钥)
虚拟机A
1.生成公钥与私钥
[root@svr7 ~]# ssh-keygen         #一路回车
[root@svr7 ~]# ls    /root/.ssh/
id_rsa(私钥)   id_rsa.pub(公钥)    known_hosts(记录曾经远程管理过的机器)
2．将公钥传递给虚拟机B
]# ssh-copy-id     root@192.168.4.207
]#rsync  -av  --delete   /mydir/    root@192.168.4.207:/opt



监控目录内容变化工具
将真机的tools.tar.gz传递数据到虚拟机A

[root@svr7 ~]# ls       /root
tools.tar.gz                         下载
公共                                      音乐
[root@svr7 ~]#

源码编译安装步骤：
步骤一:安装开发工具
]# yum   -y   install   make
]# yum   -y   install   gcc
]# rpm   -q   gcc
]# rpm   -q   make
步骤二:进行tar解包                            
]# tar  -xf  /root/tools.tar.gz  -C   /usr/local/
]# ls   /usr/local/
]# ls   /usr/local/tools/

]# tar -xf   /usr/local/tools/inotify-tools-3.13.tar.gz   -C /usr/local/
]# ls   /usr/local/


步骤三：运行configure脚本进行配置
作用1：检测系统是否安装gcc  
作用2：可以指定安装位置及功能
]# cd    /usr/local/inotify-tools-3.13/
]# ./configure     --prefix=/opt/myrpm       #指定安装位置

常见错误：没有安装gcc
checking for gcc... no
checking for cc... no
checking for cl.exe... no
configure: error: no acceptable C compiler found in $PATH
See `config.log' for more details.


步骤四：make进行编译，产生可以执行的程序
]# cd    /usr/local/inotify-tools-3.13/
]# make

步骤五：make   install进行安装
]# cd    /usr/local/inotify-tools-3.13/
]# make    install
]# ls  /opt/
]# ls  /opt/myrpm/
]# ls  /opt/myrpm/bin/               




•基本用法
inotifywait  [选项]  目标文件夹
•常用命令选项
-m，持续监控（捕获一个事件后不退出）
-r，递归监控、包括子目录及文件
-q，减少屏幕输出信息
-e，指定监视的 modify、move、create、delete、attrib 等事件类别       

inotifywait  
rsync  -av  --delete   /mydir/    root@192.168.4.207:/opt







书写shell脚本(了解)      
脚本：可以运行一个文件，实现某种功能
中文:新建用户zhangsan        shell： useradd   zhangsan

[root@svr7 /]# vim   /root/hello.sh
hostname
ifconfig  |  head -2
echo  hello  world
head -1  /etc/passwd
[root@svr7 /]# chmod   a+x  /root/hello.sh
[root@svr7 /]# /root/hello.sh    #绝对路径运行








重复性事情：循环解决
     while  条件
     do
          重复执行的事情
     done





[root@svr7 /]# vim   /etc/rsync.sh    
while   /opt/myrpm/bin/inotifywait    -rqq    /mydir/
do
rsync -a  --delete   /mydir/    root@192.168.4.207:/opt
done
[root@svr7 /]# chmod   a+x    /etc/rsync.sh  #赋予执行权限
[root@svr7 /]# /etc/rsync.sh   &     #运行脚本程序
[root@svr7 /]# jobs  -l
[1]+ 17707 运行中               /etc/rsync.sh &
[root@svr7 /]# kill  17707      #停止脚本




三、数据库服务基础（数据库系统）
数据库：存放数据的仓库
在数据库系统中，有很多的数据库，在每一个库中有很多的表格

•常见的关系型 数据库管理系统
微软的 SQL Server
IBM的 DB2
甲骨文的 Oracle、MySQL
社区开源版 MariaDB 
……




部署MariaDB 数据库系统
[root@svr7 /]# yum  -y  install   mariadb-server
[root@svr7 /]# systemctl   restart    mariadb

MariaDB基本使用 
1.Linux系统的管理指令不能使用
2.所有的数据库系统指令都必须以 ; 结尾
3.数据库系统的指令大部分不支持tab补全

[root@svr7 /]# mysql             #进入数据库系统
> create  database  nsd01;     #创建nsd01数据库
> show  databases;                #查看所有数据库
> drop   database   nsd01;    #删除数据库nsd01
> show  databases;                #查看所有数据库
> exit                                     #退出数据库系统
[root@svr7 /]# mysql             #进入数据库系统
> use      mysql;              #切换到mysql数据库
> show   tables;          #查看当前库中所有表格
> show   databases;    #查看所有数据库
> use   test;                  #切换到test数据库
> exit                            #退出数据库系统


    大家准备users.sql文件，将users.sql文件传递到虚拟机A的/root目录下




恢复数据到数据库中
1.传递备份好的数据文件users.sql到虚拟机A中
真机为Linux系统的同学             
[root@localhost /]# ls   /linux-soft/
[root@localhost /]# ls   /linux-soft/1
[root@localhost /]# scp  /linux-soft/1/users.sql root@192.168.4.7:/root
真机为windows系统的同学  

[root@svr7 /]# ls  /root
abc02  users.sql             图片  桌面
[root@svr7 /]#









2.建立新的数据库
[root@svr7 /]# mysql 
MariaDB [(none)]> create   database   nsd2109;
MariaDB [(none)]> show  databases;
MariaDB [(none)]> exit;
3.恢复数据到数据库
]# mysql    nsd2109   <   /root/users.sql  #Linux命令行完成

]# mysql   
MariaDB [(none)]> use nsd2109;     #切换到数据库nsd2109
MariaDB [nsd2109]> show  tables;     #查看当前库有哪些表格
+-------------------+
| Tables_in_nsd2109 |
+-------------------+
| base              |
| location          |
+-------------------+



表格操作：
–增(insert)     删（delete）  改（update）    查(select)
–表字段、表记录
编号	姓名	住址
1	Dc	东村
2	Tc	西村





查(select) 
格式: select    表字段，表字段，……     from   库名.表名;

[root@svr7 /]# mysql 
> use    nsd2109;
> select   *   from   base;         #查看base所有表字段内容
> select   *   from   location;   #查看location所有表字段内容

> use  test;
> select   *   from    nsd2109.base;    

> use  nsd2109;
> select   id,name    from     base;



[root@svr7 /]# mysql  
> use    nsd2109;
> select  *   from    base    where   password='456';
> select  *    from   base    where   id='4';
> select  *  from  base    where  id='4'  and   password='123';
> select  *  from base    where  id='4'  or   password='123';

增(insert)
格式：insert  表名    values (‘值’,‘值’,‘值’);
MariaDB [nsd2109]> insert   base  values('10','dc','789');
MariaDB [nsd2109]> insert   base  values('11','tcc','369');
MariaDB [nsd2109]> select   *   from base ;
改（update）
格式：
update  表名    set  表字段=‘新值’ where  表字段=’值’；
> select   *  from  base ;
> update   base   set   password='8888'    where   id='1';
> select  *  from   base ;   
> update  base   set   password='9999'    where   id='2';
> select  *  from   base ;   

删（delete）                
> delete   from   base   where     id='4' ;
> select   *   from   base ;
> delete   from   base   where     id='3' ;
> select   *   from   base ;



为数据库系统管理员设置密码
mysqladmin  [-u用户名]  [-p[旧密码]]  password  '新密码'
数据库系统管理员:对于数据库系统有最高权限，名字为root，能够登陆数据系统的用户信息有mysql库中user表进行储存

Linux系统管理员: 对于Linux系统有最高权限，名字为root，能够登陆Linux系统的用户信息/etc/passwd进行储存

[root@svr7 /]# mysqladmin  -u   root       password     '456'
[root@svr7 /]# mysql  -u   root   -p    #交互式进行登录
Enter password:

[root@svr7 /]# mysql  -u   root    -p456    #非交互式进行登录

已知旧密码修改新密码
]# mysqladmin  -u   root    -p456       password    '123'
]# mysql  -u  root    -p123


#########################################
命令补充
获取命令帮助
方式一：命令  --help
[root@localhost ~]# cat  --help
方式二：man   命令  
[root@localhost ~]# man   cat        #按q退出
[root@localhost ~]# man   passwd    #显示passwd命令帮助
[root@localhost ~]# man  5  passwd
数字5表示帮助的类型，表示配置文件类型


历史命令
管理/调用曾经执行过的命令
–history：查看历史命令列表
–history  -c：清空历史命令
–!n：执行命令历史中的第n条命令
–!str：执行最近一次以str开头的历史命令
[root@svr7 ~]# vim  /etc/profile
HISTSIZE=1000      #默认记录1000条

[root@localhost ~]# history          #显示历史命令列表
[root@localhost ~]# history   -c    #清空历史命令
[root@localhost ~]# history                 
[root@localhost ~]# cat   /etc/redhat-release 
[root@localhost ~]# ls   /root
[root@localhost ~]# history

[root@localhost ~]# !cat  #指定最近一条以cat开头的历史命令
[root@localhost ~]# !ls    #指定最近一条以ls开头的历史命令


du，统计文件的占用空间
–du  [选项]...  [目录或文件]...
–-s：只统计每个参数所占用的总空间大小
–-h：提供易读容量单位（K、M等） 
[root@localhost ~]# du  -sh   /root
[root@localhost ~]# du  -sh   /etc
[root@localhost ~]# du  -sh   /boot
[root@localhost ~]# du  -sh   /



date，查看/调整系统日期时间
–date  +%F、date +%R
–date  +"%Y-%m-%d %H:%M:%S"
–date  -s  "yyyy-mm-dd  HH:MM:SS" 

]# date
]# date  -s    "2008-9-6   11:11:11"     #修改系统时间
]# date

]# date   -s    "2020-9-5   15:37:11"   
]# date

[root@localhost ~]# date   +%Y     #显示年
[root@localhost ~]# date   +%m    #显示月
[root@localhost ~]# date   +%d    #显示日期

[root@localhost ~]# date   +%H   #显示时
[root@localhost ~]# date   +%M   #显示分
[root@localhost ~]# date   +%S    #显示秒

[root@localhost ~]# date   +%F   #显示年-月-日
[root@localhost ~]# date   +%R   #显示时:分




制作连接(链接)文件（制作快捷方式）
格式：ln  -s   /路径/源数据     /路径/快捷方式的名称    #软连接
]# ln  -s    /etc/sysconfig/network-scripts/   /ns
]# ls   /
]# ls   -l   /ns    #查看快捷方式的信息

]# touch   /ns/haha.txt
]# touch   /ns/maohehaozi.txt
]# touch   /ns/shukehebeita.txt
]# ls   /etc/sysconfig/network-scripts/

软连接优势：可以针对目录与文件制作快捷方式，支持跨分区
软连接缺点：源数据消失，快捷方式失效





格式：ln     /路径/源数据     /路径/快捷方式的名称    #硬链接
硬链接优势：源数据消失，快捷方式仍然有效
硬链接缺点：只能针对文件制作快捷方式，不支持支持跨分区
[root@localhost ~]# rm  -rf   /opt/*
[root@localhost ~]# echo  123   >   /opt/A.txt
[root@localhost ~]# ln  -s   /opt/A.txt    /opt/B.txt   #软连接
[root@localhost ~]# ls /opt/

[root@localhost ~]# ln    /opt/A.txt    /opt/C.txt   #硬链接
[root@localhost ~]# ls    /opt/
[root@localhost ~]# cat    /opt/B.txt 
[root@localhost ~]# cat    /opt/C.txt 

[root@localhost ~]# rm  -rf   /opt/A.txt 
[root@localhost ~]# ls   /opt/
[root@localhost ~]# cat  /opt/B.txt      #软连接失效
cat: /opt/B.txt: 没有那个文件或目录
[root@localhost ~]# cat   /opt/C.txt     #硬链接仍然有效

zip归档工具，跨平台
•归档+压缩操作: zip  [-r]   备份文件.zip     被归档的文档... 
[-r]:被归档的数据有目录，必须加上此选项
]# zip   -r     /opt/abc.zip        /etc/passwd     /home
]# ls   /opt/
•释放归档+解压操作:  unzip   备份文件.zip    [-d  目标文件夹]  
]# mkdir   /nsd20
]# unzip       /opt/abc.zip       -d    /nsd20
]# ls   /nsd20
]# ls   /nsd20/etc/
]# ls   /nsd20/home/




-----------------------------------------------------------------------------------
10.27
为数据库系统管理员设置密码
mysqladmin  [-u用户名]  [-p[旧密码]]  password  '新密码'
数据库系统管理员:对于数据库系统有最高权限，名字为root，能够登陆数据系统的用户信息有mysql库中user表进行储存

Linux系统管理员: 对于Linux系统有最高权限，名字为root，能够登陆Linux系统的用户信息/etc/passwd进行储存

[root@svr7 /]# mysqladmin  -u   root       password     '456'
[root@svr7 /]# mysql  -u   root   -p    #交互式进行登录
Enter password:

[root@svr7 /]# mysql  -u   root    -p456    #非交互式进行登录

已知旧密码修改新密码
]# mysqladmin  -u   root    -p456       password    '123'
]# mysql  -u  root    -p123

数据库一切重来（恢复出厂设置，此操作只适合在第一阶段）
[root@svr7 ~]# systemctl stop mariadb
[root@svr7 ~]# rm -rf  /var/lib/mysql/
[root@svr7 ~]# yum -y reinstall mariadb-server
[root@svr7 ~]# systemctl restart mariadb 

############################################################
今日所需资料：podman与rhel-8.2-x86_64-dvd.iso
设置虚拟机的主机名
[root@localhost ~]# hostnamectl set-hostname rhel8.tedu.cn
[root@localhost ~]# hostname
rhel8.tedu.cn
开启一个新的终端验证
构建Yum仓库
完整的Yum仓库：1.众多的软件包   2.仓库数据文件(仓库清单)

[root@rhel8 ~]# mkdir   /dvd
[root@rhel8 ~]# mount   /dev/cdrom     /dvd
mount: /dvd: WARNING: device write-protected, mounted read-only.
[root@rhel8 ~]# ls   /dvd

[root@rhel8 ~]# vim   /etc/yum.repos.d/dvd.repo 
[haha]
name=haha
baseurl=file:///dvd/AppStream/     
enabled=1
gpgcheck=0                               
[xixi]
name=haha
baseurl=file:///dvd/BaseOS/
enabled=1
gpgcheck=0
[root@rhel8 ~]# yum  repolist  -v
[root@rhel8 ~]# yum  -y     install   vsftpd   #用于测试

[root@rhel8 ~]# blkid  /dev/cdrom
[root@rhel8 ~]# vim   /etc/fstab 
此处省略一万字…..
/dev/cdrom   /dvd   iso9660  defaults  0  0
[root@rhel8 ~]# umount   /dvd
[root@rhel8 ~]# ls   /dvd
[root@rhel8 ~]# mount -a
mount: /dvd: WARNING: device write-protected, mounted read-only.
[root@rhel8 ~]# ls   /dvd


关闭所有虚拟机的SELinux
[root@rhel8 ~]# vim  /etc/selinux/config
SELINUX=disabled
设置所有虚拟机防火墙
[root@rhel8 ~]#  yum  remove  firewalld

修改网卡命名规则
]# ifconfig   |   head  -2
]# vim  /etc/default/grub
…….
GRUB_CMDLINE_LINUX="……. quiet  net.ifnames=0  biosdevname=0"
…….
]# grub2-mkconfig  -o   /boot/grub2/grub.cfg 
Generating grub configuration file ...
done
[root@rhel8 ~]# reboot    #重启系统


配置IP地址                        
[root@rhel8 ~]# nmcli connection show 
[root@rhel8 ~]# nmcli connection delete ens160
[root@rhel8 ~]# nmcli connection add type ethernet ifname eth0  con-name eth0

[root@rhel8 ~]# nmcli connection modify eth0 
ipv4.method manual ipv4.addresses 192.168.4.8/24 connection.autoconnect yes

[root@rhel8 ~]# nmcli connection up eth0 
[root@rhel8 ~]# ifconfig | head -2

网络类型：
KVM虚拟机选择private1  
VMware虚拟机选择vmnet1

真机为Linux传递软件素材
]# ls /linux-soft/1/podman/
]# scp -r /linux-soft/1/podman   root@192.168.4.8:/root

真机为windows传递软件素材：需要利用远程管理软件moba


模块化安装：类似于安装一组软件包
[root@rhel8 ~]# yum  module    list
[root@rhel8 ~]# yum -y module install container-tools

#####################################
一、容器基础概述
1.Linux中的容器是装应用的
2.容器就是将软件打包成标准化单元，用于开发、交付和部署
3.容器技术已经成为应用程序封装和交付的核心技术

•优点
–相比于传统的虚拟化技术，容器更加简洁高效
–传统虚拟机需要给每个VM安装操作系统
–容器使用的共享公共库和程序
–

•镜像是启动容器的核心，镜像由镜像仓库提供
•在podman中容器是基于镜像启动的


•podman和容器的关系
–podman是完整的一套容器管理系统
–podman提供了一组命令，让用户更加方便直接地使用容器技术，而不需要过多关心底层内核技术

•podman所需软件
–系统软件，位于 rhel-8.2-x86_64-dvd 源中
–利用系统光盘，构建Yum仓库


[root@localhost ~]# yum -y module install container-tools


获取镜像
•镜像的名称标识
–每一个镜像都对应唯一的镜像 id
–镜像名称（文件名称） + 标签（路径） = 唯一

–每一个镜像都有标签，如果没写就是默认标签 latest
–我们在调用镜像的时候，如果没有指定标签也是 latest


•查找镜像（需要能访问互联网）
–podman  search  关键字
podman  search  httpd
•下载镜像（需要能访问互联网）
–podman  pull  镜像名称:标签
podman   pull   localhost/myos:latest
•导入镜像
–podman  load  -i  备份文件.tar.gz
podman   load  -i  /root/httpd.tar.gz

镜像管理命令
•查看镜像
–podman images
•删除镜像
–podman rmi 镜像名称:镜像标签


镜像管理练习
]# podman images     #查看当前有哪些镜像
]# podman  load   -i   /root/httpd.tar.gz       #导入镜像
]# podman images     #查看当前有哪些镜像
]# podman load  -i   /root/nginx.tar.gz          #导入镜像
]# podman load  -i   /root/myos.tar.gz            #导入镜像
]# podman images     #查看当前有哪些镜像

镜像删除练习
]# podman  images           #查看当前有哪些镜像
]# podman   rmi   2f5        #按照镜像的ID值，删除镜像
]# podman  images           #查看当前有哪些镜像
]# podman load  -i   /root/myos.tar.gz            #导入镜像
]# podman  images           #查看当前有哪些镜像

]# podman   images
]# podman   rmi    localhost/myos:nginx    #删除镜像
]# podman   images

]# podman rmi  localhost/myos:latest      #删除镜像
]# podman   images



二、使用容器
•podman run 命令
–podman  run  -选项   镜像名称:镜像标签   启动命令
•查看 run 的选项
–podman  help  run
–man  podman-run 
•run  =  创建 + 启动 + 进入

•podman run 命令的选项
–选项 -i，交互式
–选项 -t，终端
–选项 -d，后台运行
–选项 --name  容器名字
•启动容器，并进入容器
podman run -it myos:latest /bin/bash
•可以通过命令行提示符，判定自己是否进入容器了

容器管理命令
•启动容器
–podman run –选项 镜像名称:镜像标签 启动命令
•查看容器
–podman ps [ -a 所有容器id ] [ -q 只显示容器 id ]
•删除容器
–podman rm 容器id
•容器管理命令启动、停止、重启
–podman start|stop|restart 容器id
•进入容器
–podman  exec   -it  容器id   启动命令

容器初步练习                            
]# touch  /etc/resolv.conf
]# podman run -it   localhost/myos:httpd   /bin/bash
[root@2b0b7c62ab42 /]# cat /etc/redhat-release
[root@2b0b7c62ab42 /]# useradd dc
[root@2b0b7c62ab42 /]# id  dc
[root@2b0b7c62ab42 /]# exit

[root@rhel8 ~]# podman  ps   -a        #查看当前系统容器
[root@rhel8 ~]# podman  start  2b    #利用容器id开启容器
[root@rhel8 ~]# podman exec -it   2b /bin/bash   #进入容器
[root@2b0b7c62ab42 /]# id dc
[root@2b0b7c62ab42 /]# exit

[root@rhel8 ~]# podman  ps   -a
[root@rhel8 ~]# podman stop   2b    #利用容器id停止容器
[root@rhel8 ~]# podman  ps   -a       
[root@rhel8 ~]# podman  rm   2b      #利用容器id删除容器
[root@rhel8 ~]# podman  ps   -a

容器放入后台练习 
]# podman run --name abc01  -itd localhost/myos:httpd /bin/bash

]# podman  ps  -a
]# podman  exec  -it   abc01  /bin/bash  #进入abc01容器

[root@962aa837e17b html]# cd /
[root@962aa837e17b /]# useradd tc
[root@962aa837e17b /]# id tc
uid=1000(tc) gid=1000(tc) groups=1000(tc)
[root@962aa837e17b /]# exit

[root@rhel8 ~]# podman  stop  abc01
[root@rhel8 ~]# podman  rm abc01
[root@rhel8 ~]# podman  ps  -a



三、容器进阶-对外发布容器服务

•容器可以与宿主机的端口进行绑定
•从而把宿主机变成对应的服务,不用关心容器的IP地址

•我们使用 -p 参数把容器端口和宿主机端口绑定
•同一宿主机端口只能绑定一个容器服务
•-p  [可选IP]:宿主机端口:容器端口
•例如:把宿主机变成 apache
podman run -itd -p 80:80 myos:httpd
•例如:把宿主机变成 nginx
podman run -itd -p 80:80 myos:nginx


容器放入后台,端口绑定练习
[root@rhel8 ~]# podman run --name nsdweb -p 80:80         -itd   localhost/myos:httpd    /bin/bash

[root@rhel8 ~]# podman ps -a
[root@rhel8 ~]# podman exec -it nsdweb /bin/bash

[root@5b69bf6956b0 html]# echo wo shi nsdweb > /var/www/html/index.html 
[root@5b69bf6956b0 html]# /usr/sbin/httpd   #手动启动
[root@5b69bf6956b0 html]# exit
exit

[root@rhel8 ~]# curl  http://192.168.4.100
wo shi nsdweb
[root@rhel8 ~]#




四、容器进阶-容器共享卷
•podman容器不适合保存任何数据
•podman可以映射宿主机文件或目录到容器中
–目标对象不存在就自动创建
–目标对象存在就直接覆盖掉
–多个容器可以映射同一个目标对象来达到数据共享的目的
•启动容器时，使用 -v 映射参数
podman run -itd  -v  宿主机对象:容器内对象   镜像名称:标签

容器终极练习
]# podman stop   nsdweb         #停止容器
]# podman rm -f   nsdweb        #强制删除容器
]# podman run --name rqweb  -p 80:80                                  -v  /opt:/var/www/html -itd  localhost/myos:httpd /bin/bash

[root@rhel8 ~]# podman exec -it rqweb /bin/bash
[root@56557d6f0517 html]# /usr/sbin/httpd   #手动启动
[root@56557d6f0517 html]# exit

[root@rhel8 ~]# echo wo shi niuniubenben > /opt/index.html
[root@rhel8 ~]# curl http://192.168.4.100              
wo shi niuniubenben
[root@rhel8 ~]#              




五、容器进阶-管理系统服务
•systemd一个更高效的系统&服务管理器
–开机服务并行启动，各系统服务间的精确依赖
–服务目录：/usr/lib/systemd/system/
–主要管理工具：systemctl
•管理员服务文件默认路径
–/usr/lib/systemd/system/
•生成服务启动配置文件
--files：生成文件类型
podman  generate  systemd   --name  容器名  --files  
•重新加载服务启动配置文件
systemctl  daemon-reload

容器之光练习（必须是相对路径，当前路径必须是/usr/lib/systemd/system）
]# cd  /usr/lib/systemd/system
]# podman  ps  -a
]# podman generate systemd --name rqweb --files
]# vim container-rqweb.service
]# systemctl  daemon-reload        #重新加载服务配置文件
]# systemctl  daemon-reload

]# systemctl stop container-rqweb
]# podman ps -a
]# systemctl start container-rqweb
]# podman ps -a
]# systemctl enable container-rqweb     #设置开机自启


-----------------------------------------------------------------------------------
第二阶段课程
10.28
NETWORK  01
=============================================
网络的功能：
1，信息传递
2，资源共享
3，增加可靠
4，提高系统处理能力

网络的发展：
60年代    
分组交换    提高数据传输效率
70~80年代
tcp/ip		统一网络的协议
90年代
web技术    用户最容易享受到的服务

网络传输距离
•	广域网（Wide-Area Network）   
	范围:几十到几千千米 
	作用:用于连接远距离计算机网络
	典型应用:Internet（英特网）
•	局域网（Local-Area Network）
	范围:1千米左右 
	作用:用于连接较短距离内计算机
	典型应用:企业网,校园网

网络重要设备
交换机  连接设备入网，使设备可以共享网络
路由器  是连接两个不同范围网络的设备

局域网中常用的网络拓扑结构
星形拓扑  易于实现、易于扩展网络、易于排查故障
网状拓扑  可靠性高

Tcp/ip五层参考模型以及对应的典型设备
应用层		 pc
传输层       防火墙
网络层		 路由器
数据链路层	 交换机
物理层		 网卡

网络设备命令行视图
1，<Huawei> 用户视图
<Huawei>system-view    //进入系统视图
2，[Huawei] 系统视图
[Huawei]interface ethernet 0/0/1   //进入1号接口视图
3，[Huawei-Ethernet0/0/1]  接口视图
[Huawei-Ethernet0/0/1]quit  返回上一视图
如果在接口视图输入return或快捷键ctrl+z可以直接返
回用户视图

<Huawei>display version   //查看设备型号版本
[Huawei]sysname sw1   //修改主机名
[Huawei]undo info-center enable  //关闭日志信息提示
[sw1]display current-configuration   //查看设备配置，按空格翻页，按
回车换行，或用鼠标滚轮查看

网络设备控制方式
直接控制（控制台窗口）
远程控制  ssh

为设备添加用户与配置密码，增加安全
[sw1]aaa   //进入账户管理视图
[sw1-aaa]local-user test01 password cipher 123456  //创建test01
账户，配置加密的密码123456
[sw1-aaa]quit	    //返回上一视图
[sw1]user-interface console 0  //进入用户控制台接口(直接控制设
备的方式)
[sw1-ui-console0]authentication-mode aaa  //激活刚刚创建的账户
从此，再次进入用户控制台输入命令就要先输入账户与密码
然后使用快捷键ctrl+] 退出系统，可以验证账户密码

<sw1>save     //保存配置，过程中输入y，然后回车两次看
到successfully字样，表示成功
<sw1>reboot  //重启设备，过程中输入y，如果设备的配置被修改
了，没保存就要重启的话系统会先询问是否要保存

网络的地址
ip地址，需要在使用时自定义
使用十进制 0~9   192.168.0.1

mac地址(物理地址或硬件地址)，不需要自定义，设备出厂时会携带
使用十六进制  0~F   0123456789ABCDEF
52:54:00:37:78:11   //通常用来标识设备的唯一性，不可修改，全球
唯一

交换机工作原理
学习  学习数据帧中源mac地址
广播  对数据来源之外的所有接口发送寻找目标的信息
转发  一对一传递数据	
更新  当连接交换机的设备超过300秒没有数据传递
	  以及设备从交换机断开，都会造成交换机清除对应
      Mac地址的动作

<Huawei>display mac-address   //查看mac地址

交换机工作原理可以通过下列拓扑图验证
1，首先准备2台pc，配置好ip，并连接了s3700交换机

2，pc的mac地址可以在基础配置中查看到，为了方便验证可以将其的尾号写在注释中
注意：mac地址是随机的，无需自定义，但与图中可能不一样，实验时要按照实际填写


3，然后使用pc1去ping pc2

4，然后查看交换机mac地址表可以得到结果


5，然后可以再连接一个交换机增加两台pc进行测试
让pc1 去ping  pc3


问题
然后查看交换机1和交换机2的mac地址表，结果如下则成功
1 请描述计算机网络的功能有哪些
数据通信
资源共享
增加可靠性
提高系统处理能力
2 计算机网络发展过程中，90年代诞生了什么标志性技术
web技术
3 今天学习的知识点中提到的 ISO是什么？
ISO（国际标准化组织）
4 局域网中常见网络拓扑结构有哪些？
星型
网状
5 TCP/IP的五层参考模型是哪几层，其中第二层的典型设备是什么？
物理层，数据链路层，网络层，传输层，应用层
第二层典型设备是交换机
6 华为路由交换设备常见的命令行视图有哪些？
用户视图，系统视图，接口视图
7 通常以太网MAC地址使用什么进制？
十六进制
8 交换机工作原理主要有哪几个步骤
学习
广播
转发
更新

----------------------------------------------------------------------------------------------------------------------------
10.29

NETWORK day 02
=============================================
一，vlan技术及应用
广播域
广播域指接收同样广播消息的节点的集合，比如实际环境中的相同一个办公室
相同一个教室，都可以理解为同一个广播域，同一个广播域下虽然可以方便的接收
到所有广播信息，但也容易受到干扰，网络中也一样，设备太多甚至会造成广播泛滥
的现象，那么就可以通过vlan技术解决

什么是VLAN
Virtual LAN（虚拟局域网）

为什么用VLAN
交换机的所有接口默认属于同一个广播域
随着接入设备的增多，网络中广播增多，降低了网络的效率
为了分割广播域，引入了VLAN

vlan的优点
1，广播控制
2，增加安全
3，提高带宽利用率
4，降低数据传递延迟

---------------------------------------
使用vlan配置网络：
[Huawei]vlan 2  //创建vlan2，如果要删除可以用undo vlan 2
[Huawei]interface ethernet 0/0/3  //进入3接口
port link-type access   //设置接口类型为接入链路
port default vlan 2  //将接口加入vlan2
[Huawei]interface ethernet 0/0/4  //进入4接口
port link-type access   //设置接口类型为接入链路
port default vlan 2  //将接口加入vlan2

vlan 3  //创建vlan3
[Huawei]interface ethernet 0/0/5  //进入5接口
port link-type access   //设置接口类型为接入链路
port default vlan 3  //将接口加入vlan3
[Huawei]interface ethernet 0/0/6  //进入6接口
port link-type access   //设置接口类型为接入链路
port default vlan 3  //将接口加入vlan3，如果加错vlan
将正确的命令在敲一次即可

vlan batch 2 3  //批量创建vlan，如果要删除可以用undo vlan batch 2 3
[Huawei]port-group 1    //创建(进入)1号接口组
group-member Ethernet 0/0/3 Ethernet 0/0/4  //给接口组添加
成员，包括3口和4口
port link-type access   //设置接口类型为接入链路
port default vlan 2  //将接组中的所有接口加入vlan2
[Huawei-port-group-1]quit
[Huawei]port-group 2    //创建(进入)2号接口组
group-member Ethernet 0/0/5 Ethernet 0/0/6  //给接口组添加
成员，包括5口和6口
port link-type access
port default vlan 3

---------------------------
[Huawei-port-group-2]undo  group-member Ethernet 0/0/7 
//如果接口加错，比如不小心将7口加入了2号接口组可以用该命令
删除，使用undo命令，相当于取消 
---------------------
二，Trunk
access 接入链路   可以承载1个vlan的数据
trunk  中继链路   可以承载多个vlan的数据

两台交换机都要配置以下命令
[Huawei]interface ethernet 0/0/7
port link-type trunk   //配置为中继链路
port trunk allow-pass vlan all   //放行所有vlan的数据
display  vlan   //查询vlan列表，可以看到任何vlan中
都有7号接口

如果接口配置混乱，需要还原时：
[Huawei]clear configuration interface Ethernet 0/0/7  //清空配置
[Huawei]interface ethernet0/0/7   //进入接口
[Huawei-Ethernet0/0/7]undo shutdown  //开启接口

链路聚合，可以用多条链路(网卡)捆绑在一起，实现增加可靠，提高
链路带宽的目的
[Huawei]interface eth-trunk 1   //创建(进入)1号链路聚合接口
trunkport Ethernet 0/0/7 0/0/8  //捆绑7、8接口
port link-type trunk   //配置为中继链路
port trunk allow-pass vlan all   //放行所有vlan的数据
以上配置需要在两台交换机都有
用display vlan看列表中7、8口已经消失，取而代之的是Eth-Trunk1

-----------------------------------------------------------------
三，网络层
在数据链路层可以通过交换机进行vlan划分、中继链路、链路聚合
的配置，达到组建网络的基本需求，但是该层无法将数据传输的更远，
比如不同vlan的数据无法互通，所以要想让数据传递的更远，需要使
用网络层的功能

网络层可以实现以下功能：
定义了ip地址
可以连接不同媒介类型(不同范围的网络、不同的硬件、不同的系统)
可以选择数据通过网络的最佳路径(网络结构比较复杂时自动选路)



[Huawei]interface GigabitEthernet 0/0/0   //进入0号接口
[Huawei-GigabitEthernet0/0/0]ip address 192.168.1.254 24   //配置ip
[Huawei-GigabitEthernet0/0/0]quit
[Huawei]interface GigabitEthernet 0/0/1 
[Huawei-GigabitEthernet0/0/1]ip address 192.168.2.1 24
<Huawei>display ip interface brief   //查看设备所有的ip配置
[Huawei-GigabitEthernet0/0/1]undo ip address  //如果ip配置错误，使用该
命令可以删除
之后将两台pc的ip与网关配好即可互通
pc1的ip是192.168.1.1，网关是192.168.1.254
pc2的ip是192.168.2.2，网关是192.168.2.1
网关是设备通往另外一个网络的途径，通常由路由器(具备路由
功能的设备)承担

路由器依靠路由表转发数据
路由表的产生方式：
1，直连路由，将接口配置好ip并且开启后自动产生
2，静态路由，由管理员手工配置，添加所需路由
   语法格式：ip route-static 目标网段 掩码 下一跳


再通过添加路由器与pc将之前拓扑改造成以下状态：

并按照之前方式配置好设备的ip地址
4.1的网关是4.254
3.1的网关是3.254	


第一台路由器配置静态路由：
[Huawei]ip route-static 192.168.3.0 24 192.168.2.2  //添加
静态路由，可以去3.0网段，掩码24，下一跳2.2
[Huawei]undo ip route-static 192.168.3.0 24 192.168.2.2 //如果
路由配置错误，要删除
[Huawei]ip route-static 192.168.4.0 24 192.168.2.2  //添加
静态路由，可以去4.0网段，掩码24，下一跳2.2

第二台路由器配置静态路由：
[Huawei]ip route-static 192.168.1.0 24 192.168.2.1  //添加
静态路由，可以去1.0网段，掩码24，下一跳2.1

display ip routing-table | include /24  //查看路由表

排错思路：
1，检查pc的ip与网关
2，从最短距离开始ping测试，由近至远
3，检查路由器的路由表

问题
1，VLAN的作用是什么？
广播控制（避免广播泛滥），增加安全性，提高带宽利用，降低延迟
2，TRUNK的作用是什么？
为数据帧打上VLAN标识，使不同VLAN数据可以用一条链路传递（单一链路可以承载多个vlan的数据）
3，链路聚合的作用是什么？
提供更高的带宽和增加可靠性
4，静态路由配置语法格式是？
ip route-static 目标网段 子网掩码 下一跳地址
5，路由设备依靠什么转发数据？
路由表

--------------------------------------------------------------------------------------------------------
11.1
动态路由是否可以隔着跳 例如 12345 个路由， 1直接跳到4 或 5


[Huawei]undo info-center enable  //关闭日志信息提示
<Huawei>display ip interface brief   //查看设备所有的ip配置
display ip routing-table | include /24  //查看路由表


三成交换机的接口，不能直接配IP

网络设备命令行视图
1，<Huawei> 用户视图
<Huawei>system-view    //进入系统视图
2，[Huawei] 系统视图
[Huawei]interface ethernet 0/0/1   //进入1号接口视图
3，[Huawei-Ethernet0/0/1]  接口视图
4，[Huawei]ospf 		//进入协议视图
[Huawei-ospf-1]协议视图

没通的原因
1查看路由表


[Huawei-acl-basic-2000]rule deny source 192.168.2.1 0.0.0.0    
##反掩码规定为 0 即匹配，例如 0.0.255.255则只匹配前两位，只要是192.168 开头的网段就匹配，不看后面的数字

[Huawei-GigabitEthernet0/0/1]traffic-filter inbound acl 2000  
##数据进入的那一刻用inbound；数据出去的那一刻用outbound
##每一个接口可以有inbound和outbound两个ACL规则，对应限入和出的数据。
ACL规则为，列表里匹配即停止

三层交换机
1、创建Vlan
2、对上述Vlan配置IP
3、将需要配置IP的接口加入上述Vlan
##删除vlan时，如果已经创建（即进入过Vlanif）时，则不能直接删除，需要先删除Vlanif即“undo in vlan 4”
直连路由
静态路由
动态路由
ospf 宣告

传输层
定义端口号
Tcp udp

一，三层交换机
同时具备交换机与路由器功能的强大网络设备
三层交换机=路由器(三层)+交换机(二层)
按图搭建拓扑，最上面的设备是s5700三层交换机

使用三层交换机s5700组建网络
<Huawei>system-view 		//进入系统视图
[Huawei]undo info-center enable   //关日志
[Huawei]vlan batch 2 3   //创建vlan2与3
[Huawei]display vlan   //检查
[Huawei]interface GigabitEthernet 0/0/2   //进2口
[Huawei-GigabitEthernet0/0/2]port link-type access  //配置接口类型为access
[Huawei-GigabitEthernet0/0/2]port default vlan 2   //把2口加入vlan2
[Huawei-GigabitEthernet0/0/2]in g0/0/3
[Huawei-GigabitEthernet0/0/3]port link-type access  //配置接口类型为access
[Huawei-GigabitEthernet0/0/3]port default vlan 3   //把3口加入vlan3

路由器可以在物理接口配置ip
而三层交换机要进入虚拟接口配置
[Huawei]interface Vlanif 1		//进入vlan1的接口(虚拟接口)
[Huawei-Vlanif1]ip address 192.168.1.254 24    //配置ip，该ip可以作为
vlan1的网关
[Huawei-Vlanif1]interface Vlanif 2   //进入vlan2的接口
[Huawei-Vlanif2]ip address 192.168.2.254 24   //配置ip，该ip可以作为
vlan2的网关
[Huawei-Vlanif2]interface Vlanif 3   //进入vlan3的接口
[Huawei-Vlanif3]ip address 192.168.3.254 24  //配置ip，该ip可以作为
vlan3的网关
display ip interface brief   //检查ip

再增加s3700交换机一台，将网络改造成以下状态：

在s3700交换机配置：
[Huawei]vlan batch 2 3    //首先创建vlan2与3
[Huawei]interface ethernet0/0/2
[Huawei-Ethernet0/0/2] port link-type access
[Huawei-Ethernet0/0/2] port default vlan 2    //将e0/0/2口加入vlan2
[Huawei-Ethernet0/0/2] in e0/0/3
[Huawei-Ethernet0/0/3] port link-type access
[Huawei-Ethernet0/0/3] port default vlan 3    //将e0/0/3口加入vlan3
[Huawei-Ethernet0/0/3]in e0/0/4
[Huawei-Ethernet0/0/4]port link-type trunk  //将4口配置为中继链路
[Huawei-Ethernet0/0/4]port trunk allow-pass vlan all  //放行所有数据

再回到s5700配置：
[Huawei-GigabitEthernet0/0/1]port link-type trunk       //把g0/0/1口
也配置为中继链路
[Huawei-GigabitEthernet0/0/1]port trunk allow-pass vlan all  //放行所
有vlan的数据
  
如果接口配置错乱：这里用s3700举例
[Huawei]clear configuration interface Ethernet 0/0/4  //如果某接口配
置错误可以清空，恢复默认状态
[Huawei]interface e0/0/4  //进入4接口
[Huawei-Ethernet0/0/4]undo shutdown  //开启接口

之后将拓扑延申，增加一台router路由器，与pc一台，并按图配置好ip，pc的网关是192.168.5.254


三层交换机接口配置ip思路：
1，创建一个vlan
2，进入该vlan的虚拟接口配置ip
3，将需要配置ip的接口加入上述vlan

s5700的 g0/0/2接口要按照三层交换机接口配置ip的思路进行：
[Huawei]vlan 4    //创建vlan4
[Huawei-vlan4]in vlan 4   //进入vlan4接口
[Huawei-Vlanif4]ip add 192.168.4.1 24       //为vlan4配置ip
[Huawei-Vlanif4]in g0/0/2    //进入2接口
[Huawei-GigabitEthernet0/0/2]port link-type access  //配置接口类
型为access
[Huawei-GigabitEthernet0/0/2]port default vlan 4   //把2口加
入vlan4
路由器ip也按规划配置，此处省略

二，动态路由
基于某种路由协议实现，适合大型网络，减少管理任务
宣告 对外告知自身所直连的网段
在三层交换机中配置动态路由：
[Huawei]ospf
[Huawei-ospf-1]area 0    //进入区域0 (第一个区域，表示开始使用ospf)
[Huawei-ospf-1-area-0.0.0.0]network 192.168.1.0 0.0.0.255  //依次宣告
自身所直连的网段
[Huawei-ospf-1-area-0.0.0.0]network 192.168.2.0 0.0.0.255   //宣告2网段
[Huawei-ospf-1-area-0.0.0.0]network 192.168.3.0 0.0.0.255   //宣告3网段
[Huawei-ospf-1-area-0.0.0.0]network 192.168.4.0 0.0.0.255   //宣告4网段
在路由器中配置动态路由：
[Huawei]ospf
[Huawei-ospf-1]area 0
[Huawei-ospf-1-area-0.0.0.0]network 192.168.4.0 0.0.0.255   //宣告4网段
[Huawei-ospf-1-area-0.0.0.0]network 192.168.5.0 0.0.0.255   //宣告5网段
[Huawei-ospf-1-area-0.0.0.0]display this    //可以查看当前视图的配置，比如目前
在ospf中，就可以看到ospf中都敲了什么命令
[Huawei-ospf-1-area-0.0.0.0]undo network 192.168.4.0 0.0.0.255  //如果有错
可以删除        

三，传输层
定义了端口号   65536个   0~65535  
定义了 tcp、udp两个协议

tcp 传输控制协议
可靠   效率低   面向连接

使用tcp传输数据时会用到一些标记：
syn 打算与对方建立连接
ack 确认
fin  打算与对方断开连接

三次握手
1，用户syn--->服务器
2，服务器 ack , syn --->用户
4，用户ack--->服务器

四次断开
1，用户fin--->服务器
2，服务器ack --->用户
3，服务器fin --->用户
4，用户ack--->服务器

使用tcp协议传输数据的常见服务
  

udp 用户数据报协议
不可靠   效率高   无连接
使用udp传输数据的常见服务

三，acl 访问控制列表
可以对已经建立好的网络进行管控的工具
基本acl  列表号 2000~2999  根据数据源ip地址进行控制
高级acl  列表号 3000~3999  根据数据源ip、目标ip、协议
端口进行控制

按图搭建拓扑，首先实现全网互通，然后按下列要求配置acl

禁止2.1与1.1进行数据通信，不能影响其他主机的通信

[Huawei]acl 2000    //创建acl 列表号是2000
[Huawei-acl-basic-2000]rule deny source 192.168.2.1 0.0.0.0   
//创建规则拒绝源地址是192.168.2.1的数据通过
[Huawei-acl-basic-2000]display this    //查看当前视图配置，
可以看到规则号码
[Huawei-acl-basic-2000]undo rule 5  //如果规则写错，可以根
据规则号码删除
[Huawei-acl-basic-2000]in g0/0/1   //进入1口，注意该接口
不要照抄，要检查是否为最接近2.1的接口
[Huawei-GigabitEthernet0/0/1]traffic-filter inbound acl 2000  
//定义过滤数据是入方向，并应用之前创建的acl2000
[Huawei-GigabitEthernet0/0/1]undo traffic-filter inbound

允许2.1与1.1进行通信，禁止其他设备访问1.1
[Huawei-GigabitEthernet0/0/1]acl 2001   //创建新acl，列表号
是2001
[Huawei-acl-basic-2001]rule permit source 192.168.2.1 0  //创建
规则，允许2.1通过
[Huawei-acl-basic-2001]rule deny source any  //拒绝所有设备通过
[Huawei-acl-basic-2001]in g0/0/1  //进入应用acl的接口
[Huawei-GigabitEthernet0/0/1]undo traffic-filter inbound   //在接
口取消之前的acl2000
[Huawei-GigabitEthernet0/0/1]traffic-filter inbound acl 2001   //应
用新的acl

问题
参考答案：
1，传输层有哪些协议，各有什么特点？
TCP 传输控制协议
可靠的、面向连接的协议 传输效率低
UDP 用户数据报协议
不可靠的、无连接的服务 传输效率高
2，描述TCP三次握手以及四次断开的基本过程
三次握手
1，用户发送syn给服务器
2，服务器发送ack与syn给用户
3，用户发送ack给服务器
四次断开
1，用户发送fin给服务器
2，服务器发送ack给用户
3，服务器发送fin给用户
4，用户发送ack给服务器
3，SMTP、DNS、SSH、TFTP、NTP分别是什么协议，使用了什么端口，在传输层使用什么协议？
参考答案
SMTP 简单邮件传输协议 端口号25 tcp
DNS 域名系统 端口号53 tcp/udp
ssh 远程登录 端口号22 tcp 
TFTP 简单文件传输协议 端口号69 udp
NTP 网络时间协议 端口号123 udp
FTP 文件传输协议 端口号21 tcp
HTTP 超文本传输协议 端口号80 tcp
HTTPS 超文本传输协议带加密 端口号443 tcp
4，网络设备中的ACL技术常见类型有哪些，各有什么区别？
参考答案
基本ACL
基于源IP地址过滤数据包
基本访问控制列表的列表号是2000~2999
高级ACL
基于源IP地址、目的IP地址、指定协议、端口来过滤数据包
高级访问控制列表的列表号是3000~3999

-----------------------------------------------------------------------------
11.2
NAT

私有ip地址
A 10.0.0.0~10.255.255.255
B 172.16.0.0~172.31.255.255
C 192.168.0.0~192.168.255.255
##在私有ip范围外的，都是共有地址

VRRP组成员角色
主（Master）路由器
备份（Backup）路由器
虚拟（Virtual）路由器
##出现两个主（Master）的情况，一个是因为vrid 不同，一个是因为两个设备主和设备，并没有互通

使用高级acl

基本acl  2000~2999  源地址  
高级acl  3000~3999  源地址、目标地址、协议、端口

禁止2.2访问1.1的网站服务，但不影响其他服务
[Huawei]acl 3000   //创建(进入)acl3000
[Huawei-acl-adv-3000]rule deny tcp source 192.168.2.2 0 destination
 192.168.1.1 0 destination-port eq 80   //拒绝2.2访问1.1的tcp的80
端口
[Huawei-acl-adv-3000]in g0/0/1   //进入距离2.2比较近的接口
[Huawei-GigabitEthernet0/0/1]undo traffic-filter inbound   //删除原有acl
[Huawei-GigabitEthernet0/0/1]traffic-filter inbound acl 3000   //重新在
接口应用acl3000

禁止2.1访问1.1的ftp服务，但不影响其他服务
[Huawei-acl-adv-3000]rule deny tcp source 192.168.2.1 0 destination
 192.168.1.1 0 destination-port eq 21  //拒绝2.1访问1.1的tcp
的21端口

使用acl时，同接口的同方向只能一次应用一个acl列表

nat 网络地址转换，可以将私有地址转换为全球唯一的公有
地址

广域网  外网
局域网  内网

ip地址  32位  拥有 42亿+的数量并且已经枯竭

私网有ip地址
A 10.0.0.0~10.255.255.255
B 172.16.0.0~172.31.255.255
C 192.168.0.0~192.168.255.255

nat常见的两种用法：
静态转换  1对1  双向通信
easy ip    多对1  单向通信


首先按图配置ip
然后在路由器配置静态nat
使用nat前要进入外网接口
[Huawei-GigabitEthernet0/0/0]nat static global 100.0.0.2 inside 192.168.2.1  
//使用静态nat技术，将内部的2.1与外部的公网地址100.0.0.2进行相互
转换然后测试使用192.168.2.1 ping 100.0.0.10可以互通说明地址转换成功
如果配置错误可以用undo删除，比如：
undo nat static global 100.0.0.2 inside 192.168.2.4

[Huawei-GigabitEthernet0/0/0]nat static global 100.0.0.3 inside 192.168.2.2


在路由器配置easy ip，让所有的内部主机仅仅利用唯一的一个公网地址
100.0.0.1访问外网
[Huawei]acl 2000   //通过acl定义允许访问外网的设备
[Huawei-acl-basic-2000]rule permit source any  //这里放行所有设备，如果
将any换成192.168.2.0 0.0.0.255则是仅仅允许2.0网段的设备访问外网

[Huawei-acl-basic-2000]in g0/0/0  //进入0接口(外网接口)
[Huawei-GigabitEthernet0/0/0]undo nat static global 100.0.0.3 inside 192.168.2.2 
 //删除已有的静态nat
[Huawei-GigabitEthernet0/0/0]undo nat static global 100.0.0.2 inside 192.168.2.1
[Huawei-GigabitEthernet0/0/0]nat outbound 2000  //应用nat (easy ip方式)

vrrp  虚拟路由冗余协议
vrrp能够在不改变组网的情况下，将多台路由器虚拟成一个虚拟路由器，通过配置虚拟路由器的IP地址为默认网关，实现网关的备份

三层交换机配置，第一台
[Huawei]sysname sw1		//修改主机名为sw1	
[sw1]undo info-center enable   //关闭日志
[sw1]in vlan 1  //进入vlan1
[sw1-Vlanif1]ip add 192.168.1.252 24  //配置ip
[sw1]vlan 2    //创建vlan2
[sw1-vlan2]in vlan 2   //进入vlan虚拟接口
[sw1-Vlanif2]ip add 192.168.2.2 24  //配ip
[sw1-Vlanif2]in g0/0/2  //进入要配ip的接口
[sw1-GigabitEthernet0/0/2]port link-type access
[sw1-GigabitEthernet0/0/2]port default vlan 2

另外一台s5700		
<Huawei>sys
[Huawei]sysname sw2
[sw2]undo info-center enable 
[sw2]in vlan 1
[sw2-Vlanif1]ip add 192.168.1.253 24
[sw2]vlan 3    //创建vlan3
[sw2-vlan3]in vlan 3   //进入vlan虚拟接口
[sw2-Vlanif3]ip add 192.168.3.2 24  //配ip
[sw2-Vlanif3]in g0/0/2  //进入要配ip的接口
[sw2-GigabitEthernet0/0/2]port link-type access
[sw2-GigabitEthernet0/0/2]port default vlan 3

然后分别在路由器与三层交换机上配置ospf
[Huawei]ospf    //在路由器配置ospf
[Huawei-ospf-1]area 0
[Huawei-ospf-1-area-0.0.0.0]network 192.168.2.0 0.0.0.255
[Huawei-ospf-1-area-0.0.0.0]network 192.168.3.0 0.0.0.255
[Huawei-ospf-1-area-0.0.0.0]network 192.168.4.0 0.0.0.255
[sw1]ospf  //在sw1配置ospf
[sw1-ospf-1]area 0
[sw1-ospf-1-area-0.0.0.0]network 192.168.1.0 0.0.0.255
[sw1-ospf-1-area-0.0.0.0]network 192.168.2.0 0.0.0.255
[sw2]ospf   //在sw2配置ospf
[sw2-ospf-1]area 0
[sw2-ospf-1-area-0.0.0.0]network 192.168.1.0 0.0.0.255
[sw2-ospf-1-area-0.0.0.0]network 192.168.3.0 0.0.0.255

在两台三层交换机配置vrrp
[sw1]in vlan 1  //vrrp需要在接口中配置，进入vlan接口
[sw1-Vlanif1]vrrp vrid 1 virtual-ip 192.168.1.254  //开启vrrp功能，组号
是1，虚拟设备的ip是1.254
[sw2]in vlan 1   //另外这台设备配置一样的内容
[sw2-Vlanif1]vrrp vrid 1 virtual-ip 192.168.1.254
[sw2-Vlanif1]display this  //查看当前视图配置
<sw1>display vrrp brief   //分别在两台三层交换机查看vrrp状态，看
到一台是Master一台是backup即可

VRRP组成员角色
主（Master）路由器
备份（Backup）路由器
虚拟（Virtual）路由器

如果打算将某个设备定义为主：
in vlan 1
vrrp vrid 1 priority 105   //修改vrrp优先级，默认值
是100，越高越优先成为主

问题
1，NAT的作用是什么，有哪些优点？
通过将内部网络的私网IP地址翻译成全球唯一的公网IP地址，使内部网络可以连接到互联网等外部网络上。
优点有节约公网ip、处理地址重叠(使用相同私网地址的主机不会冲突，可以利用不同的公网ip互通)、增加安全
2，私有IP地址分类有哪些？
A类 10.0.0.0~10.255.255.255
B类 172.16.0.0~172.31.255.255
C类 192.168.0.0~192.168.255.255
3，NAT常用实现方式有哪些，各有什么特点？
静态转换 可以实现1个私网地址对1个公网地址的转换 是双向通讯 
Easy IP 可以实现多个私网地址对1个公网地址的转换 是单向通讯 
4，VRRP是什么，具体的作用是什么？
vrrp是虚拟路由冗余协议
可以实现网关的冗余备份，可以保障网关设备出现故障的情况下不会对网络造成重大影响。
5，VRRP中设备的身份有哪些？
主(master)路由器，备份(backup)路由器，虚拟(virtual)路由器
6，VRRP通过什么定义路由设备的主备身份？
可以修改优先级来决定
-----------------------------------------------------------------------------
11.3
一，vrrp 负载均衡


首先按图组建拓扑，所有交换机创建vlan2
然后将所有接口都配置成tunk中继链路：这里是以第1台为例
[sw1]port-group 1    //创建接口组，组号是1
[sw1-port-group 1]group-member gigabitethernet 0/0/1 to gigabitethernet 0/0/3  
//添加组成员，从g0/0/1到g0/0/3一共3个接口
[sw1-port-group 1]port link-type trunk   //将这些接口配置为中继链路
[sw1-port-group 1]port trunk allow-pass vlan all    //放行所有vlan 的数据
所有交换机均按照以上方法将所有接口配置为trunk，注意sw3与sw4是E口

实现vrrp负载均衡要做到：
sw1  vlan1 主  vlan2 备
sw2  vlan1 备  vlan2 主


[sw1]in vlan 1     //第一台三层交换机
[sw1-Vlanif1]vrrp vrid 1 virtual-ip 192.168.1.254   //在sw1配置vrrp 
[sw1-Vlanif1]ip address  192.168.1.252 24  //配置ip
[sw1-Vlanif1]vrrp vrid 1 priority 105   //设置优先级称为vlan1的主
[sw1]in vlan 2
[sw1-Vlanif2]ip address 192.168.2.252 24	 
[sw1-Vlanif2]vrrp vrid 2 virtual-ip 192.168.2.254   //开启vrrp功能

[sw2]in vlan 1    //第二台三层交换机
[sw2-Vlanif1]ip address 192.168.1.253 24   //配置ip
[sw2-Vlanif1]vrrp vrid 1 virtual-ip 192.168.1.254  //开启vlan1的vrrp
[sw2-Vlanif1]in vlan 2
[sw2-Vlanif2]ip address 192.168.2.253 24
[sw2-Vlanif2]vrrp vrid 2 virtual-ip 192.168.2.254
[sw2-Vlanif2]vrrp vrid 2 priority 105    //为vlan2的vrrp设置优先级
配置完毕后使用display vrrp brief 检查，每个5700的vrrp是
一主一备即可

---------------------------------------------------------------
远程管理路由器：

[Huawei]aaa    //在路由器进入管理账户的视图
[Huawei-aaa]local-user test01 password cipher 123456  //创建账户test01密码123456
[Huawei-aaa]local-user test01 service-type ssh  //开启该用户的ssh远程登录功能
[Huawei-aaa]local-user test01 privilege level 3  //创建的账户操作级别默认是0，0是参
观级别，如果级别序号是1可以对系统进行简单维护，如果是2，可以使用一部分配
置命令，改为3可以成为管理员对设备进行任意配置。
[Huawei-aaa]quit   //返回上一视图
[Huawei]user-interface vty 0 4 //进入远程接口，0是允许登录的第一个账户 0 4是
允许5人登陆
[Huawei-ui-vty0]authentication-mode aaa   //允许远程时使用之前aaa中创建的
test01账户，相当于激活
[Huawei-ui-vty0]protocol inbound ssh   //放行ssh数据
[Huawei-ui-vty0]quit
[Huawei]stelnet server enable    //开启远程服务   

然后将sw1做一台pc，用来远程登录路由器
[sw1]ssh client first-time enable    //开启作为远程登录的客户端功能
[sw1]stelnet 192.168.1.1    //远程登录路由器
Please input the username:test01   //输入用户名
Trying 192.168.1.1 ...
Press CTRL+K to abort
Connected to 192.168.1.1 ...
The server is not authenticated. Continue to access it? [Y/N] :y    //首次连接是否同意？ 敲y回车
Save the server's public key? [Y/N] :y   //保存秘钥，敲y回车
The server's public key will be saved with the name 192.168.1.1 ease wait..
Enter password:      //输入密码123456后即可连接到路由器，此时敲的命令相当
于在路由器上执行(由于模拟器软件原因可能远程后敲命令时较卡，敲命令会有延
迟，属于正常现象，如果在真实设备则不会有此问题)

如果在交换机配置远程登录功能，需要比路由器多执行2条命令:
[sw1]ssh user test01 authentication-type password   //test01账户远程连接使用ssh
协议时要输入密码进行认证
[sw1]ssh user test01 service-type stelnet   //在系统视图指定test01账户支持的服
务类型为stelnet

----------------------------------------------------------------
网速
100M位/s(秒)	   12.5M字节/s(秒)		
1000M位/s(秒)     125M字节/s(秒)	


存储单位
1字节 byte   =  8位 bit
1KB			=   1024字节
1MB		    =   1024KB
1GB			=   1024MB
1TB			=   1024GB
1PB			=   1024TB
1EB			=   1024PB

数制 用来计数的方式
二进制    01
八进制    01234567     
十进制    0123456789
十六进制  0123456789ABCDEF

10B  (10)2   二进制
10O  (10)8   八进制
10D  (10)10  十进制
10H  (10)16  十六进制 

十进制与二进制相互转换的方式：
可以利用该数字序列
128   64   32   16   8    4    2   1

比如将十进制数 19转换成二进制，可以考虑上面哪些数字相加是19
将需要的数字用1标记，最终可以得出结果
128   64   32   16   8    4    2   1
0     0    0    1    0    0    1   1      比如这里16+2+1=19就将这几个数字标记1，然后得出00010011是答案，前面的0可以忽略最后是10011

反过来，如果要计算二进制数字10011的十进制是多少，那么可以直接将二进制标记在上述数字序列，最中计算对应的数字之和即可
----------------------------------------------------------
子网划分，可以将1个网段根据需求划分成若干网段

1，准备网段，将子网掩码转换为二进制
192.168.1.0  /24    255.255.255.0
11111111. 11111111. 11111111.00000000

2，根据公式计算划分过程
2的n次方=要划分的网段数量
n是借用子网掩码主机位的个数

192.168.1.0  /25    255.255.255.0
11111111. 11111111. 11111111.10000000

3, 将需要划分子网的ip地址转换成二进制
128  64  32  16   8   4   2   1

192.168.1.00000000   第1个网段
255.255.255.10000000   这里的1是根据之前公式计算的n的值
192.168.1.0~192.168.1.127    完整范围
192.168.1.1~192.168.1.126    可用范围（掐头去尾）
/25 或 255.255.255.128      掩码

192.168.1.10000000   第2个网段
255.255.255.10000000
192.168.1.128~192.168.1.255    完整范围  
192.168.1.129~192.168.1.254    可用范围（掐头去尾） 
/25 或 255.255.255.128        子网掩码

如果要划分4个网段：
192.168.1.00000000  
255.255.255.11000000   这里的11是根据之前公式计算的n的值
192.168.1.0~63   192.168.1.1~62      第1个网段 
/26 或 255.255.255.192        子网掩码

192.168.1.01000000
255.255.255.11000000
192.168.1.64~127  192.168.1.65~126   第2 个网段
/26 或 255.255.255.192        子网掩码

192.168.1.10000000
255.255.255.11000000
192.168.1.128~191  192.168.1.129~190  第3个网段
/26 或 255.255.255.192        子网掩码

192.168.1.11000000
255.255.255.11000000
192.168.1.192~255  192.168.1.193~254  第4个网段
/26 或 255.255.255.192        子网掩码

---------------------------------------------------------------------------
使用之前子网划分的地址连接4个vlan    

<Huawei>sys					
[Huawei]undo in en
[Huawei]vlan batch 2 3 4
[Huawei]in vlan1
[Huawei-Vlanif1]ip add 192.168.1.62 26
[Huawei-Vlanif1]in vlan 2
[Huawei-Vlanif2]ip add 192.168.1.126 26
[Huawei-Vlanif2]in vlan 3
[Huawei-Vlanif3]ip add 192.168.1.190 26
[Huawei-Vlanif3]in vlan 4
[Huawei-Vlanif4]ip add 192.168.1.254 26
[Huawei-Vlanif4]in g0/0/2
[Huawei-GigabitEthernet0/0/2]port link-type access
[Huawei-GigabitEthernet0/0/2]port default vlan 2
[Huawei-GigabitEthernet0/0/2]in g0/0/3
[Huawei-GigabitEthernet0/0/3]port link-type access
[Huawei-GigabitEthernet0/0/3]port default vlan 3
[Huawei-GigabitEthernet0/0/3]in g0/0/4
[Huawei-GigabitEthernet0/0/4]port link-type access
[Huawei-GigabitEthernet0/0/4]port default vlan 4

----------------------------------------------------------------
了解ipv6地址
ipv4  32位   42亿+   点分十进制
ipv6  128位  接近无穷的地址空间  冒分十六进制

2001:0002:0003:0004:0005:0006:0007:0008 /64  //一个
ipv6地址，由8段4位的十六进制组成，后续的64代表
前缀长度
2001:2:3:4:5:6:7:8 /64   //上述地址可以简写

2002:0000:0000:000A:0000:0000:0000:0001 /64   //这样的地址更容易缩减
2002:0:0:A:0:0:0:1   //初步缩减
2002::A:0:0:0:1   //最终状态
2002:0:0:A::1   //或者这样也行


按图组建网络，使用ipv6地址配通
[Huawei]ipv6       //开启ipv6
[Huawei]in g0/0/0
[Huawei-GigabitEthernet0/0/0]ipv6 enable   //在接口中也要开启ipv6
[Huawei-GigabitEthernet0/0/0]ipv6 address 2001:0:0:1::254 64  //配置ipv6地址，前缀长度64
[Huawei-GigabitEthernet0/0/0]in g0/0/1
[Huawei-GigabitEthernet0/0/1]ipv6 enable
[Huawei-GigabitEthernet0/0/1]ipv6 address 2001:0:0:2::254 64

问题
1，在多vlan的网络中，如果配置了vrrp技术，如何使效果达到最佳？
将主路由设备(master)配置在不同网关，实现vrrp负载均衡。

2，远程登录协议有哪些？区别是什么?
telnet与ssh两种协议可以实现远程登录功能
区别是telnet使用明文方式传递数据，可以在内部网络等安全要求不高的场合使用，而ssh使用加密方式传递数据，可以放心用在各种环境的网络中

3 计算机中常用数制有哪些？
二进制、十进制、十六进制、八进制

4，一个4GB大小的文件，如果利用200Mb带宽的网络传递，最快需要多久完成？
首先使用200除以8得出该网络每秒最大可以传递25MB的数据
4乘以1024等于4096MB，再用4096除以25得出时间是163.84秒
大约不到3分钟即可完成传递

5，子网划分的原因是？
满足不同网络对IP地址的需求
节省IP地址
-----------------------------------------------------------------------------
11.4
-----------------------------------------------------------------------------
11.5
非交互指令
交互指令

bash有点：tab键，快捷键，历史命令，支持别名，管道，重定向
ls abcd 2> /t.txt 
##错误输出的重定向
ls test01.sh > /t.txt
##正确输出的重定向
ls abcd test01.sh > /t.txt
##只会把正确的输出内容重定向到t.txt文本，错误的则直接显示在屏幕上，如果是"2>"重定向，则相反
ls abcd test01.sh &> /dev/null
##将所有显示内容全部丢弃到“黑洞”，即不显示不保存任何内容。

-------------------------------------------------------------------------------
11.8





------------------------------------------------------------------------------
11.9
while循环
while : 可以实现死循环“：”代表永远正确

netstat -ntulp 
##systemctl status 无法查询的服务，则用netstat查询

netstat -ntulp | grep -q nginx
##grep -q 等同于 &> /dev/null 扔黑洞


字符串处理
1字符串的截取
${变量的名称:截取位置:截取长度}

--------------------------------------------------------------------------------
11.10
.不能搜索空行
.*所有都能搜索到，包括空行


---------------------------------------------------------------------------------
11.11
awk -F: '/var/{print “ceshi”}' user  ##每找到匹配/var/的行，就输出一次ceshi
#在awk里 常量需要加"",如果没加会不显示，变量则直接用，""也可以用于添加空格

awk 'BEGIN{print "User\tUID\tHome"}'
[root@svr7 opt]# awk -F: 'BEGIN{print "User\tUID\tHome"}{print $1"\t"$3"\t"$6}END{print "一共"NR"行"}' user 
#\t为制表符，会自己生成一定距离的空格

a=["test"]
#如果数组内名字为常量，需要加"",如果是变量和数字则不用加

------------------------------------------------------------------------------
11.12
/usr/local/nginx/sbin/nginx -s reload
##-s reload作用等同于，先关闭、再开启。只有nginx服务器开启时候才可以reload重新加载，否则会报错
/usr/local/nginx
#nginx默认安装目录

#nginx.conf配置文件内http有关的服务，要写在http{}的范围内
        ssl_certificate      cert.pem;    //证书，包含公钥
        ssl_certificate_key  cert.key;     //私钥
#公钥，私钥必须要在配置文件同目录，即conf目录下
[root@proxy nginx]# curl -k http://www.c.com
#-k 是专门用来查看加密网站的选项
一，nginx基本应用
Nginx（"engine x"）
是俄罗斯人编写的HTTP服务工具
1，环境准备
yum -y install vim   //安装vim编辑器
yum list | grep bash   //查询名字和bash有关的软件包
yum -y install bash-completion   //安装支持tab键的软件包
装好之后，使用exit退出，重新登录才生效
Yum -y install net-tools  //安装网络相关软件包
yum -y install  psmisc   //安装支持killall命令的软件

yum -y install  gcc  make  //安装编译工具
yum -y install  pcre-devel  //安装可以让nginx支持正则的软件包
yum -y install  openssl-devel  //安装可以让nginx支持安装加密网站
的软件包

然后将lnmp_soft.tar.gz 传入虚拟机的root家目录
scp  /linux-soft/2/lnmp_soft.tar.gz  192.168.2.5:

cd    //到家目录
tar -xf lnmp_soft.tar.gz    //释放tar包
cd lnmp_soft/
tar -xf nginx-1.17.6.tar.gz   //释放nginx
cd nginx-1.17.6/    //进入nginx目录
./configure  --prefix=/usr/local/nginx  --user=nginx  --with-http_ssl_module
//配置，--prefix是指定安装路径，--user是指定用户  
--with-http_ssl_module是安全网站模块
make   //编译
make  install  //安装					
ls  /usr/local/nginx/     //检测看到4个目录则成功     


ls  /usr/local/nginx/    //安装好之后查看目录
conf 存放配置文件     sbin  存放主程序
html 存放网站页面     logs  存放日志

--------------------------------------------------------------------

11.15
静态网站 在不同环境下 网站内容不会变化
动态网站 在不同环境下 网站内容有可能发生变化
LNMP 环境
L linux,N nignx,M mariadb(mysql),P PHP

nginx的重新部署
killall nginx  //停止nginx程序
rm -rf  /usr/local/nginx    //删除nginx原有目录
# rm -rf nginx-1.17.6
# tar -xf nginx-1.17.6.tar.gz
# cd nginx-1.17.6/
#yum -y install gcc make pcre-devel openssl-devel 
# //安装编译工具 //安装可以让nginx支持正则的软件包 //安装可以让nginx支持安装加密网站的软件包
#如果不小心修改了nginx-1.17.6的配置文件，则执行此步骤
cd  /root/lnmp_soft/nginx-1.17.6
./configure  --with-http_ssl_module    //配置
make   //编译
make install   //安装

在nginx.conf配置文件中的server{}中添加rewrite ^/a.html$ /b.html;
#^和$的意思代表为，必须以a.html结尾，同时前面为空，不能有东西

rewrite /(.*) http://www.tmooc.cn/$1;   
先复制他访问的地址，再粘贴到新网站上
#(.*)意思为把原网站的页面内容复制，例如http://192.168.2.5/abc.html当中的"abc.html"地址会被复制；"$1"的意思为把"(.*)"复制的地址，粘贴到新网站的后面。

if ($http_user_agent ~* firefox){
rewrite /(.*) /firefox/$1;
}
内置变量http_user_agent储存浏览器类型
~*忽略分大小写

---------------------------------------------------------------------------
11.16
Tcp/ip五层参考模型以及对应的典型设备
应用层		 pc
传输层       防火墙
网络层		 路由器
数据链路层	 交换机
物理层		 网卡
nginx功能
网站服务，网站代理（网站业务），四层代理（其他业务，传输层）

默认状态下的，是轮训分配方式，weight默认为1，分配的数字为总量的占比，即weight=1,weight=2和weight=2，weight=4的两者比例相同，均为一个执行1次，一个执行2次
upstream web {
server 192.168.2.100:80;
server 192.168.2.200:80 weight=2;}

stream //创建新业务
upstream 名字  //创建集群

四层代理功能
stream {                     //创建新业务要写在http模块范围外
upstream backend {
server 192.168.2.100:22;
server 192.168.2.200:22;
}
server {
listen 12345;               //任意写没有用过的端口
proxy_pass backend;
}
}

Active connections：当前活动的连接数量（当前有多少用户访问该网站）。
Accepts：已经接受客户端的连接总数量。 //把人放进来
Handled：已经处理客户端的连接总数量。 //进来后握手，通常情况下Accepts和Handled的数字一样，如果A数字大于H太多，则表明用户访问的网络不好。
Requests：客户端发送的请求数量。       //只要用户刷新or访问一次，数字就会增加
Reading：当前服务器正在读取客户端请求头的数量。 //服务器一瞬间已经读完刷新的请求，所以显示0
Writing：当前服务器正在写响应信息的数量。       //读到请求后，正在处理的数量
Waiting：当前多少客户端在等待服务器的响应。     //有多少人在排队等待服务器

---------------------------------------------------------------------------------------------------
11.17


location ~* \.(jpg|html|txt|mp3)$ {    // "~"是使用正则表达式，~*是忽略大小写
#当发现用户访问的是以.jpg或者.html等等结尾的页面时
expires 30d;
}


vim /etc/security/limits.conf   //打开配置文件实现永久修改
修改第53、54行
*               soft    nofile          100000      //软限制，我现在想用最多100000(数字最大10W)
*               hard    nofile          100000      //硬限制，最大可以达到100000(数字最大10W)


快速配置LNMP
在proxy主机家目录下将lnmp_soft.tar.gz拷贝到web1
[root@proxy ~]# scp lnmp_soft.tar.gz root@192.168.2.100:root
首先在web1主机：
yum -y install gcc make pcre-devel openssl-devel //安装编译工具;让nginx支持正则;让nginx支持安装加密网站
[root@web1 ~]# tar -xf lnmp_soft.tar.gz
[root@web1 ~]# cd lnmp_soft/
tar -xf nginx-1.17.6.tar.gz
cd  nginx-1.17.6/
./configure   //配置
make   //编译
make  install   //安装
yum -y install mariadb mariadb-server mariadb-devel //安装数据库相关软件包;数据库,客户端服务,依赖包
yum -y install php php-mysql php-fpm   //安装php相关软件包;解释器,nginx解析php服务包,关联软件包
systemctl start mariadb
systemctl start php-fpm

[root@web1 nginx]# vim conf/nginx.conf  //修改配置文件,实现动静分离,
修改65~71行为以下状态
vim conf/nginx.conf    //修改配置文件,实现动静分离,修改65~71行为以下状态
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
        #   fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
            include        fastcgi.conf;
        }
配置好lnmp之后
[root@web1 nginx]# systemctl stop httpd
[root@web1 nginx]# sbin/nginx    //开启服务
配置好lnmp之后
cd  ~/lnmp_soft/php_scripts/
tar -xf php-memcached-demo.tar.gz    //释放带登录功能的网页
cp -r php-memcached-demo/* /usr/local/nginx/html/   //拷贝页面
到nginx中使用火狐浏览器访问http://192.168.2.100/index.php  可以
看到有登录界面的网页

##火狐浏览器强制刷新ctrl+F5
systemctl  start  memcached   //开启服务
telnet  127.0.0.1  11211   //连接memcache，进行检测 11211是memcached端口号


--------------------------------------------------------------------------------------
11.18
tomcat创建虚拟主机 
<Host  name=域名 appBase=网站页面位置  >
</Host>
#要在<Engine></Engine>框架内，切记注意不要写到其他<Host></Host>内

Context  path  匹配用户访问路径     //用户输入了什么地址
	   docBase 定义页面位置       //用户输入了地址后，要找的网页路径
<Host name="www.b.com" appBase="web_b"
      unpackWARs="true" autoDeploy="true">
<Context path="" docBase="" />  //如果docBase=""，所以网页的路径为"web_b路径下的位置，无需在默认的web_b/ROOT下；如改为docBase="abc"，"abc"为相对路径，则表示为在web_b/abc目录下；如改为docBase="/abc"，"/abc"为绝对路径，则表示为在根/abc目录下
<Context path="" docBase="abc" />    //web_b目录下的abc目录下，即web_b/abc
<Context path="" docBase="/abc" />   //根目录下的abc目录下，即/abc
<Context path="/test" docBase="/abc" /> //如用户输入/test地址后 ，则找根目录下的abc路径，即/abc
</Host>

通过maven构建项目 


---------------------------------------------------------------------------------------
11.19
tomcat加密
http 80 无加密
https 8443 
在conf/server.xml配置文件内修改
<Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS"
keystoreFile="/usr/local/tomcat/keystore"
keystorePass="123456"  />   
#添加keystoreFile(指定公钥私钥文件)和keystorePass(公钥私钥文件密码)
]# keytool  -genkeypair  -alias  tomcat  -keyalg RSA  
-keystore  /usr/local/tomcat/keystore  //创建密钥对，别名是tomcat，加密算法是RSA，最后是存储位置
]# bin/shutdown.sh    //重启tomcat
]# bin/startup.sh
]#echo  "tomcat-https~~"  >  webapps/ROOT/index.html   //准备测试页面
]#curl  -k  https://192.168.2.5:8443   //访问安全加密网站

源码包转换为rpm包
[root@proxy ~]# yum -y install  rpm-build    //安装制作rpm包的工具
[root@proxy ~]# rpmbuild  -ba  nginx.spec  //制作rpm包，但是没有配
置文件会报错，报错也需要敲，会产生所需的目录
[root@proxy ~]# ls    //可以看到一个rpmbuild目录
[root@proxy ~]# cp  ~/lnmp_soft/nginx-1.17.6.tar.gz  rpmbuild/SOURCES/  
//拷贝源码包到rpmbuild的工作目录
[root@proxy ~]# yum -y install  gcc  make  pcre-devel  openssl-devel
[root@proxy ~]# vim  rpmbuild/SPECS/nginx.spec  //编写将nginx转换为rpm包的配置文件
Name:nginx     //软件名
Version:1.17.6    //版本
Release:1       //发布的rpm包的版本
Summary:test~    //简单描述
#Group:
License:GPL    //授权协议 ，GPL表示自由软件
URL:www.abc.com    //网址
Source0:nginx-1.17.6.tar.gz     //源码包 
#BuildRequires:   
#Requires:
%description    //详细描述
test~ test~ 
%post      //可以添加安装rpm包之后执行的命令，不是必须
useradd  nginx
%prep
%setup -q
%build
./configure  //配置，如果要添加选项或者模块可以继续写
make %{?_smp_mflags}     //编译
%install
make install DESTDIR=%{buildroot}    //安装
%files
%doc
/usr/local/nginx/*     //将改路径下文件打包成rpm
%changelog
rpmbuild  -ba  rpmbuild/SPECS/nginx.spec  //根据上述文件制作rpm包
ls  rpmbuild/RPMS/x86_64/nginx-1.17.6-1.x86_64.rpm  //查看最终结果，
已经产生nginx的rpm包则成功

VPN  虚拟专用网
关闭防火墙，selinux
1，使用gre技术搭建vpn，适用于linux环境
[root@web1 ~]# modprobe  ip_gre    //在内核中开启gre功能
[root@web1 ~]# lsmod | grep  gre  //检查，可以看到gre字样信息即可
ip tunnel add tun0 mode gre remote 192.168.2.200 local 192.168.2.100    
#创建vpn隧道，名字叫tun0，mode是使用gre技术，与2.200连接，自己ip是2.100
ip  tunnel  del  tun0   //如果错误，可以删除重配
ip addr add 10.10.10.100/8 peer 10.10.10.200/8 dev tun0  
#在tun0隧道(dev)中使用私有ip地址，本机是10.100 ，对面(peer)是10.200
ip addr del 10.10.10.100/8 peer 10.10.10.200/8 dev tun0  //如果错误可以删除
ip link set tun0 up    //激活tun0
ip addr show tun0   //查看


2，使用pptpd搭建vpn，支持windows环境
[root@web1 ~]# cd  ~/lnmp_soft/vpn/
[root@web1 vpn]# yum -y install pptpd-1.4.0-2.el7.x86_64.rpm       //安装vpn工具
vim  /etc/pptpd.conf   //修改102、103行
localip  192.168.2.100   //本机ip
remoteip  10.10.10.10-18  
#给windows客户机分配的ip(vpn隧道内使用的地址，可以自定义)，范围是10.10.10.10~10.10.10.18
vim  /etc/ppp/options.pptpd   //修改配置，定义dns，66行将#去掉
vim  /etc/ppp/chap-secrets   //定义windows客户机的用户名和密码
tom  *  123456  *     //另起一行创建用户tom，配置密码123456
systemctl  restart  pptpd   //开服务
netstat  -ntulp  |  grep  pptpd    //检查

3，使用xl2tp搭建vpn，支持windows环境，更安全





编写Unit文件，使systemctl命令控制nginx
  [Unit]
  Description=The nginx HTTP Server
  After=network.target remote-fs.target nss-lookup.target   //在网络服务、网络文件服务、域名服务的程序启动之后再启动nginx
  Documentation=man:httpd(8)                     //可删除
  Documentation=man:apachectl(8)			//可删除
  
  [Service]
  Type=notify					 //nginx是多进程类型程序，要设置为forking
  EnvironmentFile=/etc/sysconfig/httpd	//可删除,变量
  ExecStart=/usr/local/nginx/sbin/nginx  //当执行了systemctl start nginx之后执行的命令
  ExecReload=/usr/local/nginx/sbin/nginx -s reload   //当执行了systemctl reload nginx之后执行的命令
  ExecStop=/bin/kill -WINCH ${MAINPID} 
  
  [Install]
  WantedBy=multi-user.target




----------------------------------------
总结
网络
Vlan access trunk 链路聚合 直连路由 静态路由 动态路由 vrrp Acl nat


---------------------------------------------------------------------------
11.22
### git的工作原理

- git重要的三个工作区域：
  - 工作区：写代码的目录。就是项目代码存放的目录。
  - 暂存区：工作区与版本库之间的缓冲地带。位置是`.git/index`
  - 版本库：工作区快照存放的目录。在工作区下，名为`.git`的目录

# 安装中文支持
[root@develop myweb]# yum list | grep zh
[root@develop myweb]# yum install -y langpacks-zh_CN.noarch
[root@develop myweb]# git status   # 查看状态
如果上面命令不行，则执行下面的方法
[root@develop myweb]# localectl set-locale zh_CN.utf8
[root@develop myweb]# reboot

# 查看版本库（快照），显示详细信息
[root@develop myweb]# git log
commit 70c927dae93dc347a40ddc24d5be7d6a8f9924eb (HEAD -> master) //快照ID
Author: zhangzhg <zhangzg@tedu.cn>       //作者
Date:   Mon Nov 15 16:34:48 2021 +0800  //快照创建日期

## git分支

- 使用分支意味着你可以把你的工作从开发主线上分离开来，以免影响开发主线。
- 可以按功能创建分支，分支可以合并。
- git的默认分支名为master。它在功能上与用户创建的分支没有区别。

- 常用的分支：

  - Master: 主分支；主要是稳定的版本分支，正式发布的版本都从Master拉。
  - Develop: 开发分支；更新和变动最频繁的分支，正常情况下开发都是在Develop分支上进行的。
  - Release：预发行分支；一般来说，代表一个版本的功能全部开发完成后递交测试，测试出Bug后进行修复的分支。
  - Features: 功能分支； 其实Features不是一个分支，而是一个分支文件夹。里面包含了每个程序员开发的功能点。Feature开发完成后合入Develop分支。
  - HotFix: 最希望不会被创建的分支；这个分支的存在是在已经正式上线的版本中，发现了重大Bug进行修复的分支。

- 使用分支

# 查看分支
[root@develop myweb]# git branch 
* master

# 创建名为b1的分支
[root@develop myweb]# git branch b1
[root@develop myweb]# git branch 
  b1
* master  # 当前在哪个分支，前面有*标识

# 切换分支
[root@develop myweb]# git checkout b1
切换到分支 'b1'
[root@develop myweb]# git branch 
* b1
  master

# 在分支上提交代码
[root@develop myweb]# echo 'b1 branch' > b1.txt
[root@develop myweb]# ls
1.txt  2.txt  b1.txt  readme
[root@develop myweb]# git add .
[root@develop myweb]# git commit -m "b1上提交b1.txt"

# 合并b1分支到master
[root@develop myweb]# git checkout master
切换到分支 'master'
[root@develop myweb]# ls   # 此时master分支上不能看到b1.txt文件
1.txt  2.txt  readme
[root@develop myweb]# git merge b1 -m "合并b1到master"
[root@develop myweb]# ls
1.txt  2.txt  b1.txt  readme

# 如果不同分支存在冲突的文件（文件名相同，内容不同），需要手工解决冲突
# 1. 创建b2分支，新建hello.txt文件
[root@develop myweb]# git branch b2
[root@develop myweb]# git checkout b2
切换到分支 'b2'
[root@develop myweb]# echo 'hello world' > hello.txt
[root@develop myweb]# ls
1.txt  2.txt  b1.txt  hello.txt  readme
[root@develop myweb]# git add .
[root@develop myweb]# git commit -m "在b2创建hello.txt"

# 2. 回到master分支，新建hello.txt文件，内容与b2分支的hello.txt不同
[root@develop myweb]# git checkout master
切换到分支 'master'
[root@develop myweb]# ls
1.txt  2.txt  b1.txt  readme
[root@develop myweb]# echo 'hello china' > hello.txt
[root@develop myweb]# ls
1.txt  2.txt  b1.txt  hello.txt  readme
[root@develop myweb]# cat hello.txt 
hello china
[root@develop myweb]# git add .
[root@develop myweb]# git commit -m "在master上创建hello.txt"

# 3. 合并b2到master将会失败，因为两个分支中的hello.txt内容不同
[root@develop myweb]# git merge b2 -m "合并b2分支"
自动合并 hello.txt
冲突（添加/添加）：合并冲突于 hello.txt
自动合并失败，修正冲突然后提交修正的结果。

# 4. 手工修改hello.txt，然后提交
[root@develop myweb]# cat hello.txt 
<<<<<<< HEAD
hello china
=======
hello world
>>>>>>> b2
[root@develop myweb]# vim hello.txt   # 改成希望的结果即可
hello china and world
[root@develop myweb]# git add .
[root@develop myweb]# git commit -m "修改冲突文件"
[master c135b10] 修改冲突文件
[root@develop myweb]# git status 
位于分支 master
无文件要提交，干净的工作区
```

### 



--------------------------------------------------------------------------------
11.23

[root@develop myproject]# vim README.md
- 这是我的第1个测试项目      //"-空格"是标号
```				//"```" 上下各三个是在文本里显示代码区域
echo 'Hello World!'
```

[root@develop ~]# git clone http://192.168.4.20/devops/myproject.git
#http的克隆模式
[root@develop ~]# git clone git@192.168.4.20:devops/myproject.git
#ssh的克隆方式（需用再it remote add origin git@192.168.4.20:devops/myproject.git）

Git 全局设置

git config --global user.name "jerry"
git config --global user.email "jerry@tedu.cn"

创建新版本库
git clone git@gitlab:devops/myweb.git
cd myweb
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

已存在的文件夹

cd existing_folder
git init
git remote add origin git@gitlab:devops/myweb.git
git add .
git commit -m "Initial commit"
git push -u origin master

已存在的 Git 版本库

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab:devops/myweb.git
git push -u origin --all
git push -u origin --tags


----------------------------------------------------------------------
11.24
#容器页面打不开处理
#关闭防火墙和selinux
[root@git ~]# systemctl stop firewalld
[root@git ~]# systemctl disable firewalld
[root@git ~]# setenforce 0
SELINUX=disabled
#设置podman开机自启
[root@git ~]# vim /etc/rc.d/rc.local   # 在文件尾部追加一行内容如下：
... ...
podman start gitlab
[root@git ~]# chmod +x /etc/rc.d/rc.local
#手动开启podman，并且查看是否开启成功
[root@git ~]# podman start gitlab  //成功则显示容器名字
[root@git ~]# podman ps          //无内容则失败
#如果还是未成功则删除容器重新建立
# 强制删除容器
[root@git ~]# podman rm -f gitlab
# 新建容器
[root@git ~]# podman run -d -h gitlab --name gitlab -p 443:443 -p 80:80 -p 22:22 --restart always -v /srv/gitlab/config:/etc/gitlab -v /srv/gitlab/logs:/var/log/gitlab -v /srv/gitlab/data:/var/opt/gitlab gitlab_zh


在Jenkins上配置FTP服务器
下面脚本提到的下载目录"myproject-$web"加上变量$web,
查看下载目录
[root@jenkins ~]# ls /var/lib/jenkins/workspace/myproject/
myproject-1.0  myproject-1.1

# 定义存储软件包路径的变量
pkg_dir=/var/ftp/deploy/packages
# 将下载的代码目录拷贝到下载目录，myproject是项目的相对路径，他会在默认的jenkins的下载路径/var/lib/jenkins/workspace/myproject/下，找到myproject-$web名字的路径文件夹
cp -r myproject-$web $pkg_dir
# 删除下载目录的版本库，不是必须的，只是为了严谨
rm -rf $pkg_dir/myproject-$web/.git
cd $pkg_dir   # 切换到下载目录
# 将下载的目录打包
tar czf myproject-$web.tar.gz myproject-$web
# 下载目录已打包，目录就不需要了，删除它
rm -rf myproject-$web
# 计算压缩包的md5值，保存到文件
md5sum myproject-$web.tar.gz | awk '{print $1}' > myproject-$web.tar.gz.md5
cd ..
echo -n $web > ver.txt   # 将版本号写入文件


web服务自动部署
(curl -s $ftp_url/ver.txt) // -s 静获模式，不在屏幕上显示

wget -q $ftp_url/ver.txt -O $web_dir/ver.txt  //wget是下载命令 -O 是下载位置



-------------------------------------------------------------------------------------
11.25

ansible管理

- ansible进行远程管理的两个方法：
  - adhoc临时命令。就是在命令行上执行管理命令。
  - playbook剧本。把管理任务用特定格式写到文件中。
- 无论哪种方式，都是通过模块加参数进行管理。

adhoc临时命令
- 语法：
ansible 主机或组列表 -m 模块 -a "参数"    # -a是可选的
- 测试到远程主机的连通性
[root@control ansible]# ansible all -m ping
#### command模块
- ansible默认模块，用于在远程主机上执行任意命令
- command不支持shell特性，如管道、重定向


#### script模块

- 用于在远程主机上执行脚本
# 在控制端创建脚本即可
[root@control ansible]# vim test.sh
#!/bin/bash
yum install -y httpd
systemctl start httpd
# 在test组的主机上执行脚本
[root@control ansible]# ansible test -m script -a "./test.sh" 
"./test.sh" 是脚本的位置，"./"为当前目录，即"/root/ansible/test.sh"也可以直接写"test.sh"作为相对路径。



无特殊权限 粘滞位 SGID SUID
0            1      2       4

## ansible

- 批量管理服务器的工具
- 2015年被红帽公司收购
- 使用Python语言编写的
- 基于ssh进行管理，所以不需要在被管端安装任何软件
- ansible在管理远程主机的时候，主要是通过各种模块进行操作的

### 环境准备

- 6台主机，需要配置主机名、IP地址、YUM。关闭SELINUX和防火墙

- control节点要求：
  - 配置名称解析，能够通过名字访问所有节点
  - 配置可以通过ssh到所有节点免密登陆
  - 拷贝`/linux-soft/2/ansible_soft.tar.gz`到control，并解压安装

# 配置名称解析
[root@control ~]# echo -e "192.168.4.253\tcontrol" >> /etc/hosts
[root@control ~]# for i in {1..5}
> do
> echo -e "192.168.4.1$i\tnode$i" >> /etc/hosts
> done
[root@control ~]# tail -6 /etc/hosts
192.168.4.253	control
192.168.4.11	node1
192.168.4.12	node2
192.168.4.13	node3
192.168.4.14	node4
192.168.4.15	node5

# 配置免密登陆
[root@control ~]# ssh-keygen   # 三个问题都直接回车，使用默认值
[root@control ~]# for i in node{1..5}   # 回答yes和密码
> do
> ssh-copy-id $i
> done

# 装包
[root@zzgrhel8 ~]# scp /linux-soft/2/ansible_soft.tar.gz 192.168.4.253:/root
[root@control ~]# yum install -y tar
[root@control ~]# tar xf ansible_soft.tar.gz 
[root@control ~]# cd ansible_soft/
[root@control ansible_soft]# yum install -y *.rpm


## 配置ansible管理环境

- 因为要管理的远程主机可能不一样。所以具有相同管理方式的配置放到一个目录下。

# 创建ansible工作目录，目录名自己定义，不是固定的。
[root@control ~]# mkdir ansible
[root@control ~]# cd ansible

# 创建配置文件。默认的配置文件是/etc/ansible/ansible.cfg，但是一般不使用它，而是在工作目录下创建自己的配置文件
[root@control ansible]# vim ansible.cfg    # 文件名必须是ansible.cfg
[defaults]
inventory = hosts    # 管理的主机，配置在当前目录的hosts文件中，hosts名是自定义的。=号两边空格可有可无。

# 创建主机清单文件。写在[]里的是组名，[]下面的是组内的主机名
[root@control ansible]# vim hosts
[test]
node1

[proxy]
node2

[webserver]
node[3:4]     # node3和node4的简化写法，表示从3到4

[database]
node5

# cluster是组名，自定义的；:children是固定写法，表示下面的组名是cluster的子组。
[cluster:children]
webserver
database

# 查看被管理的所有的主机。注意，一定在工作目录下执行命令。
[root@control ansible]# ansible all --list-hosts
  hosts (5):
    node1
    node2
    node3
    node4
    node5
# 查看webserver组中所有的主机
[root@control ansible]# ansible webserver --list-hosts
  hosts (2):
    node3
    node4

## ansible管理

- ansible进行远程管理的两个方法：
  - adhoc临时命令。就是在命令行上执行管理命令。
  - playbook剧本。把管理任务用特定格式写到文件中。
- 无论哪种方式，都是通过模块加参数进行管理。

### adhoc临时命令

- 语法：

ansible 主机或组列表 -m 模块 -a "参数"    # -a是可选的


- 测试到远程主机的连通性
[root@control ansible]# ansible all -m ping


### ansible模块

- 模块基本信息查看

# 列出ansible的所有模块数量
[root@control ansible]# ansible-doc -l | wc -l
2834
# 列出ansible的所有模块
[root@control ansible]# ansible-doc -l 
# 查看与yum相关的模块
[root@control ansible]# ansible-doc -l | grep yum

# 查看yum模块的使用说明，主要查看下方的EXAMPLE示例
[root@control ansible]# ansible-doc yum


- 学习模块，主要知道实现某种功能，需要哪个模块。
- 模块的使用方式都一样。主要是查看该模块有哪些参数。

#### command模块

- ansible默认模块，用于在远程主机上执行任意命令
- command不支持shell特性，如管道、重定向。

# 在所有被管主机上创建目录/tmp/demo
[root@control ansible]# ansible all -a "mkdir /tmp/demo"

# 查看node1的ip地址
[root@control ansible]# ansible node1 -a "ip a s"
[root@control ansible]# ansible node1 -a "ip a s | head"   # 报错


#### shell模块

- 与command模块类似，但是支持shell特性，如管道、重定向。


# 查看node1的ip地址，只显示前10行
[root@control ansible]# ansible node1 -m shell -a "ip a s | head"


#### script模块

- 用于在远程主机上执行脚本


# 在控制端创建脚本即可
[root@control ansible]# vim test.sh
#!/bin/bash

yum install -y httpd
systemctl start httpd

# 在test组的主机上执行脚本
[root@control ansible]# ansible test -m script -a "test.sh"


#### file模块

- 可以创建文件、目录、链接等，还可以修改权限、属性等
- 常用的选项：
  - path：指定文件路径
  - owner：设置文件所有者
  - group：设置文件所属组
  - state：状态。touch表示创建文件，directory表示创建目录，link表示创建软链接，absent表示删除
  - mode：设置权限
  - src：source的简写，源
  - dest：destination的简写，目标

# 查看使用帮助
[root@control ansible]# ansible-doc file
... ...
EXAMPLES:

- name: Change file ownership, group and permissions  # 忽略
  file:                           # 模块名。以下是它的各种参数
    path: /etc/foo.conf           # 要修改的文件的路径
    owner: foo                    # 文件所有者
    group: foo                    # 文件的所有组
    mode: '0644'                  # 权限
... ...
# 根据上面的example，-m file -a的内容就是doc中把各参数的冒号换成=号

# 在test主机上创建/tmp/file.txt
[root@control ansible]# ansible test -m file -a "path=/tmp/file.txt state=touch"   # touch是指如果文件不存在，则创建

# 在test主机上创建/tmp/demo目录
[root@control ansible]# ansible test -m file -a "path=/tmp/demo state=directory"

# 将test主机上/tmp/file.txt的属主改为sshd，属组改为adm，权限改为0777
[root@control ansible]# ansible test -m file -a "path=/tmp/file.txt owner=sshd group=adm mode='0777'"
[root@control ansible]# ansible test -a "ls -l /tmp/file.txt"

# 删除test主机上/tmp/file.txt
[root@control ansible]# ansible test -m file -a "path=/tmp/file.txt state=absent"    # absent英文缺席的、不存在的

# 删除test主机上/tmp/demo
[root@control ansible]# ansible test -m file -a "path=/tmp/demo state=absent"

# 在test主机上创建/etc/hosts的软链接，目标是/tmp/hosts.txt
[root@control ansible]# ansible test -m file -a "src=/etc/hosts dest=/tmp/hosts.txt state=link"


#### copy模块

- 用于将文件从控制端拷贝到被控端
- 常用选项：
  - src：源。控制端的文件路径
  - dest：目标。被控制端的文件路径
  - content：内容。需要写到文件中的内容


[root@control ansible]# echo "AAA" > a3.txt
# 将a3.txt拷贝到test主机的/root/
[root@control ansible]# ansible test -m copy -a "src=a3.txt dest=/root/"

# 在目标主机上创建/tmp/mytest.txt，内容是Hello World
[root@control ansible]# ansible test -m copy -a "content='Hello World' dest=/tmp/mytest.txt"


#### fetch模块

- 与copy模块相反，copy是上传，fetch是下载
- 常用选项：
  - src：源。被控制端的文件路径
  - dest：目标。控制端的文件路径


# 将test主机上的/etc/hostname下载到本地用户的家目录下
[root@control ansible]# ansible test -m fetch -a "src=/etc/hostname dest=~/"
[root@control ansible]# ls ~/node1/etc/   # node1是test组中的主机
hostname


#### lineinfile模块

- 用于确保存目标文件中有某一行内容
- 常用选项：
  - path：待修改的文件路径
  - line：写入文件的一行内容
  - regexp：正则表达式，用于查找文件中的内容


# test组中的主机，/etc/issue中一定要有一行Hello World。如果该行不存在，则默认添加到文件结尾
[root@control ansible]# ansible test -m lineinfile -a "path=/etc/issue line='Hello World'"

# test组中的主机，把/etc/issue中有Hello的行，替换成chi le ma
[root@control ansible]# ansible test -m lineinfile -a "path=/etc/issue line='chi le ma' regexp='Hello'"


#### replace模块

- lineinfile会替换一行，replace可以替换关键词
- 常用选项：
  - path：待修改的文件路径
  - replace：将正则表达式查到的内容，替换成replace的内容
  - regexp：正则表达式，用于查找文件中的内容


# 把test组中主机上/etc/issue文件中的chi，替换成he
[root@control ansible]# ansible test -m replace -a "path=/etc/issue regexp='chi' replace='he'"


#### 文件操作综合练习

- 所有操作均对test组中的主机生效
- 在目标主机上创建/tmp/mydemo目录，属主和属组都是adm，权限为0777
- 将控制端的/etc/hosts文件上传到目标主机的/tmp/mydemo目录中，属主和属组都是adm，权限为0600
- 替换目标主机/tmp/mydemo/hosts文件中的node5为server5
- 将目标主机/tmp/mydemo/hosts文件下载到控制端的当前目录


# 在目标主机上创建/tmp/mydemo目录，属主和属组都是adm，权限为0777
[root@control ansible]# ansible test -m file -a "path=/tmp/mydemo owner=adm group=adm mode='0777' state=directory" 

# 将控制端的/etc/hosts文件上传到目标主机的/tmp/mydemo目录中，属主和属组都是adm，权限为0600
[root@control ansible]# ansible test -m copy -a "src=/etc/hosts dest=/tmp/mydemo owner=adm group=adm mode='0600'"

# 替换目标主机/tmp/mydemo/hosts文件中的node5为server5
[root@control ansible]# ansible test -m replace -a "path=/tmp/mydemo/hosts regexp='node5' replace='server5'"

# 将目标主机/tmp/mydemo/hosts文件下载到控制端的当前目录。文件将会保存到控制端当前目录的node1/tmp/mydemo/
[root@control ansible]# ansible test -m fetch -a "src=/tmp/mydemo/hosts dest=."


#### user模块

- 实现linux用户管理
- 常用选项：
  - name：待创建的用户名
  - uid：用户ID
  - group：设置主组
  - groups：设置附加组
  - home：设置家目录
  - password：设置用户密码
  - state：状态。present表示创建，它是默认选项。absent表示删除
  - remove：删除家目录、邮箱等。值为yes或true都可以。


# 在test组中的主机上，创建tom用户
[root@control ansible]# ansible test -m user -a "name=tom"

# 在test组中的主机上，创建jerry用户。设置其uid为1010，主组是adm，附加组是daemon和root，家目录是/home/jerry
[root@control ansible]# ansible test -m user -a "name=jerry uid=1010 group=adm groups=daemon,root home=/home/jerry"

# 设置tom的密码是123456
# {{}}是固定格式，表示执行命令。password_hash是函数，sha512是加密算法，则password_hash函数将会把123456通过sha512加密变成tom的密码
[root@control ansible]# ansible test -m user -a "name=tom password={{'123456'|password_hash('sha512')}}"

# 删除tom用户，不删除家目录
[root@control ansible]# ansible test -m user -a "name=tom state=absent"

# 删除jerry用户，同时删除家目录
[root@control ansible]# ansible test -m user -a "name=jerry state=absent remove=yes"


#### group模块

- 创建、删除组
- 常用选项：
  - name：待创建的组名
  - gid：组的ID号
  - state：present表示创建，它是默认选项。absent表示删除


# 在test组中的主机上创建名为devops的组
[root@control ansible]# ansible test -m group -a "name=devops"

# 在test组中的主机上删除名为devops的组
[root@control ansible]# ansible test -m group -a "name=devops state=absent"





---------------------------------------------------------------------------------
11.26
### ansible模块

#### yum_repository

- 用于配置yum
- 常用选项：
  - file： 指定文件名
  - 其他选项，请与文件内容对照


# 在test组中的主机上，配置yum
[root@control ansible]# ansible test -m yum_repository -a "file=myrepo name=myApp description='My App' baseurl=ftp://192.168.4.254/rhel8/AppStream gpgcheck=no enabled=yes"

[root@node1 ~]# cat /etc/yum.repos.d/myrepo.repo 
[myApp]
baseurl = ftp://192.168.4.254/rhel8/AppStream
enabled = 1
gpgcheck = 0
name = My App

[root@control ansible]# ansible test -m yum_repository -a "file=myrepo name=BaseOS description='Base OS' baseurl=ftp://192.168.4.254/rhel8/BaseOS gpgcheck=no enabled=yes"

[root@node1 ~]# cat /etc/yum.repos.d/myrepo.repo 
[myApp]
baseurl = ftp://192.168.4.254/rhel8/AppStream
enabled = 1
gpgcheck = 0
name = My App

[BaseOS]
baseurl = ftp://192.168.4.254/rhel8/BaseOS
enabled = 1
gpgcheck = 0
name = Base OS


#### yum模块

- 用于rpm软件包管理，如安装、升级、卸载
- 常用选项：
  - name：包名
  - state：状态。present表示安装，如果已安装则忽略；latest表示安装或升级到最新版本；absent表示卸载。


# 在test组中的主机上安装tar
[root@control ansible]# ansible test -m yum -a "name=tar state=present"

# 在test组中的主机上安装wget、net-tools
[root@control ansible]# ansible test -m yum -a "name=wget,net-tools"

# 在test组中的主机上卸载wget
[root@control ansible]# ansible test -m yum -a "name=wget state=absent"


#### service模块

- 用于控制服务。启动、关闭、重启、开机自启。
- 常用选项：
  - name：控制的服务名
  - state：started表示启动；stopped表示关闭；restarted表示重启
  - enabled：yes表示设置开机自启；no表示设置开机不要自启。


# 在test主机上安装httpd
[root@control ansible]# ansible test -m yum -a "name=httpd state=latest"

#  在test主机上启动httpd，并设置它开机自启
[root@control ansible]# ansible test -m service -a "name=httpd state=started enabled=yes"


#### 逻辑卷相关模块

- 逻辑卷可以动态管理存储空间。可以对逻辑卷进行扩容或缩减。
- 可以把硬盘或分区转换成物理卷PV；再把1到多个PV组合成卷组VG；然后在VG上划分逻辑卷LV。LV可以像普通分区一样，进行格式化、挂载。
- 关闭虚拟机node1，为其添加2块20GB的硬盘
- LINUX下KVM虚拟机新加的硬盘，名称是`/dev/vdb`和`/dev/vdc`
- vmware虚拟机新加的硬盘，名称是`/dev/sdb`和`/dev/sdc`
- 如果选nvme硬盘，名称可能是`/dev/nvme0n1`和`/dev/nvme0n2`


[root@node1 ~]# lsblk    # 可以查看到新加的硬盘vdb和vdc
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0     11:0    1 1024M  0 rom  
vda    253:0    0   30G  0 disk 
`-vda1 253:1    0   20G  0 part /
vdb    253:16   0   20G  0 disk 
vdc    253:32   0   20G  0 disk 


##### lvg模块

- 创建、删除卷组，修改卷组大小
- 常用选项：
  - vg：定义卷组名。vg：volume group
  - pvs：由哪些物理卷构成。pvs：physical volumes


# 在test组中的主机上安装lvm2，state不写，默认是present
[root@control ansible]# ansible test -m yum -a "name=lvm2"

# 手工在node1上对vdb进行分区
[root@node1 ~]# fdisk /dev/vdb
Command (m for help): g    # 创建GPT分区表
Command (m for help): n    # 新建分区
Partition number (1-128, default 1):    # 回车，使用1号分区
First sector (2048-41943006, default 2048):   # 起始位置，回车
Last sector, +sectors or +size{K,M,G,T,P} (2048-41943006, default 41943006): +5G   # 结束位置+5G

Command (m for help): n   # 新建分区
Partition number (2-128, default 2):   # 回车，使用2号分区
First sector (10487808-41943006, default 10487808): # 起始位置，回车
Last sector, +sectors or +size{K,M,G,T,P} (10487808-41943006, default 41943006): # 结束位置，回车，分区到结尾
Command (m for help): w   # 存盘

[root@node1 ~]# lsblk    # vdb被分出来了两个分区
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0     11:0    1 1024M  0 rom  
vda    253:0    0   30G  0 disk 
`-vda1 253:1    0   20G  0 part /
vdb    253:16   0   20G  0 disk 
|-vdb1 253:17   0    5G  0 part 
`-vdb2 253:18   0   15G  0 part 
vdc    253:32   0   20G  0 disk 

# 在test组中的主机上创建名为myvg的卷组，该卷组由/dev/vdb1组成
[root@control ansible]# ansible test -m lvg -a "vg=myvg pvs=/dev/vdb1"

# 在node1上查看卷组
[root@node1 ~]# vgs
  VG   #PV #LV #SN Attr   VSize  VFree 
  myvg   1   0   0 wz--n- <5.00g <5.00g

# 扩容卷组。卷组由PV构成，只要向卷组中加入新的PV，即可实现扩容
[root@control ansible]# ansible test -m lvg -a "vg=myvg pvs=/dev/vdb1,/dev/vdb2"

[root@node1 ~]# vgs  # 在node1上查看卷组
  VG   #PV #LV #SN Attr   VSize  VFree 
  myvg   2   0   0 wz--n- 19.99g 19.99g


##### lvol模块

- 创建、删除逻辑卷，修改逻辑卷大小
- 常用选项：
  - vg：指定在哪个卷组上创建逻辑卷
  - lv：创建的逻辑卷名。lv：logical volume
  - size：逻辑卷的大小，不写单位，以M为单位


# 在test组中的主机上创建名为mylv的逻辑卷，大小为2GB
[root@control ansible]# ansible test -m lvol -a "vg=myvg lv=mylv size=2G"

# 在node1上查看逻辑卷
[root@node1 ~]# lvs
  LV   VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  mylv myvg -wi-a----- 2.00g   
  
# mylv扩容至4GB
[root@control ansible]# ansible test -m lvol -a "vg=myvg lv=mylv size=4G"

[root@node1 ~]# lvs  # 在node1上查看逻辑卷
  LV   VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  mylv myvg -wi-a----- 4.00g   


#### filesystem模块

- 用于格式化，也就是创建文件系统
- 常用选项：
  - fstype：指定文件系统类型
  - dev：指定要格式化的设备，可以是分区，可以是逻辑卷


#  在test组中的主机上，把/dev/myvg/mylv格式化为xfs
[root@control ansible]# ansible test -m filesystem -a "fstype=xfs dev=/dev/myvg/mylv"

# 在node1上查看格式化结果
[root@node1 ~]# blkid /dev/myvg/mylv
/dev/myvg/mylv: UUID="46c0af72-e517-4b15-9e53-ec72fbe1d96e" TYPE="xfs"


#### mount模块

- 用于挂载文件系统
- 常用选项：
  - path：挂载点。如果挂载点不存在，自动创建。
  - src：待挂载的设备
  - fstype：文件系统类型
  - state：mounted，表示永久挂载


# 在test组中的主机上，把/dev/myvg/mylv永久挂载到/data
[root@control ansible]# ansible test -m mount -a "path=/data src=/dev/myvg/mylv state=mounted fstype=xfs"

# 在node1上查看
[root@node1 ~]# tail -1 /etc/fstab 
/dev/myvg/mylv /data xfs defaults 0 0
[root@node1 ~]# df -h /data/
Filesystem             Size  Used Avail Use% Mounted on
/dev/mapper/myvg-mylv  4.0G   61M  4.0G   2% /data


# 在test组中的主机上，卸载/dev/myvg/mylv
[root@control ansible]# ansible test -m mount -a "path=/data state=absent"

# 在test组中的主机上，强制删除/dev/myvg/mylv
[root@control ansible]# ansible test -m lvol -a "lv=mylv state=absent vg=myvg force=yes"   # force是强制

# 在test组中的主机上，删除myvg卷组
[root@control ansible]# ansible test -m lvg -a "vg=myvg state=absent"


## Playbook剧本

- 常用于复杂任务的管理，以及管理经常要完成的任务
- playbook也是通过模块和它的参数，在特定主机上执行任务
- playbook是一个文件，该文件中需要通过yaml格式进行书写

### YAML

- YAML Ain't a Markup Language：YAML不是一个标记语言

#### yaml语法规范

1. yaml文件的文件名，一般以yml或yaml作为扩展名
2. 文件一般以`---`作为第一行，不是必须的，但是常用
3. 键值对使用冒号`:`表示，冒号后面必须有空格。
4. 数组使用`-`表示，`-`后面必须有空格。
5. 相同的层级必须有相同的缩进。如果缩进不对，则有语法错误。每一级缩进，建议2个空格。
6. 全文不能使用tab，必须使用空格。

#### 配置vim适应yaml语法


# 文件位置和名字是固定的，用于设置vim的格式
[root@control ansible]# vim ~/.vimrc
set ai        # 设置自动缩进
set ts=2      # 设置按tab键，缩进2个空格
set et        # 将tab转换成相应个数的空格


### 编写playbook

- 一个剧本（即playbook），可以包含多个play
- 每个play用于在指定的主机上，通过模块和参数执行相应的任务
- 每个play可以包含多个任务。
- 任务有模块和参数构成。


# 编写用于测试连通性的playbook，相当于执行ansible all -m ping
[root@control ansible]# vim test.yml
---
- hosts: all
  tasks:
    - ping:

[root@control ansible]# ansible-playbook test.yml  # 执行playbook

# 以上更规范的写法如下：
[root@control ansible]# vim test.yml
---
- name: test network    # play的名字，可选项
  hosts: all            # 作用于所有的主机
  tasks:                # 任务
    - name: task 1      # 第1个任务的名字，可选项
      ping:             # 第1个任务使用的模块

[root@control ansible]# ansible-playbook test.yml  # 执行playbook


# 在test组的主机和node2上创建/tmp/demo目录，权限是0755。将控制端/etc/hosts拷贝到目标主机的/tmp/demo中
[root@control ansible]# vim fileop.yml
---
- name: create dir and copy file
  hosts: test,node2    # 这里的名称，必须出现在主机清单文件中
  tasks:
    - name: create dir
      file:
        path: /tmp/demo
        state: directory
        mode: '0755'
    
    - name: copy file
      copy:
        src: /etc/hosts
        dest: /tmp/demo/hosts

# 执行playbook
[root@control ansible]# ansible-playbook fileop.yml


# 在test组中的主机上，创建用户bob，附加组是adm；在node2主机上，创建/tmp/hi.txt，其内容为Hello World.
[root@control ansible]# vim two.yml
---
- name: create user
  hosts: test
  tasks:
    - name: create bob
      user:
        name: bob
        groups: adm

- name: create file
  hosts: node2
  tasks:
    - name: make file
      copy:
        dest: /tmp/hi.txt
        content: "Hello World\n"

[root@control ansible]# ansible-playbook two.yml


- `|`和`>`的区别：`|`它保留换行符，`>`把多行合并为一行


# 通过copy模块创建/tmp/1.txt，文件中有两行内容，分别是Hello World和ni hao
[root@control ansible]# vim f1.yml
---
- name: play 1
  hosts: test
  tasks:
    - name: mkfile 1.txt
      copy:
        dest: /tmp/1.txt
        content: |
          Hello World!
          ni hao.

[root@control ansible]# ansible-playbook f1.yml
# 查看结果
[root@node1 ~]# cat /tmp/1.txt 
Hello World!
ni hao.


# 通过copy模块创建/tmp/2.txt，文件中有一行内容，分别是Hello World! ni hao
[root@control ansible]# vim f2.yml 
---
- name: play 1
  hosts: test
  tasks:
    - name: mkfile 2.txt
      copy:
        dest: /tmp/2.txt
        content: >
          Hello World!
          ni hao.

[root@control ansible]# ansible-playbook f2.yml
[root@node1 ~]# cat /tmp/2.txt 
Hello World! ni hao.


- playbook示例


# 在test组中的主机上创建john用户，它的uid是1040，主组是daemon，密码为123
[root@control ansible]# vim user_john.yml
---
- name: create user
  hosts: test
  tasks:
    - name: create user john
      user:
        name: john
        uid: 1040
        group: daemon
        password: "{{'123'|password_hash('sha512')}}"
[root@control ansible]# ansible-playbook user_john.yml

# 在test组中的主机上删除用户john
[root@control ansible]# vim del_john.yml
---
- name: delete user
  hosts: test
  tasks:
    - name: delete user john
      user:
        name: john
        state: absent
[root@control ansible]# ansible-playbook del_john.yml


#### 硬盘管理

- 常用的分区表类型有：MBR（主引导记录）、GPT（GUID分区表）
- MBR最多支持4个主分区，或3个主分区加1个扩展分区。最大支持2.2TB左右的硬盘
- GPT最多支持128个主分区。支持大硬盘

##### parted模块

- 用于硬盘分区管理
- 常用选项：
  - device：待分区的设备
  - number：分区编号
  - state：present表示创建，absent表示删除
  - part_start：分区的起始位置，不写表示从开头
  - part_end：表示分区的结束位置，不写表示到结尾


# 在test组中的主机上，对/dev/vdc进行分区，创建1个1GB的主分区
[root@control ansible]# vim disk.yml
---
- name: disk manage
  hosts: test
  tasks:
    - name: create a partition
      parted:
        device: /dev/vdc
        number: 1
        state: present
        part_end: 1GiB

[root@control ansible]# ansible-playbook disk.yml

# 在目标主机上查看结果
[root@node1 ~]# lsblk 
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
.. ...
vdc    253:32   0   20G  0 disk 
`-vdc1 253:33   0 1023M  0 part 

# 继续编辑disk.yml，对/dev/vdc进行分区，创建1个新的5GB的主分区
[root@control ansible]# vim disk.yml 
... ...
    - name: add a new partition
      parted:
        device: /dev/vdc
        number: 2
        state: present
        part_start: 1GiB
        part_end: 6GiB

[root@control ansible]# ansible-playbook disk.yml 
[root@node1 ~]# lsblk 
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
... ...
vdc    253:32   0   20G  0 disk 
|-vdc1 253:33   0 1023M  0 part 
`-vdc2 253:34   0    5G  0 part 

# 继续编辑disk.yml，创建名为my_vg的卷组，它由上面创建的vdc1和vdc2构成
[root@control ansible]# vim disk.yml 
... ...
    - name: create my_vg
      lvg:
        vg: my_vg
        pvs: /dev/vdc1,/dev/vdc2

# 继续编辑disk.yml，在my_vg卷组上创建名为my_lv的逻辑卷，大小1G
[root@control ansible]# vim disk.yml 
... ...
    - name: create my_lv
      lvol:
        vg: my_vg
        lv: my_lv
        size: 1G


# 继续编辑disk.yml，格式化my_lv为ext4
[root@control ansible]# vim disk.yml 
... ...
    - name: mkfs my_lv
      filesystem:
        dev: /dev/my_vg/my_lv
        fstype: ext4


# 继续编辑disk.yml，将my_lv挂载到/data
[root@control ansible]# vim disk.yml 
... ...
    - name: mount my_lv
      mount:
        path: /data
        src: /dev/my_vg/my_lv
        fstype: ext4
        state: mounted

# 完整的disk.yml如下
---
- name: disk manage
  hosts: test
  tasks:
    - name: create a partition
      parted:
        device: /dev/vdc
        number: 1
        state: present
        part_end: 1GiB

    - name: add a new partition
      parted:
        device: /dev/vdc
        number: 2
        state: present
        part_start: 1GiB
        part_end: 6GiB

    - name: create my_vg
      lvg:
        vg: my_vg
        pvs: /dev/vdc1,/dev/vdc2

    - name: create my_lv
      lvol:
        vg: my_vg
        lv: my_lv
        size: 1G
        
    - name: mkfs my_lv
      filesystem:
        dev: /dev/my_vg/my_lv
        fstype: ext4

    - name: mount my_lv
      mount:
        path: /data
        src: /dev/my_vg/my_lv
        fstype: ext4
        state: mounted

------------------------------------------------------------------------------------
11.29


## playbook

### 模块

```shell
# 在test组中的主机上，安装httpd、php、php-mysqlnd
[root@control ansible]# vim pkg.yml
---
- name: install pkgs
  hosts: test
  tasks:
    - name: install web pkgs
      yum:
        name: httpd,php,php-mysqlnd
        state: present
# 安装多个软件包，还可以写为：
---
- name: install pkgs
  hosts: test
  tasks:
    - name: install web pkgs
      yum:
        name: [httpd,php,php-mysqlnd]
        state: present

# 安装多个软件包，还可以写为：
---
- name: install pkgs
  hosts: test
  tasks:
    - name: install web pkgs
      yum:
        name: 
          - httpd
          - php
          - php-mysqlnd
        state: present

# 根据功能等，可以将一系列软件放到一个组中，安装软件包组，将会把很多软件一起安装上。比如gcc、java等都是开发工具，安装开发工具包组，将会把它们一起安装。
[root@node1 ~]# yum grouplist   # 列出所有的软件包组
[root@node1 ~]# yum groupinstall "Development Tools"
# 如果列出的组名为中文，可以这样进行：
[root@node1 ~]# LANG=C yum grouplist

# 继续编辑pkg.yml，在test组中的主机上安装Development tools组
[root@control ansible]# vim pkg.yml
---
- name: install pkgs
  hosts: test
  tasks:
    - name: install web pkgs
      yum:
        name:
          - httpd
          - php
          - php-mysqlnd
        state: present

    - name: install dev group
      yum:
        name: "@Development Tools"   # @表示后面的名字是组名
        state: present

[root@control ansible]# ansible-playbook pkg.yml

# 系统升级命令
[root@control ansible]# yum update
# 继续编辑pkg.yml，在test组中的主机上升级所有的包到最新版本
---
- name: install pkgs
  hosts: test
  tasks:
    - name: install web pkgs
      yum:
        name:
          - httpd
          - php
          - php-mysqlnd
        state: present

    - name: install dev group
      yum:
        name: "@Development Tools"
        state: present

    - name: update system    # 相当于yum update命令
      yum:
        name: "*"
        state: latest

[root@control ansible]# ansible-playbook pkg.yml
```

## ansible变量

### facts变量

- facts翻译过来就是事实。
- facts变量是ansible自带的预定义变量，用于描述被控端软硬件信息。
- facts变量通过setup模块获得。

```shell
# 通过setup模块查看所有facts变量
[root@control ansible]# ansible test -m setup
```

- facts变量是一个大的由`{}`构成的键值对字典。在`{}`中，有很多层级的嵌套。可以通过参数过滤出第一个层级的内容。

```shell
# 查看所有的IPV4地址，filter是过滤的意思
[root@control ansible]# ansible test -m setup -a "filter=ansible_all_ipv4_addresses"

# 查看可用内存
[root@control ansible]# ansible test -m setup -a "filter=ansible_memfree_mb"
```

- 常用的facts变量
  - ansible_all_ipv4_addresses：所有的IPV4地址
  - ansible_bios_version：BIOS版本信息
  - ansible_memtotal_mb：总内存大小
  - ansible_hostname：主机名

- 在playbook中使用变量

```shell
# 显示远程主机的主机名和内存大小。在ansible中，变量使用{{}}表示
# debug模块用于输出信息，常用的参数是msg，用于输出指定内容
[root@control ansible]# vim debug.yml
---
- name: display host info
  hosts: test
  tasks:
    - name: display hostname and memory
      debug:
        msg: "hostname: {{ansible_hostname}}; mem: {{ansible_memtotal_mb}} MB"

[root@control ansible]# ansible-playbook debug.yml
```

### 自定义变量

- 引入变量，可以方便Playbook重用。比如装包的playbook，包名使用变量。多次执行playbook，只要改变变量名即可，不用编写新的playbook。
- ansible支持10种以上的变量定义方式。常用的变量来源如下：
  - inventory变量。变量来自于主机清单文件
  - facts变量。
  - playbook变量。变量在playbook中定义。
  - 变量文件。专门创建用于保存变量的文件。推荐变量写入单独的文件。

```shell
# 使用inventory变量。
[root@control ansible]# vim hosts
[test]
node1 iname="nb"     # 主机变量定义的方法。iname是自定义名称

[proxy]
node2

[webserver]
node[3:4]

[database]
node5

[cluster:children]
webserver
database

[webserver:vars]       # 组变量定义方法。:vars是固定格式
iname="dachui"

# 通过变量创建用户
[root@control ansible]# vim var1.yml
---
- name: test create user
  hosts: test
  tasks:
    - name: create user
      user:
        name: "{{iname}}"
        state: present
        
- name: create user in webserver
  hosts: webserver
  tasks:
    - name: create some users
      user:
        name: "{{iname}}"
        state: present

[root@control ansible]# ansible-playbook var1.yml

# 上述两个play也可以合并为一个，如下：
[root@control ansible]# vim var1.yml
---
- name: test create user
  hosts: test,webserver
  tasks:
    - name: create user
      user:
        name: "{{iname}}"
        state: present
        

# 在playbook中定义变量
# 在test组中的主机上创建用户jack，他的密码是123456
[root@control ansible]# vim user_jack.yml
---
- name: create user
  hosts: test
  vars:    # 固定格式，用于声明变量
    username: "jack"    # 此处引号可有可无
    mima: "123456"      # 此处引号是需要的，表示数字字符
  tasks:
    - name: create some users
      user:
        name: "{{username}}"   # {}出现在开头，必须有引号
        state: present
        password: "{{mima|password_hash('sha512')}}"

[root@control ansible]# ansible-playbook user_jack.yml


# 将变量定义在文件中
[root@control ansible]# vim vars.yml   # 文件名自定义
---
yonghu: rose
mima: abcd
[root@control ansible]# vim user_rose.yml 
---
- name: create user
  hosts: test
  vars_files: vars.yml   # vars_files用于声明变量文件
  tasks:
    - name: create some users
      user:
        name: "{{yonghu}}"
        state: present
        password: "{{mima|password_hash('sha512')}}"

[root@control ansible]# ansible-playbook user_rose.yml 
```

## 补充模块

#### firewalld模块

- 用于配置防火墙的模块
- 常用选项：
  - port：声明端口
  - permanent：永久生效，但不会立即生效
  - immediate：立即生效，临时生效
  - state：enabled，放行；disabled拒绝
- 防火墙一般默认拒绝，明确写入允许的服务。
- 有一些服务有名字，有些服务没有名字。但是最终都是基于TCP或UDP的某些端口。比如http服务基于TCP80端口。服务名和端口号对应关系的说明文件是：`/etc/services`
- 配置服务器的防火墙，一般来说只要配置开放哪些服务或端口即可。没有明确开放的，都默认拒绝。

- 应用
  - 在test组中的主机上安装并启动httpd
  - 客户端访问服务器的http服务
  - 在test组中的主机上安装并启动firewalld
  - 客户端访问服务器的http服务
  - 在test组中的主机上开放http服务

```shell
# 配置httpd服务
[root@control ansible]# vim firewall.yml
---
- name: configure test
  hosts: test
  tasks:
    - name: install httpd pkg
      yum:
        name: httpd
        state: present

    - name: start httpd service
      service:
        name: httpd
        state: started
        enabled: yes
        
[root@control ansible]# ansible-playbook firewall.yml
[root@control ansible]# curl http://192.168.4.11/  # 可访问

# 安装并启动firewalld
[root@control ansible]# vim firewall.yml
---
- name: configure test
  hosts: test
  tasks:
    - name: install httpd pkg
      yum:
        name: httpd
        state: present

    - name: start httpd service
      service:
        name: httpd
        state: started
        enabled: yes
  
    - name: install firewalld pkg
      yum:
        name: firewalld
        state: present

    - name: start firewalld service
      service:
        name: firewalld
        state: started
        enabled: yes
  
[root@control ansible]# ansible-playbook firewall.yml
[root@control ansible]# curl http://192.168.4.11/  # 被拒绝
curl: (7) Failed to connect to 192.168.4.11 port 80: 没有到主机的路由

# 配置防火墙规则，放行http协议
[root@control ansible]# vim firewall.yml
---
- name: configure test
  hosts: test
  tasks:
    - name: install httpd pkg
      yum:
        name: httpd
        state: present

    - name: start httpd service
      service:
        name: httpd
        state: started
        enabled: yes
  
    - name: install firewalld pkg
      yum:
        name: firewalld
        state: present

    - name: start firewalld service
      service:
        name: firewalld
        state: started
        enabled: yes
  
    - name: set firewalld rules
      firewalld:
        port: 80/tcp
        permanent: yes
        immediate: yes
        state: enabled

[root@control ansible]# ansible-playbook firewall.yml 
[root@control ansible]# curl http://192.168.4.11/  # 可访问
```

#### template模块

- copy模块可以上传文件，但是文件内容固定
- template模块可以上传具有特定格式的文件（如文件中包含变量）
- 当远程主机接收到文件之后，文件中的变量将会变成具体的值 
- template模块上传的文件，使用的语法叫Jinja2。
- 常用选项：
  - src：要上传的文件
  - dest：目标文件路径

```shell
# 使用template模块将含有变量的文件上传到test组中的主机
[root@control ansible]# vim index.j2
Welcome to {{ansible_hostname}} on {{ansible_eth0.ipv4.address}}

[root@control ansible]# vim templ.yml
---
- name: upload index
  hosts: test
  tasks:
    - name: create web index
      template:
        src: index.j2
        dest: /var/www/html/index.html

[root@control ansible]# ansible-playbook templ.yml
[root@control ansible]# curl http://192.168.4.11/
Welcome to node1 on 192.168.4.11
[root@node1 ~]# cat /var/www/html/index.html 
Welcome to node1 on 192.168.4.11
```

## 进阶语法

### 错误处理

- 当Playbook中包含很多任务时，当某一个任务遇到错误，它将崩溃，终止执行

```shell
# 在test组中的主机上启动mysqld服务，然后创建/tmp/service.txt
# 因为目标主机上没有mysqld服务，所以它将崩溃，终止执行。即，不会创建/tmp/service.txt文件
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  tasks:
    - name: start mysqld service
      service:
        name: mysqld
        state: started
        enabled: yes
        
    - name: touch a file
      file:
        path: /tmp/service.txt
        state: touch

# 执行playbook，第1个任务就会失败
[root@control ansible]# ansible-playbook myerr.yml
# 到node1上查看，因为第2个任务没有执行，所以文件不会创建
[root@node1 ~]# ls /tmp/service.txt
ls: cannot access '/tmp/service.txt': No such file or directory
```

- 可以指定某一个任务如果出现错误，则忽略它

```shell
# 编辑myerr.yml，如果myslqd服务无法启动，则忽略它
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  tasks:
    - name: start mysqld service
      service:
        name: mysqld
        state: started
        enabled: yes
      ignore_errors: yes

    - name: touch a file
      file:
        path: /tmp/service.txt
        state: touch

[root@control ansible]# ansible-playbook myerr.yml
[root@node1 ~]# ls /tmp/service.txt   # 第2个任务已执行
/tmp/service.txt
```

- 通过全局设置，无论哪个任务出现问题，都要忽略

```shell
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  ignore_errors: yes
  tasks:
    - name: start mysqld service
      service:
        name: mysqld
        state: started
        enabled: yes

    - name: touch a file
      file:
        path: /tmp/mysql.txt
        state: touch

[root@control ansible]# ansible-playbook myerr.yml
[root@node1 ~]# ls /tmp/mysql.txt 
/tmp/mysql.txt
```

### 触发执行任务

- 通过handlers定义触发执行的任务
- handlers中定义的任务，不是一定会执行的
- 在tasks中定义的任务，通过notify关键通知handlers中的哪个任务要执行
- 只有tasks中的任务状态是changed才会进行通知。

```shell
# 创建目录，执行追加命令，在目录中建立文件
[root@control ansible]# vim handle.yml
---
- name: handler tasks
  hosts: test
  tasks:
    - name: create a dir
      file:
        path: /tmp/newdir
        state: directory
        
    - name: exec shell
      shell: "echo hello >> /tmp/newdir/a.txt"

[root@control ansible]# ansible-playbook handle.yml 
[root@node1 ~]# cat /tmp/newdir/a.txt 
hello
# 每次执行playbook，shell命令都会执行一次
[root@control ansible]# ansible-playbook handle.yml 
[root@node1 ~]# cat /tmp/newdir/a.txt 
hello
hello


# 修改任务执行逻辑，只有第一个任务执行，才会触发执行第2个任务
[root@control ansible]# vim handle2.yml 
---
- name: handler tasks
  hosts: test
  tasks:
    - name: create a dir
      file:
        path: /tmp/newdir2
        state: directory
      notify: exec shell

  handlers:
    - name: exec shell
      shell: "echo hello >> /tmp/newdir2/a.txt"

# 第一次运行时，不存在/tmp/newdir2，第1个任务的状态将会是changed，它将触发执行exec shell
[root@control ansible]# ansible-playbook handle2.yml
[root@node1 ~]# cat /tmp/newdir2/a.txt 
hello

# 再次运行时，/tmp/newdir2目录已存在，第1任务的状态将会是ok/success，那么它就不会触发exec shell任务
[root@control ansible]# ansible-playbook handle2.yml
[root@node1 ~]# cat /tmp/newdir2/a.txt 
hello
```

### when条件

- 只有满足某一条件时，才执行任务
- 常用的操作符：
  - ==：相等
  - !=：不等
  - `>`：大于
  - `<`：小于
  - `<=`：小于等于
  - `>=`：大于等于

- 多个条件或以使用and或or进行连接
- when表达式中的变量，可以不使用`{{}}`

```shell
# 当test组中的主机内存大于2G的时候，才安装mariadb-server
[root@control ansible]# vim when1.yml
---
- name: install mariadb
  hosts: test
  tasks:
    - name: install mariadb pkg
      yum:
        name: mariadb-server
        state: present
      when: ansible_memtotal_mb>2048

# 如果目标主机没有2GB内存，则不会安装mariadb-server
[root@control ansible]# ansible-playbook when1.yml



# 多条件。系统发行版是RedHat8才执行任务
# /etc/motd中的内容，将会在用户登陆时显示在屏幕上
[root@control ansible]# vim motd
 _____________
< hello world >
 -------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

[root@control ansible]# vim when2.yml
---
- name: when condition
  hosts: test
  tasks:
    - name: modify /etc/motd
      copy:
        dest: /etc/motd
        src: motd
      when: >     # 以下三行合并成一行
        ansible_distribution == "RedHat"
        and
        ansible_distribution_major_version == "8"

[root@control ansible]# ansible-playbook when2.yml
```

-----------------------------------------------------------------------------------------------
11.30


## 任务块

- 可以通过block关键字，将多个任务组合到一起
- 可以将整个block任务组，一起控制是否要执行

```shell
# ansible的playbook默认有一个名为Gathering facts的任务，用于收集远程主机的facts变量。如果不需要，可以把它关闭
[root@control ansible]# vim ansible.cfg 
[defaults]
inventory = hosts
gathering = explicit


# 如果test组中的主机系统发行版是RedHat，则安装并启动httpd
# 由于该playbook需要用到facts变量，所以需要明确收集facts变量
[root@control ansible]# vim block1.yml
---
- name: block tasks
  hosts: test
  tasks:
    - name: gathering facts
      setup:

    - name: define a group of tasks
      block:
        - name: install httpd
          yum:
            name: httpd
            state: present

        - name: start httpd
          service:
            name: httpd
            state: started
            enabled: yes

      when: ansible_distribution=="RedHat"

[root@control ansible]# ansible-playbook block1.yml
```

### rescue和always

- block和rescue、always联合使用：
  - block中的任务都成功，rescue中的任务不执行
  - block中的任务出现失败（failed），rescue中的任务执行
  - block中的任务不管怎么样，always中的任务总是执行

```shell
[root@control ansible]# vim block2.yml
---
- name: block test
  hosts: test
  tasks:
    - name: block / rescue / always test1
      block:
        - name: touch a file
          file:
            path: /tmp/test1.txt
            state: touch
      rescue:
        - name: touch file test2.txt
          file:
            path: /tmp/test2.txt
            state: touch
      always:
        - name: touch file test3.txt
          file:
            path: /tmp/test3.txt
            state: touch

# 执行playbook node1上将会出现/tmp/test1.txt和/tmp/test3.txt
[root@control ansible]# ansible-playbook block2.yml
[root@node1 ~]# ls /tmp/test*.txt
/tmp/test1.txt  /tmp/test3.txt

# 修改上面的playbook，使block中的任务出错
[root@node1 ~]# rm -f /tmp/test*.txt
[root@control ansible]# vim block2.yml
---
- name: block test
  hosts: test
  tasks:
    - name: block / rescue / always test1
      block:
        - name: touch a file
          file:
            path: /tmp/abcd/test11.txt
            state: touch
      rescue:
        - name: touch file test22.txt
          file:
            path: /tmp/test22.txt
            state: touch
      always:
        - name: touch file test33.txt
          file:
            path: /tmp/test33.txt
            state: touch
# 因为node1上没有/tmp/abcd目录，所以block中的任务失败。但是playbook不再崩溃，而是执行rescue中的任务。always中的任务总是执行
[root@control ansible]# ansible-playbook block2.yml
[root@node1 ~]# ls /tmp/test*.txt
/tmp/test2.txt  /tmp/test3.txt
```

## loop循环

- 相当于shell中for循环
- ansible中循环用到的变量名是固定的，叫item

```shell
# 在test组中的主机上创建5个目录/tmp/{aaa,bbb,ccc,ddd,eee}
[root@control ansible]# vim loop1.yml
---
- name: use loop
  hosts: test
  tasks:
    - name: create directory
      file:
        path: /tmp/{{item}}
        state: directory
      loop: [aaa,bbb,ccc,ddd,eee]

# 上面写法，也可以改为：
---
- name: use loop
  hosts: test
  tasks:
    - name: create directory
      file:
        path: /tmp/{{item}}
        state: directory
      loop: 
        - aaa
        - bbb
        - ccc
        - ddd
        - eee

[root@control ansible]# ansible-playbook loop1.yml


# 使用复杂变量。创建zhangsan用户，密码是123；创建lisi用户，密码是456
# item是固定的，用于表示循环中的变量
# 循环时，loop中每个-后面的内容作为一个整体赋值给item。
# loop中{}中的内容是自己定义的，写法为key:val
# 取值时使用句点表示。如下例中取出用户名就是{{item.uname}}
[root@control ansible]# vim loop_user.yml
---
- name: create users
  hosts: test
  tasks:
    - name: create multiple users
      user:
        name: "{{item.uname}}"
        password: "{{item.upass|password_hash('sha512')}}"
      loop:
        - {"uname": "zhangsan", "upass": "123"}
        - {"uname": "lisi", "upass": "456"}
[root@control ansible]# ansible-playbook  loop_user.yml
```

## role角色

- 为了实现playbook重用，可以使用role角色
- 角色role相当于把任务打散，放到不同的目录中
- 再把一些固定的值，如用户名、软件包、服务等，用变量来表示
- role角色定义好之后，可以在其他playbook中直接调用

```shell
# 使用常规playbook，修改/etc/motd的内容
# 1. 修改默认配置
[root@control ansible]# vim ansible.cfg 
[defaults]
inventory = hosts
# gathering = explicit    # 注释该行，使得playbook默认收集facts

# 2. 创建motd模板文件
[root@control ansible]# vim motd.j2
Hostname: {{ansible_hostname}}     # facts变量，主机名
Date: {{ansible_date_time.date}}   #  facts变量，日期
Contact to: {{admin}}              # 自定义变量

# 3. 编写playbook
[root@control ansible]# vim motd.yml
---
- name: modifty /etc/motd
  hosts: test
  vars:
    admin: root@tedu.cn            # 自定义名为admin的变量
  tasks:
    - name: modify motd
      template:
        src: motd.j2
        dest: /etc/motd

[root@control ansible]# ansible-playbook motd.yml
[root@node1 ~]# cat /etc/motd 
Hostname: node1
Date: 2021-11-01
Contact to: root@tedu.cn


# 创建角色
# 1. 声明角色存放的位置
[root@control ansible]# vim ansible.cfg 
[defaults]
inventory = hosts
# gathering = explicit
roles_path = roles    # 定义角色存在当前目录的roles子目录中

# 2. 创建角色目录
[root@control ansible]# mkdir roles

# 3. 创建名为motd的角色
[root@control ansible]# ansible-galaxy init roles/motd
[root@control ansible]# ls roles/
motd     # 生成了motd角色目录
[root@control ansible]# yum install -y tree
[root@control ansible]# tree roles/motd/
roles/motd/
├── defaults         # 定义变量的目录，一般不用，因为优先级太低
│    └── main.yml
├── files            # 保存上传的文件（如copy模块用到的文件）
├── handlers         # handlers任务写到这个目录的main.yml中
│    └── main.yml
├── meta             # 保存说明数据，如角色作者、版本等
│    └── main.yml
├── README.md        # 保存角色如何使用之类的说明
├── tasks            # 保存任务
│    └── main.yml
├── templates        # 保存template模块上传的模板文件
├── tests            # 保存测试用的playbook。可选
│    ├── inventory
│    └── test.yml
└── vars             # 定义变量的位置，推荐使用的位置
     └── main.yml

# 4. 将不同的内容分别写到对应目录的main.yml中
# 4.1 创建motd.j2模板文件
[root@control ansible]# vim roles/motd/templates/motd.j2
Hostname: {{ansible_hostname}}
Date: {{ansible_date_time.date}}
Contact to: {{admin}}

# 4.2 创建变量
[root@control ansible]# vim roles/motd/vars/main.yml  # 追加一行
admin: zzg@tedu.cn

# 4.3 创建任务
[root@control ansible]# vim roles/motd/tasks/main.yml  # 追加
- name: modify motd
  template:
    src: motd.j2      # 这里的文件，自动到templates目录下查找
    dest: /etc/motd

# 5. 创建playbook，调用motd角色
[root@control ansible]# vim role_motd.yml
---
- name: modify motd with role
  hosts: test
  roles:
    - motd

# 6. 执行playbook
[root@control ansible]# ansible-playbook role_motd.yml 
```

- ansible的公共角色仓库：https://galaxy.ansible.com/

```shell
# 在公共仓库中搜索与httpd相关的角色
[root@zzgrhel8 ~]# ansible-galaxy search httpd
# 如果找到相应的角色，如名字为myhttpd，可以下载它到roles目录
[root@zzgrhel8 ~]# ansible-galaxy install myhttpd -p roles/
```

### role练习

1. 创建名为pkgs的角色。用于装包。包名使用变量pkg代表
2. 创建inst_http.yml，调用pkgs角色，安装httpd
3. 创建inst_php.yml，调用pkgs角色，安装php

```shell
# 1. 创建名为pkgs的角色。
# 1.1 创建角色目录
[root@control ansible]# ansible-galaxy init roles/pkgs
# 1.2 创建装包的任务，包名使用变量pkg代表
[root@control ansible]# vim roles/pkgs/tasks/main.yml 
---
# tasks file for roles/pkgs
- name: install rpm pkg
  yum:
    name: "{{pkg}}"
    state: present
# 1.3 定义变量
[root@control ansible]# vim roles/pkgs/defaults/main.yml 
---
# defaults file for roles/pkgs
pkg: httpd

# 2. 创建inst_http.yml，调用pkgs角色，安装httpd
[root@control ansible]# vim inst_httpd.yml
---
- name: install httpd pkg
  hosts: test
  roles:
    - pkgs
[root@control ansible]# ansible-playbook inst_httpd.yml

# 3. 创建inst_php.yml，调用pkgs角色，安装php
[root@control ansible]# vim inst_php.yml 
---
- name: install php pkg
  hosts: test
  vars:
    pkg: php
  roles:
    - pkgs
[root@control ansible]# ansible-playbook inst_php.yml
```



## ansible加解密文件

- ansible加解密文件使用ansible-vault命令

```shell
[root@control ansible]# echo "Hi ni hao" > hello.txt 
[root@control ansible]# cat hello.txt
Hi ni hao

# 加密文件
[root@control ansible]# ansible-vault encrypt hello.txt
New Vault password: 123456
Confirm New Vault password: 123456
Encryption successful
[root@control ansible]# cat hello.txt
$ANSIBLE_VAULT;1.1;AES256
37373366353566346235613731396566646533393361386131313632306563633336333963373465
6164323461356130303863633964393339363738653036310a666564313832316263393061616330
32373133323162353864316435366439386266616661373936363563373634356365326637336165
6336636230366564650a383239636230623633356565623461326431393634656666306330663533
6235

# 解密
[root@control ansible]# ansible-vault decrypt hello.txt
Vault password: 123456
Decryption successful
[root@control ansible]# cat hello.txt 
Hi ni hao


# 加密后更改密码
[root@control ansible]# ansible-vault encrypt hello.txt 
New Vault password: 123456
Confirm New Vault password: 123456
Encryption successful

[root@control ansible]# ansible-vault rekey hello.txt   # 改密码
Vault password: 123456    # 旧密码
New Vault password: abcd  # 新密码
Confirm New Vault password: abcd
Rekey successful

# 不解密文件，查看内容
[root@control ansible]# ansible-vault view hello.txt 
Vault password: abcd
Hi ni hao


# 使用密码文件进行加解密
# 1. 将密码写入文件
[root@control ansible]# echo 'tedu.cn' > pass.txt
# 2. 创建明文文件
[root@control ansible]# echo 'hello world' > data.txt
# 3. 使用pass.txt中的内容作为密码加密文件
[root@control ansible]# ansible-vault encrypt --vault-id=pass.txt data.txt
Encryption successful
[root@control ansible]# cat data.txt    # 文件已加密
# 4. 使用pass.txt中的内容作为密码解密文件
[root@control ansible]# ansible-vault decrypt --vault-id=pass.txt data.txt
Decryption successful
[root@control ansible]# cat data.txt 
hello world
```

## sudo命令

- 一般用于普通用户执行需要root权限的命令
- 在node1上配置zhangsan拥有sudo权限

```shell
# 如果没有zhangsan，手工创建
[root@node1 ~]# visudo   # 将会打开vi，在尾部追加以下一行
zhangsan        ALL=(ALL)       ALL
# 中间的ALL=(ALL)在集中认证的域环境中才有效，单机忽略即可
# zhangsan是用户名，最后的ALL表示zhangsan可以以管理员的身份执行所有命令

# 切换成zhangsan用户，执行命令
[root@node1 ~]# su - zhangsan
[zhangsan@node1 ~]$ useradd wangwu   # 失败，因为还是张三身份
[zhangsan@node1 ~]$ sudo useradd wangwu  # 以管理员身份执行
... ...
[sudo] password for zhangsan: # 输入zhangsan的密码，不是root


# 配置lisi不输入密码可以直接运行sudo
[root@node1 ~]# visudo    # 在最后追加一行
lisi    ALL=(ALL)       NOPASSWD: ALL

# 切换成lisi运行
[root@node1 ~]# su - lisi
[lisi@node1 ~]$ ls /root/   # 没权限
ls: cannot open directory '/root/': Permission denied
[lisi@node1 ~]$ sudo ls /root/    # 成功运行，无需输入密码
a3.txt	anaconda-ks.cfg
```

## 特殊的主机清单变量

- 如果远程主机没有使用免密登陆，如果远程主机ssh不是标准的22端口，可以设置特殊的主机清单变量
- `ansible_ssh_user`：指定登陆远程主机的用户名
- `ansible_ssh_pass`：指定登陆远程主机的密码
- `ansible_ssh_port`：指定登陆远程主机的端口号


# 删除远程主机的/root/.ssh/authorized_keys，以便恢复通过密码登陆
[root@control ansible]# ansible all -m file -a "path=/root/.ssh/authorized_keys state=absent"

# 创建新的工作目录
[root@control ~]# mkdir myansible
[root@control ~]# cd myansible
[root@control myansible]# vim ansible.cfg
[defaults]
inventory = hosts
[root@control myansible]# vim hosts
[group1]
node1
node2
node3
[root@control myansible]# ansible all -m ping  # 报错，因为无法免密执行

# 修改node1 ssh服务的端口为220
[root@node1 ~]# systemctl stop firewalld
[root@node1 ~]# vim +17 /etc/ssh/sshd_config 
Port 220
[root@node1 ~]# systemctl restart sshd
# 退出再登陆时，需要指定端口号
[root@zzgrhel8 ~]# ssh -p220 192.168.4.11 



# 配置ssh通过用户名、密码管理远程主机，通过220端口连接node1
[root@control myansible]# vim hosts 
[group1]
node1 ansible_ssh_user=root ansible_ssh_pass=a ansible_ssh_port=220
node2 ansible_ssh_user=root ansible_ssh_pass=a
node3 ansible_ssh_user=root ansible_ssh_pass=a

[root@control myansible]# ansible all -m ping




---------------------------------------------------------------------------------------------------
12.3

## ceph

- ceph被称作面向未来的存储
- 中文手册：
  - https://access.redhat.com/documentation/zh-cn/red_hat_ceph_storage/5/html/architecture_guide/index
  - http://docs.ceph.org.cn/

- ceph可以实现的存储方式：
  - 块存储：提供像普通硬盘一样的存储，为使用者提供“硬盘”
  - 文件系统存储：类似于NFS的共享方式，为使用者提供共享文件夹
  - 对象存储：像百度云盘一样，需要使用单独的客户端

- ceph还是一个分布式的存储系统，非常灵活。如果需要扩容，只要向ceph集中增加服务器即可。
- ceph存储数据时采用多副本的方式进存储，生产环境下，一个文件至少要存3份。ceph默认也是三副本存储。

### ceph的构成

- **Ceph OSD 守护进程：** Ceph OSD 用于存储数据。此外，Ceph OSD 利用 Ceph 节点的 CPU、内存和网络来执行数据复制、纠删代码、重新平衡、恢复、监控和报告功能。存储节点有几块硬盘用于存储，该节点就会有几个osd进程。
- **Ceph Mon监控器：** Ceph Mon维护 Ceph 存储集群映射的主副本和 Ceph 存储群集的当前状态。监控器需要高度一致性，确保对Ceph 存储集群状态达成一致。维护着展示集群状态的各种图表，包括监视器图、 OSD 图、归置组（ PG ）图、和 CRUSH 图。
- **MDSs**: Ceph 元数据服务器（ MDS ）为 Ceph 文件系统存储元数据。
- RGW：对象存储网关。主要为访问ceph的软件提供API接口。

### 




ceph-deploy mon create-initial

-------------------------------------------------------------------------------------------------------------------
12.6
#如果没有挂载第一种
ceph -s //查看集群状态
lsblk //查看硬盘
rbd list//客户端查看镜像
rbd map demo-image //将ceph提供的镜像映射到本地
lsblk //查看硬盘
mount /dev/rbd0 /mnt //挂载
#如果没有挂载第二种
df   				//查看挂载，如果rdb0已经挂载，卸载它
umount /mnt 		//卸载挂载
rbd showmapped 		//查看映射信息
rbd unmap /dev/rbd0 	//取消映射
lsblk 			//看不到rbd0了
rbd rm demo-image 	//删除镜像

每个节点的映射是单独的，其他镜像、快照都是可以在不同节点共同查看到的


存储的方式
块存储：提供了一块硬盘
文件系统：共享文件夹
对象存储：通过编成

块存储：
1.ceph上创建镜像
2.客户端映射
3.客户端第一次使用，对它进行格式化
4.客户端挂载
5.防止数据丢失，可以创建快照

文件系统
1.元数据
2.数据



-----------------------------------------------------------------------------------------
12.7
zabbix监控安装
安装nginx依赖包gcc pcre-devel openssl-devel
编译安装nginx
配置nginx支持php。配置php可以连接mysql的软件包php php-fpm php-mysql mariadb-server mariadb-devel
安装zabbix的依赖包net-snmp-devel curl-devel autoconf libevent-devel
编译安装zabbix
创建zabbix存储数据的数据库。创建名为zabbix的数据库，使用的字符集是utf8
创建名为zabbix的用户
导入zabbix表结构，3个sql文件按顺序导入
配置zabbix_server服务
zabbix_server服务端口号10051，日志文件位置tmp/zabbix_server.log 
创建用于运行zabbix的用户,-s /sbin/nologin: 用户不能登陆系统
创建用于管理zabbix的service文件
配置zabbix_agent服务自己被监控
zabbix_agent服务端口号10050日志文件位置/tmp/zabbix_agentd.log 

拷贝zabbix的web页面到nginx





----------------------------------------------------------------------------------------------------------------
12.8
## 在zabbix web管理平台中配置监控

- 主机：安装了agent，被监控的主机
- 主机组：根据需求，将多台主机加入到一个主机组中，方便管理。系统默认已经创建了一些主机组。
- 模板：是监控项的集合。将模板应用到主机，主机就可以直接拥有模板中的所有监控项。系统中默认已经创建了一些模板。


## 自定义监控项

### 实现监控web1用户数量的监控项

1. 在被控端创建key。被控端被监控的内容叫作key，可以理解为它就是一个变量名，具体的名字自己决定。
2. 在web页面中创建监控项。监控项对应key值。
3. 监控项存在应用集中。应用集就是相似监控项的集合。
4. 应用集存在模板中。一个模板可以包含多个应用集。

模板 应用集 监控项 key


编写ansible剧本使用loop循环为node2主机自动创建多个用户和密码，要求如下： 用户名tom，密码：ilovelinux 用户名jerry，密码：tedu




-----------------------------------------------------------------------------------------------------------------
12.9











----------------------------------------------------------------------------------------------------------------
12.10
linux本质是一个内核

## kali
- 实际上它就是一个预安装了很多安全工具的Debian Linux
[root@zzgrhel8 ~]# kali reset
kali reset OK.
该虚拟机系统用户名为:kali,密码为:kali
- 基础配置
$ ip a s    # 查看网络
$ nmcli connection show    # 查看到网络连名为"Wired connection 1"
$ nmcli connection modify "Wired connection 1" ipv4.method manual ipv4.address 192.168.4.40/24 autoconnect yes
$ systemctl start ssh      # 启ssh服务，弹出的窗口输入密码kali
[root@zzgrhel8 ~]# ssh kali@192.168.4.40
kali@192.168.4.40's password: kali
┌──(kali㉿kali)-[~]
└─$ 

### nmap扫描

- 一般来说扫描是攻击的前奏。
- 扫描可以识别目标对象是什么系统，开放了哪些服务。
- 获知具体的服务软件及其版本号，可以使得攻击的成功率大大提升。
- 扫描可以检测潜在的风险，也可以寻找攻击目标、收集信息、找到漏洞
- windows下，扫描可以使用xscan / superscan
- Linux，扫描可以采用nmap
- 吾爱破解：https://www.52pojie.cn/
- 中国黑客团队论坛：https://www.cnhackteam.org/

┌──(kali㉿kali)-[~]
└─$ nmap
# -sS: TCP半开扫描，效率高，对服务器不友好。TCP握手只完成2步
# -sT: TCP全开扫描。TCP三次握手全部完成。
# -U: 扫描目标的UDP端口。
# -sP：ping扫描
# -A：对目标系统全面分析

# 扫描整个网段，哪机器可以ping通
┌──(kali㉿kali)-[~]
└─$ nmap -sP 192.168.4.0/24

# 扫描192.168.4.11开放了哪些TCP端口
┌──(kali㉿kali)-[~]
└─$ sudo nmap -sT 192.168.4.11

# 扫描192.168.4.11开放了哪些UDP端口。非常耗时！
┌──(kali㉿kali)-[~]
└─$ sudo nmap -sU 192.168.4.11

# 全面扫描192.168.4.11系统信息
┌──(kali㉿kali)-[~]
└─$ sudo nmap -A 192.168.4.11 

- 使用脚本扫描
# 通过脚本扫描目标主机的ftp服务
# 在目标主机上安装vsftpd服务
[root@node1 ~]# yum install -y vsftpd
[root@node1 ~]# systemctl start vsftpd
[root@node1 ~]# useradd tom
[root@node1 ~]# echo 123456 | passwd --stdin tom

# 在kali主机上查看有哪些脚本
┌──(kali㉿kali)-[~]
└─$ ls /usr/share/nmap/scripts/

# 扫描ftp服务是否支持匿名访问。ftp控制连接端口号是21
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-anon.nse 192.168.4.11 -p 21
21/tcp open  ftp
| ftp-anon: Anonymous FTP login allowed   # 允许匿名访问

# 扫描ftp相关信息，如版本号、带宽限制等
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-syst.nse 192.168.4.11 -p 21

# 扫描ftp后门漏洞
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-vsftpd-backdoor 192.168.4.11 -p 21

- 扫描口令

# 通过ssh协议，使用nmap自带的密码本扫描远程主机的用户名和密码
# 在目标主机上创建名为admin的用户，密码为123456
[root@node1 ~]# useradd admin
[root@node1 ~]# echo 123456 | passwd --stdin admin

# 在kali上扫描弱密码
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ssh-brute.nse 192.168.4.11 -p 22

# 通过ssh协议，使用nmap以及自己的密码本扫描远程主机的密码
# 1. 创建用户名文件
┌──(kali㉿kali)-[~]
└─$ sudo echo root > /tmp/users.txt
                                                        
┌──(kali㉿kali)-[~]
└─$ cat /tmp/users.txt 
root

[root@node1 ~]# echo 20010322 | passwd --stdin root
# 2. 生成1990-01-01到2020-12-31之间的所月日期
[root@zzgrhel8 ~]# vim mydate.py
from datetime import datetime, timedelta
  
d1 = datetime(1989, 12, 31)
d2 = datetime(2021, 1, 1)
dt = timedelta(days=1)

with open('/tmp/mima.txt', 'w') as f:
    while d1 < d2:
        d1 += dt
        f.write(f"{d1.strftime('%Y%m%d')}\n")
[root@zzgrhel8 ~]# python3 mydate.py   # 将会生成/tmp/mima.txt
[root@zzgrhel8 ~]# scp /tmp/mima.txt kali@192.168.4.40:/tmp/

# 3. 使用自己的密码本破解密码
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ssh-brute.nse --script-args userdb=/tmp/users.txt,passdb=/tmp/mima.txt 192.168.4.11 -p 22

# 4. 目标主机将会记录所有的登陆事件
[root@node1 ~]# vim /var/log/secure
# 查看最近的登陆失败事件
[root@node1 ~]# lastb
# 查看最近的登陆成功事件
[root@node1 ~]# last


- 扫描windows口令

[root@zzgrhel8 ~]# cat /tmp/winuser.txt    # windows用户名
administrator
admin
# 通过samba服务扫描密码
[root@zzgrhel8 ~]# nmap --script=smb-brute.nse --script-args userdb=/tmp/winuser.txt,passdb=/tmp/mima 172.40.0.151

- 手工扫描

[root@node1 ~]# yum install -y telnet
# 查看目标主机80端口是否开放
[root@node1 ~]# telnet 192.168.4.11 80
Trying 192.168.4.11...
telnet: connect to address 192.168.4.11: Connection refused

## 使用john破解密码

- 在线破解哈值的网站：https://cmd5.com/

- kali系统提供了一个名为john的工具，可用于密码破解

[root@node1 ~]# echo 123456 | passwd --stdin root
[root@node1 ~]# useradd tom
[root@node1 ~]# echo abc123 | passwd --stdin tom
[root@node1 ~]# useradd jerry
[root@node1 ~]# echo 123123 | passwd --stdin jerry
[root@node1 ~]# scp /etc/shadow kali@192.168.4.40:/home/kali/

# 破解傻瓜式密码
┌──(kali㉿kali)-[~]
└─$ sudo john --single shadow 

# 字典暴力破解，密码本是/usr/share/john/password.lst
┌──(kali㉿kali)-[~]
└─$ sudo john shadow  

# 直接显示破解的密码，不显示其他额外信息
┌──(kali㉿kali)-[~]
└─$ sudo john --show shadow                
root:123456:18912:0:99999:7:::
tom:abc123:18912:0:99999:7:::
jerry:123123:18912:0:99999:7:::


# linux系统自带的密码本
[root@node1 ~]# yum install -y words
[root@node1 ~]# ls /usr/share/dict/
linux.words  words
[root@node1 ~]# wc -l /usr/share/dict/words 
479828 /usr/share/dict/words

# 字典暴力破解，指定密码本文件
[root@node1 ~]# scp /usr/share/dict/linux.words kali@192.168.4.40:/home/kali/
┌──(kali㉿kali)-[~]
└─$ sudo john --wordlist=linux.words shadow
## 

## 抓包

- 传输的各种数据，在网络中都是一个个的数据包

┌──(kali㉿kali)-[~]
└─$ sudo tcpdump
# -i：指定网络
# -A：转换为ASCII码，使得可读
# -w：抓包写入文件
# -r：从文件中读取抓包信息
# 抓包时可以过滤要抓哪些包
# 使用host过滤主机，使用net过滤网段，使用port过滤端口... ...

# 1. 抓包：抓取eth0上进出的、与192.168.4.11有关的、涉及TCP21端口的软件包。以下命令执行后，打开新终端。
┌──(kali㉿kali)-[~]
└─$ sudo tcpdump -i eth0 -A host 192.168.4.11 and tcp port 21

# 2. 在新终端登陆ftp
┌──(kali㉿kali)-[~]
└─$ ftp 192.168.4.11
Connected to 192.168.4.11.
220 (vsFTPd 3.0.2)
Name (192.168.4.11:kali): tom   # 用户名
331 Please specify the password.
Password:abc123   # 此处是tom的密码
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> exit    # 退出
221 Goodbye.

# 3.在tcpdump终端可以看到明文的用户名和密码



# 1. 将抓到的包存入文件ftp.cap
┌──(kali㉿kali)-[~]
└─$ sudo tcpdump -i eth0 -A -w ftp.cap host 192.168.4.11 and tcp port 21
# 2. 在另一个终端访问ftp
# 在新终端登陆ftp
┌──(kali㉿kali)-[~]
└─$ ftp 192.168.4.11
Connected to 192.168.4.11.
220 (vsFTPd 3.0.2)
Name (192.168.4.11:kali): tom   # 用户名
331 Please specify the password.
Password:abc123   # 此处是tom的密码
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> exit    # 退出
221 Goodbye.

# 3. 在抓包终端ctrl+c停止
# 4. 读取抓到的包，并过滤
┌──(kali㉿kali)-[~]
└─$ tcpdump -A -r ftp.cap | egrep 'USER|PASS' 


- 图形工具：wireshark

[root@zzgrhel8 ~]# yum install wireshark ftp

选择抓哪块网卡进出的数据，然后点左上角的开始



抓到包后，点击左上角同样位置停止，查看数据



## 安全加固

### nginx安全

- 安装启服务

[root@node1 lnmp_soft]# yum install -y gcc pcre-devel zlib-devel
[root@node1 lnmp_soft]# tar xf nginx-1.12.2.tar.gz 
[root@node1 lnmp_soft]# cd nginx-1.12.2/
[root@node1 nginx-1.12.2]# ./configure && make && make install
[root@node1 ~]# /usr/local/nginx/sbin/nginx 


- 访问不存在的路径



- 隐藏版本信息


[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 17 http {
 18     server_tokens off;
... ...
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload


再次访问不存在的路径，版本号消失


- 防止DOS、DDOS攻击
- DDOS：分布式拒绝服务


# 压力测试，每批次发送100个请求给web服务器，一共发200个
[root@zzgrhel8 ~]# yum install -y httpd-tools
[root@zzgrhel8 ~]# ab -c 100 -n 200 http://192.168.4.11/ 
... ...
Benchmarking 192.168.4.11 (be patient)
Completed 100 requests
Completed 200 requests
Finished 200 requests    # 发送200个请求完成
... ... 
Complete requests:      200   # 完成了200个请求
Failed requests:        0     # 0个失败
... ...

- 配置nginx连接共享内存为10M，每秒钟只接收一个请求，最多有5个请求排队，多余的拒绝

[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
 17 http {
 18     limit_req_zone $binary_remote_addr zone=one:10m
    rate=1r/s;   # 添加
... ...
 37     server {
 38         listen       80;
 39         server_name  localhost;
 40         limit_req zone=one burst=5;  # 添加
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload

# 再次测试
[root@zzgrhel8 ~]# ab -c 100 -n 200 http://192.168.4.11/ 
... ...
Benchmarking 192.168.4.11 (be patient)
Completed 100 requests
Completed 200 requests
Finished 200 requests
... ...
Complete requests:      200
Failed requests:        194   # 失败了194个
... ...


### 拒绝某些类型的请求

- 用户使用HTTP协议访问服务器，一定是通过某种方法访问的。
- 最常用的HTTP方法
  - GET：在浏览器中输入网址、在页面中点击超链接、搜索表单。
  - POST：常用于登陆、提交数据的表单

- 其他HTTP方法不常用，如：
  - HEAD：获得报文首部。HEAD 方法和 GET 方法一样，只是不返回报文主体部分。
  - PUT：传输文件。要求在请求报文的主体中包含文件内容，然后保存到请求 URI 指定的位置。
  - DELETE：删除文件。DELETE 方法按请求 URI 删除指定的资源。

# 使用GET和HEAD方法访问nginx。两种方法都可以访问
[root@zzgrhel8 ~]# curl -i -X GET http://192.168.4.11/
[root@zzgrhel8 ~]# curl -i -X HEAD http://192.168.4.11/


# 配置nginx只接受GET和POST方法
[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 37     server {
 38         listen       80;
 39         if ($request_method !~ ^(GET|POST)$ ) {
 40             return 444;
 41         }
... ...
# $request_method是内置变量，表示请求方法。~表示正则匹配，!表示取反。^表示开头，$表示结尾，|表示或

[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload

# 使用GET和HEAD方法访问nginx。只有GET可以工作
[root@zzgrhel8 ~]# curl -i -X GET http://192.168.4.11/
[root@zzgrhel8 ~]# curl -i -X HEAD http://192.168.4.11/


> 附：取出nginx.conf中注释和空行以外的行
>
> # -v是取反。^ *#表示开头有0到多个空格，然后是#。^$表示空行
> [root@node1 ~]# egrep -v '^ *#|^$' /usr/local/nginx/conf/nginx.conf
> ```

### 防止缓冲区溢出

- 缓冲区溢出定义：程序企图在预分配的缓冲区之外写数据。
- 漏洞危害：用于更改程序执行流，控制函数返回值，执行任意代码。

# 配置nginx缓冲区大小，防止缓冲区溢出
[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 17 http {
 18     client_body_buffer_size     1k;
 19     client_header_buffer_size   1k;
 20     client_max_body_size        1k;
 21     large_client_header_buffers 2 1k;
... ...
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload

## Linux加固

- 设置tom账号，有效期为2022-1-1

# 查看tom的账号信息
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：从不
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

[root@node1 ~]# chage -E 2022-1-1 tom
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：1月 01, 2022
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

# 设置账号永不过期，注意-E后面是数字-1，不是字母l
[root@node1 ~]# chage -E -1 tom
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：从不
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

# 设置新建用户的密码策略
[root@node1 ~]# vim /etc/login.defs 
 25 PASS_MAX_DAYS   99999    # 密码永不过期，设置最长有效期
 26 PASS_MIN_DAYS   0        # 密码最短使用时间，0表示随时可改密码
 27 PASS_MIN_LEN    5        # 密码最短长度
 28 PASS_WARN_AGE   7        # 密码过期前7天发警告
 33 UID_MIN                  1000   # 新建用户最小的UID
 34 UID_MAX                 60000   # 新建用户最大的UID

- 用户安全设置

# 锁定tom账号
[root@node1 ~]# passwd -l tom
锁定用户 tom 的密码 。
passwd: 操作成功

[root@node1 ~]# passwd -S tom   # 查看状态
tom LK 2021-10-12 0 99999 7 -1 (密码已被锁定。)

# 解锁tom账号
[root@node1 ~]# passwd -u tom
解锁用户 tom 的密码。
passwd: 操作成功
[root@node1 ~]# passwd -S tom
tom PS 2021-10-12 0 99999 7 -1 (密码已设置，使用 SHA512 算法。)

- 保护文件

# 查看文件的特殊属性
[root@node1 ~]# lsattr /etc/passwd
---------------- /etc/passwd    # 没有特殊属性

# 修改属性
chattr +i 文件    # 不允许对文件做任何操作
chattr -i 文件    # 去除i属性
chattr +a 文件    # 文件只允许追加
chattr -a 文件    # 去除a属性

[root@node1 ~]# chattr +i /etc/passwd
[root@node1 ~]# lsattr /etc/passwd
----i----------- /etc/passwd
[root@node1 ~]# useradd zhangsan
useradd：无法打开 /etc/passwd
[root@node1 ~]# rm -f /etc/passwd
rm: 无法删除"/etc/passwd": 不允许的操作
[root@node1 ~]# chattr -i /etc/passwd
[root@node1 ~]# rm -f /etc/passwd   # 可以删除
[root@node1 ~]# ls /etc/passwd
ls: 无法访问/etc/passwd: 没有那个文件或目录

# 恢复passwd文件
[root@node1 ~]# cp /etc/passwd- /etc/passwd



--------------------------------------------------------------------------------------------------------
12.13

高层协议依赖底层协议
应用 ssh http ftp
表示
会话
传输 tcp udp 端口号
网络 icmp
数据网络层
物理层

防火墙filter表

配置iptables时，不指定表，就是使用filter表
配置时不指定规则链，则配置所有链
可以向规则链中加入很多规则，数据包进入该链时，从上向下匹配，一旦匹配就停止，开始应用规则。如果全都不匹配，则应用默认规则
命令选项、链名、目标操作使用大写字母，其他小写
F I A L 
INPUT
REJECT DROP ACCEPT
-s192.168.1.1 -p tcp -dport 80
iptables的语法
iptables [-t 表名] 选项 [链名] [条件] [-j 满足条件的操作]


全不
----------------------------------------------------------------------------------------
12.14



-------------------------------------------------------------------------------------------
12.15

if判断 
所有空都为假，其他为真
0为假，空列表为假，空元祖为假，空字典为假。

print输出默认在结尾打印回车，通过end=" "命令，可以把回车改为空格，或者自定义改为其他内容
print输出通过sep=“”命令，可以把同一行内容中，不同输出字符之间的间距进行更改，不更改默认为空格
---------------------------------------------------------------------------------------------

12.16










---------------------------------------------------------------------------------------------------
12.29

快速配置LNMP
在proxy主机家目录下将lnmp_soft.tar.gz拷贝到web1
[root@proxy ~]# scp lnmp_soft.tar.gz root@192.168.2.100:root
首先在web1主机：
yum -y install gcc make pcre-devel openssl-devel //安装编译工具;让nginx支持正则;让nginx支持安装加密网站
[root@web1 ~]# tar -xf lnmp_soft.tar.gz
[root@web1 ~]# cd lnmp_soft/
tar -xf nginx-1.17.6.tar.gz
cd  nginx-1.17.6/
./configure   //配置
make   //编译
make  install   //安装
yum -y install mariadb mariadb-server mariadb-devel //安装数据库相关软件包;数据库,客户端服务,依赖包
yum -y install php php-mysql php-fpm   //安装php相关软件包;解释器,nginx解析php服务包,关联软件包
systemctl start mariadb
systemctl start php-fpm


./configure --with-http_ssl_module --with-http_stub_status_module
./configure --with-http_ssl_module --with-http_stub_status_module


LNMP
wordpress 快速创建网站的开源软件（php语言编写）

nginx的重新部署
killall nginx  //停止nginx程序
rm -rf  /usr/local/nginx    //删除nginx原有目录
# rm -rf nginx-1.17.6
# tar -xf nginx-1.17.6.tar.gz
# cd nginx-1.17.6/
#yum -y install gcc make pcre-devel openssl-devel 
# //安装编译工具 //安装可以让nginx支持正则的软件包 //安装可以让nginx支持安装加密网站的软件包
#如果不小心修改了nginx-1.17.6的配置文件，则执行此步骤
cd  /root/lnmp_soft/nginx-1.17.6
./configure  --with-http_ssl_module    //配置
make   //编译
make install   //安装


--------------------------------------------------------------------------------------------------------
12.30
一、LNP+Mariadb数据库分离
一台机器最好只运行一个服务。
单点故障
提高服务器被访问的速度

备份数据库
mysqldump 数据库      >     备份的名字
mysqldump wordpress > wordpress.bak 

远程访问数据库
[root@web1 ~]# mysql -h192.168.2.21 -uwordpress  -pwordpress wordpress


二、网站集群
nfs之提供资源的共享，当你来访问的时候，nfs不负责传递。
rpcbind 帮助nfs做数据传输的
/web_share  192.168.2.0/24(rw,no_root_squash) //以管理员身份执写入

验证是否挂载成功，显示共享目录和网段
showmount -e localhost

如果提示rpcbind需要升级则
stop nfs
stop rpcbind
rpm -e --nodeps rpcbind

挂载
首先查看客户机是否能访问到共享
~]# which showmount  //查询是否有这条命令，没有则需要安装nfs-utils
showmount -e 192.168.2.31 //查询是否可以访问到192.168.2.31主机的共享目录

挂载fstab里没有加载的设备
–设备路径    挂载点    文件系统类型   参数    备份标记   检测顺序
/dev/cdrom   /dvd   iso9660  defaults  0  0
挂载点的绝对路径                      要挂载到的位置             类型
192.168.2.31:/web_share/html /usr/local/nginx/html/ nfs defaults 0 0

三、部署HAProxy代理服务器

三：部署DNS域名服务器
     架设DNS服务器，让客户端可以通过主机名访问公司的网站

启动顺序如下
	1启动31主机 并查看nfs服务rpcbind服务
	showmount -e localhost查看共享文件夹
	如果看不到，就重启nfs,rpcbind

	2启动21主机 并查看mariadb状态
	查看3306端口，查不到就重启mariadb服务

	3依次启动 web1 web2 web3 并查看80端口 9000端口
	如果没有80端口，就重启nginx服务
	如果没有9000端口就重启php-fpm
	showmount -e 192.168.2.31 可以查看到共享文件
	/usr/local/nginx/sbin/nginx -t 可以检测nginx的配置文件是否正确

	4启动调器其主机系统并查看 haproxy服务状态
	查看80端口是否对应haproxy,如果没有则启动haprxoy服务
	5在31主机访问调度器能够访问到网页
	比如curl http://192.168.2.5/test.php


--------------------------------------------------------------------------------------------------
12.31
三：部署DNS域名服务器
     架设DNS服务器，让客户端可以通过主机名访问公司的网站
     将主机名解析到调度器
    @        NS     dns.lab.com.
@ = 主机名 lab.com
NS = 名称服务器nameserver
dns.lab.com. 随便写
四、使用KeepAlived软件 部署 调度器的高可用集群
持续性提供服务就是高可用
interface eth0 //要选择配置公网IP地址的网卡
五、把存储在NFS 服务器里的网页文件 迁移到ceph集群里
防止数据库服务器出现单点故障，导致整个网络布局崩溃
ceph集群环境准备
[root@node1 ~]#echo 'mount /dev/cdrom /ceph/' >> /etc/rc.local
[root@node1 ~]# chmod +x /etc/rc.local 

1 主机名绑定
[root@node1 ~]# vim /etc/hosts      #修改文件，手动添加如下内容（不要删除原文件的数据）
192.168.2.41    node1
192.168.2.42     node2
192.168.2.43    node3
2 免密登陆
ssh-keygen node1 node2 node3

3 配置NTP服务

4 配置yum源
使用系统安装光盘做yum源
使用ceph10.iso光盘做yum源
   1 ceph10.iso放到虚拟机添加的光驱里
    2 把ceph10.iso镜像挂载到创建的/ceph目录里
    3 编写yum源配置文件 ceph.repo
[MON]
name=monitor soft
baseurl=file:///ceph/MON
enabled=1
gpgcheck=0

[OSD]
name=storage soft
baseurl=file:///ceph/OSD
enabled=1
gpgcheck=0

[Tools]
name=tools soft
baseurl=file:///ceph/Tools
enabled=1
gpgcheck=0
因为系统安装光盘里没有ceph集群的软件包，ceph集群的软件由ceph10.iso提供
在安装ceph软件有依赖，依赖的是操作系统安装光盘里的软件

部署ceph集群

清理错误文件
ceph-deploy purge node1
ceph-deploy purge node1
ceph-deploy purge node1

ceph-deploy purgedata node1
ceph-deploy purgedata node2
ceph-deploy purgedata node3



-----------------------------------------------------------------------------------
1.4
ceph 集群共享储存空间的3种方式
块存储 文件存储 对象存储
对象存储 客户端需要有图形界面，比较适合客户端是windows操作系统，去访问linux系统的ceph集群
块和文件 适合linux系统
客户端使用ceph就像用本机硬盘一样，就选在块存储
在ceph端已经划分好文件系统的大小，在给客户端的模式，就选择文件存储

文件储存方式
启动mds服务（可以在node1或node2或node3启动，也可以在多台主机启动mds）
inode和block
inode存储数据信息的数据：元数据（类似目录）
block存储数据的（类似正文）

web1
卸载nfs
挂载ceph
拷贝nfs的网页文件到自己目录下，也就是相当于拷贝到ceph集群的myfs1空间中
web2，web3同操作，在工作中需要一台一台的停+拷贝转移，这样不会影响当前访问的服务。

挂载ceph集群的文件系统时需要书去ceph集群的用户名和密码
在node1，node2，node3上查看都可以
key = AQCP0s5hJEUxKRAAt+zaUZtgOi88ilStC66m0Q==

挂载ceph集群的文件系统有3种方式

mount -t ceph 192.168.2.41:6789:/ /usr/local/nginx/html/ -o name=admin,secret=AQCP0s5hJEUxKRAAt+zaUZtgOi88ilStC66m0Q==

echo 'mount -t ceph 192.168.2.41:6789:/ /usr/local/nginx/html/ -o name=admin,secret=AQCP0s5hJEUxKRAAt+zaUZtgOi88ilStC66m0Q==' >> /etc/rc.local 

tar -czpf /root/html.tar.gz ./*
tar -czpf /root/html.tar.gz ./*

scp /root/html.tar.gz 192.168.2.11:/usr/local/nginx/html
scp /root/html.tar.gz 192.168.2.11:/usr/local/nginx/html


部署Git版本控制系统

一个应用程序软件是由多个脚本组成，所以需要有一个存放脚本文件的服务器，服务器要支持回滚（就是快照恢复）
所以就要用git。

3种连接方式
第一种采用ssh协议的方式连接版本库,缺点是需要输入服务器的管理员密码，不安全。
第二种通过git协议连接版本库,在版本库服务器上安装git-daemon并开启服务，客户端可以直接通过协议访问，不需要输入管理员密码
yum -y install git-daemon
这里两种方式适合客户端没有图形界面的时候，适合linux系统
第三种http协议
适合有图形界面的操作系统 安装httpd gitweb 

优化web服务器
nginx特性平滑升级，在不停止nginx服务的情况下，升级软件的版本，不影响网站的访问，如果停止服务，反倒无法升级
不可以跨版本升级，容易出现软件依赖的问题
/usr/local/nginx/sbin/nginx -v    //查办本
/usr/local/nginx/sbin/nginx -V    //查看安装模块

./configure --with-http_ssl_module --with-http_stub_status_module
	      --with-http_ssl_module --with-http_stub_status_module

访问没有网页的时候不显示404，显示默认页

首页文件可以有多个
访问时 先访问列表中的第一个 如果第一个没有就访问第二个

日至的切割
把服务器上每天的日至信息都储存 单独的日志文件储存
而不是所有的日至信息都存在一个日志文件里

chmod +x /usr/local/nginx/logbak.sh






-------------------------------------------------------------------------------
1.5
数据库课程

ps -C mysqld //查服务进程

mysql> show variables like "%password%";  查看与密码相关的配置项
mysql> set global validate_password_policy=0;  修改密码等级为0 //0 1 2 代表低中高
mysql> set global validate_password_length=6;  修改最小密码长度
mysql> alter user  root@"localhost" identified by "tarena"; 根据新密码策略修改密码

[root@host50 ~]# vim /etc/my.cnf  （永久配置）把修改 添加到配置文件里 数据库服务重启了 依然有效 
[mysqld]
validate_password_policy=0
validate_password_length=6

登陆后修改 alter user
登陆前修改密码
mysqladmin修改 （操作系统管理员修改 本机数据库服务的登陆密码）
]# mysqladmin  -uroot    -p旧密码   password  新密码

隐藏旧密码和新密码,根据提示输入密码
]# mysqladmin  -uroot    -p  password 
第一次提示 输入旧密码
第二次提示 输入新密码  （新密码 要服务密码策略要求

在数据库服务器上安装图形软件
安装phpmyadmin软件，跨平台，开源，浏览器访问使用

apache不需要安装php-fpm调用php，只有nginx需要

cp config.sample.inc.php  config.inc.php  //拷贝模板，创建主配置文件 

rpm -e --nodeps httpd //忽略依赖关系卸载

Mysql相关参数
/etc/my.cnf	       主配置文件
/var/lib/mysql	 数据库目录
默认端口		  3306
进程名			  mysql
传输协议		  TCP
进程所有者	     	  mysql
进程所属组		  mysql
错误日志文件   	  /var/log/mysqld.log

客户端连接mysql服务的方法
命令行连接
图形软件连接
脚本连接
常用图形软件
MySQL-Workbench 跨平台
MySQL-Front      win
Navicat          win
phpMyAdmin      跨平台

mysql 的所有者所有组都必须要是 mysql 不然不能进行操作

SQL语句分类
DQL数据查询语言
负责进行数据查询而不会对数据本身进行修改的语句
DDL 数据定义语言
负责数据结构定义与数据库对象定义的语言，由 CREATE，ALTER，DROP三个语法组成
DML数据操纵语言
负责对数据库对象运行数据访问工作的指令集 INSERT，UPDATE，DELETE，SELECT
DCL数据控制语言
它可以控制特定用户账户对数据访问权限，由GRANT，REVOKE组成

命令格式
1、as 或 空格 作用为别名
mysql> select name , homedir  from tarena.user;
mysql> select name as 用户名 , homedir 家目录 from tarena.user;

2、给查找到的数据拼接   concat()
mysql> select name,uid from tarena.user;
mysql> select concat(name,"-",uid) from tarena.user;
mysql> select concat(name,"-",uid)  as  用户信息  from tarena.user;

3、查询的数据有重复时 ，不显示重复     distinct  字段名列表
mysql> select shell from  tarena.user;
mysql> select distinct shell from  tarena.user;

范围匹配条件   in   、  not  in  、  between  数字1 and   数字2 
mysql> select  id, name,uid  from  tarena.user where  id between 10 and 20 ;

mysql> select name , uid  from  tarena.user where  uid  in (10 , 20 , 30 , 50);


select name , shell  from  tarena.user where  shell  not in ("/bin/bash","/sbin/nologin");
select name,shell from user where shell not in ("/bin/bash","/bin/sync");



select name , uid  from  tarena.user where  uid  in (1 , 3 , 5 , 7);
select id,name from user where id in (1,3,5,7);



-----------------------------------------------------------------------------------------
1.6

客户端把数据存储到数据库服务器上过程是什么样的？
			第一步:连接数据库服务器（连接的方式：命令行  脚本  访问图形工具）
		   	第二步:创建存储数据的库 （存放表的目录）
			第三步:创建存储数据的表  (表就是存储数据的文件)
			第四步:插入表记录 (向文件里添加行)
			第五步:断开连接

库名是有命名规则？ （要记牢）
仅可以使用数字、字母、下划线、不能纯数字
区分字母大小写，
具有唯一性
不可使用指令关键字、特殊字符

建库
mysql> CREATE DATABASE gamedb ;
重名建库命令不报错
mysql> CREATE DATABASE IF NOT EXISTS gamedb ; 
删库
drop database 库名
mysql> drop database gamedb;
删没有的库命令不报错
drop database if exists gamedb;

建表
create table 库名(在当前库下可以不写库名).表名(表头名 类型(长度限制)) 
删表
drop table 表名

表的修改
alter table 库名(在当前库下可以不写库名).表名 操作命令
添加新表头   add  默认在最后，first为首位，after为在某字段的后面       
删除表头   drop          
修改表头存储数据的 数据类型  modify 
修改表头名  change 
修改表名  rename 
mysql> alter table studb.stu rename studb.stuinfo;  修改表名 

mysql> alter table studb.stuinfo drop age ;  	  删除字段
mysql> alter table stuinfo drop gender,drop class;  删除多字段

mysql> alter table stuinfo add mail char(30);       添加字段
mysql> alter table  studb.stuinfo add  number  char(9) first , add  school char(10) after name; 添加多字段

mysql> alter table stuinfo modify mail varchar(50) not null default "plj@tedu.cn";  修改字段类型、设置是否为空、默认值。
mysql> alter table user add birthday year default 1990 after name;

mysql> alter table stuinfo modify name char(10) after mail; 修改表头位置

mysql> alter table stuinfo change number id char(9);  修改表头名 
mysql> alter table stuinfo change mail Email char(60); 原有表头规则内容例如是否为空、默认值如果不写，则还原默认


#复制表，拷贝已有的表

表头和数据复制都复制的 命令格式create  table  库名.表名   select  *  from  库名.表名 ；
mysql> create table studb.user select * from tarena.user;

仅仅复制表头命令格式：（只复制表结构）
命令格式  CREATE  TABLE 库.表   LIKE  库.表
mysql> create table studb.user1 like tarena.user;



管理表记录
一、insert 插入命令
不指定列名插入记录(必须给所有列赋值)
insert into 表名 values (对应表头的内容，顺序填写，""括起 ,号间隔)
mysql> insert into stuinfo values("1","bob","tarena","nsd2109","boy","bob@tedu.cn")   插入一行
mysql>insert into stuinfo values("1","bob","tarena","nsd2109","boy","bob@tedu.cn"),("2","tom","tarena","nsd2110","boy","tom@tedu.cn");  插入多行

指定列名插入记录(仅须给指定列赋值)
insert into 表名(表头名) values (对应前面表头的内容，顺序填写，""括起 ,号间隔)
mysql> insert into stuinfo(name,school) values("lucy","tarena"),("lili","yhy")  

根据查询结果插入
insert into 表名(表头名) (通过select查询的内容，查询那几个表头，就插入对应的表头内容)
mysql> insert into stuinfo (name,school,class) (select name,school,class from stuinfo where id in (1,2));

set 命令插入数据
insert into 库.表 set  字段名=值 , 字段名=值 , 字段名=值
mysql> insert into stuinfo set name="john",gender="boy",Email="john@163.com";

二、update 修改命令

修改行中列的数据  使用update 
批量修改  （不加筛选条件修改） 
命令格式 ：update  库名.表名   set  字段名 =  值 ，  字段名 =  值 ；
mysql> update stuinfo set Email="plj@tedu.cn",gender="gril"

仅修改与筛选条件匹配的字段 （修改时加筛选条件）
命令格式 ：update  库名.表名   set  字段名 =  值 ，  字段名 =  值 where 筛选条件；
mysql> update stuinfo set Email="plj@tedu.cn",gender="gril" where name = "lucy";
mysql> update stuinfo set class="nsd2022" where class is null;

三、delete 删除命令

仅删除符合条件的行   （删除命令有筛选条件）  delete   from  库.表  where  筛选条件；
mysql> delete from stuinfo where class !="nsd2022";

删除表里的所有行（删除命令有筛选条件）       delete   from  库.表
mysql> delete from stuinfo 



数据类型

数值类型 工资 体重 编号
	整型 只能储存整数
	浮点型 能储存带小数点
字符类型 姓名 地址 网址
日期类型 注册时间   出生日期 
枚举类型 表头值只能在列举的范围内选择  爱好 性别 婚姻

整型类型
类型            名称          有符号范围       无符号范围
tinyint     微小整数      -128~127           0~255
smallint    小整数	      -32768～32767      0～65535
mediumint   中整数	    
int         大整数
bigint      极大整数   
unsigned    使用无符号储存范围

浮点类型
float
double
decimal

字符类型
char       定长类型    最多255字符
varchar    变长类型    最多65532字符

-----------------------------------------------------------------------------------------------------
1.7
#查看表使用的字符集
mysql> show create table studb.t1 \G
*************************** 1. row ***************************
       Table: t1
Create Table: CREATE TABLE `t1` (
  `level` tinyint(3) unsigned DEFAULT NULL,
  `money` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1  西欧字符编码
1 row in set (0.00 sec)

说明 ：
ENGINE=InnoDB  定义存储引擎（存储引擎后边会讲） 
DEFAULT   CHARSET=latin1 定义字符集 （默认字符集是 latin1  西方国家使用的字符编码）


#建表时指定表使用的字符集 utf8  
mysql> create  table  studb.t3( name  char(3) , address  varchar(10) )  default  charset  utf8;


#枚举类型    给表头赋值时 ，值必须在类型规定的范围内选择
单选    enum(值1，值2，值3 ....)
多选    set(值1，值2，值3 ....)

mysql>  create  table studb.t8(姓名 char(10),性别  enum("男","女","保密"), 爱好 set("帅哥","金钱","吃","睡") ) DEFAULT CHARSET utf8;



#日期时间类型  表头存储与日期时间格式的数据
年        year       YYYY     2021
日期    date        YYYYMMDD    20211104
时间    time       HHMMSS         143358        

日期时间（既有日期又有时间）  
datetime   或  timestamp       YYYYMMDDHHMMSS     20211104143648
datetime存储范围大  timestamp范围小）
timestamp类型      不赋值的情况下自动使用系统时间自动赋值
datetime类型       不赋值的情况下自动使用null  赋值 

mysql> create table studb.t6( 姓名  char(10), 生日  date , 出生年份 year , 家庭聚会  datetime , 
聚会地点  varchar(15), 上班时间 time)default charset utf8;

mysql> insert into  studb.t6 values ("翠华",20211120,1990,20220101183000,"天坛校区",090000);

 使用2位数给year的表头赋值也是可以的  但会自动补全4位数
 01-69  之间的数字使用20补全4位数的年 2001~2069
 70-99  之间的数字使用19补全4位数的年 1970~1999


注意：数据导入或导出 存放数据的文件必须在mysql服务要求的目录下 叫检索目录
所有在学习数据导入导出前要先掌握 检索目录的管理
第一 要知道数据库服务默认的检索目录
#查看默认的检索目录
mysql> show variables like  "secure_file_priv";
#修改数据库服务默认的检索目录
[root@host50 ~]# vim /etc/my.cnf
[mysqld]
secure_file_priv=/myload      #手动添加
:wq
[root@host50 ~]# mkdir /myload
[root@host50 ~]# chown mysql /myload/
[root@host50 ~]# ls -ld /myload/
drwxr-xr-x 2 mysql root 6 11月  5 16:37 /myload/
[root@host50 ~]# setenforce 0
setenforce: SELinux is disabled
[root@host50 ~]# systemctl  restart mysqld
[root@host50 ~]# mysql -uroot -p123456
mysql> show variables like  "secure_file_priv";


数据导入（一次性向表里存储很多数据）把系统文件的内容存储到数据库服务的表里
文件的内容要规律
诉求：将/etc/passwd文件导入db1库的t3表里			 
数据导入格式：
mysql>  load   data  infile   "/检索目录/文件名"   into    table    库名.表名   fields  terminated by  "文件中列的间隔符号"   lines   terminated by "\n";
mysql> load data infile "/myload/passwd" into table db1.user fields terminated by ":" lines terminated by "\n";

 
数据导入的操作步骤：
1） 要创建存储数据库（如果没有的话）
2）  建表 (根据导入文件的内容 创建 表头名  表头个数  表头数据类型  根据文件内容定义)
3） 把系统文件拷贝的检索目录里
4)  数据库管理执行导入数据的命令
5） 查看数据




数据导出（一次性把表里的数据都取出来）把数据库服务的表里数据保存到系统文件里
注意导出的数据不包括表头名 ，只有表里行。存放导出数据的文件名 不需要事先创建且具有唯一。
数据导出命令格式：
1) select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名" ;
mysql> select * from salary where basic > 20000 and date = "2020-05-10" into outfile "/myload/salary.txt";

2) select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名" fields  terminated by  "符号" ；	
fields  terminated by  指定导出的列 在文件中的间隔符号 		不指定默认是一个 tab 键的宽度		
mysql> select * from salary where basic > 20000 and date = "2020-05-10" and employee_id = 7 into outfile "/myload/salary2.txt" fields  terminated by ":";		
3）select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名"  fields  terminated by  "符号"    lines     terminated by  "符号" ；	
lines     terminated by   指定导出的行在文件中的间隔符号 不指定默认一条记录就是文件中的1行





1  字段约束
字段约束的作用是什么？ 设置在表头上 ，用来限制字段赋值
字段都有哪些约束？（每一种约束都有各自的功能）
字段约束分类：
1、 PRIMARY KEY：主键，用于保证该字段的值具有唯一性并且非空。


2、NOT NULL ：非空，用于保证该字段的值不能为空。

3、DEFAULT：默认值，用于保证该字段有默认值。
mysql> create table db1.t31(name char(10) not null , class char(7) default "nsd2107", likes set("money","game","film","music") not null default "film,music");

4、UNIQUE：唯一索引，用于保证该字段的值具有唯一性，可以为空。
唯一索引 （unique） 约束的方式：表头的值唯一(表头的值不能重复) 但可以赋null 值
mysql> create 	 table  DB1.t43 (name  char(10) , hz_id   char(18)  unique  );
#删除索引
mysql> alter table t43 drop index hz_id;


5、 FOREIGN KEY：外键，用于限制两个表的关系，用于保证该字段的值必须来自于主表的关联列的值，在从表添加外键约束，用于引用主表中某些的值。


*********主键********
主键使用规则
1、 字段值不允许重复，且不允许赋NULL值
2、一个表中只能有一个primary  key字段
3、多个字段都作为主键，称为复合主键，必须一起创建
4、主键字段的标志是PRI
5、主键通常与auto_increment  连用
6、通常把表中唯一标识记录的字段设置为主键[记录编号字段] 

#建表时，创建主键 （2种语法格式 要记住）
语法格式1
create  table  库.表（ 字段名 类型  primary key  , 字段名 类型 , ..... ）;
mysql> create table t35(name char(10),hz_id char(10) primary key,class char(10));

语法格式2
create  table  库.表（ 字段名 类型 , 字段名 类型 , primary key（字段名） ）;
mysql> create table t36(name char(10),hz_id char(10),class char(10),primary key(hz_id));

#删除主健
删除主键命令格式（要记住）向表头下存储数据不受主键的限制
mysql> alter  table   库.表   drop primary  key ;
mysql> alter  table db1.t36  drop primary key ;

#添加主键
添加主键命令格式（要记住）
mysql> alter  table  库.表  add  primary key(表头名);
mysql> alter  table  db1.t36  add  primary key(hz_id);


#复合主键
复合主键  表中的多个表头一起做主键 
复合主键的约束方式: 多条记录 主键字段的值不允许同时相同
例子如下
create  table  db1.t39(cip   varchar(15) , port  smallint ,status  enum("deny","allow") , primary key(cip,port));



主键与auto_increment连用: 
当给字段设置了auto_increment属性后，插入记录时，如果不给字段赋值
字段会通过自加1的计算结果赋值
要想让字段有自增长 那么字段必须有主键的设置才可以
查看表结构时 在  Extra （额外设置） 位置显示   

命令格式
mysql> create table t38(id int primary key auto_increment,name char(5),class char(10),address varchar(50)) default charset utf8;

mysql> alter table user add id int primary key auto_increment first;

#删除自增长的命令
命令格式 truncate table 表名;
mysql> truncate table t38;



------------------------------------------------------------------------------------------------------
1.10

外键的使用
#使用规则
表储存引擎必须是innodb  //create table 库.表() engine=innodb
字段类型要一致
被参照字段必须要是索引类型的一种（primary_key）
向有外键标签的表头下储存数据时，必须在参考的表中指定表头的值范围内选择
#作用
插入记录时，字段值在另一个表字段值范围内选择。

存储引擎
create table 库.表() engine=innodb
#创建外键命令格式
create table   库.表(表头列表 , 
foreign key(表头名)   #指定外键
references 库.表(表头名)   #指定参考的表头名
on update  cascade   #同步更新
on  delete  cascade  #同步删除
)engine=innodb;
mysql> create table gztab(gz_id int,pay double,foreign key(gz_id) references ygtab(yg_id) on update cascade on delete cascade)engine=innodb;
#查看外键
mysql> show create table gztab\G
#添加外键
mysql> alter table 库.表 add  foreign key(表头名)  references 库.表(表头名)  on update cascade  on delete cascade;
mysql> alter table gztab add foreign key(gz_id) references ygtab(yg_id) on update cascade on delete cascade ;
#删除外键
mysql> alter table 库.表 drop FOREIGN KEY   外键名;
mysql> alter table gztab drop foreign key gztab_ibfk_1;

alter table gztab add foreign key(gz_id) references ygtab(yg_id) on update cascade on delete cascade ;

外键验证：
1   外键字段的值必须在参考表字段值范围内
2   验证同步更新（ on update cascade)
3   验证同步删除（ on  delete  cascade)  



mysql索引的使用
#什么是索引
索引是设置在比表中的表头上，作用是给表头下存储的数据生成排队信息
是帮助mysql高效获取数据的数据结构
为快速查找数据而排好序的一种数据结构
类似书的目录
可以用来快速查询表中的特定记录，所有的数据类型都可以被索引
mysql索引主要有三种结构: Btree,B+Tree,Hash
#索引的优点缺点
优点
1、可以大大提高MySQL的检索速度
2、索引大大减小了服务器需要扫描的数据量
3、索引可以帮助服务器避免排序和临时表
4、索引可以将随机IO变成顺序IO
缺点
1、虽然索引大大提高了查询速度，同时却会降低更新表的速度，如对表进行insert，update，delete。因为更新表示时，mysql不仅要保存数据，还要保存索引文件。
2、建立索引会占用磁盘空间的索引文件。一般情况这个问题不太严重，但如果你在一个大表上创建了多种组合索引，索引文件会膨胀很快。
3、如果某个数据列包含许多重复的内容，为它建立索引就没有太大的实际效果。
4、对于非常小的表，大部分情况下简单的全表扫描更高效。

索引分类
#普通索引 index 任何数据都可以，可null，可重复
不应用任何限制条件的索引，该索引可以在任何数据类型中创建。
字段本身的约束条件可以判断其值是否为空或唯一。
创建该类型索引后，用户在查询时，便可以通过索引进行查询。
#唯一索引 unique 
使用UNIQUE参数可以设置唯一索引。
创建该索引时，索引的值必须唯一。
通过唯一索引，用户可以快速定位某条记录。
主键是一种特殊唯一索引。
#全文索引 fulltext
使用FULLTEXT参数可以设置索引为全文索引。
全文索引只能创建在CHAR、VARCHAR或者TEXT类型的字段上。
查询数据量较大的字符串类型的字段时，使用全文索引可以提高查询速度。
在默认情况下，应用全文搜索大小写不敏感。如果索引的列使用二进制排序后，
可以执行大小写敏感的全文索引。
#单列索引
顾名思义，单列索引即只对应一个字段的索引。
应用该索引的条件只需要保证该索引值对应一个字段即可。
可以包括普通、唯一、全文索引。
#多列索引
多列索引是在表的多个字段上创建一个索引。
该索引指向创建时对应的多个字段，用户可以通过这几个字段进行查询。
要想应用该索引，用户必须使用这些字段中的第一个字段。
#索引的使用规则

#索引查看 创建 删除 添加 验证索引
#建表时创建索引命令格式    
CREATE TABLE  库.表(字段列表 ,INDEX(字段名) ,INDEX(字段名));	
CREATE TABLE tea4(id char(6) NOT NULL,name varchar(6) NOT NULL,age int(3) NOT NULL,gender ENUM('boy','girl') DEFAULT 'boy',INDEX(id),INDEX(name));

#在已有表添加索引命令格式  CREATE  INDEX  索引名  ON  库.表(字段名)；
mysql> create index shengri on t31(birthday);

#删除索引  的命令格式   DROP  INDEX   索引名   ON  库.表；
mysql> drop index shengri on t31;

#查看索引详细信息   show index   from   库.表；
mysql> show index from tea4;
explain命令 : 	 可以查看执行的查询select语句， 是否使用到索引做查询了
命令格式explain select查询


1  用户授权
数据库管理员root用户连接数据库服务后，添加普通用户、设置用户权限和 用户密码
1.2  为什么要学习用户授权： 
默认情况，只允许数据管理员root 在本机访问数据服务。默认不允许其他客户端访问服务  也不能使用其他用户在本机连接数据库服务

#用户授权命令格式
GRANT 权限列表 ON 库名 TO 用户名@"客户端地址"   IDENTIFIED  BY  “密码”    WITH GRANT OPTION;
mysql> grant all on *.* to root@"%" identified by "123qqq...A" with grant option;

权限列表  ： 就是添加的用户对指定的库名具有的访问权限表示方式(就是学过的sql命令)
ALL        表示所有权限  （表示所有命令）
USAGE    表示无权限  （除了查看之外啥权限都没有 ， 看的命令show    desc ）
SELECT,UPDATE,INSERT          表示只有个别权限
SELECT, UPDATE (字段1,字段N)   表示权限仅对指定字段有访问权限

库名  ：添加的使用户对服务器上的那些库有访问权限 可以表示的方式有：
*.*          表示 所有库所有表   第1个表示所有库名  第2个 表示所有表名
库名.* 	  表示一个库下的所有表   例如 tarena.*
库名.表名	  表是一张表   例如  tarena.user

用户名： 添加用户时自定义即可，  存储在mysql库下user 表的user字段下  

客户端地址：     表示网络中的那些主机可以使用添加的用户连接数据库服务表示的方式有：
%  	         	     表示网络中的所有主机
192.168.4.%    	 表示 192.168.4网段内的所有主机
192.168.4.1     	 表示仅仅是192.168.4.1   一台主机
localhost          表示数据库服务器本机

密码  ： 添加的用户连接数据服务时使用的密码 ，要符合数据库服务的密码策略
WITH GRANT OPTION ：  让添加的用户也可以使用grant命令再添加用户，但用户本身要对mysql库有insert的权限


#显示登陆用户访问权限
mysql> show grants;

#查询所有授权用户
mysql> select user,host from mysql.user;

#查询指定用户访问权限
mysql> show grants for admin@"localhost";


#修改授权用户登陆密码
修改已有授权用户的连接密码（修改的密码也要与当前数据库服务的密码策略匹配 ，修改的密码要使用password() 函数加密 ， 密码存储在 mysql.user表的authentication_string字段下,存储的是加密后的密码）
mysql> set password for 用户名@"客户端地址"=password("新密码");
mysql> set password for root@"%"=password("123456");



#给已有追加权限（库名 用户名 客户端地址都不变就是追加权限）	给admin用户追加插入记录的权限
mysql> grant insert on  *.* to admin@"localhost" ;

#只撤销 with grant option的权限。
mysql> revoke grant option on  *.* from root@"%";

#只撤销用户删除记录的权限(库名要与原先保持一致)
mysql> revoke 权限列表 on 库民 from 用户@"客户端地址"
mysql> revoke delete on  *.* from root@"%";

#撤销用户全部权限
mysql> revoke all on *.* from root@"%";
    revoke all on db1.user from plj@"192.168.4.%";
#删除用户  drop user  用户名@"客户端地址";
例子： drop usessr  root@"%";
例子： drop user  admin@"localhost";

*************************************************************
用户授权命令格式
grant  权限列表  on  库名  to   用户@”客户端地址" 
identified by  “密码”    with grant option ;

撤销用户权限  revoke  权限列表  on  库名  from  用户@”客户端地址" ;

删除用户  drop  user  用户@”客户端地址" ;  

查看服务器已有的用户  select   host ， user  from  mysql.user;

查看已有用户的权限  show  grants  for  用户@”客户端地址" ; 

给已有用户追加权限 （库名 用户名 客户端地址都不变就是追加权限）
grant  权限列表 on   库名  to   用户@”客户端地址" ;

修改已有授权的密码grant   all  on  gamedb.*  to  adminONE@"%" identified by "123qqq...A" ；
set password  for 用户@”客户端地址"=password("密码");


*************************************************************




#授权库mysql的使用（存储的已有的用户和访问权限）

user 记录已授权的用户
select host,user from mysql,user;

db   记录授权用户对库的访问权限
select * from mysql.db;

tables_priv 记录授权用户对表的访问权限

columns_priv  记录授权用户对表头的访问权限



---------------------------------------------------------------------------------------------------
1.11
**基础查询进阶**
字符函数    数学函数 日期时间函数 统计函数 数学计算
判断语句 if判断 和 case判断
查询表中的数据时 可以对查找到的数据作判断，根据判断结果输出数据
什么是函数：MySQL服务内置的命令  函数的格式
函数名称()
函数名称(参数)
有不同功能的函数 比如获取系统时间的函数
数据计算函数 字符处理函数
可以使用select 命令输出函数的处理结果
可以使用函数对查找到的数据做处理
函数也就可以单独使用，例如 获取操作系统的时间 select now()


#常用函数的使用：
1.1  字符函数的使用  具体函数的使用见案例
     作用:处理字符或字符类型的字段
LENGTH(str)	     		返字符串长度，以字节为单位
CHAR_LENGTH(str)		返回字符串长度，以字符为单位
UPPER(str)和UCASE(str)   将字符串中的字母全部转换成大写
LOWER(str)和LCASE(str)	将str中的字母全部转换成小写
SUBSTR(s, start,end) 	从s的start位置开始取出到end长度的子串
INSTR(str,str1)		返回str1参数，在str参数内的位置
TRIM(s)			返回字符串s删除了两边空格之后的字符串

1.2  数学函数的使用  具体函数的使用见案例
     作用:处理数据或数值类型的字段
ABS(x)			返回x的绝对值
PI()				返回圆周率π，默认显示6位小数
MOD(x,y)			返回x被y除后的余数 
CEIL(x)、CEILING(x)	返回不小于x的最小整数 (x 是小数)
FLOOR(x）			返回不大于x的最大整数 (x 是小数)
ROUND(x)			返回最接近于x的整数，即对x进行四舍五入 (x 是小数)
ROUND(x,y) 			返回最接近x的数，其值保留到小数点后面y位，若y为负值，则将保留到x到小数点左边y位 (x 是小数)

1.3  聚集函数的使用  sum()  avg()  min()  max()  count()
     作用：数据统计命令 ，的返回值只有1个
avg(字段名)			//计算平均值
sum(字段名) 			//求和
min(字段名) 			//获取最小值
max(字段名) 			//获取最大值
count(字段名) 		//统计字段值个数

数学计算
作用：对行中的列做计算
符号两边必须是数字或数值类型的表头

日期时间函数
作用:用来获取日期时间类型表头下的对应日期时间


1.6  流程控制函数 ： 查询表记录事可以加判断语句
 if语句 语法格式
 语法：
if(条件,v1,v2) 如果条件是TRUE则返回v1，否则返回v2
ifnull(v1,v2)  如果v1不为NULL，则返回v1，否则返回v2  

#if()判断例子
mysql> select * ,if(dept_name="运维部","技术部","非技术部") as 部门类型 from departments;
mysql> select * ,if(dept_name in ("运维部","测试部","开发部"),"技术部","非技术部") as 部门类型 from departments;

#ifnull()判断例子
mysql> select name as 用户名字,ifnull(shell,"noshell") 解释器 from user;

case语句 语法格式  (可以有多个判断添加)
如果字段名等于某个值，则返回对应位置then后面的结果，
如果与所有值都不相等，则返回else后面的结果	
		
语法格式1
CASE 字段名              
WHEN 值1 THEN 结果 
WHEN 值2 THEN 结果  
WHEN 值3 THEN 结果 
ELSE 结果  
END 
#case语法例子
mysql> select name ,shell,case shell when "/bin/bash" then "yes login" when "/sbin/nologin" then "no login" else "other login " end as 登录类型 from user;


语法格式2
CASE              
WHEN  判断条件 THEN 结果 
WHEN  判断条件  THEN 结果  
WHEN  判断条件 THEN 结果 
ELSE 结果  
END  

#case语法例子
mysql> select * ,case when dept_name in ("运维部","开发部","测试部") then "技术部"
    -> when dept_name in ("市场部","销售部") then "商务部"
    -> when dept_name in ("财务部","人事部","财务部") then "行政部"
    -> else "小卖部"
    -> end as 部门类型
    -> from departments;




查询结果处理：对查找出来的数据在做处理
select查询 分组、排序、过滤、分页

#分组 相同的数据为一组
select查询 group by 表头名
mysql> select shell from user group by shell;
mysql> select shell from user where uid < 1000 group by shell;
mysql> select count(name),shell from user where uid < 1000 group by shell;

#排序 
注意 表头是数值类型的排序才是有意义的
      可以按照表头下存储数值的降序或升序，默认是升序
select查询 order by 表头名
order by   字段名   [asc];     从小到大排序（升序）默认的排序方式
order by   字段名   desc;   从大到小排序（降序）
升序命令
mysql> select employee_id,sum(basic) from salary where employee_id <=10 group by employee_id order by sum(basic);
mysql> select employee_id,sum(basic) as 总工资 from salary where employee_id <=10 group by employee_id order by 总工资;
降序命令
mysql> select name,uid from user where uid >=10 and uid <=100 order by uid desc;

#过滤 
在查找到的数据里 在找一遍数据
select查询 having 条件
mysql> select employee_id,date,basic from salary where basic >=20000 and basic <=30000 and year(date) = 2020 having employee_id =88;

#分页
把复合查询条件的记录 限制显示的行数
默认一次性把符合条件的记录全都显示出来
select查询 limit 数字; //仅显示查询结果的头几行
命令格式1  (只显示查询结果的头几行)
select  字段名列表  from   库.表  limit 数字；
select  字段名列表  from   库.表  where   条件  limit 数字；
例如   limit   1  ;  只显示查询结果的第1行
      limit   3  ;  显示查询结果的前3行
      limit   10  ;  显示查询结果的前10行
mysql> select employee_id,date,basic from salary where year(date)=2018 and month(date)=12 limit 3;  //只显示查询结果的前三行
mysql> select name,uid from user where uid between 10 and 100 order by uid desc limit 1; //只显示查询结果的第一行

select查询 limit 数字1,数字2; //只显示制定范围内的行
命令格式2 （显示查询结果指定范围内的行）
select  字段名列表  from   库.表  limit 数字1，数字2；
select  字段名列表  from   库.表  where   条件  数字1，数字2；

数字1   表示起始行   第1行用数字0表示   第2行用数字1表示   第3行用数字2表示 
数字2  表示显示的总行数
例如   limit   0 , 1  ;		从查询结果的第1行开始显示，共显示1行
      limit   3, 5;         从查询结果的第4行开始显示，共显示5行
      limit   10,10       从查询结果的第11行开始显示，共显示10行
mysql> select name,uid,shell from user where shell="/sbin/nologin" limit 1,3;  //从第2行开始显示，共显示3行





***********连接查询**************
连接查询：把多张表 通过连接条件 组成1张新表  ，然后在组成的新表里查找数据 
在工作中 ，不是把所有数据都放在一张表里存储，把数据找全就得从多张表里一起找。
总结：
连接查询也叫多表查询 常用于查询字段来自于多张表
通过不同连接方式把多张表重新组成一张新表对数据做处理

笛卡尔积 两张表记录行数相乘的积 就查询结果的总行数
连接查询的种类(连接查询的语法格式)

连接查询分类
按功能分类
	内连接
		等值连接，非等值连接，自连接		
	外连接
		左连接，右连接
	自连接
	交叉连接
按年代分类
SQL92标准：仅支持内连接
SQL99标准：支持所功能的连接

SQL99标准多表查询 的语法格式1：
SELECT 字段列表  
FROM 表1 [AS] 别名  [连接类型]
JOIN   表2 [AS] 别名
ON 连接条件
WHERE 筛选条件 ；

**连接查询 之 内连接**
语法格式 
SELECT  字段列表
FROM  表1  别名
INNER JOIN  表2  别名  ON 连接条件  
INNER JOIN  表3  别名  ON 连接条件
[WHERE 筛选条件]
[GROUP BY 分组]
[HAVING 分组后筛选]
[ORDER BY 排序列表]

连接查询 之 内连接  根据连接条件的不同又分为：
等值连接：使用相等判断做连接条件
非等值连接：连接条件不是相等判断
自连接：  自己连接自己，把1张表当做2张表（需要给表定义别名）
select 表名.字段名 from 表A inner join 表B on 相同判断条件 where 筛选数据的条件 having 
select 表名.字段名  from  表a inner join  表b 
on    相同判断条件  where 筛选数据的条件  
having 过滤条件 | group  by  表头名 | order by  表头名 |
limit  数字 ；



#笛卡尔积模式
mysql> select employee_id,name,dept_name 部门名称  from employees inner join departments on employees.dept_id = departments.dept_id where employee_id=8;

#内连接的等值连接
mysql> select employee_id,name,dept_name 部门名称  from employees inner join departments on employees.dept_id = departments.dept_id where employee_id=8;
为表设置的别名，简化输入
mysql> select dept_name,e.*  from employees as e inner join departments as d on e.dept_id = d.dept_id limit 2;

案例查询2018年所有员工的全年工资并且按照降序排列
mysql> select name,sum(basic+bonus) as total from employees as e inner join salary as s on e.employee_id=s.employee_id where year(date)=2018 group by name order by total desc;

案例三表联合查询员工全部信息
mysql> select e.employee_id,d.dept_name,name,date,basic+bonus as total from employees as e inner join salary as s on e.employee_id=s.employee_id inner join departments as d on e.dept_id = d.dept_id  where year(date)=2018 and e.employee_id=11;

查询2018年总工资大于30万的员工，按工资降序排列
mysql> select name,sum(basic+bonus) as total from employees as e inner join salary as s on e.employee_id=s.employee_id where year(date)=2018 group by name having total > 300000 order by total desc;

#内连接的非等值连接
使用非相等做判断做连接条件

案例查询2018年12月员工基本工资级别   //连接条件为salary表中的basic字段的范围在wage_grade表中 low和high字段的范围之间
mysql> select employee_id,date,basic,grade from salary as s inner join wage_grade as g on s.basic between g.low and g.high where year(date)=2018 and month(date)=12;

查询2018年12月员工各基本工资级别的人数
mysql> select count(employee_id),grade from salary as s inner join wage_grade as g on s.basic between g.low and g.high where year(date)=2018 and month(date)=12 group by grade;

查询2018年12月员工基本工资级别，员工需要显示姓名 	//三表查询，前两表为等值查询，后两张表为不等值查询
mysql> select e.employee_id,name,date,basic,grade from employees as e inner join salary as s on e.employee_id = s.employee_id inner join wage_grade as g on s.basic between g.low and g.high where year(date)=2018 and month(date)=12;


#内连接的自连接
内连接之自连接：  自己连接自己 把1张当2张表使用，实现的方法
就是查询表的时候给表定义别名来实现。

查看哪些员工的生日月份与入职月份相同
mysql> select e.name,e.hire_date,e.birth_date from employees as e inner join employees as em on e.employee_id = em.employee_id and month(e.hire_date) = month(em.birth_date);


**连接查询 之 外连接**
外连接的应用场景： 1）比较2个表里记录的不同
		    2）者哪些数据当前表有而另一张表没有。
#左外连接   LEFT JOIN      
左边表的记录全都显示出来 右边的表只显示与条件匹配记录，右边表比左边表少的记录使用NULL匹配
命令格式 ：表名a left join 表名b on 连接条件
查看哪些部门没有员工
mysql> select d.dept_name,e.name from departments as d  left  join employees as e on d.dept_id=e.dept_id;


#右外连接 RIGHT JOIN  
右边表的记录全都显示出来 左边的表只显示与条件匹配记录，左表比右边表少的记录使用NULL 匹配
命令格式 ：表名a right join 表名b on 连接条件
查看哪些员工没有部门
mysql> select e.name,d.dept_name from departments as d  right  join employees as e on d.dept_id=e.dept_id;

#**联合查询**
全外连接(mysql不支持，可以使用UNION实现相同的效果) ：合并查询结果
也称联合查询，用来合并查询结果
可以合并同一张的表的查询记录（不同表的查询记录也可合并）
要求查询时，多个select语句的检索到的字段数量必须一致
每一条记录的各字段类型和顺序最好是一致的
UNION关键字默认去重，可以使用UNION ALL包含重复项

语法格式 1  (SELECT语句 ) UNION (SELECT语句);
语法格式 2  (SELECT语句 ) UNION  ALL (SELECT语句);

mysql> (select * from user where uid is not null order by uid limit 1) union (select * from user where uid is not null order by uid desc limit 1);



**嵌套查询**
查询select命令里 包含select 查询命令
外层的查询数据 在内层查询结果里查找

命令格式 select 字段名列表 from 表名 where 条件;
select e.*,d.dept_name from employees as e inner join departments as d on e.dept_id = d.dept_id where dept_id = (select dept_id from departments where dept_name = "运维部");

mysql> select employee_id,name,hire_date,birth_date,email,phone_number,d.dept_name from employees as e inner join departments as d on e.dept_id = d.dept_id where e.dept_id = (select dept_id from departments where dept_name = "运维部");


select * from salary where year(date)=2018 and month(date)=12 and employee_id =100
select * from salary where year(date)=2018 and month(date)=12;

查询部门员工人数  比   开发部人数少 的 部门
mysql> select dept_id,count(name) as 人数 from employees group by dept_id having 人数 < (select count(name) from employees where dept_id=(select dept_id from departments where dept_name="开发部"));

查询每个部门的人数
mysql> select d.*,(select count(name) from employees as e where e.dept_id = d.dept_id) as 部门人数 from departments as d;

查询人事部和财务部员工信息
mysql> select * from employees as e where e.dept_id in (select dept_id from departments where dept_name in ("人事部","财务部"));

mysql> select e.*,d.dept_name from departments as d inner join employees as e on e.dept_id = d.dept_id inner  join salary as s e.employee_id = s.employee_id where d.dept_name in ("人事部");



查询人事部2018年12月所有员工工资
select   *   from salary where year(date)=2018 and month(date)=12 
and employee_id in (select employee_id from employees 
where dept_id=(select dept_id from departments where dept_name='人事部')
);

mysql> select e.employee_id,d.dept_name,name,date,basic+bonus as total from employees as e inner join departments as d on e.dept_id=d.dept_id inner join salary as s on e.employee_id = s.employee_id where dept_name = "人事部" and year(date)=2018 and month(date)=12;

查找2018年12月基本工资和奖金都是最高的工资信息
select * from salary 
where  year(date)=2018 and month(date)=12  and 
basic=(select max(basic) from salary where year(date)=2018 and month(date)=12 ) 
and 
bonus=(select max(bonus) from salary where year(date)=2018 and month(date)=12);

mysql> select e.employee_id,d.dept_name,name,date,basic,bonus  from employees as e inner join departments as d on e.dept_id=d.dept_id inner join salary as s on e.employee_id = s.employee_id where year(date)=2018 and month(date)=12 and basic = (select max(basic) from salary where year(date)=2018 and month(date)=12) and bonus = (select max(bonus) from salary where year(date)=2018 and month(date)=12);


查询3号部门及其部门内   员工的编号、名字 和 email
select dept_id, dept_name, employee_id, name, email  from (
select  d.dept_name, e.*  from departments as d  inner join employees as e 
on d.dept_id=e.dept_id ) as tmp_table  where dept_id=3;

mysql> select d.dept_id,dept_name,e.employee_id,e.name,e.email from employees as e inner join departments as d on e.dept_id=d.dept_id where d.dept_id = 3;


*******多表更新与删除**************************

uid 字段 是  t3 和 t4 表的 关联字段 
select * from t3 inner join t4 on  t3.uid = t4.uid ;
 
#多表修改
mysql> update   t3 inner join t4 on  t3.uid = t4.uid  
set  t3.uid=101 , t4.uid=102   where t3.uid=0 ;

#多表删除
mysql> delete t3,t4  from t3 inner join t4 on  t3.uid = t4.uid;



mysql视图
什么是视图 只有表头的表 数据是存储在创建视图时访问的基表里
1） 视图介绍（什么是视图）
视图是由数据库中的一个表或多个表导出的虚拟表，是一种虚拟存在的表。
视图是一张虚拟表，是从数据库中一个或多个表中导出来的表，其内容由查询定义。
同真实表一样，视图包含一系列带有名称的列和行数据
数据库中只存放了视图的定义，而并没有存放视图中的数据。这些数据存放在原来的表中。
使用视图查询数据时，数据库系统会从原来的表中取出对应的数据。
一旦表中的数据发生改变，显示在视图中的数据也会发生改变。


			2） 视图的优点（为什么要使用视图）
简单
用户无需关心视图中的数据如何查询获得的
视图中的数据已经是过滤好的符合条件的结果集

安全：用户只能看到视图中的数据

数据独立
一旦视图结构确定，可以屏蔽表结构对用户的影响






3） 视图管理
#创建视图语法格式

create  view  库.视图名称  as  SQL查询;
create  view  库.视图名称(字段名列表) as  SQL查询;

mysql> create view viewdb.v1 as select name,uid,shell from tarena.user;
mysql> create view viewdb.v2(姓名,家目录,登录状态) as select name,homedir,shell from tarena.user;
mysql>create view viewdb.emp_view as select name,email,dept_name from tarena.employees as e inner join tarena.departments as d on e.dept_id = d.dept_id;

#删除已有视图
mysql> drop  view viewdb.v1;

#查看视图表里的数据是从哪个基表获取的
mysql> show create view viewdb.v1 \G


--------------------------------------------------------------------------------------
1.13

****视图进阶****
#覆盖的方式创建视图 （达到修改已有视图的目的）

命令格式 create or replace view 库.视图名称 select查询
mysql> create or replace view viewdb.v2 as select employee_id,basic from tarena.salary where basic > 10000 and basic < 20000;


with  check option  (支持的检查选项)
选项 local   首先满足自身的限制 ，同时要满足基本的限制
选项 cascaded 	(默认值 )  满足视图自身限制即可
	
默认情况下 通过视图修改数据是不受限制
可以设置通过视图修改数据受限制：限制的方式如下


选项 cascaded 	(默认值 )  满足视图自身限制即可
with  check option 
mysql> create view tarena.v21  as 
    -> select name , uid from  tarena.user where uid > 10 
    -> with check option;

选项 local   首先满足自身的限制 ，同时要满足基本的限制
with  local check option
mysql> create view tarena.v21  as 
    -> select name , uid from  tarena.user where uid > 10 
    -> with local check option;


****mysql 存储过程****
mysql服务的脚本 可以把重复执行的sql命令 定义一个名称 保存到数据库服务器上，调用定义的名称，就可以反复的执行sql命令
存储过程 是存储在服务库下的表里

变量 判断语句 循环语句 循环控制语句 传参

什么是存储过程
mysql服务的脚本

2、创建存储过程的命令格式（需牢记）

演示delimiter 命令的作用   
命令行的结束符号 默认是  ；

mysql> delimiter //                  把命令行的结束符号 改为//
mysql> desc tarena.user //    执行命令是得使用//结束命令

mysql> delimiter ;    再改回默认的 ；
mysql> desc tarena.user ;

********存储过程的基本使用
#创建
#创建存储过程  pria()
mysql> use  tarena;
mysql> delimiter //
mysql> create procedure pria()
    -> begin
    -> select  count(*) from   tarena.salary  ;
    -> select  count(*) from   tarena.employees  ;
    -> end
    -> //
mysql> delimiter  ;

#执行
#执行 存储过程  pria()
mysql> call pria();  或  call  tarena.pria(); 
mysql> call pria;   存储创建时括号（） 里没有参数 ，执行可以省略()

#查看
#查当前所在库 已有的存储过程
mysql> use tarena; 
mysql> show procedure status \G
                Db: tarena
                Name: pria
                Type: PROCEDURE
或
mysql库里proc表存放所有的存储过程

#列出服务器上所有的存储过程
mysql> select  db, name ,  type  from mysql.proc where   type="PROCEDURE"; 
#查看存储过程的功能代码
mysql> select body from mysql.proc where type="procedure" and name="pria";

#删除
#删除存储过程 
mysql> drop  procedure  库.存储过程名 ;
mysql> drop  procedure   tarena.pria;

***********存储过程进阶
1  变量的使用
1.1 变量的分类 ：
系统变量： mysql服务定义
 包括：全局变量（任意用户连接服务查看到值都一样的） 
	会话变量：连接服务器的用户登录期间使用的变量
	自定义变量： 连接数据库服务的用户定义
	       	  包括：用户变量：用户登录数据库服务器，自己定义的变量
		  局部变量 ：在begin 和 end 用declare定义的变量仅存储过程执行中有效


#全局变量（任意用户连接服务查看到值都一样的）
mysql> show global variables \G      查看所有的全局变量
mysql> show global variables  like  "%time%" ;模糊查看一个全局变量
mysql> show global variables  like  "version_compile_os" ;仅查看一个全局变量
修改全局变量的值  set   global   变量名="值"； 
mysql> set global  wait_timeout = 20000;


#会话变量：连接服务器的用户登录期间使用的变量
会话变量管理 ：当前连接用户使用的变量，会话变量只在连接过程中有效
mysql> show session variables \G    查看连接的所有变量
mysql> show session variables  like   "%关键字%"  \G
    
mysql> show session variables  like  "%cache%" ;   仅查看与内存相关的变量
mysql> set session sort_buffer_size=50000;  修改 


#自定义变量
#用户变量的使用
#定义并赋值
mysql> set @age=19 , @name="pangljing" ;
mysql> select @age , @name;

mysql> select count(name) into @numbers from tarena.user where shell = "/bin/bash";
mysql> select count(*) into @lines from tarena.user
mysql> select @lines as  总行数  ,   @numbers  as 登录系统用户数;


#局部变量的使用  
局部变量只在  存储过程执行中有效
declare  命令 用来的 定义局部变量      休息到 16:02 
例子：
mysql> use tarena;
mysql> delimiter  //
mysql> create  procedure  say48() 
begin declare  x  int default 9;     ///如果不赋值，默认是9
declare  y  char(10); 	  		///只定义类型 不赋值
set y = "plj";            		///declare局部变量不用加@
select x , y ; 
end 
//
mysql> delimiter  ;
mysql> call say48; 


mysql> create  procedure  say1() 
begin declare  x  int default 9;     
set x = 18;           		
select x; 
end 
//


#参数的使用
create  procedure    名(参数,参数,......)
参数定义的语法格式      参数类型    变量名    数据类型 

参数类型分为如下3种：

#in      in类型的参数负责把数据传给存储过程
例如    create     proucedure  p2(  in   x   int )
          begin
			 ......
	       end
			//

mysql> use tarena;
mysql> delimiter //
mysql> create procedure p3(in  dept_no int)
    -> begin
    -> select dept_id , count(*) as 总人数  from 
	-> tarena.employees where dept_id=dept_no group by  dept_id;
    -> end
    -> //
mysql> delimiter ;	
mysql> call  p3() ;    不给参数会报错
mysql> call  p3(1) ;  查看部门编号1  的员工人数
mysql> call  p3(3) ;  查看部门编号3  的员工人数




#out类型的参数  负责接收存储过程的处理结果。
   存储过程执行结束后 可以调用 out类型的参数 获取存储过程的处理结果

例如    create     proucedure  tarena.p31(  out   x   int )
          begin
			 ......
	       end
			//
			
		call   tarena.p31(@名)；
		select  @名；

编写存储过程tarena.p4   功能获取员工表里指定用户的邮箱 
mysql> delimiter //
mysql> create procedure  tarena.p4( in emp_name varchar(10) , OUT mail varchar(25))
begin
       select email into mail  from employees  where name=emp_name;
end  //
mysql> delimiter ;	

insert into employees(name,email)   插入做测试的员工
values("john","john@163.com"),("jerry","jerry@tedu.cn");

mysql> call tarena.p4("jerry",@m);  执行存储过程

存储过程 中文字符集
mysql> select db from mysql.proc where name="p4";      ///查询p4的存储过程属于哪个库
mysql> alter database tarena default CHARACTER SET utf8;  修改库使用的字符集
mysql> drop  procedure  tarena.p4;  删除已经的存储过程重新创建 ， 因为字符集 对已经存储的存储过程无效




#使用INOUT参数(既有in参数的功能又有out参数的功能)
mysql>  delimiter //
mysql>  create procedure tarena.myadd(INOUT i int)
    -> begin
    -> set i=i+100;
    -> end //
mysql> delimiter ;

mysql> set  @x = 8 , @y = 9 ;
Query OK, 0 rows affected (0.00 sec)

mysql> call tarena.myadd(@x);
Query OK, 0 rows affected (0.00 sec)

mysql> call tarena.myadd(@y);
Query OK, 0 rows affected (0.00 sec)

mysql> select  @x , @y;
+------+------+
| @x   | @y   |
+------+------+
|  108 |  109 |


#存储过程里的判断语句
IF 条件1 THEN
  语句1;
ELSEIF 条件2 THEN
  语句2;
ELSE
  语句3;
END IF;

mysql> delimiter //
mysql> create procedure tarena.deptype_pro(IN no int, OUT dept_type varchar(5))
    -> begin
    -> declare type varchar(5);
    -> select dept_name into type from departments where dept_id=no;
    ->      if type='运维部' then
    ->          set dept_type='技术部';
    ->        elseif type='开发部' then
    ->          set dept_type='技术部';
    ->        elseif type='测试部' then
    ->          set dept_type='技术部';
    ->        else
    ->          set dept_type='非技术部';
    ->        end if;
    ->      end //
Query OK, 0 rows affected (0.00 sec)
mysql> delimiter ;
mysql> call deptype_pro(2,@t);
mysql> select  @t;


8.2 case语句
				
语法格式
CASE 变量|表达式|字段
WHEN 判断的值1 THEN 返回值1;
WHEN 判断的值2 THEN 返回值2;
... ...
ELSE 返回值n;
END CASE;

delimiter  //
create procedure tarena.deptype_pro2(IN no int, OUT dept_type varchar(5))
begin
declare type varchar(5);
select dept_name into type from departments  where dept_id=no;
case type
when '运维部' then set dept_type='技术部';
when '开发部' then set dept_type='技术部';
when '测试部' then set dept_type='技术部';
else set dept_type='非技术部';
end case; 
end //      




#loop  循环结构  没有判断条件 重复执行同一段代码 只要不人为结束就一直执行 所以被称为死循环
语法格式
loop
	代码
end loop；

loop循环结构例子
delimiter  //
create procedure tarena.loop2()
begin
declare i int default 1;
loop
	select  sleep(1) , i;
end loop；
end  //
delimiter ;
call  tarena.loop1();


在mysql登录状态下 查看正在执行的命令
mysql>  show  processlist;

在mysql登录状态下终止命令的执行
mysql>  kill   id号;



#repeat循环的例子:至少循环一次
因为先执行循环体 ，再判断条件（当判断条件成立时继续执行循环体（判断条件不成立为为真），反之结束循环）
注释：条件不成立则循环，条件成立则退出
语法格式
repeat
   循环体
   until 判断条件
end repeat;

delimiter //
create procedure tarena.repeat_pro(IN i int)
begin
declare j int default 1;
repeat
   set j=j+1;  
   insert into tarena.departments(dept_name) values('sales');
   until j>i  #判断条件不成立执行循环体，反之循环结束
end repeat;
end //
delimiter ;


#while 循环结构  条件判断成立时 重复执行同一段代码
			   如果第1次判断条件就不成立 会直接结束循环
语法格式
while 判断条件 do      
     代码
end while;

while 循环结构的例子： 
条件判断成立就执行do下边的命令  反之执行end while 结束循环
delimiter //
create procedure tarena.while_pro(IN i int)
begin
declare j int default 1;
while j<i do      
    insert into tarena.departments(dept_name) values('hr');  
	set j=j + 1; 
end while;
end //
delimiter ;


------------------------------------------------------------------------------------
1.14

循环控制语句的功 能在循环条件满足的情况下
结束循环 leave
跳过某次循环并开始下次循环 iterate




leave例子
delimiter //
create procedure tarena.p0()
begin
	p:loop
             leave p;
	     select sleep(1);
	     select "one";
	end loop p;
end
//
delimiter ;
mysql> call tarena.p0 ;  没有输出

delimiter //
create procedure tarena.p1()
begin
	declare j int default 1;
	w:while j<=3 do
		if j = 2 then
			set j = j + 1;
			iterate w;
		end if;
	select j;
	set j = j +1;
	end while w;
end //
		

*******************************************
***********数据备份和恢复****************

数据的备份与恢复
相关概念 
数据备份：就是把当前服务器的上数据拷贝一份 放到其他的存储设备里
恢复数据： 放到其他的存储设备里备份 ， 还原丢失的数据。

物理备份又称为冷备份 需要停止服务，适合线下服务器

数据备份方式：物理备份  、逻辑备份					
数据备份策略 ：
#完全备份 ：
备份所有数据，可以是一台数据库服务器上的所有数据，也可以是 一个数据库下所有表 比如 tarena 库下的所有表还可以仅一张表里的所有记录
#增量备份:  备份上次备份后，所有新产生的数据。		
#差异备份：  备份完全备份后，所有新产生的数据。


通常 备份策略的使用方式：
	完全备份+增量备份
	完全备份+差异备份		  
就是 每周一 对数据库服务器上数据做完全备份 ，每周的周二到周日对数据做增量备份或差异备份
		  
通过计划 执行 备份脚本实现		
00  23  *   *  1          执行完全备份脚本
59  23  *   *   2-0     执行备份新产生数据的脚本


******数据备份命令的使用 之  物理备份与恢复

物理备份
拷贝方式
[root@host50 ~]# systemctl  stop   mysqld 
[root@host50 ~]# mkdir  /bakdir
[root@host50 ~]# cp -r /var/lib/mysql  /bakdir/mysql.bak
打包方式
[root@host50 ~]# cd /var/lib/mysql
[root@host50 ~]# tar -zcvf  /bakdir/mysql.tar.gz ./*

物理恢复数据
[root@host50 ~]# cp -r /bakdir/mysql.bak/* /var/lib/mysql/
[root@host50 ~]# chown -R mysql:mysql /var/lib/mysql


******数据备份命令的使用 之  逻辑备份与恢复 
#使用数据库服务软件自带命令或安装其他软件提供的命令备份和恢复
数据库服务必须是运行状态!!!!
完全备份数据命令  mysqldump  命令格式 
]# mysqldump   -uroot    -p密码 [选项](只备份表则无需选项) 库名   >  /目录名/备份文件名.sql

无选项
库名  表名                    仅备份库下一张的所有记录	
库名  表名1    表名2      一起备份库下2张表的所有记录
]# mysqldump -uroot -p123456 tarena employees salary > /bakdir/twotable.sql


有选项 
-A  或   --all-databases    备份服务器上的所有库
-B  库名                         仅备份1个库里的所有表
-B  库名1[空格隔开]库名2        一起备份2个库里的所有表

]# mysqldump -uroot -p123456 -A > /bakdir/allbak.sql
]# mysqldump -uroot -p123456 -B tarena db1 > /bakdir/twodb.sql

#恢复数数据库
完全恢复数据命令 mysql  命令格式 
]# mysql   -uroot    -p密码    <  /目录名/备份文件名.sql
]# mysql -uroot -p"NSD2021...a" < /root/tarena+2022-01-14.sql 

使用当个表的备份文件恢复数据的时候必须写库名，不然数据库服务不知道把数据恢复那个库里
]# mysql   -uroot    -p密码  库名  <  /目录名/备份文件名.sql	
]# mysql -uroot -p123456 tarena < /bakdir/twotable.sql 

binlog日志
错误日志
慢查询日志
查询日志


mysqldump的备份缺点：  
1、mysqldump 在备份数据和恢复数据的时候会锁表  
2、使用 mysqldump备份命令生成备份文件恢复数据，只能把数据恢复备份时刻的数据。

时时 | 实时备份数据

什么是binlog日志
称为二进制日志
mysql服务日志文件的一种
记录除查询之外的所有SQL命令
可用于数据的备份和恢复
配置mysql主从同步的必要条件



启用binlog日志
]#vim  /etc/my.cnf
[mysqld]
log_bin                   
server_id = 50               
 
日志文件
/var/lib/mysql/ 目录下的
host50-bin.000001
日志文件大于1G后 自动生成新的日志文件，以此类推
host50-bin.index

mysql> show master status;    //查看binlog日志信息


在50 主机 	启用binlog日志时，指定日志文件存放的目录和日志文件命令
]# vim /etc/my.cnf				
[mysqld]
server_id =  50
log_bin=/mylog/db50				
：wq
[root@host50 ~]# mkdir /mylog
[root@host50 ~]# chown  mysql /mylog
[root@host50 ~]# setenforce  0
setenforce: SELinux is disabled
[root@host50 ~]# systemctl  restart mysqld


***************创建新的binlog日志文件
默认情况下日志文件记录的sql名让文件大容量大于 1G 的时候数据服务
会自动创建新的日志文件 文件编号顺延。
      也可以手动创建新的binlog日志文件记录sql命令如下：
						 方法1   systemctl restart  mysqld 
						 
						 方法2  ：  创建新日志文件的个数个完全备份数据库的个数相同
]# mysqldump  -uroot -p123456 --flush-logs  -A > /bakdir/tarena.sql


***********binlog日志相关管理命令
#查看正在使用binlog日志名和偏移量（记录sql命令的编号）
mysql>  show  master  status；

#查看数据库服务器当前已有全部 binlog日志文件
mysql> show binary  logs;

#删除编号之前的所有日志文件
mysql> purge master logs to "db50.000004";

#删除当前所有的日志文件重新创建新日志文件和索引文件
mysql> reset master ;

#查看日志文件内容
mysql> show binlog events in "日志文件名" //

查看日志文件内容 2 种方法：
方法1 使用系统命令 mysqlbinlog 查看
[root@host50 ~]# mysqlbinlog   /mylog/db50.000001   
						
方法2 使用sql命令查看
mysql>  mysql> show binlog events in "db50.000001" ;
	
例子：测试 binlog日志 记录的命令类型(记录除查询之外的所有sql命令)
mysql> reset master ; 
mysql> show  master status;
mysql> select  count(*) from  tarena.user;  查看日志名和偏移量
mysql> show  master status;
mysql> insert into  tarena.user(name) values("boba");
mysql> show  master status;  查看日志名和偏移量
mysql> insert into  tarena.user(name) values("boba");
mysql> show  master status;  查看日志名和偏移量
mysql> show binlog events in  "db50.000001"; 


*****************使用binlog日志文件恢复数据
命令格式1  :   ]# mysqlbinlog   /目录名/日志文件名   |   mysql  -uroot   -p密码
命令格式2 :   ]# mysqlbinlog  选项   /目录名/日志文件名   |   mysql  -uroot   -p密码
加了选项后会根据选项指定的范围查看记录的sql命令
选项包括
#查看指定偏移量范围内记录的sql命令
--start-position=偏移量      --stop-position=偏移量 

#查看指定时间范围内记录的sql命令
--start-datetime="yyyy/mm/dd  hh:mm:ss"   --stop-datetime="yyyy/mm/dd  hh:mm:ss"
开始时间																	结束时间




命令格式1 例子
查看日志的所有内容恢复数据 （恢复所有数据）
适用于日志文件里只记录了 insert 和 update 命令 没有delete 命令的情况下  
]# mysqlbinlog   /目录名/日志文件名   |   mysql  -uroot   -p密码  


命令格式2 例子 ： 
查看日志的部分内容恢复数据（恢复部分数据） 适用于 既有insert 又有delete的情况
]# mysqlbinlog  选项   /目录名/日志文件名   |   mysql  -uroot   -p密码

************修改binlog日志文件的格式（格式就是 日志文件记录sql命令的方式）
查看日志文件的格式
mysql> show variables like "binlog_format";
三种记录方式
报表模式 statement
行模式ROW
混合模式MIXED
修改日志文件的格式
host50]# vim /etc/my.cnf
   [mysqld]
    binlog_format="mixed"   #自己添加 其他行不需要动



在mysqlbinlog命令中直接查看时间和偏移量
在语句往上数第二个at后为起始偏移量，COMMIT/*!*/ 下的at为结束偏移量
时间也是这么找
mysqlbinlog    /目录名/日志文件名
[root@host50 ~]# mysqlbinlog /mylog/plj.000005


[root@host51 ~]# mysqlbinlog --start-position=296 --stop-position=813 /root/plj.000005 | mysql -uroot -p"NSD2021...a"

 plj.000005 |  296 | Intvar         |        50 |         328 | INSERT_ID=82 
 plj.000005 |  782 | Xid            |        50 |         813 | COMMIT /* xid=10 */ 






----------------------------------------------------------------------------------------------------------------------------
1.17

操作环境50备份 51恢复
**********innobackupex*********
特点:在线热备不锁表 适合生产环境下备份业务。
使用innobackupex备份文件恢复数据，要求数据库文件目录必须是空目录
可以对数据做完全备份、增量备份、差异备份的备份与恢复
使用innobackupex还可以完全备份文件，仅恢复一张表的所有数据

#备份命令格式
]#innobackupex  -uroot -p密码  /备份目录名  --no-timestamp     ///目录不需要事先创建，但是名称必须是唯一的
--no-timestamp	 可选项，不用系统的日期做存放备份文件的目录名
~]# innobackupex -uroot -p123456 /allbak --no-timestamp

#恢复数据命令格式
第1步 准备恢复数据
]#innobackupex    --apply-log  /备份目录名 
第2步 拷贝数据
]#innobackupex    --copy-back  /备份目录名
恢复数据的步骤
1） 停止数据库服务
systemctl stop mysqld
2） 清空数据库目录
rm -rf /var/lib/mysql/*
3） 准备恢复数据
innobackupex --apply-log /opt/allbak/
4） 拷贝数据
innobackupex --copy-back /opt/allbak/
5） 修改数据库目录的所有者者组用户为mysql
chown -R mysql:mysql /var/lib/mysql
6)  启动数据库服务
systemctl start mysqld
7）  管理员root用户登录服务查看数据   //密码为拷贝数据源的管理员密码
mysql -uroot -p123456 -e "show databases"


#使用完全备份文件 恢复1张的数据 表头必须还在 要求数据库文件目录必须是空目录
删除表空间就是把数据库目录下没有记录的表名.idb文件删除
mysql> ALTER  TABLE  库名.表名  DISCARD  TABLESPACE; 

mysql> ALTER  TABLE  库名.表名   IMPORT  TABLESPACE;   
mysql> alter  table  tarena.user  import  tablespace;

具体操作步骤 
1) 删除表空间 (表名.ibd) 用来存储表记录的文件 select * from  表;
删除表空间就是把数据库目录下没有记录的表名.idb文件删除
mysql> ALTER  TABLE  库名.表名  DISCARD  TABLESPACE;  
mysql> alter  table  tarena.user discard tablespace;
2) 导出表信息(生成备份目录下备份数据的信息文件)
]# innobackupex --apply-log --export   数据完全备份目录 
host51~]# innobackupex  --apply-log --export  /allbak
3) 拷贝表信息文件到数据库目录下
]# cp /allbak/tarena/user.ibd  /var/lib/mysql/tarena/
]# cp /allbak/tarena/user.cfg  /var/lib/mysql/tarena/
]# cp /allbak/tarena/user.exp  /var/lib/mysql/tarena/
或
]# cp /allbak/tarena/user.{ibd,cfg,exp} /var/lib/mysql/tarena/
4) 修改表信息文件的所有者及组用户为mysql
]# chown  mysql:mysql /var/lib/mysql/tarena/user.*			
5) 导入表空间
mysql> ALTER  TABLE  库名.表名   IMPORT  TABLESPACE;   
mysql> alter  table  tarena.user  import  tablespace;
6) 删除数据库目录下的表信息文件
]# rm -rf /var/lib/mysql/tarena/user.cfg 
]# rm -rf /var/lib/mysql/tarena/user.exp 
7) 查看表记录			
mysql> select  * from tarena.user;	

#数据增量备份与恢复
说明：增量备份是备份 上次备份后新产生的数据
所以在执行增量备份之前 必须得现有一次备份 不然的话无法得知那些数据是新数据
通常增量备份之前的备份 就应该是完全备份 比如 每周的周一都对数据做完全备份
每周的周二到周日 对数据都做增量备份

增量备份数据的命令格式
--incremental  备份新数据并定义新数据存放的目录名 
--incremental-basedir  备份新数据参考的备份目录名
		
]# innobackupex   -uroot     -p密码   --incremental   /目录名  --incremental-basedir=/目录名   --no-timestamp 
]# innobackupex -uroot -p123456 --incremental /new1dir --incremental-basedir=/fullbak --no-timestamp
]#innobackupex -uroot -p123456 --incremental /new2dir --incremental-basedir=/new1dir --no-timestamp
]#innobackupex -uroot -p123456 --incremental /new3dir --incremental-basedir=/new2dir --no-timestamp

/////////可以查看增量备份的起始和结束的号
[root@host50 ~]# cat /fullbak/xtrabackup_checkpoints 
backup_type = full-backuped
from_lsn = 0
to_lsn = 6035073
last_lsn = 6035082
compact = 0
recover_binlog_info = 0


#增量恢复数据的命令格式 要求数据库文件目录必须是空目录

增量恢复数据的命令格式
--incremental-dir	增量备份数据存放的目录名		
]#innobackupex   --apply-log   --redo-only   /首次备份目录名  	#准备恢复数据
]#innobackupex   --apply-log   --redo-only   /首次备份目录名   --incremental-dir=/目录名  	#合并数据

]# innobackupex --apply-log --redo-only /root/fullbak/
]# innobackupex --apply-log --redo-only /root/fullbak/ --incremental-dir=/root/new1dir 


#使用增量备份文件恢复数据的具体操作步骤：
1） 停止数据库服务
]#systemctl stop mysqld
2） 清空数据库目录
]#rm -rf /var/lib/mysql/*
3） 准备恢复数据
]# innobackupex --apply-log --redo-only /root/fullbak/
4） 合并数据 （合并的次数要增量备份的次数一样 并且合并的顺序也有与增量备份的顺序一致）
]# innobackupex --apply-log --redo-only /root/fullbak/ --incremental-dir=/root/new1dir
]# innobackupex --apply-log --redo-only /root/fullbak/ --incremental-dir=/root/new2dir
]# innobackupex --apply-log --redo-only /root/fullbak/ --incremental-dir=/root/new3dir
5） 恢复数据
]# innobackupex --copy-back /root/fullbak
6） 修改数据库目录的所有者和组用户为mysql
chown -R mysql:mysql /var/lib/mysql
7)  启动服务
]# systemctl start mysqld
8） 查看数据



mysql主从同步

1 主从同步介绍（什么主从同步结构）：存储数据的服务结构 能够实现数据的自动同步（也就是数据备份功能）
实现数据自动同步的服务结构
主服务器（master）：接受客户端访问连接
从服务器(slave)：自动同步主服务器数据
任何一台都可以作为主或者从
原则上来说网站连接的是哪一台服务器，那一台就是主，另外则是从


2 主从同步工作过程
        Master服务器 ： 启用binlog日志
        Slave服务器：
                Slave_IO：复制master主机 binlog日志文件里的SQL命令到本机的relay-log文件里。
                Slave_SQL：执行本机relay-log文件里的SQL语句，实现与Master数据一致。

主从同步结构模式
一主一从同步结构 （主从角色的数据库服务器各有一台）
单点故障风险   
一主多从同步结构  （主角色的服务器仅有1台 从角色的数据库服务器有多台） 
		  
主从从同步结构 （给一主一从结构中的从服务器 也配置从服务器） 
		  	  
主主同步结构 （ 2台数据库服务器 彼此是对方的主服务器 同时还是对方的从服务器）


#配置主从同步
第一步 配置master服务器
具体步骤如下：
1) 启用binlog日志文件
2）用户授权
3）查看日志信息
第二步 配置slave服务器
具体步骤如下：
1) 指定server_id 并重启mysqld服务
2）确保数据一致（如果一致的此步骤可以省略）
3）指定主服务器信息
4）启动slave进程
5）查看状态 （IO线程和 SQL线程必须同时是YES 状态）



#第一种同步结构 (一主一从同步结构)
第一步 配置master服务器 192.168.4.51
具体步骤如下：
1) 启用binlog日志文件
vim /etc/my.cnf
[mysqld]
server_id=51
log_bin=master51   #日志默认存储在数据库目录下
:wq
~]# systemctl  restart mysqld
2）用户授权
~]# mysql -uroot  -p密码
replication slave 让用户有复制命令权限
mysql> grant  replication slave on *.*  to  repluser@"%" identified by  "123qqq...A";
3）查看日志信息
 #查看到的日志名和偏移量是给 从服务器使用的 
mysql> show master status;


第二步 配置slave服务器 192.168.4.52
具体步骤如下：
1) 指定server_id 并重启mysqld服务
vim /etc/my.cnf
[mysqld]
server_id=52  #自己添加的
:wq						
systemctl  restart mysqld					
2）确保数据一致（如果一致的此步骤可以省略）
3）指定主服务器信息
mysql> show slave status \G  #不是从数据库服务器
Empty set (0.00 sec)
				   
mysql> change master to  master_host="192.168.4.51",
master_user="repluser" , master_password="123qqq...A",
master_log_file="master51.000001" , master_log_pos=441 ;
		   			   					   
4）启动slave进程 
mysql> start slave;					   
5）查看状态 （IO线程和 SQL线程必须同时是YES 状态）
mysql> show slave status \G  
                  Master_Host: 192.168.4.51
                  Master_User: repluser
                  Master_Port: 3306
		      Master_Log_File: master51.000001
			Slave_IO_Running: Yes    #IO线程
                  Slave_SQL_Running: Yes   #SQL线程

根据 IO线程和 SQL线程的报错信息排错
mysql> show slave status \G					   
Last_IO_Error:  IO线程的报错信息 
Last_SQL_Error:  SQL线程的报错信息
如果查看后是错误，则先停止slave服务，再重新通过change master配置
stop slave
UUID修改
/var/lib/mysql/auto.cnf






#第二种同步结构 (一主多从同步结构)

第二种同步结构 (一主多从同步结构)
创建1台新的数据库服务器 配置地址是 192.168.4.53 
把新数据库服务器也同时配置为192.168.4.51 的从服务器
在数据库服务器host53 做如下配置：
配置slave服务器
具体步骤如下：
1) 指定server_id 并重启mysqld服务
[root@host53 ~]# vim /etc/my.cnf
[mysqld]
server_id=53  #添加
:wq
[root@host53 ~]# systemctl  restart mysqld				   
2）确保数据一致（如果一致的此步骤可以省略）
具体操作如下：
第1步：在主服务器host51 对db1库做完全备份 ，然后把
备份文件拷贝给host53 主机
--master-data 做完全备份数据时 ，在备份文件里记录使用的日志名和偏移量							
~]# mysqldump -uroot -pNSD2107...a --master-data -B db1 > /root/db1.sql
~]# scp /root/db1.sql  192.168.4.53:/opt/
							
第2步：host53 主机 使用备份文件恢复数据
~]# mysql -uroot -p123qqq...A  <  /opt/db1.sql                          
~]# mysql -uroot -p123qqq...A -e 'show databases'
~]# mysql -uroot -p123qqq...A -e 'select count(*) from db1.t1'			   
3）指定主服务器信息
 #在备份文件里查看日志名和偏移量
~]# grep master51 /opt/db1.sql 
CHANGE MASTER TO MASTER_LOG_FILE='master51.000001', MASTER_LOG_POS=2000;

~]# mysql -uroot -p123qqq...A		   
mysql> change master to master_host="192.168.4.51" , master_user="repluser",
master_password="123qqq...A",master_log_file="master51.000001",
master_log_pos=2000;
4）启动slave进程  mysql> start slave;		   
5）查看状态 （IO线程和 SQL线程必须同时是YES 状态）
mysql> show slave status \G
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
6) 查看数据
mysql> select  * from  db1.t1;

测试一主多从的配置
					#在主服务器插入新数据
					mysql> insert into db1.t1 values(99988);
					#2 台从服务器可以看到一样的数据
					[root@host52 mysql]# mysql -uroot -p123qqq...A -e 'select * from db1.t1'
					[root@host53 mysql]# mysql -uroot -p123qqq...A -e 'select * from db1.t1'


#主从从结构 : 给一主一从结构中的从服务器也配置从服务器
删除机器的主从配置
*******把主从服务器还原成普通状态********
把主机host53 恢复为独立的数据库服务器
~]#cd /var/lib/mysql
~]#rm -rf master.info 
~]#rm -rf host53-relay-bin.* 或者(rm -rf *-replay-bin.*)
~]#rm -rf relay-log.info 
~]#systemctl  restart mysqld

54开启级联复制功能 作用是把中级日志里的SQL命令写在binlog日志里
第1步：配置主数据库服务器 192.168.4.53
1） 启用binlog日志
vim /etc/my.cnf
[mysqld]
server_id=53
log_bin=master53
2） 用户授权
mysql> grant replication slave on *.*  to repluser@"%" identified by "123qqq...A";
3） 查看日志信息
mysql> show master status;

第2步：配置主机192.168.4.54  
说明：因为host54主机同时是2种角色，所以2种角色的配置都要有
1) 修改主配置文件 并重启mysqld服务
vim /etc/my.cnf
[mysqld]
server_id=54
log_bin=master54
log_slave_updates #允许级联复制，host54主机把自己主服务器的数据
				                   拷贝给自己的从服务器。
2）确保数据一致（如果一致的此步骤可以省略）
3）指定主服务器信息
mysql> grant  replication slave on *.*  to repluser@"%" identified by "123qqq...A";
4）启动slave进程
mysql> start slave;
5）查看状态 （IO线程和 SQL线程必须同时是YES 状态）

第3步：配置主机192.168.4.55 (做host54主机的从服务器)
1) 指定server_id  并重启mysqld服务
]# vim /etc/my.cnf
[mysqld]
server_id=55
2）指定主服务器信息（binlog日志名和偏移量要做host54主机查看后填写）
mysql> change master to master_host="192.168.4.54",master_user="repluser",master_password="123qqq...A",
    -> master_log_file="master54.000001",master_log_pos=441;
3）启动slave进程
mysql> start slave;
4）查看进程状态
第4步：验证主从从结构的配置
 #在主服务器Host53主机创建的数据 在主机host54 和 host55 都能看到




#主主结构也叫互为主从
准备 新数据库服务器 192.168.4.68 和  192.168.4.69 
2台数据库服务器分别做彼此的从服务器和主服务器，2台服务器2种角色的配置都要有
第1步: 配置数据库服务 host68
1) 启用binlog日志文件
~]# vim /etc/my.cnf
[mysqld]
server_id=68
log_bin=master68
2）用户授权
mysql> grant  replication slave on *.*  to  repluser@"%" identified by "123qqq...A";
3）查看日志信息
mysql> show master status;

第2步: 配置数据库服务 host69
1) 启用binlog日志文件
~]# vim /etc/my.cnf
[mysqld]
server_id=69
log_bin=master69
2）用户授权
mysql> grant replication slave on *.*  to repluser@"%"  identified by "123qqq...A";
3）查看日志信息
mysql> show master status;
4) 指定主服务器信息（binlog日志名和偏移量要做host54主机查看后填写）
#把自己配置为host68的slave服务器，日志名和偏移量要在host68主机查看后填写
mysql> change master to  master_host="192.168.4.68",master_user="repluser",master_password="123qqq...A",
    -> master_log_file="master68.000001",master_log_pos=441;
5）启动slave进程
mysql> start slave;
6）查看状态信息

第3步：在host68  主机数据库管理员登录服务后 
1） 把自己指定为host69 主机的slave 服务器
mysql> change master to  master_host="192.168.4.69" , master_user="repluser" , master_password="123qqq...A",
    -> master_log_file="master69.000001",master_log_pos=441;
mysql> start slave;

第4步：测试主主结构的配置
先在host68主机健库表插入记录 ， 在host69 主机可以看到同样的数据
然后再在host69 库下的表插入新记录，在host68主机可以看到同样的数据




mysql 主从同步模式
1）主从同步复制模式 可以在任意数据库服务器启用半同步服务模式 （统一在 192.168.4.50  主机 做实验） 
主从数据库服务实现数据同步的工作方式
支持的工作方式：
第1种 异步复制模式（默认）
主服务器执行完一次事务后，立即将结果返给客户端，
不关心从服务器是否已经同步数据。

第2种 半同步复制模式
主服务器在执行完一次事务后，等待至少一台从服务器同步数据完成，
才将结果返回给客户端。


#把主服务器和从服务器的工作模式都修改为半同步复制模式
方法1  命令行设置 
好处：不用重启服务马上生效
					
安装模块 
 #安装master模块
MySQL> INSTALL  PLUGIN  rpl_semi_sync_master   SONAME  "semisync_master.so";
  								
 #安装slave模块
MySQL> INSTALL  PLUGIN  rpl_semi_sync_slave   SONAME  "semisync_slave.so";	
				
 #查看模块是否安装
mysql> SELECT  plugin_name, plugin_status FROM  information_schema.plugins  
WHERE plugin_name LIKE "%semi%"; 

启用模块
 #启用master模块
mysql> SET  GLOBAL rpl_semi_sync_master_enabled=1;
 								
 #启用slave模块
mysql> SET  GLOBAL rpl_semi_sync_slave_enabled=1;	
							
 #查看模块是否启用
MySQL> SHOW  VARIABLES  LIKE  "rpl_semi_sync_%_enabled"; 	


#方法2  永久配置 编辑主配置文件
vim  /etc/my.cnf
[mysqld]
 #安装模块
plugin-load="rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so"
												
 #启用模块
rpl_semi_sync_slave_enabled=1
rpl_semi_sync_master_enabled=1
:wq
						
通过重启服务 验证半同步工作模式的永久配置
systemctl   restart mysqld

MySQL> SHOW  VARIABLES  LIKE  "rpl_semi_sync_%_enabled"; 			



######################################################################################################
SQL排错思路
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~											
正常的排错方法： 根据 IO线程和 SQL线程的报错信息排错
mysql> show slave status \G					   
Last_IO_Error:  IO线程的报错信息 
Last_SQL_Error:  SQL线程的报错信息


~~~~~~~~~~~~~~~
Last_IO_Error: Got fatal error 1236 from master
 when reading data from binary log: 'Could not find first log file name in binary log index file'

    在从服务器执行如下操作：
	mysql> stop slave;
	
	在主服务器查看日志名和偏移量
	host51  show  master status;
	
	在从服务器 重新指定日志名和偏移量
	mysql> change  master to  master_log_file="日志名" , master_log_pos=偏移量；
	mysql> start slave;
	mysql> show  slave status \G
~~~~~~~~~~~~~~~~~~~~~~~~~	
Last_IO_Error: Fatal error: 
The slave I/O thread stops because 
master and slave have equal MySQL server UUIDs; 
these UUIDs must be different for replication to work

在从服务器做如下操作：
vim /var/lib/mysql/auto.cnf
[auto]
server-uuid=2dac4865-4770-11ec-9103-6462be92a06e   修改一个字母（字母个数不能少）
:wq
systemctl   restart  mysqld

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
公共的解决办法：把slave角色的服务器恢复为独立的数据库服务器，重新配置为从服务器。
在从服务器执行如下操作：
 systemctl  stop   mysqld 
 cd  /var/lib/mysql/
 rm  -rf master.info
 rm -rf   *-relay-bin.*
 rm -rf relay-log.info
 systemctl   start  mysqld
	
~~~~~~~~~~~~~~~~~~~~		
 Last_IO_Error: error connecting to master 'repluser@192.168.4.51:3306' - 
 retry-time: 60  retries: 2
 在从服务器做如下操作：
		MySQl> stop  slave;
		mysql> change master to  master_user="repluser",master_password="123qqq...A";
		MySQL>  start slave;
		mysql> show slave status \G
		
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

###############################################################################################################



--------------------------------------------------------------------------------------------------
1.18

mysql中间件 架设在数据库和客户端之间的服务软件 统称中间件
有多种中间件功能软件 
实现mysql数据读写分离功能的中间件软件mysql-proxy mycat maxscale 




2 修改主配置文件
[root@host57 ~]# cp /etc/maxscale.cnf /root/  备份主配置文件
	

[root@host57 ~]# vim 	/etc/maxscale.cnf
#服务启动后线程的数量 auto是几个cpu就有几个线程
[maxscale]
threads=auto

[server1]    指定第1台数据库服务器的ip地址
type=server
address=192.168.4.51
port=3306
protocol=MySQLBackend


[server2]   指定第2台数据库服务器的ip地址
type=server
address=192.168.4.52
port=3306
protocol=MySQLBackend

[MySQL Monitor]   定义监视的数据库服务器
type=monitor
module=mysqlmon
servers=server1,server2    监视server1和server2
user=mysqla      监控用户
passwd=123qqq...A  连接密码
monitor_interval=10000


#禁止只读服务
#[Read-Only Service]
#type=service
#router=readconnroute
#servers=server1
#user=myuser
#passwd=mypwd
#router_options=slave

[Read-Write Service]   启用读写分离服务
type=service
router=readwritesplit
servers=server1,server2   读写分离服务在server1和server2服务器之间进行
user=mysqlb   路由用户
passwd=123qqq...A  连接密码
max_slave_connections=100%

[MaxAdmin Service]   管理服务（通过访问管理服务可以查看监控信息）
type=service
router=cli

因为只读服务没有启用所有也不需要定义服务使用的端口号
#[Read-Only Listener]
#type=listener
#service=Read-Only Service
#protocol=MySQLClient
#port=4008

[Read-Write Listener]   定义读写分离服务使用端口号
type=listener
service=Read-Write Service
protocol=MySQLClient
port=4006    端口号

[MaxAdmin Listener]   定义管理服务使用端口号
type=listener
service=MaxAdmin Service
protocol=maxscaled
socket=default
port=4016  端口号
:wq




3  配置数据库服务器(在数据库服务器上添加监控用户和路由用户)
注意：因为是主从结构 ，所以只需要在主服务器添加，从服务器会自动同步
[root@host51 ~]# mysql -uroot -p123qqq...A					   
添加监控用户 mysqla 用户
mysql> grant  replication   slave , replication  client  on  *.*  to  
mysqla@"%" identified by "123qqq...A";
				
权限说明：
replication client   监视数据库服务的运行状态  
replication slave 	 数据库服务器的主从角色	

添加路由用户 mysqlb 用户
mysql> grant  select on  mysql.*  to  mysqlb@"%" identified by "123qqq...A";  #对授权库下的表有查询权限

#在从服务器查看用户是否同步
[root@host52 ~]# mysql -uroot -p123qqq...A -e  'select user from mysql.user where user="mysqla"'
[root@host52 ~]# mysql -uroot -p123qqq...A -e  'select user from mysql.user where user="mysqlb"'

4 启动读写分离服务
[root@host57 ~]# maxscale  -f /etc/maxscale.cnf     启动服务 
[root@host57 ~]# 
[root@host57 ~]# ls /var/log/maxscale/   查看日志文件
maxscale.log 
[root@host57 ~]# netstat  -utnlp  | grep 4006    查看读写分离服务端口号 
tcp6       0      0 :::4006                 :::*                    LISTEN      1580/maxscale       
[root@host57 ~]# 
[root@host57 ~]# netstat  -utnlp  | grep 4016  查看读写分离服务端口号
tcp6       0      0 :::4016                 :::*                    LISTEN      1580/maxscale       

#把服务杀死 再启动  相当于重启服务 （修改了配置文件后要重启服务使其配置生效）
[root@host57 ~]# killall -9 maxscale   通过杀进程的方式停止服务 
[root@host57 ~]# maxscale  /etc/maxscale.cnf     启动服务

                   #在56本机访问管理服务查看数据库服务的监控信息
[root@host57 ~]# maxadmin -uadmin -pmariadb -P4016
MaxScale> list servers
Servers.
-------------------+-----------------+-------+-------------+--------------------
Server             | Address         | Port  | Connections | Status              
-------------------+-----------------+-------+-------------+--------------------
server1            | 192.168.4.51    |  3306 |           0 | Master, Running
server2            | 192.168.4.52    |  3306 |           0 | Slave, Running
-------------------+-----------------+-------+-------------+--------------------
MaxScale> exit                                                 
[root@maxscale56 ~]# 						 



mysql多实例
在一台物理服务器上运行多个数据库服务
网站服务的虚拟主机
                              
                                           
B/yTy:QyV1Mo



-------------------------------------------------------------------------------------------------------------------------
1.20
集群的分类 负载均衡 高可用
负载均衡
把请求给调度器，然后调度器平均分配给服务器
高可用
主备模式，备用机器一直在监视主服务器，如果主挂掉，备用服务器就顶替。
vip地址是根据哪个服务器能够提供服务，它就在那台服务器上，客户端是通过VIP访问服务器。

负载均衡软件 haproxy lvs nginx
高可用软件   keepalvied

MHA集群配置步骤
准备服务器至少3台
环境准备：ssh 主从 公共配置（binlog日志，）
配置管理主机：安装mha_manager软件，编辑app1.cnf文件，创建故障切换脚本并设置vip地址
配置数据服务器：创建监控用户 app1.conf配置文件里的 user=plj，安装mha_node软件，把vip地址部署在主从结构中的master服务器上
测试配置：免密登录，主从同步
启动管理服务 查看管理服务 停止管理服务
测试配置 测试高可用功能
测试的结果：任何时刻都可以连接vip地址
访问数据库服务 查询数据和存储数据
把宕机的数据库服务器再添加到集群里


MHA集群缺点总结：
		必须要有vip地址
		
		宕机的主服务器需要手动添加到集群里,还需要手动同步宕机期间的数据
		
		管理服务发现主服务器宕机后，会调用故障切换脚本，
		把vip地址部署在新的主数据库服务器上。管理服务会
		自动停止，需要手动启动管理服务器，才能监视新的主数据服务器
		
		故障切换期间会有数据丢失的情况


缺点 
操作特别繁琐
是作一次故障切换就要停止重启服务，如果不重启，添加的服务器就不会添加
故障切换时，如果写入数据会丢失

配置MHA集群，具体步骤如下：
#第一步：集群环境准备 （在3台数据库服务器都要做的配置）
1）公共配置（3台数据库服务器都要配置）
 #启用binlog日志  
 #开启半同步复制模式
 #禁止删除本机的中继日志文件 
 #重启数据库服务
 #添加从服务器拷贝sql命令时连接使用的用户


集群环境准备
1 所有数据库服务器的公共配置
2 ssh免密登配置，包括所有服务器之间免密登录和管理机免密登录所有主机
3 配置mysql 一主多从或一主一从  //对决不能是其结构，例如主主 主从从




#第二步：配置管理主机 192.168.4.57
1） 安装软件#安装依赖软件
cd mha-soft-student/
yum -y install mha4mysql-node-0.56-0.el6.noarch.rpm
yum -y install perl-*.rpm
yum -y install  perl-ExtUtils-*   perl-CPAN*
tar -xf mha4mysql-manager-0.56.tar.gz 
cd mha4mysql-manager-0.56/
[root@mgm57 mha4mysql-manager-0.56]# perl Makefile.PL
[root@mgm57 mha4mysql-manager-0.56]#  make   && make  install  
[root@mgm57 mha4mysql-manager-0.56]# masterha_ 按2次tab键列出所有命令

3） 创建并编辑管理服务的主配置文件 (!!!重要！！！)
3.1 创建工作目录  
[root@mgm57 ~]# mkdir /etc/mha

3.2 拷贝模板文件创建主配置文件
[root@mgm57 mha-soft-student]# cd  mha4mysql-manager-0.56
[root@mgm57 mha4mysql-manager-0.56]# cp samples/conf/app1.cnf  /etc/mha/

3.3 编辑主配置文件
说明：模版文件是个半成品 需要根据环境准备完善
[root@mgm57 ~]# vim /etc/mha/app1.cnf	（没有第4台数据库服务器所有把[server4]删除）					
[server default]
manager_workdir=/etc/mha
manager_log=/etc/mha/manager.log
master_ip_failover_script=/etc/mha/master_ip_failover

ssh_user=root
ssh_port=22

repl_user=repluser
repl_password=123qqq...A

user=plj
password=123qqq...A

 #定义监视的数据库服务器
[server1]
hostname=192.168.4.51
port=3306
candidate_master=1

[server2]
hostname=192.168.4.52
port=3306
candidate_master=1

[server3]
hostname=192.168.4.53
port=3306
candidate_master=1

4） 创建故障切换脚本
创建脚本并指定vip地址部署在哪块网卡上
[root@mgm57 mha-soft-student]# cp master_ip_failover  /etc/mha/
[root@mgm57 mha-soft-student]# chmod +x /etc/mha/master_ip_failover 

[root@mgm57 mha-soft-student]# vim +35 /etc/mha/master_ip_failover 
my $vip = '192.168.4.100/24';  # Virtual IP 
my $key = "1";
my $ssh_start_vip = "/sbin/ifconfig eth0:$key $vip";        ##机器上真实的网卡名字，绑定vip地址的命令
my $ssh_stop_vip = "/sbin/ifconfig eth0:$key down";     ##机器上真实的网卡名字,down是释放vip地址的命令，如果不释放无法绑定



#第三步：配置数据库服务器
说明：要保证3台数据库都有ifconfig 命令
]# which ifconfig  || yum -y install net-tools
/usr/sbin/ifconfig

1）把故障切换脚本里指定的vip地址
配置在当前主从结构种的主数据库服务器Host51 主机上
[root@host51 ~]# ifconfig  eth0:1 192.168.4.100/24
[root@host51 ~]# ifconfig  eth0:1
eth0:1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.4.100  netmask 255.255.255.0  broadcast 192.168.4.255
        ether 52:54:00:98:33:28  txqueuelen 1000  (Ethernet)
如果eth0的虚拟ip配置错误或者配置到其他主机上，需要在虚拟机(非ssh远程)上，通过nmcli connection down eth0
nmcli connection up eth0 恢复


2）安装软件（3台数据库器软件都要安装）
						#先安装依赖
[root@host51 ~]# cd mha-soft-student/
[root@host51 mha-soft-student]# yum -y install perl-*.rpm

						#在安装主软件
[root@host51 mha-soft-student]# yum -y install mha4mysql-node-0.56-0.el6.noarch.rpm	


3) 添加监控用户（只需在master服务器添加 在slave服务器查看）
[root@host51 ~]# mysql -uroot -p123qqq...A
mysql> grant all on *.*  to plj@"%"  identified by "123qqq...A";
[root@host52 ~]# mysql -uroot -p123qqq...A -e 'select user from mysql.user where user="plj"'


#第四步：测试配置,在管理主机mgm57 如下测试：
				
1) 测试ssh免密登录配置
[root@mgm57 ~]# masterha_check_ssh  --conf=/etc/mha/app1.cnf				
Sat Nov 20 16:31:08 2021 - [info] All SSH connection tests passed successfully.  #成功提示

2）测试主从同步配置
[root@mgm57 ~]# masterha_check_repl --conf=/etc/mha/app1.cnf
MySQL Replication Health is OK.   #成功提示


###############################################################################################################
~~~~~~~~~~~~~~排错思路~~~~~~~~~~~~~~~~~~
ssh 测试失败 公共解决办法

     在 51-53 、57   rm -rf /root/.ssh   删除错误的密码对和公钥
     然后把环境准备中的 ssh 免密登录重新配置一遍
		1  三台数据库服务器之间密码登录
		2  管理主机57 能够免密登录 三台数据库服务器
步骤 笔记的108 行到  142 行


主从同步配置失败的解决办法

  第一步： 在52 和 53本机查看 slave 进程的状态 如果io 和 sql 线程 不同时是yes  

需要执行如下操作（2台都执行）
stop slave;
然后重新使用change master to  指定主服务器的信息；
start slave;
show slave  status \G


如果2个从服务器都是 io 和 sql 线程 yes  在 51 主机查看是否有 slave 状态
     show slave status \G
     stop slave；


   
如果以上都没有问题  就检查 app1.cnf 编写 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################################################


#第五步： 启动管理服务(192.168.4.57) 
说明：2个测试都成功了  管理服务才能启动成功
[root@mgm57 ~]# nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &

--remove_dead_master_conf 删除配置文件内的server,    //vim /etc/mha/app1.cnf 
--ignore_last_failover  如果不写，就只做一次故障切换

查看后台运行状态
[root@mgm57 ~]# jobs
[1]+  运行中               nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &
[root@mgm57 ~]# 

查看是否运行
[root@mgm57 ~]# masterha_check_status  --conf=/etc/mha/app1.cnf;
app1 (pid:1977) is running(0:PING_OK), master:192.168.4.51
[root@mgm57 ~]# 

停止服务
[root@mgm57 ~]# masterha_stop --conf=/etc/mha/app1.cnf

如果服务器坏了后，mha集群会自动更换主从，但是服务会自动停止，需要重新手动启动

[root@host51 ~]# ifconfig  eth0:1
eth0:1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.4.100  netmask 255.255.255.0  broadcast 192.168.4.255
        ether 52:54:00:98:33:28  txqueuelen 1000  (Ethernet)

排错思路
如果数据库连接不上，查看是不是多台机器都有VIP地址


~~~~~~~~~~~~~~~~~~~~~~~~修复宕机的51数据库服务器~~~~~~~


第一步： 把192.168.4.51 配置为 当前 master服务器 52  的  slave主机，  具体操作如下：

[root@host51 ~]#systemctl  start  mysqld
[root@host52 ~]# mysqldump -uroot -p123qqq...A  --master-data  -B db1 > /root/db1.sql
mysqldump: [Warning] Using a password on the command line interface can be insecure.
[root@host52 ~]# scp /root/db1.sql  192.168.4.51:/root/
db1.sql                                                              100% 2073   210.9KB/s   00:00    
[root@host52 ~]#



[root@host51 ~]# mysql -uroot -p123qqq...A  < /root/db1.sql 
[root@host51 ~]# grep master52 /root/db1.sql 
CHANGE MASTER TO MASTER_LOG_FILE='master52.000001', MASTER_LOG_POS=1676;
[root@host51 ~]#
[root@host51 ~]# mysql -uroot -p123qqq...A
mysql> change master to master_host="192.168.4.52" , master_user="repluser" , master_password="123qqq...A" , master_log_file="master52.000001" , master_log_pos=1676 ;
Query OK, 0 rows affected, 2 warnings (0.12 sec)

mysql> start slave;
Query OK, 0 rows affected (0.02 sec)

mysql> show slave status \G
            Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
mysql> exit;

第二步 ：把51主机添加到集群里 具体配置如下：

[root@host57 ~]# jobs
[1]+  运行中               nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &
[root@host57 ~]# 

[root@host57 ~]# vim /etc/mha/app1.cnf  #把51主机添加到配置文件里 
[server1]
hostname=192.168.4.51
port=3306
candidate_master=1
:wq

masterha_secondary_check  masterha_stop             
[root@host57 ~]# masterha_stop --conf=/etc/mha/app1.cnf
Stopped app1 successfully.
[1]+  退出 1                nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null
[root@host57 ~]# 

[root@host57 ~]# nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &
[1] 3618

[root@host57 ~]# jobs
[1]+  运行中               nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &
[root@host57 ~]# 


[root@host57 ~]# masterha_check_status  --conf=/etc/mha/app1.cnf
app1 (pid:3618) is running(0:PING_OK), master:192.168.4.52
[root@host57 ~]# 






--------------------------------------------------------------------------------------------------
1.21

部署PXC集群


1.2  pxc集群特点
数据强一致性、无同步延迟
没有主从切换操作，无需使用虚拟IP
支持InnoDB存储引擎
多线程复制
部署使用简单
支持节点自动加入，无需手动拷贝数据


1.3  相关端口号
3306	数据库服务端口
4444	SST 端口
4567	集群通信端口
4568	IST 端口
SST	State Snapshot Transfer 全量同步
IST	Incremental State Transfer 增量同步

#第1步：安装软件 （3台主机都要安装）
cd pxc/
yum -y install libev-4.15-1.el6.rf.x86_64.rpm
yum -y install percona-xtrabackup-24-2.4.13-1.el7.x86_64.rpm
yum -y install qpress-1.1-14.11.x86_64.rpm
tar -xf Percona-XtraDB-Cluster-5.7.25-31.35-r463-el7-x86_64-bundle.tar
yum -y install Percona-XtraDB-Cluster-*.rpm

#第2步：修改配置文件
				        2.1 指定集群中主机的server_id  修改 mysqld.cnf文件
                        2.2 指定集群信息 修改 wsrep.cnf文件

				        修改71主机的server_id
						vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
						server_id=71
						
					    修改72主机的server_id
						vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
						server_id=72
						
					    修改73主机的server_id
						vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
						server_id=73
				
				        
						修改71主机的wsrep.cnf 文件
						]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
						8 wsrep_cluster_address=gcomm://192.168.4.71,192.168.4.72,192.168.4.73
						25 wsrep_node_address=192.168.4.71
 						27 wsrep_cluster_name=pxc-cluster
						30 wsrep_node_name=pxcnode71
						39 wsrep_sst_auth="sstuser:123qqq...A"
						:wq
						
						修改72主机的wsrep.cnf 文件
						]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
						8 wsrep_cluster_address=gcomm://192.168.4.71,192.168.4.72,192.168.4.73
						25 wsrep_node_address=192.168.4.72
 						27 wsrep_cluster_name=pxc-cluster
						30 wsrep_node_name=pxcnode72
						39 wsrep_sst_auth="sstuser:123qqq...A"
						:wq
						修改73主机的wsrep.cnf 文件
						]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
						8 wsrep_cluster_address=gcomm://192.168.4.71,192.168.4.72,192.168.4.73
						25 wsrep_node_address=192.168.4.73
 						27 wsrep_cluster_name=pxc-cluster
						30 wsrep_node_name=pxcnode73
						39 wsrep_sst_auth="sstuser:123qqq...A"
						:wq



#第3步：初始化集群
说明：在任意1台服务器上执行初始化集群操作（仅需要执行一遍）
                  
统一在71 主机执行 ，具体步骤如下			
1、启动服务
2、使用初始密码连接服务
3、添加授权用户sstuser
4、查看数据库服务器端口 3306
5、查看集群端口4567
6、管理员root 登录 建库表存储记录

[root@pxcnode71 ~]# systemctl  start mysql@bootstrap.service    初始化集群
[root@pxcnode71 ~]# ls /var/lib/mysql  查看数据数据库文件列表 有文件说明成功
[root@pxcnode71 ~]# grep password /var/log/mysqld.log  查看管理员初始化密码
[root@pxcnode71 ~]# mysql -uroot -p'UfjF5UQGb/fq'	初始密码登录				
mysql> alter user root@"localhost" identified by "123456";  强制修改密码
mysql> grant all on *.*  to  sstuser@"localhost" identified by "123qqq...A";  添加全量同步用户sstuser
#第4步：	在72 和  73 主机 分别启动数据库服务，启动服务后会自动同步71主机的数据
注意pxc软件内置的数据库服务名叫mysql


###############################################################################################
~~~~~~~~错误统一解决办法：
	   		
1、查看mysql服务的父进程pid 通过kill -9   杀死父进程
]# which   pstree || yum  -y install psmisc 
]# pstree -p | grep mysqld
]# kill  -9  pid号
		
killall -9 mysqld

2、清空数据库目录  rm  -rf  /var/lib/mysql/*
		
3、检查 mysqld.cnf   和 wsrep.cnf 文件的配置项目   
4、检查 防火墙和 selinux服务关闭了吗
		
如果初始集群存储，就重新执行集群初始化操作 
		
如果启动MySQL服务错误，就重新执行启动mysql服务的操作 

###############################################################################################


二、mysql存储引擎 （80%是理论 着重在于理解）
2.1 存储引擎介绍
1）MySQL服务体系结构 （mysql服务的功能分类）
			
管理工具： 安装MySQL服务软件后，提供的管理命令
				
连接池：验证客户端连接时使用的用户和密码是否正确 同时验证数据库服务器是否有mysqld进程相应连接
						
SQL接口: 把用户执行的SQL命令传递给本机的mysqld 进程
				
分析器：检查SQL命令的语句及对数据的访问权限

优化器：对要执行的 SQL命令做优化（是内存自动功能程序）
				
查询缓存：划分出一定的物理内存空间给MySQL服务存储查找过的数据。
					  
存储引擎：当对表里的数据做查询（select） 或写操作（insert /update /delete）会调用存储引擎对表中的数据做处理，至于如何处理取决表使用的存储引擎的功能。
				
文件系统：通常指的就是电脑的硬盘 

2）MySQL服务的工作过程
2.1 处理查询select访问的工作过程 
第1步： 客户端向服务器发起连接请求
第2步： 服务器接收到客户端连接请求并响应
第3步： 
如果客户端执行的selcet 访问，先在查询缓存里提取数据回复给客户端，如果数据库服务器在查询缓存里没有找到用户访问的数据，这时就要到数据库服务器的表里查找数据，对数据库目录下的表做访问是就会调用表使用的存储引擎对表做处理，然后把查找到的数据先放到查询缓存 在回复给客户端

第4步：断开连接
							
					
在数据库服务器查看与查询缓存相关的配置项
mysql> show variables like "%query_cache%";


2.2 处理存储insert访问的工作过程
第1步： 客户端向服务器发起连接请求
第2步： 服务器接收到客户端连接请求并响应
第3步:  根据表使用的存储引擎 对表中的数据做对应的处理。
第4步： 断开连接
					
3）什么是存储引擎：（当对表里的数据做select 或insert 访问时，会根据表使用的
存储引擎对数据做处理。）
作为可插拔式的组件提供
MySQL服务软件自带的功能程序
不同的存储引擎有不同的功能和数据存储方式
MySQL 5.0/5.1 (MyISAM)     MySQL 5.5/5.6 (InnoDB)




查看数据库默认使用的存储引擎
mysql> show engines;

1.2 查看当前已有表使用的存储引擎
mysql> show create table DB1.t3 \G
					
					
2） 修改存储引擎
2.1 修改数据库服务默认使用的存储引擎   休息到  16:05 
~]#vim /etc/my.cnf
[mysqld]
default-storage-engine=myisam
:wq
			
~]# systemctl  restart mysqld
~]# mysql  -uroot -pNSD2107...a 

mysql> show engines;


说明：建表时，不指定表使用的存储引擎，使用默认存储引擎表使用的存储引擎不同，存储方式和使用MySQL服务的功能都不一样

2.2建表时指定表使用的存储引擎
mysql> create  table db10.b(name char(10))engine = innodb;
mysql> create  table db10.c(addr char(10))engine = memory;


说明:
innodb存储引擎的表 每个表对应2个表文件
myisam存储引擎的表 每个表对应3个表文件
memory存储引擎的表 每个表对应3个表文件



mysql> system ls /var/lib/mysql/db10/a.*
/var/lib/mysql/db10/a.frm  /var/lib/mysql/db10/a.MYD  /var/lib/mysql/db10/a.MYI

mysql> system ls /var/lib/mysql/db10/b.*
/var/lib/mysql/db10/b.frm  /var/lib/mysql/db10/b.ibd
mysql> 

mysql> system ls /var/lib/mysql/db10/c.*
/var/lib/mysql/db10/c.frm


2.3修改表使用的存储引擎（一般在表存储存储数据之前修改）
					说明：存储引擎修改了，存储数据的位置也会改变，
mysql> alter  table  db10.c engine=myisam;
mysql> system ls /var/lib/mysql/db10/c.*
/var/lib/mysql/db10/c.frm  /var/lib/mysql/db10/c.MYD  /var/lib/mysql/db10/c.MYI
mysql> 
					
					
	
	2.3 常用存储引擎特点（生产环境下常用的存储引擎）
			1） myisam存储引擎特点
				支持表级锁 、不支持事务、事务回滚、外键
				 myisam存储引擎的表 每个表对应3个表文件
				 表名.frm  存储表头信息      mysql> desc  库.表；
				 表名.MYI  存储表索引信息    mysql> show index from  库.表;
				 表名.MYD  存储表里的数据    mysql> select  * from  库.表;
				 
			2） innodb存储引擎特点
				支持行级、支持事务、事务回滚、外键

			
				innodb存储引擎的表 每个表对应2个表文件
表名.frm  存储表头信息      mysql> desc  库.表；
表名.ibd  存储表的索引信息+表的数据信息     mysql> show index from  库.表; +mysql> select  * from  库.表;


	2.3 常用存储引擎特点（生产环境下常用的存储引擎）
			1） myisam存储引擎特点
				支持表级锁 、不支持事务、事务回滚、外键
				 myisam存储引擎的表 每个表对应3个表文件
				 表名.frm  存储表头信息      mysql> desc  库.表；
				 表名.MYI  存储表索引信息    mysql> show index from  库.表;
				 表名.MYD  存储表里的数据    mysql> select  * from  库.表;
				 
			2） innodb存储引擎特点
				支持行级、支持事务、事务回滚、外键

			
				innodb存储引擎的表 每个表对应2个表文件
表名.frm  存储表头信息      mysql> desc  库.表；
表名.ibd  存储表的索引信息+表的数据信息     mysql> show index from  库.表; +mysql> select  * from  库.表;

			3) 专业术语
				说明: 给表加锁，是为了解决客户端并发访问的冲突问题
				
				
				3.1 锁类型:根据对数据的访问类型加锁
					读锁：又称为共享锁，对数据做查询select 访问
						  加了读锁表，允许多个访问同时查询一张。
						  
					写锁：又称为排它锁或互斥锁，对数据做写访问（写访问通常指定的是 insert | delete | update ）
						   加了写锁的表，同一时间只允许1个连接做写操作，后续的读和写都得等待，
						   等待写锁释放后，才允许后续的读或写访问。
				
				3.2 锁粒度:指的就是给表加锁的范围	
					行级锁： 仅仅对被访问的行分别加锁，没有被访问的行不加锁
					表级锁： 只要是对表做访问，就会把整张表加锁(不管访问的是1行 还是更多行)
					

2.4 事务特性
			1）什么是事务？
			   指的是一组不可分割的SQL操作。
               使用Innodb 存储引擎的表才支持事务。
               事务处理可以用来维护数据的完整性，保证成批的 SQL 语句要么全部执行，要么全部不执行。
               事务用来管理对数据的 insert,update,delete 操作
			2） 事务的特性简称ACID （表的存储引擎必须是innodb 才有事务的特性）
				Atomic ：原子性
一个事务（transaction）中的所有操作，要么全部完成，要么全部不完成。
				
				Consistency ： 一致性
在事务开始之前和事务结束以后，数据库的完整性不会被破坏。
               执行sql命令时，敲回车前 称为事务开始之前
			   执行sql命令时，敲回车后 称为事务结束以后

				Isolation ：隔离性
数据库允许多个并发事务同时对其数据进行读写和修改而互不影响。
				MySQL服务是支持多并连接的服务，同一时刻可是同时接收多个客户端的访问，
				如果访问的是innodb存储引擎的表，彼此不知道操作的是同1张表
				
				Durability ：持久性
事务处理结束后，对数据的修改就是永久的，即便系统故障也不会丢失。
				执行回车后，事务就结束。数据会永久有效。





-----------------------------------------------------------------------------------------------
1.24

查看服务的端口号
[root@host51 redis-4.0.8]# netstat  -utnalp  | grep  6379
[root@host51 redis-4.0.8]# ps -C redis-server

停止redis服务
[root@host51 redis-4.0.8]# /etc/init.d/redis_6379 stop
			
启动redis服务
[root@host51 redis-4.0.8]# /etc/init.d/redis_6379 start


#127.0.0.1:6379 常用命令的使用
连接redis服务
~]# redis-cli -h 127.0.0.1 -p 6379
或者
~]#redis-cli   //本机必须使用的时6379 和 127.0.0.1才可以
查看已经存储的数据 	 keys *
存储数据 		 set 变量名 值
查看变量的数据 	 get 变量名

存储多个key值
mset key 名列表
127.0.0.1:6379> mset name bob class nsd2109 age 19

获取多个变量值
127.0.0.1:6379> mget name class school

select 数据库编号0-15    //切换库

exists 变量名       //查看变量是否存在0为没有，1为有

ttl 变量名      //查看变量存储时间

type 变量名	//查看变量类型

move 变量名 库编号      //移动变量到指定库

del 变量名  //删除变量，可删除多个

flushall //删除内存里所有变量

flushdb  //删除所在库的所变量

save     //保存所有变量到硬盘，如果不手动保存，系统也会在一定时间内自动保存






#/etc/redis/6379.conf配置文件的说明
port 6379        //端口
bind 127.0.0.1   //IP地址
daemonize yes    //守护进程方式运行
databases 16 //数据库个数
logfile/var/log/redis_6379.log //日志文件
maxclients 10000 //并发连接数量
dir /var/lib/redis/6379  //数据库目录

配置文件解析 文件里常用配置项说明
通过修改配置项改变redis服务的运行配置，需要重启redis服务才能生效
					
注意：修改服务使用的ip地址、端口号 、连接密码  三项中的任意一项
都无法再使用脚本停止服务，
解决办法1 使用命令停止服务
解决办法2 修改脚本


-h  指定ip地址  -p(小写p) 端口号
[root@host51 ~]# redis-cli  -h 192.168.4.51 -p 6351
192.168.4.51:6351> ping
(error) NOAUTH Authentication required.  没有输入报错
192.168.4.51:6351> 
192.168.4.51:6351> keys *
(error) NOAUTH Authentication required. 报错
192.168.4.51:6351> 
192.168.4.51:6351> auth 123456 输入密码 
OK
192.168.4.51:6351> ping  可以正常访问

			连接时直接指定密码
[root@host51 ~]# redis-cli  -h 192.168.4.51 -p 6351 -a 123456

			#使用命令停止服务
[root@host51 ~]# redis-cli  -h 192.168.4.51 -p 6351 -a 123456 shutdown

掌握redis支持的内存清除策略及相关的配置项
内存清除策略：当向内存里存储数据时，内存空间不足，无法储存当前要保存的数据，会自动调用定义的策略，把内存里已经储存的数据删除，删除到能储存下前要储存的数据为止。
内存清除策略：是软件开发者编写好的数据删除程序，我们只需要在服务的配置文件里，指定是要的删除策略即可

把网站的数据存储在redis服务器里


二、LNP+Redis （把网站的数据存储在redis服务器里）
生产环境下会被网站的热点数据存放在内存存储服务器里，这样的好处是可以加快存取数据的速度，
能够实现网站访问加速
通常网站会把 频繁被访问的数据（热点数据）、数据小的数据 、可再生的数据 存储在内存存储服务器里
			
			2.1 部署网站运行环境LNP环境 统一使用host50做网站服务器
				 具体配置步骤如下：  
						1） 安装源码的nginx软件
						2） 安装php软件
						3） 修改nginx服务的配置文件实现动静分离
						4） 启动服务
								启动nginx服务 和 php-fpm服务
								查看服务对应的端口80 和 9000
						5） 测试Nginx服务能否解释PHP代码
						
yum -y install gcc pcre-devel  zlib-devel
tar -xf nginx-1.12.2.tar.gz 
cd nginx-1.12.2/
./configure 
make 
make install
[root@host50 nginx-1.12.2]# ls /usr/local/nginx/
conf  html  logs  sbin
[root@host50 nginx-1.12.2]# 

[root@host50 ~]# yum -y install php php-fpm php-devel
[root@host50 ~]# vim +65 /usr/local/nginx/conf/nginx.conf
       location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }  
:wq
[root@host50 ~]# /usr/local/nginx/sbin/nginx  -t  检查文件的修改
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
[root@host50 ~]# 

         
[root@host50 ~]# systemctl stop httpd   如果有httpd服务的话要停止
[root@host50 ~]# systemctl disable httpd
Removed symlink /etc/systemd/system/multi-user.target.wants/httpd.service.

启动nginx服务
[root@host50 ~]# /usr/local/nginx/sbin/nginx
[root@host50 ~]# netstat  -utnalp | grep  80
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      9853/nginx: master  
[root@host50 ~]# systemctl  start php-fpm  启动php-fpm服务
[root@host50 ~]# systemctl  enable php-fpm
Created symlink from /etc/systemd/system/multi-user.target.wants/php-fpm.service to /usr/lib/systemd/system/php-fpm.service.
[root@host50 ~]# netstat  -utnalp | grep  9000 查看端口
tcp        0      0 127.0.0.1:9000          0.0.0.0:*               LISTEN      9863/php-fpm: maste 
[root@host50 ~]#        
						
[root@host50 ~]# vim  /usr/local/nginx/html/test.php  #编写php脚本
<?php
$i=99;
echo $i;
?>
:wq

[root@host50 ~]# 
[root@host50 ~]# curl  http://localhost/test.php  命令行访问脚本
99

            2.2 配置php支持redis 
				意思就是在在网站服务器编写php 脚本 可以连接redis服务存储数据和查询数据
				默认PHP不支持redis （也就是不能连接redis服务）
				在网站服务器主机上做如下配置：
				1） 安装软件提供连接redis服务的功能模块
				2） 让PHP进程在运行时 调用redis模块
				3） 查看是否支持redis服务
				4） 测试配置
						4.1 在网站服务器编写PHP脚本 存储数据 和查询数据
								#编写存储数据的脚本
								#编写查询数据的脚本
						4.2 在客户端访问网站服务器php脚本

						4.3 在Redis服务器本机查看内存里的数据
							能够看到PHP存储的数据为成功
							
[root@host50~]#tar -xf redis-cluster-4.3.0.tgz 
[root@host50~]#cd redis-4.3.0/
[root@host50 redis-4.3.0]# phpize  创建配置命令configure 和生产PHP配置信息文件/usr/bin/php-config 
Configuring for:
PHP Api Version:         20100412
Zend Module Api No:      20100525
Zend Extension Api No:   220100525
[root@host50 redis-4.3.0]# ./configure  --with-php-config=/usr/bin/php-config  #配置
[root@host50 redis-4.3.0]# make 编译
[root@host50 redis-4.3.0]# make install 安装 
Installing shared extensions:     /usr/lib64/php/modules/   #提示模块的安装目录
[root@host50 redis-4.3.0]# 
[root@host50 redis-4.3.0]# ls /usr/lib64/php/modules/  #查看目录下的模块列表 有redis.so 模块文件即可
curl.so      json.so    mysql.so      pdo.so         phar.so   sqlite3.so
fileinfo.so  mysqli.so  pdo_mysql.so  pdo_sqlite.so  redis.so  zip.so
[root@host50 redis-4.3.0]# 
			
[root@host50 redis-4.3.0]# vim /etc/php.ini
 728 extension_dir = "/usr/lib64/php/modules/"  模块文件所在的目录
 730 extension = "redis.so" 模块名 
:wq
			
[root@host50 redis-4.3.0]# systemctl  restart php-fpm
[root@host50 ~]# php -m  | grep -i  redis  #查看支持的模块
redis
[root@host50 ~]# 


                4） 测试配置（PHP脚本都网站的程序员 运维不需要，这里编写时为了测试部署的环境）
				
						说明： 要检查脚本连接的redis服务器的redis 服务开启了没有
						
						4.1 在网站服务器编写PHP脚本 存储数据 和查询数据
								
								脚本里都要写什么？
									1、连接redis服务(要指定Ip 、 端口 、连接密码（如果有密码的话）)
									2、执行存储数据或查询数据的命令
									
								#编写存储数据的脚本 set.php	
[root@host50 ~]# vim /usr/local/nginx/html/set.php								
<?php
$redis = new redis(); 定义连接命令
$redis->connect("192.168.4.51",6351); 指定服务器的ip和端口
$redis->auth("123456"); 指定连接密码
$redis->set("redistest","666666"); 存储数据
echo "save ok";
?>
:wq
							    #编写查询数据的脚本 get.php
[root@host50 ~]# vim /usr/local/nginx/html/get.php								
<?php
$redis = new redis(); 定义连接命令
$redis->connect("192.168.4.51",6351); 指定服务器的ip和端口
$redis->auth("123456"); 指定连接密码
echo  $redis->get("redistest"); 输出查询结果

?>
:wq							
								
						4.2 在客户端访问网站服务器php脚本
[root@host50 ~]# curl http://localhost/set.php   #访问存储数据的脚本
save ok

[root@host50 ~]# curl http://localhost/get.php  #访问查询数据的脚本
666666
						4.3 在Redis服务器本机查看内存里的数据
							能够看到PHP存储的数据为成功
[root@host51 ~]# redis-cli  -h 192.168.4.51 -p 6351 -a 123456
192.168.4.51:6351> keys  redistest
1) "redistest"
192.168.4.51:6351> get redistest
"666666"
192.168.4.51:6351> exit






守护进程方式运行服务（常住部队）：服务启动后，对应的进程一直在服务器的内存里运行着，等待客户来连接。
非守护进程方式运行服务（机动部队）：服务启动后，若在一段时间内没有接收到客户端的连接，进程就休眠。等有客户端连接时再苏醒继续提供服务。







不用redis集群存在
数据备份 存储空间扩容 高可用（单点故障） 的问题

集群可以解决的问题
1服务的高可用
2数据的自动备份
3数据的分布式存储


1.3 创建集群
1.1 配置6台Redis服务器（分别做如下配置）
第1步 安装redis软件并做初始化配置
第2步 停止按照初始化配置启动的Redis服务 
第3步 修改主配置文件(启用集群功能)
第4步启动redis服务 并查看端口号
重要说明：内存里不允许有数据 不能设置连接密码  （如果有要清除）

[root@host51 ~]# yum -y install gcc 
[root@host51 ~]# tar -xf redis-4.0.8.tar.gz 
[root@host51 ~]# cd redis-4.0.8/
[root@host51 ~]# make && make install
[root@host51 ~]# ./utils/install_server.sh  遇到提示就回车
[root@host51 ~]# /etc/init.d/redis_6379  stop
[root@host51 ~]# sed -n '70p;93p;815p;823p;829p' /etc/redis/6379.conf 
bind 192.168.4.51 
port 6379
cluster-enabled yes #启用集群功能
cluster-config-file nodes-6379.conf 保存集群信息的配置文件
cluster-node-timeout 5000  集群中主机的连接超时时间
[root@host51 ~]# 	       
[root@host51 ~]# /etc/init.d/redis_6379  start
[root@host51 ~]# netstat  -utnlp  | grep redis-server
tcp        0      0 192.168.4.51:16379      0.0.0.0:*               LISTEN      4249/redis-server 1 
tcp        0      0 192.168.4.51:6379      0.0.0.0:*               LISTEN      4249/redis-server 1 


 1.2 配置管理主机 192.168.4.57
第一步：准备ruby脚本的运行环境 
第二步：创建脚本
第三步：查看脚本帮助信息
		 
]#yum  -y  install   rubygems   ruby 
]#gem  install  redis-3.2.1.gem
]#mkdir  /root/bin     			//创建命令检索目录
]#tar -xf redis-4.0.8.tar.gz
]#cp  redis-4.0.8/src/redis-trib.rb   /root/bin/  	
]#chmod  +x   /root/bin/redis-trib.rb  设置执行权限
]#redis-trib.rb   help   （能看到帮助信息为成功）

#在管理主机mgm57 执行创建集群的命令


创建集群的命令格式   
~]# redis-trib.rb  create --replicas 数字   ip地址:端口  ip地址:端口  ip地址:端口  ....
说明，创建集群时，会把前3台服务器 配置为主服务器，剩下的其他主机全做从服务器
--replicas 从服务器的台数（指定每个主服务器有几台从服务器）

数字：每台主数据库服务器，所配从服务器的台数（给每个主服务器，配从服务器，几就配置几台，必须是3的整数倍），1的话默认前三台为主服务器，后3台为从服务器，如果一共只有6台机器的话，只能写1，写2的话则需要准备9台机器


如果创建是提示某台机器上的数据库不为空，则组要在该机器上进行相关操作
停止redis服务
~]# redis-cli  -h 192.168.4.51 -p 6379  shutdown
清空数据库目录
~]# rm -rf /var/lib/redis/6379/*
启动服务
~]# /etc/init.d/redis_6379 start

1.4 查看集群信息
#查看集群统计信息 
[root@mgm57 ~]# redis-trib.rb info  192.168.4.56:6379

#查看集群详细信息
[root@mgm57 ~]# redis-trib.rb check  192.168.4.51:6379



二、访问集群存取数据
命令格式  
~]# redis-cli  -c   -h redis服务器的ip   -p 端口号
~]# redis-cli -c  -h 192.168.4.51 -p 6379
说明：
连接集群中的任意一台服务器都可以查询数据和存储数据）
-c 连接集群中的主机 使用集群算法存储数据(如果连接的时集群，必须要加这个-c选项)
hash槽 实现数据的 分布书存储
连接集群中的任意主机都可以访问集群存储数据

查询当前主机的主从身份
192.168.4.53:6379> info replication


集群分布式


2.2 集群存储数据的工作原理	（！！！重点难点！！！）
Redis集群是如何实现数据的分布式存储的？
存储数据和查询数据时 调用集群算法  集群算法会计算出1个数字， 数字
在哪个主服务占用的范围内 ，就连接对应的主服务器存储数据或查询数据。


#3.1 向集群里添加新服务器
说明：什么情况下需要向集群里添加新主机（添加master角色服务器）
扩大内存空间（添加master角色服务器）。
为了保证服务的可靠性（给主服务器添加多个从服务器

命令格式
[root@mgm57 ~]# redis-trib.rb add-node 新主机Ip:端口  集群中已有主机的ip:端口

[root@mgm57 ~]# redis-trib.rb add-node 192.168.4.58:6379 192.168.4.51:6379

新添加的master角色主机没有hash slots
分配hast slots (master角色的服务器没有hast slots得不到存储数据机会)
命令格式						
[root@mgm57 ~]# redis-trib.rb   reshard  集群中已有主机的ip:端口
[root@mgm57 ~]# redis-trib.rb reshard  192.168.4.56:6379



#3.2.2  移除master角色的主机

说明：master角色的服务器会占用hash slots  要先释放hash slots  再执行移除主机的命令
		
具体操作步骤：
第一步：释放 hash slots （再次执行reshard命令）
第二步：移除主机 （执行删除主机的命令）

诉求把master角色 主机 host58移除集群，在管理主机mgm57 做如下操作：
#释放 hash slots （再次执行reshard命令）
[root@mgm57 ~]# redis-trib.rb  reshard  192.168.4.56:6379
		
第1个问题：释放hash slots 的个数
How many slots do you want to move (from 1 to 16384)? 4096  （host58主机占用hash slots 的个数）

第2个问题：接收4096的个hash slots的主数据库服务器的ID （随便给那个主服务器都可以） 
What is the receiving node ID?0eb3b7aa0493a19189cba35b0c658202cc20884b   (host51主机的id ,就是把释放的4096个hash slots给主数据库服务器host51)

第3个问题：从那台主服务器移除4096个hash slots 
Source node #1:87cc1c128166e08a16cc294758611453bbc71437  (host58主机的id)
Source node #2:done 结束指定
第4个问题：确认配置 yes同意  no 退出		
Do you want to proceed with the proposed reshard plan (yes/no)? yes  同意


查看集群信息（发现host51 主服务器hash slots变多了 ）
[root@mgm57 ~]# redis-trib.rb  info  192.168.4.56:6379
192.168.4.53:6379 (f2c1bdb7...) -> 1 keys | 4096 slots | 1 slaves.
192.168.4.51:6379 (0eb3b7aa...) -> 3 keys | 8192 slots | 1 slaves. 槽多了 
192.168.4.52:6379 (a9cb8ccd...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.58:6379 (87cc1c12...) -> 0 keys | 0 slots | 0 slaves. 一个槽也没有了 
[OK] 6 keys in 4 masters. 
0.00 keys per slot on average.
[root@mgm57 ~]#				

查看host58主机的id 然后删除主机 Host58
				
[root@mgm57 ~]# redis-trib.rb  check  192.168.4.56:6379 | grep  192.168.4.58
M: 87cc1c128166e08a16cc294758611453bbc71437 192.168.4.58:6379
[root@mgm57 ~]# 
[root@mgm57 ~]# redis-trib.rb  del-node   192.168.4.56:6379  87cc1c128166e08a16cc294758611453bbc71437				
>>> Removing node 87cc1c128166e08a16cc294758611453bbc71437 from cluster 192.168.4.56:6379
>>> Sending CLUSTER FORGET messages to the cluster...
>>> SHUTDOWN the node.
[root@mgm57 ~]# 
					再次查看集群信息 （没有host58 主机了 ）
[root@mgm57 ~]# redis-trib.rb  info  192.168.4.56:6379
192.168.4.53:6379 (f2c1bdb7...) -> 1 keys | 4096 slots | 1 slaves.
192.168.4.51:6379 (0eb3b7aa...) -> 3 keys | 8192 slots | 1 slaves.
192.168.4.52:6379 (a9cb8ccd...) -> 2 keys | 4096 slots | 1 slaves.
[OK] 6 keys in 3 masters.
0.00 keys per slot on average.
[root@mgm57 ~]# 					

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~扩展知识
		
	   1） 平均分配当前所有主服务器的hash slots 
[root@mgm57 ~]# redis-trib.rb  info  192.168.4.56:6379  槽不平均
192.168.4.53:6379 (f2c1bdb7...) -> 1 keys | 4096 slots | 1 slaves.
192.168.4.51:6379 (0eb3b7aa...) -> 3 keys | 8192 slots | 1 slaves.
192.168.4.52:6379 (a9cb8ccd...) -> 2 keys | 4096 slots | 1 slaves.
[OK] 6 keys in 3 masters.
0.00 keys per slot on average.
[root@mgm57 ~]# 
	   
	   平均分配hash slots
[root@mgm57 ~]# redis-trib.rb	rebalance 192.168.4.56:6379
		
		再次查看平均了
	[root@mgm57 ~]# redis-trib.rb info 192.168.4.56:6379
192.168.4.53:6379 (f2c1bdb7...) -> 2 keys | 5462 slots | 1 slaves.
192.168.4.51:6379 (0eb3b7aa...) -> 2 keys | 5461 slots | 1 slaves.
192.168.4.52:6379 (a9cb8ccd...) -> 2 keys | 5461 slots | 1 slaves.
[OK] 6 keys in 3 masters.
0.00 keys per slot on average.










3.2 把服务器移除集群
#3.2.1  移除slave角色的主机
命令格式  
[root@mgm57 ~]# redis-trib.rb del-node  集群中任意主机的Ip:端口  被移除主机的id
[root@mgm57 ~]# redis-trib.rb  del-node 192.168.4.56:6379 d50aa7c1acebe69af0834f1838c8b17b2348472e
说明：slave角色的主机的没有hash slots 直接移除即可
	  主机被移除集群后redis服务会自动停止

#2）添加slave角色的服务器
说明：从角色服务器的数据是master服务器同步过来的数据
所以slave角色的服务器不需要分配hash slots 
只要把主机添加到集群了做slave服务器就可以了。
！！！！会自动做从服务器个数最少的 master服务器的从服务器。！！！
命令格式 
[root@mgm57 ~]# redis-trib.rb  add-node   --slave  新主机Ip:端口  集群中已有主机的ip:端口
[root@host57 ~]# redis-trib.rb add-node --slave 192.168.4.56:6379 192.168.4.52:6379

#该主机添加前需要准备操作
~]# /etc/init.d/redis_6379 start    //开服务
192.168.4.56:6379> cluster info	//查看该主机集群遗留状态
192.168.4.56:6379> cluster reset	//清除该主机集群遗留状态
192.168.4.56:6379> keys *		//查看该主机变量
192.168.4.56:6379> flushall		//清除该主机变量








redis服务的主从复制
持久化
数据类型


NoSQL_day03		
一、主从复制 
redis服务的主从复制（与mysql服务主同步的功能一样，都是实现数据自动同步的存储结构。
主服务器：接收客户端连接
从服务器：连接主服务器同步数据

主从复制结构模式： 一主一从   、 一主多从 、 主从从 

redis主从复制工作过程（数据同步原理）：
！！！说明！！！：从服务器首次做的是全量同步，且同步的数据会覆盖本机的数据
第1步：slave向master发送sync命令
第2步：master启动后台存盘进程，并收集所有修改数据命令
第3步：master完成后台存盘后，传送整个数据文件到slave 
第4步：slave接收数据文件，加载到内存中完成首次完全同步，后续有新数据产生时，master继续收集数据修改命令依次传给slave，完成同步

命令行配置命令（马上生效 但不永久 适合配置线上服务器） 
info  replication     #查看复制信息
slaveof   主服务器ip地址   主服务器端口号   #指定主服务服务器IP地址和服务端口号
slaveof   no  one                         #临时恢复为主服务器 
config rewrite       //配置永久生效
修改配置文件(永久有效,重启了redis服务依然有效)
]# vim /etc/redis/6379.conf
slaveof  主服务器ip地址   主服务器端口号
:wq

!!!说明!!!!
redis服务运行后 默认角色就是master（主）所以一台主机做master 服务器的话 无需配置。
主从结构中的从服务器 都是只读的， 客户端连接从服务器对数据仅有查询权限 



#配置带验证的主从复制
意思就是：
主从结构中的master服务器设置了连接密码，
slave服务器要指定连接密码才能正常同步master主机数据

诉求： 给主从从结构中的master 服务器的redis服务设置连接123456 ,slave服务器配置连接master服务服务器密码
第一步 给Redis服务器Host51 设置连接密码 123456  
第二步 在redis服务器host52 设置连接 master服务器  host51的连接密码  并设置本机redis服务的连接密码为123456
第三步 在redis服务器host53 设置连接 master服务器  host52的连接密码


第一步：给主服务器51 设置连接密码
[root@host51 ~]# redis-cli  -h 192.168.4.51 -p 6379
192.168.4.51:6379> config get requirepass 
1) "requirepass"
2) ""
192.168.4.51:6379> config set requirepass 123456
OK
192.168.4.51:6379> config get requirepass
(error) NOAUTH Authentication required.
192.168.4.51:6379> 
192.168.4.51:6379> auth 123456
OK
192.168.4.51:6379> config get requirepass
1) "requirepass"
2) "123456"
192.168.4.51:6379> config rewrite
OK
192.168.4.51:6379> exit
[root@host51 ~]# tail -1 /etc/redis/6379.conf 
requirepass "123456"
[root@host51 ~]# 
 

第二步 ：配置redis服务器 host52
[root@host52 ~]# redis-cli  -h 192.168.4.52  -p 6379
192.168.4.52:6379> config get requirepass
1) "requirepass"
2) ""
192.168.4.52:6379> config set requirepass 123456
OK
192.168.4.52:6379> auth 123456
OK
192.168.4.52:6379> config get masterauth
1) "masterauth"
2) ""
192.168.4.52:6379> config set masterauth 123456
OK
192.168.4.52:6379> config get masterauth
1) "masterauth"
2) "123456"
192.168.4.52:6379> config rewrite
OK
192.168.4.52:6379> 
192.168.4.52:6379> info replication 
192.168.4.52:6379> info replication
# Replication
role:slave
master_host:192.168.4.51
master_port:6379
master_link_status:up
....
....
192.168.4.52:6379> exit
[root@host52 redis-4.0.8]# tail -2 /etc/redis/6379.conf 
masterauth "123456"
requirepass "123456"
[root@host52 redis-4.0.8]# 

第三步 在redis服务器host53 设置连接 master服务器  host52的连接密码
[root@host53 ~]# redis-cli  -h 192.168.4.53
192.168.4.53:6379> config set masterauth 123456
OK
192.168.4.53:6379> slaveof 192.168.4.52 6379
OK
192.168.4.53:6379> config rewrite
OK
192.168.4.53:6379>
192.168.4.53:6379> info  replication
# Replication
role:slave
master_host:192.168.4.52
master_port:6379
master_link_status:up
....
192.168.4.53:6379> exit





哨兵服务 
什么是哨兵服务：监视主从复制结构中主服务器，发现主服务器无法连接后，会把对应的从升级为主数据库服务器，
继续监视新的主数据库服务器，坏掉的主数据库服务器恢复后，会自动做当前主服务器的从主机。

哨兵服务+redis主从服务 能够实现redis服务高可用和数据的自动备份，但远比Redis集群的资金成本和运维成本要低。

说明： 
1）可以使用一主一从或 一主多从 或 主从从   +   哨兵服务 做服务的高可用 和 数据自动备份  
2）如果主从结构中的redis服务设置连接密码的话必须全每台数据库都要设置密码且密码要一样，
3）宕机的服务器 启动服务后，要人为指定主服务器的连接密码					




步骤一：配置哨兵服务（192.168.4.57）
                具体操作步骤：
                    1）安装源码软件redis （无需做初始化配置，如果做了初始化把redis服务停止即可） 
                    2）创建并编辑主配置文件 （说明源码包里有哨兵服务配置文件的模板sentinel.conf）

				1） 安装源码软件redis （无需做初始化配置，如果做了初始化把redis服务停止即可）
[root@redis57 ~ ]# yum -y install gcc 
[root@redis57 ~ ]# tar -zxf redis-4.0.8.tar.gz
[root@redis57 redis]# cd redis-4.0.8/
[root@redis1 redis-4.0.8]# make
[root@redis1 redis-4.0.8]# make install			

				2）创建并编辑主配置文件
                #说明源码包里有哨兵服务配置文件的模板sentinel.conf
				[root@host57 ~]# vim  /etc/sentinel.conf
			    bind  192.168.4.57
				sentinel   monitor    redis_server   192.168.4.51   6379   1
				sentinel auth-pass    redis_server   123456   #如果主服务器没有连接密码此配置项可用省略
				:wq
					
			    
					3）启动哨兵服务  （会占用当前终端显示启动信息）
 [root@mgm57 redis-4.0.8]# nohup redis-sentinel  /etc/sentinel.conf  &
[1] 5701
[root@mgm57 redis-4.0.8]# nohup: 忽略输入并把输出追加到"nohup.out"   回车即可

[root@mgm57 redis-4.0.8]# jobs  查看当前终端后台进程
[1]+  运行中               nohup redis-sentinel /etc/sentinel.conf &
[root@mgm57 redis-4.0.8]# 

[root@host57 redis-4.0.8]# nohup redis-sentinel /etc/sentinel.conf 2> /dev/null &






--------------------------------------------------------------------------------------------------
1.26
持久化 支持数据永久储存

实现的方式2种
RDB 数据库目录下的dump.rdb
AOF 类似于mysql服务的binlog日志文件

2.1、什么是持久化: redis服务可以永久的保存数据，怎么实现的呢？
	              
	  
2.2、实现方式有2种：分别是 RDB文件 和  AOF文件
	  
1) RDB文件
指定就是数据库目录下的 dump.rdb 文件
redis运行服务后，会根据配置文件的设置的存盘频率 把内存里的数据复制到数据库目录下的dump.rdb文件里（覆盖保存）
	  	  
2）AOF文件
redis服务AOF文件（与mysql服务的binlog日志文件的功能相同）
是一个文件，记录连接redis服务后执行的写操作命令并且是以追加的方式记录写操作命令
默认没有开启，使用需要人为启用。

2.4.4 RDB方式的优/缺点
优点：
高性能的持久化实现 —— 创建一个子进程来执行持久化，先将数据写入临时文件，持久化过程结束后，再用这个临时文件替换上次持久化好的文件；
过程中主进程不做任何IO操作比较适合大规模数据恢复，且对数据完整性要求不是非常高的场合
           
缺点：意外宕机时，丢失最后一次持久化的所有数据



2.5.2 使用AOF文件 实现数据的备份和恢复
		        第1步  备份aof文件
[root@host56 ~]# cp /var/lib/redis/6379/appendonly.aof  /opt/
[root@host56 ~]# ls /opt/*.aof
/opt/appendonly.aof
[root@host56 ~]#
 		
		        第2步  使用备份的aof文件恢复数据
				       #删除数据
[root@host56 ~]# redis-cli -h 192.168.4.56 -p 6379
192.168.4.56:6379> flushall 
192.168.4.56:6379> exit

				
#恢复数据
第1步： 把没有数据的服务停止
[root@host56 ~]# redis-cli -h 192.168.4.56 -p 6356  shutdown

第2步： 删除没有数据的aof文件和rdb文件
[root@host56 ~]# rm -rf  /var/lib/redis/6379/*
						 
第3步：把备份的aof文件拷贝到数据库目录
[root@host56 ~]# cp /opt/appendonly.aof  /var/lib/redis/6379/

第4步：启动redis服务并查看数据
[root@host56 ~]# /etc/init.d/redis_6379 start




2.5.4  与aof 相关的配置项 ( vim /etc/redis/6379.conf )
                    appendfsync  always     	//时时记录，并完成磁盘同步
                    appendfsync  everysec  	    //每秒记录一次，并完成磁盘同步
                    appendfsync  no           	//写入aof ，不执行磁盘同步
                    auto-aof-rewrite-min-size   64mb   //首次重写触发值
                    auto-aof-rewrite-percentage  100   //再次重写，增长百分比



2.5.6 AOF文件的优缺点
AOF优点
可以灵活设置持久化方式
出现意外宕机时，仅可能丢失1秒的数据

AOF缺点
持久化文件的体积通常会大于RDB方式
执行fsync策略时的速度可比RDB方式慢




192.168.4.55:6379> CONFIG GET appendonly
192.168.4.55:6379> CONFIG sET appendonly yes
192.168.4.55:6379> CONFIG rewrite





三、数据类型（操作多 ，主要掌握对不同类型数据的管理命令）
在数据是通过程序员写网站脚本存储在 redis 服务器的内存里。
数据库运维需要对存储在服务器内存里的数据做管理,
管理包括: 查看 修改 删除 存储新数据 ，不同类型的数据要使用对应的命令管理
redis服务支持的数据类型如下：
1) 字符类型（ string）    字符串  可以是英文字母 或 汉字 

2) 列表类型  （list）     一个变量名 存储多个数据 （类似于shell脚本语言的数组）

3）Hash 类型 （hash）    一个变量里可以存储多列 每列对应一个值

4）集合类型		也是让一个变量可以存储多个数据 （和python 语言集合类型是一个意思）
                         集合分为有序集合类型（zset）   和 无序集合类型（ set）

工作如何对程序存储在内存里的数据做管理
第1步 查看变量是否存在  
	192.168.4.56:6356> EXISTS  NAME
	(integer) 0     表示不存在
	192.168.4.56:6356> EXISTS  age
	(integer) 1   表示变量存在
	192.168.4.56:6356> 
第2步  查看数据类型 （知道数据类型后，才能明确用来管理命令）
			注意： 每种类型的数据都是用固定的关键字表示 
		   192.168.4.56:6356> type age
			string
			192.168.4.56:6356> 
第3步：掌握每种类型数据对应的管理命令
			操作命令包括:  变量赋值   查看变量值  修改变量值   输出变量值



~~~~~~~~~~~~~~~~数据类型的使用~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
### 字符类型 string 

- 字符串类型是 Redis 中最基本的数据类型，它能存储任何形式的字符串，包括二进制数据
- 可以用其存储用户的邮箱、JSON 化的对象甚至是一张图片
- 一个字符串类型键允许存储的数据的最大容量是512 MB
- 字符串类型是其他4种数据类型的基础，其他数据类型和字符串类型的差别从某种角度来说只是组织字符串的形式不同

#### 字符串操作命令



1字节=8位

### 列表类型

- 列表类型（list）可以存储一个有序的字符串列表
- 常用的操作是向列表两端添加元素，或者获得列表的某一个片段

- 列表类型内部是使用双向链表（double linked list）实现的，获取越接近两端的元素速度就越快
- 使用链表的代价是通过索引访问元素比较慢
- 这种特性使列表类型能非常快速地完成关系数据库难以应付的场景：如社交网站的新鲜事，我们关心的只是最新的内容，使用列表类型存储，即使新鲜事的总数达到几千万个，获取其中最新的100条数据也是极快的

#### 列表类型操作命令







############################################################################################
Mysql项目2

给网站搭建数据存储架构
实现功能
数据库服务的高可用（7×24小时不间断的提供服务）
实现数据的自动备份
实现数据库服务的负载均衡
搭建内存存储数据库服务器，用来存储网站的热点数据

1 案例1：配置逻辑卷
1.1 问题

具体配置如下：
添加磁盘
磁盘分区
创建LV
格式化
systemctl stop mysqld
rpm -q lvm2
yum -y install lvm2
rpm -q lvm2
lsblk
pvcreate /dev/vdb /dev/vdc
vgcreate  vg0 /dev/vdb /dev/vdc
vgs
lvcreate -n lv0 -L 5.99g vg0
lvscan 
mkfs.xfs /dev/vg0/lv0
lsblk /dev/vg0/lv0
blkid /dev/vg0/lv0
lsblk
systemctl status mysqld.service 
2 案例2：配置数据库服务器
2.1 问题

具体操作如下：
安装MySQL软件
挂载LV分区
启动服务
管理员登录
rm -rf /var/lib/mysql/*
vim /etc/fstab 
mount -a
mount | grep "/dev/vg0/lv0"
mount | grep "/var/lib/mysql"
systemctl start mysqld
grep password /var/log/mysqld.log | tail -1
grep password /var/log/mysqld.log
grep password /var/log/mysqld.log | tail -1
mysql -uroot -p"BxG9ie;GOeNq" 
mysql -uroot -p"123qqq...A"
scp /etc/fstab 192.168.4.22:/etc/


3 案例3：配置主从同步
3.1 问题

配置步骤如下：
配置主服务器
配置从服务器

步骤一：配置主服务器
1）启用binlog日志
[root@mysql11 ~]# vim /etc/my.cnf
[mysqld]
server_id=11
log-bin=master11
:wq
[root@mysql11 ~]# systemctl  restart mysqld
[root@mysql11 ~]#

2）用户授权
mysql> grant replication  slave  on  *.*  to  repluser@"%" identified by "123qqq...A";

3）查看日志信息
mysql> show master status;
+-----------------+----------+--------------+------------------+-------------------+
| File            | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+-----------------+----------+--------------+------------------+-------------------+
| master11.000001 |      441 |              |                  |                   |
+-----------------+----------+--------------+------------------+-------------------+


步骤二：配置从服务器

1）指定server_id
[root@mysql22 ~]# vim /etc/my.cnf
[mysqld]
server_id=22
:wq
[root@mysql22 ~]# systemctl  restart mysqld

2）指定主服务器信息
mysql> change master to  master_host="192.168.4.11",master_user="repluser",
    -> master_password="123qqq...A",master_log_file="master11.000001",master_log_pos=441;

3）启动slave进程
mysql> start slave ;

4）查看状态信息
[root@mysql22 ~]# mysql -uroot -p123qqq...A -e "show slave status\G" |grep -i yes
[root@mysql22 ~]# mysql -uroot -p123qqq...A -e "show slave status\G" |grep -i 192.168.4.11




4 案例4：配置读写分离服务
4.1 问题

配置步骤如下：
安装软件
修改配置文件
配置数据库服务器
启动服务
查看服务状态
查看监控信息







5 案例5：准备NFS服务存储磁盘
5.1 问题

具体配置如下：
添加磁盘
磁盘分区
创建LV
格式化





步骤一：磁盘分区

1）创建分区，分1个区即可
[root@nfs30 ~]# fdisk  -l /dev/vdb  //查看磁盘信息
磁盘 /dev/vdb：10.7 GB, 10737418240 字节，20971520 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
[root@nfs30 ~]#
[root@nfs30 ~]# fdisk  /dev/vdb   //磁盘分区
欢迎使用 fdisk (util-linux 2.23.2)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。
Device does not contain a recognized partition table
使用磁盘标识符 0x67bb10cf 创建新的 DOS 磁盘标签。
命令(输入 m 获取帮助)：n  //新建分区
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p):
Select (default p): p //创建主分区
分区号 (1-4，默认 1)： //回车
起始 扇区 (2048-20971519，默认为 2048)： //回车
将使用默认值 2048
Last 扇区, +扇区 or +size{K,M,G} (2048-20971519，默认为 20971519)：//回车
将使用默认值 20971519
分区 1 已设置为 Linux 类型，大小设为 10 GiB
命令(输入 m 获取帮助)：w  //保存退出
The partition table has been altered!
Calling ioctl() to re-read partition table.
正在同步磁盘。
[root@nfs30 ~]#


2）查看分区
 [root@nfs30 ~]# fdisk  -l /dev/vdb
磁盘 /dev/vdb：10.7 GB, 10737418240 字节，20971520 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x67bb10cf
   设备 Boot      Start         End      Blocks   Id  System
/dev/vdb1            2048    20971519    10484736   83  Linux
[root@nfs30 ~]#


步骤二：挂载磁盘

1）格式化
[root@nfs30 ~]# mkfs.xfs /dev/vdb1  //格式化
meta-data=/dev/vdb1              isize=512    agcount=4, agsize=655296 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=2621184, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
[root@nfs30 ~]#
[root@nfs30 ~]# blkid /dev/vdb1  //查看
/dev/vdb1: UUID="81740d7a-51f1-4ce1-a830-7b6517cc778e" TYPE="xfs"
[root@nfs30 ~]#
2）配置开机挂载
 [root@nfs30 ~]# vim  /etc/fstab   //修改配置文件
/dev/vdb1       /sitedir        xfs     defaults        0 0
[root@nfs30 ~]#
[root@nfs30 ~]# mkdir /sitedir  //创建挂载目录
[root@nfs30 ~]# chmod o+w /sitedir   //赋予写权限
[root@nfs30 ~]# mount –a  //加载文件中所有未加载的设备
[root@nfs30 ~]# mount | grep  "/sitedir"  //查看加载信息
/dev/vdb1 on /sitedir type xfs (rw,relatime,attr2,inode64,noquota)
[root@nfs30 ~]#



6 案例6：配置NFS服务
6.1 问题

具体配置如下：
安装软件
修改配置文件
启动服务
查看共享信息

步骤一：部署NFS服务

1）安装软件
[root@nfs30 ~]# yum -y install  nfs-utils   rpcbind


2）修改配置文件
[root@nfs30 ~]# 
[root@nfs30 ~]# vim /etc/exports
/sitedir *(rw)
:wq
[root@nfs30 ~]# exportfs –r  //加载配置
[root@nfs30 ~]#
步骤二：启动服务

1）启动服务
**********注意一定要先启动rpcbind
[root@nfs30 ~]# systemctl  start rpcbind
[root@nfs30 ~]#
[root@nfs30 ~]# systemctl  start nfs
[root@nfs30 ~]#
[root@nfs30 ~]# systemctl  enable nfs
Created symlink from /etc/systemd/system/multi-user.target.wants/nfs-server.service to /usr/lib/systemd/system/nfs-server.service.
[root@nfs30 ~]#
[root@nfs30 ~]# systemctl  enable rpcbind
[root@nfs30 ~]#
2）查看共享信息
[root@nfs30 ~]# showmount  -e localhost
Export list for localhost:
/sitedir *
[root@nfs30 ~]#









java命令拷贝完，需要重启tomcat
[root@web33 ~]# /usr/local/tomcat/bin/shutdown.sh 
[root@web33 ~]# /usr/local/tomcat/bin/startup.sh



测试添加库、表、用户
mysql> create database gamedb;
Query OK, 1 row affected (0.05 sec)

mysql> create table gamedb.user(name char(10),password char(6));
Query OK, 0 rows affected (0.42 sec)

mysql> grant select,insert on gamedb.* to yaya@"%" identified by "123qqq...A";
Query OK, 0 rows affected, 1 warning (0.04 sec)



----------------------------------------------------------------------------------------------------
1.27



redis-trib.rb create  --replicas 1 192.168.4.51:6379 192.168.4.52:6379 192.168.4.53:6379 192.168.4.54:6379   192.168.4.55:6379 192.168.4.56:6379








-----------------------------------------------------------------------------------------------------
1.28
mysql数据在线迁移
在不停止线上服务器的数据库服务的情况下，把存储

一般备份时候都是在从服务器上操作

innobackupex  --user root --password 123qqq...A  --slave-info  /allbak --no-timestamp //备份所有数据，并记录备份数据对应的binlog日志名
--slave-info
和--master-data作用一样，记录binlog日志对应的文件名 和 偏移量

查看备份时的日志名和偏移量
[root@host66 ~]# grep master11 /root/allbak/xtrabackup_info 


查看集群主机
mysql> show status like "%wsrep%";


[root@mysql11 ~]# mysql -uroot -p123qqq...A
#访问pxc集群存储数据需要表中有主键字段
mysql> alter table gamedb.user add  id int primary key auto_increment first; 
mysql> exit;


*************************************************
****pxc集群添加失败处理办法****************
killall -9 mysqld (多执行几遍，杀干净进程)
清空mysql目录
rm -rf /var/lib/mysql/*
检查配置文件是否写对
检查3台主机防火墙和selinux是否关闭
66主机 要有授权了 sstuser用户
执行启动mysql服务




change master to  master_host="192.168.4.11",master_user="repluser", master_password="123qqq...A",master_log_file="master11.000003",master_log_pos=419;



grant replication slave, replication client on *.* to  mysqla@"%"identified by "123qqq...A";

grant select  on  mysql.*  to mysqlb@"%" identified by "123qqq...A";



----------------------------------------------------------------------------------------------------------
2.7

3273252716


luckfurit

网桥 就是 虚拟交换机

[root@localhost ~]# vim /etc/libvirt/qemu/networks/vbr.xml
<network>
  <name>vbr</name>      
  <forward mode='nat'/>
  <bridge name='vbr' stp='on' delay='0'/>
  <ip address='192.168.100.254' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.100' end='192.168.100.200'/>
    </dhcp>
  </ip>
</network>

name 网桥名字
mode 工作模式
name 网卡识别名 ifconfig查看到的名字 stp生成树协议
address ip地址

自动分配ip dhcp
<dhcp>
    <range start='192.168.100.100' end='192.168.100.200'/>
</dhcp>


-------------------------------------------------------------------------------------
2.8

openstack
用来管理大批量的虚拟机


云服务
私有云
共有云
混合云

openstack组件

Horizon  组件,提供web管理界面
Keystone 组件,提供集中的认证和授权
Nova     组件,计算节点创建管理云主机
Glance   组件,管理云主机镜像
Swift    组件,存储云使用的对象存储服务
Neutron  组件,管理云服务的内部、外部网络路由等
Cinder   组件,管理云主机的存储卷服务



--------------------------------------------------------------------------------------------------------
2.9

公网ip分配给跳板机，然后管理员通过连接跳板机 从而连接各种内部的机器



同一地区云主机 可以互通 例如北京和北京通


云主机默认情况下是不能访问互联网，如果要访问需要给它购买弹性ip和带宽，并且绑定。



[root@ecs-proxy ~] #yum install -y net-tools lftp rsync psmisc vim-enhanced tree vsftpd  bash-completion createrepo lrzsz iproute




## 公有云配置

区域： 同一个区域中的云主机是可以互相连通的，不通区域云主机是不能使用内部网络互相通信的

选择离自己比较近的区域，可以减少网络延时卡顿

华为云yum源配置 https://support.huaweicloud.com/ecs_faq/ecs_faq_1003.html

## 跳板机配置 

#### 配置yum源，安装软件包


[root@ecs-proxy ~]# rm -rf /etc/yum.repos.d/*.repo
[root@ecs-proxy ~]# curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo
[root@ecs-proxy ~]# yum clean all
[root@ecs-proxy ~]# yum makecache
[root@ecs-proxy ~]# yum install -y net-tools lftp rsync psmisc vim-enhanced tree vsftpd  bash-completion createrepo lrzsz iproute
[root@ecs-proxy ~]# mkdir /var/ftp/localrepo
[root@ecs-proxy ~]# cd /var/ftp/localrepo
[root@ecs-proxy ~]# createrepo  .
[root@ecs-proxy ~]# createrepo --update . # 更新
[root@ecs-proxy ~]# systemctl enable --now vsftpd

yum clean all 删除缓存
yum makecache 删除，更新缓存
yum repolist  查看缓存

ifconfig命令
lftp rsync 俩命令
psmisc 命令pstree
vim-enhanced 命令vim
tree 类似ls的命令
vsftpd  ftp服务
bash-completion  TAB键 
createrepo 创建yum仓库的命令
lrzsz 命令rz，sz
iproute 命令查看网关



#### 优化系统服务


[root@ecs-proxy ~]# systemctl stop postfix atd tuned
[root@ecs-proxy ~]# yum remove -y postfix at audit tuned kexec-tools firewalld-*
[root@ecs-proxy ~]# vim /etc/cloud/cloud.cfg
# manage_etc_hosts: localhost 注释掉这一行
[root@ecs-proxy ~]# reboot


#### 

#### 安装配置ansible管理主机


[root@ecs-proxy ~]# tar zxf ansible_centos7.tar.gz
[root@ecs-proxy ~]# yum install -y ansible_centos7/*.rpm
//非交互式的创建密钥   -t 加密算法 -b长度 -N 密码空 -f 密钥文件的路径和文件名
[root@ecs-proxy ~]# ssh-keygen -t rsa -b 2048 -N '' -f /root/.ssh/id_rsa   
[root@ecs-proxy ~]# chmod 0400 /root/.ssh/id_rsa
//如果密钥文件多，可以通过   -i选项  选择指定密钥对
[root@ecs-proxy ~]# ssh-copy-id -i /root/.ssh/id_rsa 模板主机IP


## 


----------------------------------------------------------------------
2.10

linux里面容器是做什么的
linux中的容器是装应用的
容器就是将软件打包成标准化单元，用于开发，交付和部署
容器技术已经成为应用程序封装和交付的和新技术


容器技术的核心有以下几个内核技术组成
Cgroup资源管理
selinux安全
NameSpace-命名空间

linux的NameSpace
UTS，NETWORK，MOUNT，USER，PID，IPC

优点
相比于传统的虚拟化技术，容器更加简洁高效
传统虚拟机需要给每个VM安装操作系统
容器使用的共享公共库和程序
缺点
容器的隔离性没有虚拟化强
共用Linux内核，安全性有先天缺陷

docker与容器
docker是实现容器的一个软件，并不等于容器
docker是完整的一套容器管理系统
docker提供了一组命令，让用户更加方便直接的使用容器技术，而不需要过多关心底层内核技术

自定义yum源步骤
1把安装包拷贝到yum的自定义目录下
2通过（createrepo --update . ）指令，在yum的自定义目录下更新
3yum makecache 清理缓存


#### 跳板机yum源添加docker软件
[root@ecs-proxy ~]# cp -a docker /var/ftp/localrepo/ 
[root@ecs-proxy ~]# cd /var/ftp/localrepo/
把软件包加入到repodata
[root@ecs-proxy localrepo]# createrepo --update .        


#### 在 node 节点验证软件包
[root@docker-0001 ~]# yum makecache
[root@docker-0001 ~]# yum list docker-ce*


以下操作所有 node 节点都需要执行

#### 开启路由转发
[root@docker-0001 ~]# vim /etc/sysctl.conf
net.ipv4.ip_forward = 1
[root@docker-0001 ~]# sysctl -p



[root@docker-0001 ~]# yum install -y docker-ce
[root@docker-0001 ~]# systemctl enable --now docker
[root@docker-0001 ~]# ifconfig # 验证，能看见 docker0
[root@docker-0001 ~]# docker version # 验证，没有报错


## 


镜像概述
惊险是启动容器的核心
在docker中容器是基于镜像启动的
镜像采用分层设计
使用COW技术

镜像的名称和标签
-每一个镜像都对应唯一的镜像id（id理解为hash）
-镜像名称+标签 =唯一
-每一个镜像都有标签，如果没写就是默认标签 latest
-我们在调用镜像的时候，如果没有指定标签也是 latest








## 镜像管理&容器管理

######################## docker镜像管理命令 #############################################33

| 镜像管理命令                                      | 说明                                       |
| :------------------------------------------------ | :----------------------------------------- |
| docker images                                     | 查看本机镜像                               |
| docker  search  镜像名称                          | 从官方仓库查找镜像                         |
| docker  pull  镜像名称:标签                       | 下载镜像                                   |
| docker  push  镜像名称:标签                       | 上传镜像                                   |
| docker  save 镜像名称:标签  -o 备份镜像名称.tar   | 备份镜像为tar包                            |
| docker  load -i  备份镜像名称                     | 导入备份的镜像文件                         |
| docker  rmi  镜像名称:标签                        | 删除镜像（必须先删除该镜像启动的所有容器） |
| docker  history  镜像名称:标签                    | 查看镜像的制作历史                         |
| docker  inspect  镜像名称:标签                    | 查看镜像的详细信息                         |
| docker  tag  镜像名称:标签  新的镜像名称:新的标签 | 创建新的镜像名称和标                       |


# 依照上面方法依次导入 nginx.tar.gz redis.tar.gz ubuntu.tar.gz
[root@docker-0001 ~]# docker load -i centos.tar.gz

# 查看镜像
[root@docker-0001 ~]# docker images

# 备份镜像 centos 到 tar 包
[root@docker-0001 ~]# docker save centos:latest -o centos.tar

# 删除镜像，不能删除已经创建容器的镜像
[root@docker-0001 ~]# docker rmi ubuntu:latest

# 查看镜像的详细信息
[root@docker-0001 ~]# docker inspect centos:latest

# 查看镜像的历史信息
[root@docker-0001 ~]# docker history nginx:latest

# 给镜像添加新的名词和标签
[root@docker-0001 ~]# docker tag ubuntu:latest newname:newtag

# ----------------------以下操作必须在一台可以访问互联网的机器上执行---------------------------
# 搜索镜像
[root@docker-0001 ~]# docker search busybox
在查询到的内容里OFFICIAL列为显示是否为官方镜像

# 下载镜像
[root@docker-0001 ~]# docker pull busybox







########################### docker容器管理命令############################

| 容器管理命令                                | 说明                                            |
| ------------------------------------------- | ----------------------------------------------- |
| docker  run  -it(d) 镜像名称:标签  启动命令 | 创建启动并进入一个容器，后台容器使用参数 d      |
| docker  ps                                  | 查看容器 -a 所有容器，包含未启动的，-q 只显示id |
| docker  rm  容器ID                          | -f 强制删除，支持命令重入                       |
| docker  start\|stop\|restart  容器id        | 启动、停止、重启容器                            |
| docker  cp  本机文件路径  容器id:容器内路径 | 把本机文件拷贝到容器内（上传）                  |
| docker  cp  容器id:容器内路径  本机文件路径 | 把容器内文件拷贝到本机（下载）                  |
| docker  inspect  容器ID                     | 查看容器的详细信息                              |
| docker  attach  容器id                      | 进入容器的默认进程，退出后容器会关闭            |
| docker  attach  容器id  [ctrl+p, ctrl+q]    | 进入容器以后，退出容器而不关闭容器的方法        |
| docker  exec  -it  容器id  启动命令         | 进入容器新的进程，退出后容器不会关闭            |






# 在后台启动容器
[root@docker-0001 ~]# docker run -itd nginx:latest 
9cae0af944d81770c90fdeacf7a632aaa749b0c9fbc0f4cb104e1d1257579e5e
# 在前台启动容器
[root@docker-0001 ~]# docker run -it --name myos centos:latest /bin/bash
[root@de46e6254efd /]# ctrl+p, ctrl+q # 使用快捷键退出，保证容器不关闭

# 查看容器
[root@docker-0001 ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED
de46e6254efd        centos:latest       "/bin/bash"              2 minutes ago  
9cae0af944d8        nginx:latest        "nginx -g 'daemon of…"   7 minutes ago  
# 只查看id
[root@docker-0001 ~]# docker ps -q
# 查看所有容器，包含未启动的
[root@docker-0001 ~]# docker ps -a

# 进入容器的默认进程
[root@docker-0001 ~]# docker attach de46e6254efd
[root@de46e6254efd /]# exit # 退出后容器会关闭

# 启动、停止、重启容器
[root@docker-0001 ~]# docker start   de46e6254efd
[root@docker-0001 ~]# docker stop    9cae0af944d8
[root@docker-0001 ~]# docker restart 9cae0af944d8

# 查看容器详细信息
[root@docker-0001 ~]# docker inspect 9cae0af944d8
... ...
      "IPAddress": "172.17.0.2",
... ...
[root@docker-0001 ~]# curl http://172.17.0.2/

# 进入容器，查看路径
[root@docker-0001 ~]# docker exec -it 9cae0af944d8 /bin/bash
root@9cae0af944d8:/# cat /etc/nginx/conf.d/default.conf
... ...
      root   /usr/share/nginx/html;
... ...

# 从容器内拷贝首页文件到宿主机，修改后拷贝回容器内
[root@docker-0001 ~]# docker cp 9cae0af944d8:/usr/share/nginx/html/index.html ./index.html
[root@docker-0001 ~]# vim index.html
Hello Tedu
Hello Tedu
Hello Tedu
[root@docker-0001 ~]# docker cp ./index.html 9cae0af944d8:/usr/share/nginx/html/index.html
[root@docker-0001 ~]# curl http://172.17.0.2/

# 删除容器
[root@docker-0001 ~]# docker rm -f de46e6254efd
# 删除所有容器
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
















修改 跳板机  /etc/ssh/sshd_config ,添加 ClientAliveInterval 30 和 ClientAliveCountMax 86400
14:05

讲师李欣

ClientAliveInterval 60 和 ClientAliveCountMax 3



真机
/etc/ssh/ssh_config  配置  ServerAliveInterval 60 和 ServerAliveCountMax 3


容器和虚拟机相比没有操作系统
启动运行容器
[root@docker-0001 ~]# docker run -it centos:latest /bin/bash
//bin后面可以跟任何的命令，例如/bin/cat /etc/passwd 就是查看该容易内的 /etc/passwd文件
意思是在容器内查看
[root@docker-0001 ~]# docker run -it centos:latest /bin/cat /etc/passwd
//如果不写/bin/bash 则这次启动的是默认的启动命令
每一个镜像都可以设置唯一的一个启动命令
[root@docker-0001 ~]# docker run -it busybox:latest
//查看镜像信息
docker inspect 镜像名称
[root@docker-0001 ~]# docker inspect busybox:latest 
Cmd该镜像默认的启动命令



apache

父辈被杀死，子进程就会变成孤儿进程
所有的孤儿进程都会被上帝进程收养

容器的启动进程必须在前台运行
容器内的服务 必须在前台运行

容器里没有systemd 所以需要手动启动

ls /usr/lib/systemd/system 找启动项
/usr/lib/systemd/system/httpd.service


启动不同服务都要设置环境变量
export LANG=C      //环境变量
为apache的日志和报错的输出内容，设置标准语言，C代表英文，主要是为了防止出现乱码。
之前没有手动设置，是因为之前通过systemd启动服务，systemd已经为用户通过程序自动实现、无需手动
[root@docker-0001 ~]# echo $LANG
[root@docker-0001 ~]# locale
查看环境变量命令



----------------------------------------------------------------------------------------------------------------------
2.11
云平台部署与管理

一、自定义镜像原理
镜像采用分层设计
创建读写层
修改配置
重新打包

#查看指定镜像的创建历史
标签内的<missing>表示该层不能单独使用
[root@docker-0001 ~]# docker history myos:latest 
#通过tag标签，恢复已经删除的镜像
[root@docker-0001 ~]# docker tag 76d6bc25b8a5 centos:latest 



二、Dockerfile语法解析


commit的局限
很容易制作简单的镜像，但碰到复杂的情况就十分不方便例如
需要设置默认的启动命令
需要设置环境变量
需要指定竞相开放某些特定端口

Dockerfile 是一种更强大的镜像制作方式
编写类似脚本的Dockerfile文件，通过该文件制作镜像


#### Dockerfile打包镜像

###### Dockerfile语法

| 语法指令 | 语法说明                              |
| -------- | ------------------------------------- |
| FROM     | 基础镜像                              |
| RUN      | 制作镜像时执行的命令，可以有多个      |
| ADD      | 复制文件到镜像，自动解压              |
| COPY     | 复制文件到镜像，不解压                |
| EXPOSE   | 声明开放的端口                        |
| ENV      | 设置容器启动后的环境变量              |
| WORKDIR  | 定义容器默认工作目录（等于cd）        |
| CMD      | 容器启动时执行的命令，仅可以有一条CMD |



###### 使用Dockerfile创建镜像

**docker  build  -t  镜像名称:标签  Dockerfile所在目录**


###### 制作apache镜像 #########################################

CMD  指令可以查看 service 文件的启动命令 ExecStart（/lib/systemd/system/httpd.service）

ENV  环境变量查询服务文件中的环境变量配置文件 EnvironmentFile 指定的文件内容



[root@docker-0001 ~]# mkdir apache; cd apache
[root@docker-0001 apache]# vim Dockerfile
FROM myos:latest
##只在该容器中运行，运行逻辑类似 ssh 192.168.1.10 操作命令，执行一条断开一次，不能CD储存路径
RUN  yum install -y httpd php && yum clean all                  
ENV  LANG=C
ADD  webhome.tar.gz  /var/www/html/
##设置默认永久工作目录，类似永久CD
WORKDIR /var/www/html/
EXPOSE 80
CMD ["/usr/sbin/httpd", "-DFOREGROUND"]
# 拷贝 webhome.tar.gz 到当前目录中
[root@docker-0001 apache]# docker build -t myos:httpd .



查看与验证镜像

[root@docker-0001 web]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
myos                httpd               db15034569da        12 seconds ago      355MB
myos                latest              867409e412c8        2 hours ago         281MB
[root@docker-0001 web]# docker rm -f $(docker ps -aq)
[root@docker-0001 web]# docker run -itd myos:httpd
[root@docker-0001 web]# curl http://172.17.0.2/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 172.17.0.1
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host: 	6c9e124bee1a
1229


#查找开启php-fpm服务的方法
yum install php-fpm
rpm -ql php-fpm | grep service
cat /usr/lib/systemd/system/php-fpm.service
cat /etc/sysconfig/php-fpm

[root@b345e1ae7ff3 /]# rpm -ql php-fpm | grep service
/usr/lib/systemd/system/php-fpm.service

[root@b345e1ae7ff3 /]# cat /usr/lib/systemd/system/php-fpm.service
[Unit]
Description=The PHP FastCGI Process Manager
After=syslog.target network.target

[Service]
Type=notify
PIDFile=/run/php-fpm/php-fpm.pid
EnvironmentFile=/etc/sysconfig/php-fpm
ExecStart=/usr/sbin/php-fpm --nodaemonize
ExecReload=/bin/kill -USR2 $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target

[root@b345e1ae7ff3 /]# cat /etc/sysconfig/php-fpm
# Additional environment file for php-fpm



CMD  ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

daemon off 意思是让nginx的守护进程在前台运行



######################################################################################



###### 制作php-fpm镜像

[root@docker-0001 ~]# mkdir php; cd php
[root@docker-0001 php]# vim Dockerfile
FROM myos:latest
RUN  yum install -y php-fpm && yum clean all
EXPOSE 9000
CMD ["/usr/sbin/php-fpm", "--nodaemonize"]
[root@docker-0001 php]# docker build -t myos:php-fpm .



验证服务
[root@docker-0001 ~]# docker run -itd myos:php-fpm
deb37734e52651161015e9ce7771381ee6734d1d36bb51acb176b936ab1b3196
[root@docker-0001 ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS
deb37734e526        myos:php-fpm        "/usr/sbin/php-fpm -…"   17 seconds ago      Up 15 seconds       
[root@docker-0001 ~]# docker exec -it deb37734e526 /bin/bash
[root@deb37734e526 ~]# ss -ltun
Netid  State      Recv-Q        Send-Q          Local Address:Port         Peer Address:Port 
tcp    LISTEN     0             128             127.0.0.1:9000             *:*
[root@deb37734e526 ~]# 




###### 制作nginx镜像

1、制作 nginx 软件包

```shell
[root@docker-0001 ~]# yum install -y gcc make pcre-devel openssl-devel
[root@docker-0001 ~]# useradd nginx
[root@docker-0001 ~]# tar -zxvf nginx-1.12.2.tar.gz
[root@docker-0001 ~]# cd nginx-1.12.2
[root@docker-0001 nginx-1.12.2]# ./configure --prefix=/usr/local/nginx --user=nginx --group=nginx --with-http_ssl_module
[root@docker-0001 nginx-1.12.2]# make && make install
[root@docker-0001 nginx-1.12.2]# # 拷贝 docker-images/info.html和info.php 到 nginx/html 目录下
[root@docker-0001 nginx-1.12.2]# cd /usr/local/
[root@docker-0001 local]# tar czf nginx.tar.gz nginx
```

2、制作镜像


```dockerfile
[root@docker-0001 local]# mkdir /root/nginx ;cd /root/nginx
[root@docker-0001 nginx]# cp /usr/local/nginx.tar.gz ./
[root@docker-0001 nginx]# vim Dockerfile 
FROM myos:latest
RUN  yum install -y pcre openssl && useradd nginx && yum clean all
ADD  nginx.tar.gz /usr/local/
EXPOSE 80
WORKDIR /usr/local/nginx/html
CMD  ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
[root@docker-0001 nginx]# docker build -t myos:nginx .
```


验证服务

```shell
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
deb37734e526
[root@docker-0001 ~]# docker run -itd myos:nginx
e440b53a860a93cc2b82ad0367172c344c7207def94c4c438027c60859e94883
[root@docker-0001 ~]# curl http://172.17.0.2/info.html
<html>
  <marquee  behavior="alternate">
      <font size="12px" color=#00ff00>Hello World</font>
  </marquee>
</html>
[root@docker-0001 ~]# 
```

#### 




#### 发布容器服务

###### 对外发布服务

给 docker-0001 绑定一个公网IP

docker  run  -itd  -p 宿主机端口:容器端口  镜像名称:标签

```shell
# 把 docker-0001 变成 apache 服务
[root@docker-0001 ~]# docker run -itd -p 80:80 myos:httpd

# 把 docker-0001 变成 nginx 服务，首先必须停止 apache
[root@docker-0001 ~]# docker stop $(docker ps -q)
[root@docker-0001 ~]# docker run -itd -p 80:80 myos:nginx
```

验证方式： 通过浏览器访问即可

###### 容器共享卷

docker  run  -itd  -v 宿主机对象:容器内对象  镜像名称:标签

使用共享卷动态修改容器内配置文件

```shell
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# mkdir /var/webconf
[root@docker-0001 ~]# cp /usr/local/nginx/conf/nginx.conf /var/webconf/
[root@docker-0001 ~]# vim /var/webconf/nginx.conf
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }
[root@docker-0001 ~]# docker run -itd -p 80:80 --name nginx \
                          -v /var/webconf/nginx.conf:/usr/local/nginx/conf/nginx.conf myos:nginx
```


验证方式： 使用 exec 进入容器查看

```shell
[root@docker-0001 ~]# docker exec -it nginx /bin/bash
[root@e440b53a860a html]# cat /usr/local/nginx/conf/nginx.conf
[root@e440b53a860a html]# # 查看 php 相关配置是否被映射到容器内
```

###### 





###### 容器间网络通信

实验架构图例


实验步骤

```shell
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# mkdir -p /var/{webroot,webconf}
[root@docker-0001 ~]# cp /usr/local/nginx/html/info.* /var/webroot/
[root@docker-0001 ~]# cp /usr/local/nginx/conf/nginx.conf /var/webconf/
[root@docker-0001 ~]# vim /var/webconf/nginx.conf
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }
# 启动前端 nginx 服务，并映射共享目录和配置文件
[root@docker-0001 ~]# docker run -itd --name nginx -p 80:80 \
      -v /var/webconf/nginx.conf:/usr/local/nginx/conf/nginx.conf \
      -v /var/webroot:/usr/local/nginx/html myos:nginx
# 启动后端 php 服务，并映射共享目录
[root@docker-0001 ~]# docker run -itd --network=container:nginx \
      -v /var/webroot:/usr/local/nginx/html myos:php-fpm
```

验证服务

```shell
[root@docker-0001 ~]# curl http://docker-0001/info.html
<html>
  <marquee  behavior="alternate">
      <font size="12px" color=#00ff00>Hello World</font>
  </marquee>
</html>
[root@docker-0001 ~]# curl http://docker-0001/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 172.17.0.1
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host: 	f705f89b45f9
1229
```




#### docker私有仓库

###### docker私有仓库图例


[root@registry ~]# yum install -y docker-distribution
[root@registry ~]# systemctl enable --now docker-distribution
[root@registry ~]# curl http://192.168.1.100:5000/v2/_catalog
{"repositories":[]}




###### docker客户端配置

所有node节点都需要配置，这里 docker-0001，docker-0002都要配置

native.cgroupdriver   cgroup驱动，docker默认 cgroupfs

registry-mirrors          默认下载仓库，使用国内源能快一点

insecure-registries     私有仓库地址（重点）

```shell
[root@docker-0001 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["https://hub-mirror.c.163.com"],
    "insecure-registries":["192.168.1.100:5000", "registry:5000"]
}
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# systemctl restart docker
```

###### 


###### 上传镜像

```shell
# 上传 myos:latest, myos:httpd, myos:nginx, myos:php-fpm
[root@docker-0001 ~]# docker tag myos:latest 192.168.1.100:5000/myos:latest
[root@docker-0001 ~]# docker push 192.168.1.100:5000/myos:latest
```

###### 验证测试

********curl http://仓库IP:5000/v2/_catalog*********

*********curl http://仓库IP:5000/v2/镜像名称/tags/list*******

```shell
[root@docker-0002 ~]# curl http://192.168.1.100:5000/v2/_catalog
{"repositories":["myos"]}
[root@docker-0002 ~]# curl http://192.168.1.100:5000/v2/myos/tags/list
{"name":"myos","tags":["latest"]}
# 使用远程镜像启动容器
[root@docker-0002 ~]# docker run -it 192.168.1.100:5000/myos:latest
Unable to find image '192.168.1.100:5000/myos:latest' locally
latest: Pulling from myos
7dc0dca2b151: Pull complete 
95c297b4d705: Pull complete 
Digest: sha256:d61ffc053895e2dc16f63b8a2988dfe5f34207b48b1e74d397bb3267650ba4ce
Status: Downloaded newer image for 192.168.1.100:5000/myos:latest
[root@674ebe359e44 /]# 
```






--------------------------------------------------------------------------------
2.14

K8s起源

k8s是容器集群管理系统，是一个开源的平台，可以实现容器集群的自动化部署、自动扩缩容、维护等功能

k8s概述

K8s集群核心角色
1、master管理节点
2、node计算节点
3、image镜像仓库


一、K8S Master节点（管理节点）
1、master提供集群的控制
2、对集群进行全局决策
3、检测和相应集群事件
4、master主要由apiserver、scheduler、etcd和controllermanager服务组成

二、K8S node节点（计算节点）
1、运行容器的实际节点
2、维护运行pod，宾提具体应用的运行环境
3、node由kubelet、kube-proxy和docker组成
3、计算节点被设计成水平扩展，该组件在多个节点上运行


master概述
master节点服务
apiserver
scheduler
etcd
controllermanager

API Server
是整个系统的对外接口，提供客户端和其它组件调用
后段元数据储存于etcd中（键值数据库）
Scheduler
负责对集群内部的资源进行调度，相当于“调度宝”
Controller manager
督责管理控制器，相当于“大总管”
etcd的定义
-etcd是CoreOS的开源项目，它的目标是构建一个高可用的分布式键值（key-value）数据库，基于Go语言实现。在分布式系统中，各种服务的配置信息的管理分享，服务的发现是一个很基本同时也是很重要的问题。CoreOS项目就希望基于etcd来解决这一问题。
-kubernetes在运行过程中产生的原数据全部储存在etcd中。
etcd 键值管理
-在键的组织上etcd采用了层次化的空间结构（类似于文件系统中的目录概念），用户指定的键可以为单独的名字。
-如创建key键值，此时实际上放在根目录/key下面。
-也可以为指定目录结构，如/dir1/dir2/key，则将创建相应的目录结构。
-etcd 有kubernetes集群自动管理，用户无需手动干预。
-etcdctl是etcd的客户端管理程序。



K8S 安装工具部署
kubeadm 用来初始化集群的指令
kubelet 在集群中的每个节点上用来启动pod和容器等
kubectl 用来与集群通信的命令行工具
docker 容器管理工具

为docker配置私有仓库，和cgroup控制器
-kubelet使用的cgroup控制器是systemd，而docker使用的是cgroupfs，必须设置成统一的（查询命令：docker info）


kube-proxy是什么
-kube-proxy是实现Kubernetes Service的通信与组在均衡机制的重要组件，是Kubernetes的核心组件。
kube-proxy代理模式
Kubernetes v1.0，用户空间代理模式
1.1 iptables模式代理
1.8 ipvs代理模式，如果不满足条件退回至iptables模式代理





kubeadm命令
config      配置管理命令
help		查看帮助
init		初始命令
join		node加入集群的命令
reset		还原状态命令
token		token凭证管理命令
version	产看版本



node概述
node节点服务
-docker
-kubelet
-kube-proxy

docker服务
-容器管理
kubelet服务
-负责监视pod，包括创建、修改、删除等。
kube-proxy服务
-主要负责为pod对象提供代理
-实现service的通信与负载均衡

pod是什么
pod是kubernetes调度的基本单元
一个pod包涵1个或多个容器
这些容器是用的相同的网络命名空间和端口号
pod是一个服务的多个进程的聚合单位
pod作为一个独立的部署单位，支持横向扩展和复制


flannel概述
flannel实质上是一种覆盖网络，也就是将TCP数据包装在另一种网络包里面进行路由转发和通信，目前已经支持UDP、VxLAN、AWS VPC和GCE路由等数据转发方式。
使用flannel目标
不同主机内的容器实现互联互通





###### 1、重新购买云主机

| 主机名称 | IP地址        | 最低配置    |
| -------- | ------------- | ----------- |
| registry | 192.168.1.100 | 1CPU,1G内存 |

###### 2、安装仓库服务


[root@registry ~]# yum makecache
[root@registry ~]# yum install -y docker-distribution
[root@registry ~]# systemctl enable --now docker-distribution


###### 3、使用脚本初始化仓库

**拷贝云盘 kubernetes/v1.17.6/registry/myos目录 到 仓库服务器**
**拷贝下面这个文件到 仓库机**
[root@ecs-proxy ~]# ls kubernetes/v1.17.6/registry/

[root@registry ~]# mkdir myos
[root@registry ~]# cd myos
[root@registry myos~]# chmod 755 init-img.sh
[root@registry myos~]# ./init-img.sh
[root@registry myos~]# curl http://192.168.1.100:5000/v2/myos/tags/list
{"name":"myos","tags":["nginx","php-fpm","v1804","httpd"]}


#### kube-master安装

按照如下配置准备云主机

| 主机名    | IP地址        | 最低配置    |
| --------- | ------------- | ----------- |
| master    | 192.168.1.21  | 2CPU,2G内存 |
| node-0001 | 192.168.1.31  | 2CPU,2G内存 |
| node-0002 | 192.168.1.32  | 2CPU,2G内存 |
| node-0003 | 192.168.1.33  | 2CPU,2G内存 |
| registry  | 192.168.1.100 | 1CPU,1G内存 |

###### 1、防火墙相关配置

参考前面知识点完成禁用 selinux，禁用 swap，卸载 firewalld-*
[root@master ~]# sestatus 
SELinux status:                 disabled

[root@master ~]# free -m
              total        used        free      shared  buff/cache   available
Mem:           3788         310        1381           8        2097        3225
Swap:             0           0           0

[root@master ~]# rpm -qa | grep firewalld


###### 2、配置yum仓库(跳板机)

[root@ecs-proxy localrepo]# cp -a kubernetes/v1.17.6/k8s-install/ /var/ftp/localrepo/
或
[root@ecs-proxy ~]# cp -a v1.17.6/k8s-install  /var/ftp/localrepo/
[root@ecs-proxy ~]# cd /var/ftp/localrepo/
[root@ecs-proxy localrepo]# createrepo --update .

###### 3、安装软件包(master)
查看所需安装包
kubeadm config images list 
安装kubeadm、kubectl、kubelet、docker-ce
[root@master ~]# yum makecache
[root@master ~]# yum install -y kubeadm kubelet kubectl docker-ce
[root@master ~]# mkdir -p /etc/docker
[root@master ~]# vim /etc/docker/daemon.json 
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["https://hub-mirror.c.163.com"],
    "insecure-registries":["192.168.1.100:5000", "registry:5000"]
}
[root@master ~]# systemctl enable --now docker kubelet
[root@master ~]# docker info |grep Cgroup
Cgroup Driver: systemd


4、镜像导入私有仓库

# 把云盘 kubernetes/v1.17.6/base-images 中的镜像拷贝到 master
[root@master ~]# cd base-images/
[root@master base-image]# for i in *.tar.gz;do docker load -i ${i};done
[root@master base-image]# docker images
[root@master base-image]# docker images |awk '$2!="TAG"{print $1,$2}'|while read _f _v;do
    docker tag ${_f}:${_v} 192.168.1.100:5000/${_f##*/}:${_v}; 
    docker push 192.168.1.100:5000/${_f##*/}:${_v}; 
    docker rmi ${_f}:${_v}; 
done
# 查看验证
[root@master base-image]# curl http://192.168.1.100:5000/v2/_catalog


###### 5、Tab键设置

[root@master ~]# kubectl completion bash >/etc/bash_completion.d/kubectl
[root@master ~]# kubeadm completion bash >/etc/bash_completion.d/kubeadm
[root@master ~]# exit

###### 6、安装IPVS代理软件包

[root@master ~]# yum install -y ipvsadm ipset


####### 7、配置master主机环境

[root@master ~]# vim /etc/hosts
192.168.1.21	master
192.168.1.31	node-0001
192.168.1.32	node-0002
192.168.1.33	node-0003
192.168.1.100	registry


[root@master ~]# vim /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
[root@master ~]# modprobe br_netfilter      //重新载入内和模块
[root@master ~]# sysctl --system


###### 8、使用kubeadm部署

应答文件在云盘的 kubernetes/v1.17.6/config 目录下

[root@master ~]# mkdir init;cd init
# 拷贝 kubeadm-init.yaml 到 master 云主机 init 目录下
[root@master init]# kubeadm init --config=kubeadm-init.yaml |tee master-init.log
# 根据提示执行命令
[root@master init]# mkdir -p $HOME/.kube
[root@master init]# sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
[root@master init]# sudo chown $(id -u):$(id -g) $HOME/.kube/config


9、验证安装结果
[root@master ~]# kubectl version
[root@master ~]# kubectl get componentstatuses
NAME                        STATUS      	MESSAGE             	ERROR
controller-manager       	Healthy         ok
scheduler                   Healthy   		ok
etcd-0                 		Healthy   		{"health":"true"}

####如果需要重新安装，先执行下面命令，在重新执行kubeadm init --config=kubeadm-init.yaml |tee master-init.log
[root@master init]# kubeadm reset --force


#### 计算节点安装


###### 1、获取token

# 创建token
ttl=0 代表无限时间
[root@master ~]# kubeadm token create --ttl=0 --print-join-command
[root@master ~]# kubeadm token list
# 获取token_hash
[root@master ~]# openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt |openssl rsa -pubin -outform der |openssl dgst -sha256 -hex
writing RSA key
(stdin)= aff45cf362bada22e4e0cb7603b61f48f3404407c3b5335545a85e2dd82d4cd1

52ju8j.w7ce1il1krwnnllv 
--token 52ju8j.w7ce1il1krwnnllv     --discovery-token-ca-cert-hash sha256:aff45cf362bada22e4e0cb7603b61f48f3404407c3b5335545a85e2dd82d4cd1 


###### 2、node安装

拷贝云盘上 kubernetes/v1.17.6/node-install 到跳板机

[root@ecs-proxy ~]# cd node-install/
[root@ecs-proxy node-install]# vim files/hosts
::1             localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1       localhost localhost.localdomain localhost4 localhost4.localdomain4
192.168.1.21    master
192.168.1.31    node-0001
192.168.1.32    node-0002
192.168.1.33    node-0003
192.168.1.100   registry

文件内填写之前记下的token和token_hash值
[root@ecs-proxy node-install]# vim node_install.yaml
... ...
  vars:
    master: '192.168.1.21:6443'
    token: 'fm6kui.mp8rr3akn74a3nyn'
    token_hash: 'sha256:f46dd7ee29faa3c096cad189b0f9aedf59421d8a881f7623a543065fa6b0088c'
... ...
[root@ecs-proxy node-install]# ansible-playbook node_install.yaml

3、验证安装
[root@master ~]# kubectl get nodes
NAME        STATUS     ROLES    AGE     VERSION
master      NotReady   master   130m    v1.17.6
node-0001   NotReady   <none>   2m14s   v1.17.6
node-0002   NotReady   <none>   2m15s   v1.17.6
node-0003   NotReady   <none>   2m9s    v1.17.6


#### 网络插件安装配置

拷贝云盘 kubernetes/v1.17.6/flannel 目录到 master 上

###### 1、上传镜像到私有仓库

[root@master ~]# cd flannel
[root@master flannel]# docker load -i flannel.tar.gz
[root@master flannel]# docker tag quay.io/coreos/flannel:v0.12.0-amd64 192.168.1.100:5000/flannel:v0.12.0-amd64
[root@master flannel]# docker push 192.168.1.100:5000/flannel:v0.12.0-amd64

2、修改配置文件并安装

[root@master flannel]# vim kube-flannel.yml
128: "Network": "10.244.0.0/16",
172: image: 192.168.1.100:5000/flannel:v0.12.0-amd64
186: image: 192.168.1.100:5000/flannel:v0.12.0-amd64
227-结尾: 删除
[root@master flannel]# kubectl apply -f kube-flannel.yml


3、验证结果

[root@master flannel]# kubectl get nodes
NAME  		STATUS	ROLES	AGE		VERSION
master		Ready	master	26h		v1.17.6
node-0001	Ready	<none>	151m	v1.17.6
node-0002	Ready	<none>	152m	v1.17.6
node-0003	Ready	<none>	153m	v1.17.6




出现错误可以执行删除命令
kubectl delete -f kube-flannel.yml






------------------------------------------------------------------------------------------
2.15

**重点背会**
kubectl常用命令

kubectl是用于控制kubernetes集群的命令行工具

语法格式
kubectl [command] [TYPE] [NAME] [flags]
command：子命令,如create，get，describe，delete
type：资源类型，可以表示为单数，复数或缩写形式
name：资源的名称，如果省略，则显示所有资源信息
flags：指定可选标志，或附加的参数

命名空间
系统命名空间默认有4个
default 默认的命名空间，不声明命名空间的pod都在这里
kube-node-lease 为高可用提供心跳监视的命名空间
kube-public 公共数据，所有用户都可以读取它
kube-system 系用服务对象所和iyong的命名空间

#### kubectl 命令

###### 命令说明

| 命令格式                                            | 命令说明                             |
| --------------------------------------------------- | ------------------------------------ |
| kubectl run 资源名称 -参数 --image=镜像名称:标签    | 创建资源对象，常用参数-i交互，-t终端 |
| kubectl get 查询资源  可选参数 -o wide 显示主机信息 | 常用查询的资源 node\|deployment\|pod |
| kubectl exec -it 容器id  执行的命令                  | 同 docker exec 指令，进入容器内      |
| kubectl describe 资源类型  资源名称                    | 查询资源的详细信息                   |
| kubectl attach                                 | 同 docker attach 指令，连接容器      |
| kubectl logs 容器id                             | 查看容器控制台的标准输出             |
| kubectl delete 资源类型  资源名称                      | 删除指定的资源                       |
| kubectl create\|apply  -f 资源文件                  | 执行指定的资源文件                   |

#查看主机详细的信息
[root@master ~]# kubectl get node node-0001 -o wide

#查看这个资源是怎么定义的
[root@master ~]# kubectl get node node-0001 -o yaml

#查看kube-system（系统命名空间）中的pods
[root@master ~]# kubectl -n kube-system get pods
第一列 POD名称 （不是容器） 
第二列预期的状态和启动结果，如果显示2/3(例如x/y格式)，就代表一共3个容器，启动了2个，只有x=y的时候代表启动成功。

第四列重启的次数，如果某个POD重启的次数过多（比其他容器的次数多很多），代表有问题。
第五列POD创建的时间。

**排错三兄弟**
#排错命令，找到有错误的资源对象
[root@master ~]# kubectl -n kube-system get pod
#排错命令，查看显示中的Events选项
[root@master ~]# kubectl -n kube-system describe pod kube-flannel-ds-amd64-2794b 
#排错命令 查看日志
[root@master ~]# kubectl -n kube-system logs coredns-f6bfd8d46-dk5gm


#创建容器
[root@master ~]# kubectl run testos -i -t --image=192.168.1.100:5000/myos:v1804  

#查看默认空间的容器
[root@master ~]# kubectl get pods

#进入一个正在运行的容器
“--”叫做选项终止符，例如 有个文件叫做“-f” 通过 rm -f -f 无法删除，需要通过终止符 rm -f -- -f 删除
[root@master ~]# kubectl exec -it testos-6d7c98965-t8l52 -- /bin/bash

# 删除资源，直接删除POD会自动重建
[root@master ~]# kubectl delete pod testos-6d7c98965-t8l52
#需要删除上层的Deploy
[root@master ~]# kubectl get deployments.apps       //查看名称
[root@master ~]# kubectl delete deployments.apps testos      //删除命令 
删除时候会卡住
宽限期
程序突然中断，造成数据丢失


#####以“-”分割，分别为1 2 3级的控制器查看
[root@master ~]# kubectl get deployments.apps 
[root@master ~]# kubectl get replicasets.apps 
[root@master ~]# kubectl get pods


docker 没有pod，k8s有pod
**pod创建的逻辑过程**
pod创建用到三个组件
apiserver 收到用户创建的pod指令，就把他写入到etcd数据库内，现在pod属于pending状态，
apiserver然后去找 scheduler 询问到底去那里创建pod更好，scheduler根据一些规则后告诉apiserver把pod放在那里更好，例如放到node003上更好，规则例如字段冲突，cpu使用量等判断条件、算法，根据你的适配性选择

然后apiapiserver在把结果写入etcd数据库，并告诉scheduler已经收到选择了
然后这时pod就变成containercraete状态

然后apiserver就去找kubelet 创建pod容器
kubelet调用docker来创建容器
docker创建完后把创建结果返回给 kubelet，kubelet再把结果返回给apiserver ，apiserver写入到etcd后 告诉kubelet创建成功

apiserver etcd scheduler 这三个是在master上，kubelet docker是在node上
**结束**



pod启动过程

主容器 mainc 必须要配置的，其他可选
初始化容器 init c 可选

在主容器启动之前，mysql要做初始化
就是var lib mysql 创建出来

在主容器启动之前（运行时机），要先定义 initc 可以实现初始化 数据库初始化操作

初始化时会生成随即密码，密码使用不是很方便，因为mainc（也就是主进程）没有启动，所以通过initc不能修改密码
这时候可以在posts  上 修改（启动后钩子），他时根主程序一起启动的

然后main就开始运行了，一般两种程序，有限和无限生命周期的，统计类的为有限生命周期，类似服务的程序就是无限生命周期

无限生命周期 有两个探针 live生命探测器    如果它发现主进程端口不再监听，就会通知pod，然后pod会main重新启动
read(就绪性探测)检查 主从的两个sql_io和xxxx线程，
pre（结束前钩子）时在容器结束之前，可以把统计结果拷贝保存到其他地方



pod相位状态
pending是等待调度 还没有开始调度的状态

running容器正在运行的状态
running下分有限和无限生命周期
有限生命运行万，容器会进入succeed状态 到结束
无限不会进入succeed，他会一直在running转圈，不会进入到succeed

POD资源文件
---				资源定义起始标志
kind:Pod			当前创建资源的类型
apiVersion:v1		当前格式的版本
metadata:			当前资源的元数据
  name:mypod		当前资源的名称(Pod)
spec：			当前资源的详细定义
  containers：		容器定义
  - name:mylinux   	容器名称,多容器再一个Pod中名称不能重复
  image:myos:v1804	启动容器的镜像地址
  stdin:true		相当于-i参数,分配标准输出(-i 交互式)
  tty:true			相当于-t参数,分配终端(-t 终端)
-i 交互式，-t 终端， -d 在后台启动

1---一个文件的开始
2[root@master ~]# kubectl api-resources
可查询资源类型
3当前资源类型的版本，每个资源类型的版本是唯一的
[root@master ~]# kubectl api-versions 
可查询版本
[root@master ~]# kubectl explain Pod
4metadata:跟谁对齐，name就是谁的名字
5name名字
6spec：跟谁对齐，它就是谁的详细定义
7


deploy资源文件
---				资源定义起始标识
kind:Deployment		当前创建资源的类型	
apiVersion:app/v1		当前格式的版本
metadata:			元数据
  name:myapache		deployment的名称
spec:				详细配置信息
  selector:			选择器
    matchLabels:		标签选择
      myapp:httpd		标签，在POD中定义
  replicas:1		副本数量

template:
  metadata:
    labels:
      myapp:httpd
  spec:
    containers:
    - name:webcluster








#### 集群调度

###### 扩容与缩减
##扩容缩减命令replicas= 数字写多少就改多少
[root@master ~]# kubectl scale deployment myapache --replicas=3

[root@master ~]# kubectl get deployments.apps
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
myapache   1/1     1            1           113s
##扩容缩减命令replicas= 数字写多少就改多少
[root@master ~]# kubectl scale deployment myapache --replicas=3
deployment.apps/myapache scaled
[root@master ~]# kubectl get deployments.apps
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
myapache   3/3     3            3           2m12s


#集群更新与回滚
#通过命令查看编号
[root@master config]# kubectl rollout history deployment myapache
#回滚命令，输入后修改镜像并保存
[root@master ~]# kubectl edit deployments.apps myapache 

[root@master config]# kubectl rollout history deployment myapache 
deployment.apps/myapache 
REVISION  CHANGE-CAUSE
1         <none>
[root@master ~]# curl http://10.244.3.5
this is apache
[root@master ~]# kubectl edit deployments.apps myapache 
deployment.apps/myapache edited
[root@master ~]# curl http://10.244.2.6
this is nginx
[root@master ~]# kubectl rollout history deployment myapache 
deployment.apps/myapache 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
[root@master ~]# kubectl rollout undo deployment myapache --to-revision=1
deployment.apps/myapache rolled back
[root@master ~]# curl http://10.244.3.6
this is apache
[root@master ~]#


















-------------------------------------------------------------------------------------------
2.16
DaemonSet控制器

跟deploy不同，DaemonSet控制器类型是 DaemonSet直接管理pod，没有rs

创建时候与硬件节点绑定，有几台机器，就能创建几个pod副本


污点政策
污点标签
NoSchedule 		不会被调度
PreferNoSchedule	尽量不调度
前两个标签不会影响正在运行的容器


NoExecute 		驱逐节点
驱逐标签是对已经运行的容器都有效

查看污点标签
[root@master ~]# kubectl describe nodes master | grep Taints

污点标签对所有的pod和容器都有效


#### 污点与容忍

污点策略：NoSchedule、PreferNoSchedule、NoExecute

查看污点标签
[root@master ~]# kubectl describe nodes master | grep Taints

设置污点标签
[root@master ~]# kubectl taint node node-0001 k1=v1:NoSchedule

删除污点标签
[root@master ~]# kubectl taint node node-0001 k1-



job/cronjob 控制器


[root@master ~]# vim clusterip.yaml 
---
kind: Service
apiVersion: v1
metadata:
  name: myapache
spec:
  ports:
  - protocol: TCP
    port: 80               service 端口
    targetPort: 80		pod端口
  selector:
    myapp: httpd      # 标签必须与 deploy 资源文件中一致
  type: ClusterIP



k8S里有两个网段
一个是service
一个是pod

service只能再pod内部使用

每一台node 都有lvs


lvs 4层快
haproxy 4 和 7 都 可可以
7层服务 nginx 

[root@master ~]# kubectl delete -f mynodeport.yaml 

docker tag quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.30.0 192.168.1.100:5000/nginx-ingress-controller:0.30.0


nodeport 基于4层服务
ingress 使用ingress控制器7层 只能用于web文文件


-------------------------------------------------------------------------------------------------------------------
2.17
kubernetes 性能与监控
一、metrics介绍
metrics是一个监控系统资源使用的插件，可以监控node节点上的cpu、内存的使用率，或pod对资源的占用率，通过对资源占用的了解，可以更加合理的部署容器应用。


四个资源文件的地址
[root@master ~]# ls /etc/kubernetes/manifests/


#### 部署metrics-server

###### 开启apiserver聚合服务
[root@master ~]# vim /etc/kubernetes/manifests/kube-apiserver.yaml
# spec.containers.command 最下面手动添加如下一行
    - --enable-aggregator-routing=true
[root@master ~]# systemctl restart kubelet
[root@master ~]# kubectl -n kube-system get pod kube-apiserver-master -o yaml |grep enable-aggregator-routing
    - --enable-aggregator-routing=true



###### 证书的申请与签发

要在所有节点执行（master，node-0001，node-0002，node-0003）

申请的多余证书可以使用 （kubectl delete certificatesigningrequests 证书名称） 删除



ConfigMap不适用于大文件，会拖慢整个集群的速度，一般用于10M以下的小文件，例如配置文件



[root@master ~]# kubectl create configmap nginx-conf --from-file=/var/webconf/nginx.conf


[root@master ~]# kubectl delete configmaps nginx-conf

[root@master ~]# kubectl get configmaps nginx-conf -o yaml




emptyDir 临时卷
内存里的文件夹
内存目录

把emp映射到宿主机 ，宿主机被挂了 ，emp依旧还在pod中、不会消失，但是pod如果没了，emp就消失了
并非是共享的硬盘，只是从pod中拿出一块数据空间给不同的容器使用，也可以同一个数据空间给同时给多个容器使用，并非强制共享，容器消失，数据空间不会消失。

ConfigMap 也是类似的作用，只不过针对的是小文件，比较适合配置文件，给同样的机器写配置文件，只需要在ConfigMap写一份，同时映射多个容器即可。



文件适合方哪里
nfs    网页
cept   网页
hostp  日志不能丢
secar 密码加密

config 
emp 缓存



mount --bind /root/myos  /xxhh


pvc是资源使用者
pv是资源提供者













-----------------------------------------------------------------------------------------------------------------
2.18

dashboard
是基于网页的kubernetes用户界面


[root@master dashboard]# kubectl get namespaces 
NAME                   STATUS   AGE
default                Active   3d19h
ingress-nginx          Active   40h
kube-node-lease        Active   3d19h
kube-public            Active   3d19h
kube-system            Active   3d19h
kubernetes-dashboard   Active   24m


[root@master dashboard]# kubectl -n kubernetes-dashboard get pods
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-57bf85fcc9-dfz9b   1/1     Running   0          24m
kubernetes-dashboard-7b7f78bcf9-hptdk        1/1     Running   0


[root@master dashboard]# kubectl -n kubernetes-dashboard get service
NAME                        TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
dashboard-metrics-scraper   ClusterIP   10.254.77.3    <none>        8000/TCP   27m
kubernetes-dashboard        ClusterIP   10.254.82.19   <none>        443/TCP    27m


ClusterIP 只能在集群内访问


比较两个文件命令
vimdiff
[root@master ~]# vimdiff admin-token.yaml service.yaml 


一
[root@master dashboard]# kubectl -n kubernetes-dashboard get serviceaccounts 
NAME                   SECRETS   AGE
admin-user             1         4s
default                1         65m
kubernetes-dashboard   1         65m

二
[root@master dashboard]# kubectl -n kubernetes-dashboard describe serviceaccounts admin-user 
Name:                admin-user
Namespace:           kubernetes-dashboard
Labels:              <none>
Annotations:         kubectl.kubernetes.io/last-applied-configuration:
                       {"apiVersion":"v1","kind":"ServiceAccount","metadata":{"annotations":{},"name":"admin-user","namespace":"kubernetes-dashboard"}}
Image pull secrets:  <none>
Mountable secrets:   admin-user-token-xnmp6
Tokens:              admin-user-token-xnmp6
Events:              <none>

三
[root@master dashboard]# kubectl -n kubernetes-dashboard describe secrets admin-user-token-xnmp6
Name:         admin-user-token-xnmp6
Namespace:    kubernetes-dashboard
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: admin-user
              kubernetes.io/service-account.uid: 8435d6d0-b36f-4118-bde0-cf8dfa647ba0

Type:  kubernetes.io/service-account-token

Data
====
namespace:  20 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6InFOaXNKa09NOURidGEzUExaQy1zNzlPV2JfNlJiN0ktRjlpaDVzb1d3RjQifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLXhubXA2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI4NDM1ZDZkMC1iMzZmLTQxMTgtYmRlMC1jZjhkZmE2NDdiYTAiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.amfb3PatYsTFSgM0vQhAs50Gpczu2oMqZe_AwqNn03GcVuMMNfwxnlpVrZ-w6f_KKVWaGG57ioJoNCyuTP3X0BVrpDDdo1ZYRgHcwP32q97AKQWJrZgyzF_hICw7yXN2BjuRefd-VRRPfZ2m1j3k850PqeHk6E4fmlv-hB8cmTORDeHAmPIJR4C_hPYlueGwI7IM56IMmtzUPew_zPCcfYEEx-vZIZ9qWvCfCjlMDdywxN1dge06Phkrgk-ZLI4X-KsYw_GRsTpHYI4oYKdXKKHaOa6CCG80PtN4QSwxa3nqIEAZESONKACzWq8XlieJ_zHd3uWZM523LJaDIQe6kQ
ca.crt:     1025 bytes


安装普罗米修斯

三个数据采集插件
metrics-state 
node-exporter 
prom-adapter



查找普罗米修斯地址，在容器内访问，可以写名字也可以写名字
[root@master prometheus]# kubectl -n monitoring get service
prometheus-k8s          ClusterIP   10.254.235.221   <none>        9090/TCP 




--------------------------------------------------------------------------------------------------------------
2.21

ELK
分布式的日志收集平台，可以把日志可视化。
Elasticsearch：负责日志检索和存储
Logstash：负责日志的收集和分析、处理
Kibana：扶着日志的可视化。

ELK组件在海量日志系统的运维中，可用于解决
-分布式日志数据集中式查询和管理
-系统监控，包含系统硬件和应用各个组件的监控
-故障排查
-安全信息和事件管理
-报表功能


和zabixx区别
它们所作用的时间和地点不同
Z理解火葬场 服务如果不死，就算出现问题就不不管，如果挂了才告诉你
E理解医院 监控这个服务，如果这个服务不死，它就认为这个服务是正常的，就算负载高也不影响。
Z集中在系统的运行阶段，




Elasticsearch：


从大到小
mysql
库
表
行
列

从大到小
Ela
索引
类型
文档
字段



[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
17:  cluster.name: my-es
23:  node.name: es-0001 # 本机主机名
55:  network.host: 0.0.0.0
##集群创始人，必须有人，几个没有关系，作用是为公司把关，所以启动集群时候，必须先启动创始人节点
##最好至少有两个创始人，为了集群的高可用
68:  discovery.zen.ping.unicast.hosts: ["es-0001", "es-0002"]       


集群配置管理
命令行配置或者网页图形配置


Elasticsearch：
图形页面中，颜色深的是原数据，浅色是副本
数据尽量平均
副本数必须小于分篇数
分布式存储，一份数据存储时分成指定份数，并且为了保证数据不丢失，每份数据都做了副本（可以设置副本数据），相同份的数据存储位置不相同，
缺点是，副本数越多，占用存储空间越多，但是安全性好。




Elasticsearch使用的请求方法
增 PUT
删 DELETE
改 POST
查 GET


#### Elasticsearch基本操作

###### 查询_cat方法

# 查询支持的关键字
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/
# 查具体的信息
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master
# 显示详细信息 ?v
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master?v
# 显示帮助信息 ?help
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master?help

案例
[root@ecs-proxy ~]# curl 192.168.1.41:9200/_cat
[root@ecs-proxy ~]# curl 192.168.1.41:9200/_cat/master
N9uCQy9cRf6piHD7hPXBcQ 192.168.1.42 192.168.1.42 es-0002
[root@ecs-proxy ~]# curl 192.168.1.41:9200/_cat/master?v
id                     host         ip           node
N9uCQy9cRf6piHD7hPXBcQ 192.168.1.42 192.168.1.42 es-0002
[root@ecs-proxy ~]# curl 192.168.1.41:9200/_cat/master?help
id   |   | node id    
host | h | host name  
ip   |   | ip address 
node | n | node name




"Content-Type: application/json"：定义下面的json表达式

curl -XPUT -H "Content-Type: application/json" http://es-0001:9200/tedu -d \
'{
    "settings":{
       "index":{
          "number_of_shards": 5, 
          "number_of_replicas": 1
       }
    }
}'



--------------------------------------------------------------------------------
2.22

Logstash
input 	负责收集数据
filter	负责处理数据
output	负责输出数据

Logstash类似流水线


Logstash依赖java环境，需要安装java
Logstash没有默认配置文件，需要手动配置
Logstash安装在/usr/share/logstash目录下


##查看Logstash插件
[root@logstash ~]# cd /usr/share/logstash/
[root@logstash logstash]# ./bin/logstash-plugin list

###### 插件与调试格式

使用json格式字符串测试  {"a":"1", "b":"2", "c":"3"}


[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input { 
##指定输入模式为json模式
  stdin { codec => "json" }
}

filter{ }

output{ 
##调试输出为看的更为舒服的格式
  stdout{ codec => "rubydebug" }
}
[root@logstash ~]# /usr/share/logstash/bin/logstash


Logstash中，键值对语法格式
key:value
key=>value


###### input file插件


[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input { 
  file {
    path => ["/tmp/c.log"]
#一个配置段里只能打一个标签如果想分开打标签，可以把file模块写多次
    type => "test"
#读取一个全新的文件时候，读取的策略，例如读取全文全部读取
    start_position => "beginning"
#设置书签文件，保证不会重复读取已读过的数据，并且后面未读的数据不会丢失
#如果书签丢失后，则默认按照start_position的策略开始读取
    sincedb_path => "/var/lib/logstash/sincedb"
  }
}
filter{ }
output{ 
  stdout{ codec => "rubydebug" }
}
#每一次启动都会创建的默认书签，建议删除、因为不好管理
[root@logstash ~]# rm -rf /var/lib/logstash/plugins/inputs/file/.sincedb_*
[root@logstash ~]# /usr/share/logstash/bin/logstash




match => { "message" =>  "(?<client_IP>(25[0-5]|2[0-4]\d|1?\d?\d\.){3}(25[0-5]|2[0-4]\d|1?\d?\d))"}

(25[0-5]|2[0-4]\d|1?\d?\d\.){3}(25[0-5]|2[0-4]\d|1?\d?\d)


"(?(25[0-5]|2[0-4]\d|1?\d?\d\.){3}(25[0-5]|2[0-4]\d|1?\d?\d)) (?\S+) (?\S+) \[(?.+)\] \"(?[A-Z]+) (?\S+) (?[A-Z]+\/\d+.\d+)\" (?\d+) (?\d+) \"(?\S+)\" \"(?[^\"]+)\""


#查看配置文件中日志格式的名字
[root@web ~]# vim /etc/httpd/conf/httpd.conf 
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
#查看Logstash中的宏
[root@logstash ~]# vim /usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/logstash-patterns-core-4.1.2/patterns/grok-patterns
#搜索匹配名字的宏是否有
[root@logstash patterns]# grep -i combined ./*


filebeat是一款轻量级
weblog 把日志给filebeat，然后直接给Elasticsearch
jsonweblog  把日志给 filebeat 然后filebeat再给 logstash
EFK不经过中间商Logstash 直接把json格式写入Elasticsearch数据库，并且在图形管理kibana上显示
ELK是经过中间商Logstash，把数据经过处理，并改为json格式后，写入Elasticsearch数据库，并且在图形管理kibana上显示






----------------------------------------------------------------------------------------
2.23
大数据
大数据5V特性
1数量
2真实性
3价值
4种类
5速度







Hadoop是什么
Hadoop是一种分析和处理海量数据的软件平台
Hadoop是一款开源软件，使用java开发
Hadoop可以提供一个分布式基础架构
Hadoop
高可靠性、高扩展性、高效性、高容错性、低成本。


Hadoop组件介绍
HDFS:分布式文件系统（核心组件）
MapReduce:分布式计算框架（核心组件）
Yarn:集群资源管理系统
Zookeeper:分布式协作服务
kafka:分布式消息队列
Hive:基于Hadoop的数据仓库
Hbase:分布式列存数据库

Hadoop的部署模式有三种 生产环境用完全分布式
Hadoop是绿色安装软件
-单机
在一台机器上安装部署
-伪分布式
在一台机器上安装部署，但区分角色
-完全分布式
多机部署，不同角色服务安装在不同的机器上


Hadoop对域名主机强依赖，主机名必须能ping通


./bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.7.jar wordcount ./input ./output

使用jar
./bin/hadoop jar 

jar路径
share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.7.jar 

算法
wordcount

分析这个路径./input 的数据
./input 

把分析结果放到路径./output里
./output

#正序查看
[root@hadoop1 hadoop]# sort -nk2 output/part-r-00000 
sort -nk2 


客户端负责切块，存储位置是需要询问namenode
客户端访问datename读取写入数据

namenode把文件存储的位置 记录在了数据库名叫fsimage


每块128M
文件需要切成块才能存储
根据存储原则村初到datanode
datanode是存储节点

为了防止切块的数据丢失 namenode决定副本策略，也就是一份资料 存储几份副本


sname类似namenode的小秘书，定期合并数据库名fsimage和fsedits补丁





命令文件夹
[root@hadoop1 ~]# ls /usr/local/hadoop/sbin/


排错思路
[root@hadoop1 ~]# jps
641 WrapperSimpleApp
4200 Jps
3611 SecondaryNameNode
3421 NameNode
如果没有NameNode
保证配置文件正确
[root@hadoop1 ~]# vim /usr/local/hadoop/etc/hadoop/slaves
[root@hadoop1 ~]# vim /usr/local/hadoop/etc/hadoop/hadoop-env.sh
[root@hadoop1 ~]# vim /usr/local/hadoop/etc/hadoop/core-site.xml
[root@hadoop1 ~]# vim /usr/local/hadoop/etc/hadoop/hdfs-site.xml


[root@hadoop1 ~]# /usr/local/hadoop/sbin/stop-dfs.sh
[root@hadoop1 ~]# for i in node-{0001..0003};do
                      rsync -aXSH --delete /usr/local/hadoop ${i}:/usr/local/
                  done
[root@hadoop1 ~]# /usr/local/hadoop/sbin/start-dfs.sh

查看日志的INFO列
[root@hadoop1 hadoop]# cat /usr/local/hadoop/logs/hadoop-root-namenode-hadoop1.log

删除logs
[root@hadoop1 ~]# rm -rf /usr/local/hadoop/logs/
格式化
[root@hadoop1 ~]# /usr/local/hadoop/bin/hdfs namenode -format


集群验证
[root@hadoop1 ~]# for i in hadoop1 node-{0001..0003};do  
                      echo ${i}; 
                      ssh ${i} jps; 
                      echo -e "\n"; 
                  done
[root@hadoop1 ~]# /usr/local/hadoop/bin/hdfs dfsadmin -report

**重复格式化会遇到的问题排错**

删除每个节点上的 两个文件
[root@node-0001 ~]# rm -rf /var/hadoop/ /usr/local/hadoop/logs/
[root@node-0002 ~]# rm -rf /var/hadoop/ /usr/local/hadoop/logs/
[root@node-0003 ~]# rm -rf /var/hadoop/ /usr/local/hadoop/logs/

[root@hadoop1 ~]# /usr/local/hadoop/sbin/stop-dfs.sh
[root@hadoop1 ~]# rm -rf /var/hadoop/ /usr/local/hadoop/logs/
[root@hadoop1 ~]# mkdir /var/hadoop/
[root@hadoop1 ~]# /usr/local/hadoop/bin/hdfs namenode -format
[root@hadoop1 ~]# /usr/local/hadoop/sbin/start-dfs.sh

验证计算节点是否正常命令
[root@hadoop1 ~]# /usr/local/hadoop/bin/yarn node -list
Total Nodes:3
 Node-Id             Node-State Node-Http-Address       Number-of-Running-Containers
 node-0003:33212     RUNNING    node-0003:8042                                  0
 node-0001:40201     RUNNING    node-0001:8042                                  0
 node-0002:38830     RUNNING    node-0002:8042                                  0


HDFS部署文件系统集群

YARN部署的计算集群






--------------------------------------------------------------------------------------------------
2.24





























http://124.71.232.214:9200/_plugin/head/


114 116 224 16






change master to  master_host="192.168.4.11",master_user="repluser",master_password="123qqq...A",master_log_file="master11.000001",master_log_pos=441;






#该主机添加前需要准备操作

~]# /etc/init.d/redis_6379 start    //开服务
192.168.4.56:6379> cluster info	//查看该主机集群遗留状态
192.168.4.56:6379> cluster reset	//清除该主机集群遗留状态
192.168.4.56:6379> keys *		//查看该主机变量
192.168.4.56:6379> flushall		//清除该主机变量














change master to master_host="192.168.4.52" , master_user="repluser" , master_password="123qqq...A" , master_log_file="master52.000001" , master_log_pos=401;



nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &
nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf 
nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf --ignore_last_failover 2> /dev/null &



change master to master_host="192.168.4.53",master_user="repluser",master_password="123qqq...A",master_log_file="master53.000001",master_log_pos=441
change master to master_host="192.168.4.53",master_user="repluser",master_password="123qqq...A",master_log_file="master53.000001",master_log_pos=441;

change master to master_host="192.168.4.54",master_user="repluser",master_password="123qqq...A",master_log_file="master54.000001",master_log_pos=441
change master to master_host="192.168.4.54",master_user="repluser",master_password="123qqq...A",master_log_file="master54.000001",master_log_pos=441;



change master to master_host="192.168.4.68",master_user="repluser",master_password="123qqq...A",master_log_file="master68.000001",master_log_pos=441
change master to master_host="192.168.4.68",master_user="repluser",master_password="123qqq...A",master_log_file="master68.000001",master_log_pos=441;



change master to master_host="192.168.4.69",master_user="repluser",master_password="123qqq...A",master_log_file="master69.000001",master_log_pos=441
change master to master_host="192.168.4.69",master_user="repluser",master_password="123qqq...A",master_log_file="master69.000001",master_log_pos=441;







6267205









240398019
第一题：
编写防火墙规则，限制其他主机无法ping本机，但是本机可以ping其他主机？
第二题：
编写防火墙规则，使用Linux软路由如何实现将内网192.168.4.0/24网段地址转换为公网201.108.22.11的IP地址？
第三题：
编写防火墙规则，一次性限制客户端无法访问本机的25,80,110三个端口？
第四题：
编写防火墙规则，禁止192.168.4.11使用tcp协议访问本机的25端口？
第五题：
编写防火墙规则，限制客户端通过本机的eth0网卡访问自己？
第六题：
写出iptables的四表五链？
一、iptables -P INPUT DROP
二、iptables -t nat -A POSTROUTING -s 192.168.4.0/24 -j MASQUERADE
三、iptables -A INPUT -p tcp --dport 25,80,110 -j REJECT
四、iptables -A INPUT -p tcp -s 192.168.4.11/24 --dport 25 -j REJECT
六、状态跟踪表(raw表) ,包标记表(mangle表)，地址转换表(nat表)，过滤表(filter表)
    INPUT链
    OUTPUT链
    FORWARD链
    PREROUTING链
    POSTROUTING链




创建randpass.py脚本，要求如下：
编写一个能生成8位随机密码的程序
使用random的choice函数随机取出字符
改进程序，用户可以自己决定生成多少位的密码



第一题： 简单描述在Zabbix系统中定义邮件报警的流程？ 
第二题： 简单描述Zabbix主动监控与被动监控的差别？ 
第三题： 结合下列关键词描述prometheus监控各个组件的作用和监控流程： prometheus、node_exporter(prometheus target)、TSDB、Grafana

1、创建触发器规则,创建邮件类型的报警媒介,为用户关联邮箱,创建动作并当满足触发器规则时给admin发邮件。
2、被动监控：Server向Agent发起请求，索取监控数据。
   主动监控：Agent向Server发起连接，向Server汇报。
3、 prometheus
     主要用在容器监控方面，也可以用于常规的主机监控
     配置流程
     1.安装绿色包并解压prometheus_soft.tar.gz
       修改/usr/local/prometheus/prometheus.yml配置文件
       创建服务文件并且启动服务
    node_exporter(prometheus target)
    node-exporter用于监控硬件和系统的常用指标
    exporter运行于被监控端，以服务的形式存在。每个exporter所使用的端口号都不一样。
     1.安装绿色包并解压node_exporter-1.0.0-rc.0.linux-amd64.tar.gz
     2.移动绿色包到/usr/local/node_exporter目录下
     3.创建服务文件并且启动服务
     4.在Prometheus服务器上添加监控节点
    TSDB
     时序数据库，主要用来储存时序数据  
    Grafana
    grafana是一款开源的、跨平台的、基于web的可视化工具。
    1.安装软件包grafana-6.7.3-1.x86_64.rpm
    2.启动grafana-server服务
    3.修改配置，对接prometheus




---
- name: user loop
  hosts: node2
  tasks:
    - name: create user
      user:
        name: "{{item.um}}" 
        password: "{{item.pwd|password_hash('sha512')}}"
	loop: 
        - {"um": "tom", "upwd": "ilovelinux"}
        - {"um": "jerry", "upwd": "tedu"} 











# 查看虚拟机
[root@zzgrhel8 ~]# virsh list --all

# 启动虚拟机
[root@zzgrhel8 ~]# for i in {1..4}
> do
> virsh start tedu_node0$i
> donef

virsh console tedu_node01
virsh console tedu_node09
virsh console tedu_node03
virsh console tedu_node04


virsh console tedu_mysql58
hostnamectl set-hostname host58
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.58/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_mysql57
hostnamectl set-hostname host57
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.57/24
nmcli connection down eth0
nmcli connection up eth0


virsh console tedu_mysql68
hostnamectl set-hostname host68
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.68/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_mysql53
hostnamectl set-hostname host53
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.53/24
nmcli connection down eth0
nmcli connection up eth0


virsh console tedu_mysql52
hostnamectl set-hostname host52
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.52/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_mysql51
hostnamectl set-hostname host51
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.51/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_mysql54
hostnamectl set-hostname host54
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.54/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_mysql55
hostnamectl set-hostname host55
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.55/24
nmcli connection down eth0
nmcli connection up eth0


virsh console tedu_node71
hostnamectl set-hostname pxcnode71
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.71/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node72
hostnamectl set-hostname pxcnode72
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.72/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node73
hostnamectl set-hostname pxcnode73
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.73/24
nmcli connection down eth0
nmcli connection up eth0


virsh console tedu_node51
hostnamectl set-hostname host51
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.51/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node52
hostnamectl set-hostname host52
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.52/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node53
hostnamectl set-hostname host53
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.53/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node54
hostnamectl set-hostname host54
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.54/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node55
hostnamectl set-hostname host55
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.55/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node56
hostnamectl set-hostname host56
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.56/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node57
hostnamectl set-hostname host57
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.57/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node58
hostnamectl set-hostname host58
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.58/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node59
hostnamectl set-hostname host59
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.59/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node04
hostnamectl set-hostname client10
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.10/24
nmcli connection down eth0
nmcli connection up eth0

virsh console tedu_node05
hostnamectl set-hostname nfs31
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.31/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node06
hostnamectl set-hostname web2
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.12/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node07
hostnamectl set-hostname web3
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.13/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node08
hostnamectl set-hostname proxy
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.5/24
nmcli connection down eth0
nmcli connection up eth0
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.5/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node09
hostnamectl set-hostname proxy2
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.6/24
nmcli connection down eth0
nmcli connection up eth0
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.6/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node10
hostnamectl set-hostname node1
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.41/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node11
hostnamectl set-hostname node2
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.42/24
nmcli connection down eth1
nmcli connection up eth1

virsh console tedu_node12
hostnamectl set-hostname node3
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.43/24
nmcli connection down eth1
nmcli connection up eth1

rm -f ~/.ssh/known_hosts 





hostnamectl set-hostname client1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.2.11/24
nmcli connection down eth0
nmcli connection up eth0
nmcli connection modify eth0 ipv4.gateway 192.168.4.11
nmcli connection down eth0
nmcli connection up eth0

hostnamectl set-hostname node1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.11/24
nmcli connection down eth0
nmcli connection up eth0
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.11/24
nmcli connection down eth1
nmcli connection up eth1

hostnamectl set-hostname server1
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.200/24
nmcli connection down eth1
nmcli connection up eth1
nmcli connection modify eth1 ipv4.gateway 192.168.2.11
nmcli connection down eth1
nmcli connection up eth1

hostnamectl set-hostname node3
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.13/24
nmcli connection down eth0
nmcli connection up eth0






#!/bin/bash
git clone http://192.168.4.5/var/lib/git/project
cp -r /var/tmp/project /var/ftp
rm -rf /var/ftp/project/.git
tar -czf /var/ftp/project.tar.gz /var/ftp/project
hash=$(/var/ftp/project.tar.gz/md5sum project.tar.gz | awk '{print $1}')
echo $hash > /var/ftp/ver.txt









#!/bin/bash
for ((i=1;i>0;i++))
do
        x=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789
        for i in {1..8}
        do
                n=$[RANDOM%62]
                a=${x:n:1}
                c=$a$c
        #sum=$z$i
        #mkdir /root/桌面/$sum
        #touch /root/桌面/$sum
        #touch /root/test/$sum
        #echo "$sum"
        done
        touch /root/test/$c
        #echo "$c"
done



简单描述keepalived如何实现服务器的高可用？
1、在两个节点上安装keepalived和httpd软件包
2、在/etc/keepalived/keepalived.conf路径下修改配置文件
第一个节点作为主MASTER的配置文件修改内容如下
#设置本机在集群中的名称
router_id 名称 	
#添加自动配置iptable的放行规则		
vrrp_iptables 
#设置VIP地址			
virtual_ipaddress {VIP地址} 
3、启动keepalived服务，查看eth0是否增加额外地址
4、第二个节点作为备BACKUP的配置文件修改内容如下
#设置本机在集群中的名称
router_id 名称 	
#将状态改为BACKUP
state BACKUP 
#添加自动配置iptable的放行规则		
vrrp_iptables 
#设置VIP地址
virtual_ipaddress {VIP地址} 
5、启动keepalived服务




p y t h o n
0  1 2 3 4  5      

