数据批量处理  (一次性向表里存储很多数据或一次性把表里的数据都取出来)
注意：数据导入或导出 存放数据的文件必须在mysql服务要求的目录下 叫检索目录
所有在学习数据导入导出前要先掌握 检索目录的管理
第一 要知道数据库服务默认的检索目录
#查看默认的检索目录

mysql> show variables like  "secure_file_priv";
+------------------+-----------------------+
| Variable_name    | Value                          |
+------------------+-----------------------+
| secure_file_priv  | /var/lib/mysql-files/ |
+------------------+-----------------------+

[root@host50 ~]# ls -l /var/lib/mysql-files/
总用量 0
[root@host50 ~]# ls -ld /var/lib/mysql-files/
drwxr-x--- 2 mysql mysql 6 11月 29 2016 /var/lib/mysql-files/
[root@host50 ~]#			 

第二  会修改数据库服务默认的检索目录
[root@host50 ~]# vim /etc/my.cnf
[mysqld]
secure_file_priv=/myload      #手动添加
:wq
[root@host50 ~]# mkdir /myload
[root@host50 ~]# chown mysql /myload/
[root@host50 ~]# ls -ld /myload/
drwxr-xr-x 2 mysql root 6 11月  5 16:37 /myload/
[root@host50 ~]# setenforce 0
setenforce: SELinux is disabled
[root@host50 ~]# systemctl  restart mysqld
[root@host50 ~]# mysql -uroot -p123qqq...B
mysql> show variables like  "secure_file_priv";
+------------------+----------+
| Variable_name    | Value      |
+------------------+----------+
| secure_file_priv   | /myload/ |
+------------------+----------+

数据导入（一次性向表里存储很多数据）把系统文件的内容存储到数据库服务的表里
文件的内容要规律
诉求：将/etc/passwd文件导入db1库的t3表里			 
数据导入格式：
mysql>  load   data  infile   "/检索目录/文件名"   into    table    库名.表名   
fields  terminated by  "文件中列的间隔符号"   lines   terminated by   "\n"  ;

数据导入的操作步骤：
1） 要创建存储数据库（如果没有的话）
2）  建表 (根据导入文件的内容 创建 表头名  表头个数  表头数据类型  根据文件内容定义)
3） 把系统文件拷贝的检索目录里
4 )  数据库管理执行导入数据的命令
5） 查看数据

mysql> create database if not exists db1;			 
mysql> drop  table db1.t3;		 #删除重名的t3表(如果有的话)	 
mysql> create table db1.t3(name char(50) , password char(1) , 
uid int , gid int , comment varchar(200) , homedir varchar(60) , 
shell  varchar(30) );

mysql> desc db1.t3;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| name     | char(50)     | YES  |     | NULL    |       |
| password | char(1)      | YES  |     | NULL    |       |
| uid      | int(11)      | YES  |     | NULL    |       |
| gid      | int(11)      | YES  |     | NULL    |       |
| comment  | varchar(200) | YES  |     | NULL    |       |
| homedir  | varchar(60)  | YES  |     | NULL    |       |
| shell    | varchar(30)  | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

mysql> select  * from db1.t3;
Empty set (0.00 sec)

mysql> system  cp /etc/passwd  /myload/
mysql> system ls /myload
passwd
mysql> load data infile "/myload/passwd" into table db1.t3
fields terminated by ":"  lines terminated by "\n" ;

mysql> select * from db1.t3 ;

数据导出（一次性把表里的数据都取出来）把数据库服务的表里数据保存到系统文件里
注意导出的数据不包括表头名 ，只有表里行。存放导出数据的文件名 不需要事先创建
且具有唯一。

数据导出命令格式：
1)select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名" ;

2)select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名" 
fields  terminated by  "符号" ；

fields  terminated by  指定导出的列 在文件中的间隔符号 		不指定默认是一个 tab 键的宽度

3)select   字段名列表 from  库.表   where  条件  into  outfile   "/检索命令名/文件名"
fields  terminated by  "符号"    lines     terminated by  "符号" ；

lines     terminated by   指定导出的行在文件中的间隔符号 不指定默认一条记录就是文件中的1行

mysql> system ls /myload
passwd
mysql> select * from db1.t3  where  uid <= 10   into  outfile  "/myload/a.txt";
Query OK, 9 rows affected (0.00 sec)

mysql> system ls /myload
a.txt  passwd
mysql> 
mysql> system cat  /myload/a.txt ;
root    x       0       0       root    /root   /bin/bash
bin      x       1       1       bin     /bin    /sbin/nologin
daemon  x       2       2       daemon  /sbin   /sbin/nologin
adm     x       3       4       adm     /var/adm        /sbin/nologin
lp      x       4       7       lp      /var/spool/lpd  /sbin/nologin
sync    x       5       0       sync    /sbin   /bin/sync
shutdown        x       6       0       shutdown        /sbin   /sbin/shutdown
halt    x       7       0       halt    /sbin   /sbin/halt
mail    x       8       12      mail    /var/spool/mail /sbin/nologin
mysql> 


mysql> select name , homedir , uid  from db1.t3  where  uid <= 10  into outfile "/myload/b.txt"
    -> fields  terminated by ":"   ;
Query OK, 9 rows affected (0.00 sec)

mysql> system cat /myload/b.txt
root:/root:0
bin:/bin:1
daemon:/sbin:2
adm:/var/adm:3
lp:/var/spool/lpd:4
sync:/sbin:5 
shutdown:/sbin:6
halt:/sbin:7
mail:/var/spool/mail:8
mysql> 

mysql> select name , homedir , uid  from db1.t3  where  uid <= 10  into outfile "/myload/c.txt"
fields  terminated by ":"   lines  terminated by "!!!" ;
	
mysql> system cat /myload/c.txt
root:/root:0!!!bin:/bin:1!!!daemon:/sbin:2!!!adm:/var/adm:3!!!lp:/var/spool/lpd:4!!!sync:/sbin:5!!!shutdown:/sbin:6!!!halt:/sbin:7!!!mail:/var/spool/mail:8!!!mysql> 

